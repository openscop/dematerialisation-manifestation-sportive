# coding: utf-8
"""
Fonctions et utilitaires liés à la configuration de l'application.
Permet de récupérer les paramètres de l'instance en cours.
"""
import logging
import re

from django.conf import settings


logger = logging.getLogger('django.request')


def get_current_subdomain(request):
    """ Renvoyer le nom du sous-domaine actuel """
    request.domain = ''
    if 'HTTP_HOST' in request.META:
        request.domain = request.META['HTTP_HOST']
    request.subdomain = ''
    parts = request.domain.split('.')
    # accepte les nnd de la forme sub.dom.ext ou sub.localhost:8000
    if len(parts) == 3 or (re.match("^localhost", parts[-1]) and len(parts) == 2):
        request.subdomain = parts[0]
        request.domain = '.'.join(parts[1:])
    if settings.DEBUG is True:
        if request.subdomain:
            logger.debug("core.util.application : Sous-domaine détecté : {}".format(request.subdomain or 'Aucun'))
    return request.subdomain
