# coding: utf-8

from django.urls import reverse

from agreements.factories import CGAvisFactory
from sub_agreements.tests.base import PreavisTestsBase
from ..factories import PreAvisServiceCGFactory


class PreAvisServiceCGTests(PreavisTestsBase):
    """ Tests des préavis de services de conseils généraux """

    # Configuration
    def setUp(self):
        super().setUp()
        self.avis = CGAvisFactory.create(authorization=self.authorization)

    # Tests
    def test_str(self):
        """ Test des représentations chaîne """
        preavis = PreAvisServiceCGFactory.create(avis=self.avis, cg_service=self.cgservice)
        self.assertEqual(str(preavis), ' - '.join([str(self.manifestation), str(preavis.cg_service)]))

    def test_access_url(self):
        """ Tester les URLs """
        preavis = PreAvisServiceCGFactory.create(avis=self.avis, cg_service=self.cgservice)
        self.assertEqual(preavis.get_absolute_url(), reverse('sub_agreements:cgservice_subagreement_detail', kwargs={'pk': preavis.pk}))

    def test_workflow(self):
        """ Tester la cr"ation automatique des préavis """
        self.assertEqual(self.avis.get_preavis_count(), 0)  # Pas de préavis créé pour l'avis par défaut
        self.avis.concerned_services.add(self.cgservice)
        self.assertEqual(self.avis.get_preavis_count(), 1)  # Prévis de service
