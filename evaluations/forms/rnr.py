# coding: utf-8

from bootstrap_datepicker_plus import DateTimePickerInput
from crispy_forms.bootstrap import FormActions, AppendedText
from crispy_forms.layout import Submit, Layout, Fieldset, HTML
from django import forms
from django.conf import settings

from core.forms.base import GenericForm
from ..models import RNREvaluation


FACILITIES_FIELDSET_RNR = (
    'night_lighting',
    'night_lighting_desc',
    'trails_marks',
    'trails_marks_desc',
    'other_facilities',
    'other_facilities_desc',
)

RNR_IMPLICATIONS_FIELDSET = (
    HTML(
        "<div class='well'>"
        "<p>{paragraph}</p>"
        "<ul>"
        "<li>{parking}</li>"
        "<li>{trash}</li>"
        "<li>{picking}</li>"
        "<li>{light}</li>"
        "<li>{noise}</li>"
        "<li>{animals}</li>"
        "</ul>"
        "</div>".format(
            paragraph='sont interdits sur la réserve naturelle : '.capitalize(),
            parking='le stationnement et passage de participants et du public en dehors des '
                    'sentiers autorisés'.capitalize(),
            trash="l'abandon de déchets ou produits".capitalize(),
            picking='la cueillette de végétaux'.capitalize(),
            light="d'utiliser un éclairage artificiel (en dehors de ceux utilisés par les "
                  "services publics de secours)".capitalize(),
            noise="de troubler le calme et la tranquillité des lieux et des animaux, par des "
                  "cris ou bruits divers, par l'utilisation d'un appareil radiophonique, ou "
                  "tout autre instrument sonore".capitalize(),
            animals="les animaux domestiques non tenus en laisse et la divagation de ces mêmes "
                    "animaux".capitalize(),
        )
    ),
    'noise_emissions',
    'waste_management',
    'waste_management_desc',
    'awareness',
    'impact_reduction',
    'impact_reduction_desc',
    'plane_attendance',
    'plane_attendance_desc',
    'trampling',
    'trampling_desc',
    'markup',
    'markup_desc',
)
FREQUENCY_FIELDSET = ('every_year', 'first_edition', 'other_frequency')
RNR_CONCLUSIONS_FIELDSET = (
    'regulatory_compliance',
    'rnr_conclusion',
    'estimated_impact',
)
PROVISIONS_FIELDSET = ('administrator_contact_date', 'sensitization', 'forbidden_areas', 'administrator_attendance',)
ATTENDANCE_FIELDSET = (
    AppendedText('rnr_entries', 'personnes'),
    AppendedText('rnr_audience', 'personnes'),
)
LOCALIZATION_FIELDSET = (
    HTML(
        "<div class='well'>"
        "<a href='/protected_areas/RNR/{{ dept }}/' target='_blank'>"
        "coordonnées des opérateurs des différents sites RNR</a></div>"
    ),
    'sites',
    AppendedText('track_total_length', 'km'),
)
DESCRIPTION_FIELDSET = ('place', 'request_duration',)

FORM_WIDGETS = {
    'trampling_desc': forms.Textarea(attrs={'rows': 1}),
    'markup_desc': forms.Textarea(attrs={'rows': 1}),
    'plane_attendance_desc': forms.Textarea(attrs={'rows': 1}),
    'night_lighting_desc': forms.Textarea(attrs={'rows': 1}),
    'trails_marks_desc': forms.Textarea(attrs={'rows': 1}),
    'other_facilities_desc': forms.Textarea(attrs={'rows': 1}),
    'waste_management_desc': forms.Textarea(attrs={'rows': 1}),
    'impact_reduction_desc': forms.Textarea(attrs={'rows': 1}),
    'administrator_contact_date': DateTimePickerInput(options={"format": "DD/MM/YYYY", 'locale': 'fr'}),
}


class RNREvaluationForm(GenericForm):
    """ Formulaire d'évaluation des incidences sur une RNR """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(RNREvaluationForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset("Formulaire d'évaluation RNR", *DESCRIPTION_FIELDSET),
            Fieldset("fréquence de la manifestation".capitalize(), *FREQUENCY_FIELDSET),
            Fieldset("Localisation de la manifestation par rapport aux sites RNR", *LOCALIZATION_FIELDSET),
            Fieldset("fréquentation".capitalize(), *ATTENDANCE_FIELDSET),
            Fieldset("budget de la manifestation".capitalize(), 'cost',),
            Fieldset("prise en compte de l'environnement".capitalize(), *PROVISIONS_FIELDSET),
            Fieldset("présence d'aménagements connexes".capitalize(), *FACILITIES_FIELDSET_RNR),
            Fieldset("Incidences potentielles sur RNR", *RNR_IMPLICATIONS_FIELDSET),
            Fieldset("conclusion".capitalize(), *RNR_CONCLUSIONS_FIELDSET),
            FormActions(
                Submit('save', "soumettre".capitalize())
            )
        )

    # Meta
    class Meta:
        model = RNREvaluation
        exclude = ('content_type', 'object_id', 'manifestation', 'manif')
        labels = {'track_total_length': "longueur totale des parcours en RNR"}
        widgets = FORM_WIDGETS
