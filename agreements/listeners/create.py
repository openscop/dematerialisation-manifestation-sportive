# coding: utf-8
from django.conf import settings
from django.db.models.signals import m2m_changed, post_save
from django.dispatch import receiver

from administration.models import Service
from administrative_division.models import Commune
from agreements.models.cg import CGAvis
from agreements.models.ddsp import DDSPAvis
from agreements.models.edsr import EDSRAvis
from agreements.models.federation import FederationAvis
from agreements.models.ggd import GGDAvis
from agreements.models.mairie import MairieAvis
from agreements.models.sdis import SDISAvis
from agreements.models.service import ServiceAvis
from authorizations.models import ManifestationAutorisation
from core.models.instance import Instance


if not settings.DISABLE_SIGNALS:

    @receiver(m2m_changed, sender=ManifestationAutorisation.concerned_services.through)
    def create_service_agreements(sender, action=None, pk_set=None, instance=None, **kwargs):
        """ Créer les avis Service lorsqu'un service est ajoué à l'autorisation """
        if action == 'post_add':
            for pk in pk_set:
                # Créer des avis automatiquement s'ils n'existent pas déjà
                if not ServiceAvis.objects.filter(authorization=instance, service=Service.objects.get(pk=pk)).exists():
                    ServiceAvis.objects.create(authorization=instance, service=Service.objects.get(pk=pk))


    @receiver(m2m_changed, sender=ManifestationAutorisation.concerned_cities.through)
    def create_townhall_agreements(sender, action=None, pk_set=None, instance=None, **kwargs):
        """ Créer les avis Mairie lorsque qu'une commune est ajoutée à une autorisation """
        if action == 'post_add':
            for pk in pk_set:
                # Créer des avis automatiquement s'ils n'existent pas déjà
                if not MairieAvis.objects.filter(authorization=instance, commune=Commune.objects.get(pk=pk)).exists():
                    MairieAvis.objects.create(authorization=instance, commune=Commune.objects.get(pk=pk))


    @receiver(post_save, sender=ManifestationAutorisation)
    def create_federation_agreement(sender, instance=None, created=None, **kwargs):
        """ Créer l'avis Fédération pour chaque nouvelle autorisation """
        if created:
            FederationAvis.objects.create(authorization=instance)


    @receiver(post_save, sender=ManifestationAutorisation)
    def create_edsr_agreement(sender, instance=None, **kwargs):
        """
        Créer l'avis EDSR pour certaines nouvelles autorisations

        Uniquement si une demande d'avis EDSR existe (attribut edsr_concerned == True)
        """
        if instance.edsr_concerned:
            if instance.get_instance().get_workflow_ggd() == Instance.WF_EDSR:
                if not EDSRAvis.objects.filter(authorization=instance).exists():
                    EDSRAvis.objects.create(authorization=instance)


    @receiver(post_save, sender=ManifestationAutorisation)
    def create_ggd_agreement(sender, instance=None, **kwargs):
        """ Créer l'avis GGD pour chaque nouvelle autorisation """
        if instance.ggd_concerned:
            # Ne pas définir d'EDSR par défaut dans le nouvel avis GGD (fix circuit GGD)
            if not GGDAvis.objects.filter(authorization=instance).exists():
                GGDAvis.objects.create(authorization=instance, concerned_edsr=None)


    @receiver(post_save, sender=ManifestationAutorisation)
    def create_sdis_agreement(sender, instance=None, **kwargs):
        """ Créer l'avis SDIS pour chaque nouvelle autorisation """
        if instance.sdis_concerned:
            if instance.get_instance().get_workflow_sdis() == Instance.WF_SDIS:
                if not SDISAvis.objects.filter(authorization=instance).exists():
                    SDISAvis.objects.create(authorization=instance)


    @receiver(post_save, sender=ManifestationAutorisation)
    def create_ddsp_agreement(sender, instance=None, **kwargs):
        """ Créer l'avis DDSP pour chaque nouvelle autorisation """
        if instance.ddsp_concerned:
            if not DDSPAvis.objects.filter(authorization=instance).exists():
                DDSPAvis.objects.create(authorization=instance)


    @receiver(post_save, sender=ManifestationAutorisation)
    def create_cg_agreement(sender, instance=None, **kwargs):
        """ Créer l'avis CG pour chaque nouvelle autorisation """
        if instance.cg_concerned:
            if not CGAvis.objects.filter(authorization=instance).exists():
                CGAvis.objects.create(authorization=instance)
