# coding: utf-8
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin
from ajax_select.helpers import make_ajax_form

from administration.models.people import Instructeur, Secouriste
from administration.models.service import Prefecture
from core.util.admin import RelationOnlyFieldListFilter
from core.models import User
from .agent import UserFullNameAndUsernameMixin

@admin.register(Instructeur)
class InstructeurAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """
    list_display = ['pk', 'agent_name', 'user__is_active', 'prefecture', 'prefecture__arrondissement__departement']
    list_filter = [('prefecture__arrondissement__departement', RelationOnlyFieldListFilter), 'user__is_active']
    search_fields = ['user__username', 'user__last_name__unaccent', 'prefecture__arrondissement__name']
    list_per_page = 50

    form = make_ajax_form(Instructeur, {'user': 'user'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(prefecture__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        form.base_fields['user'].help_text = "Recherche par nom ou/et prénom ou nom d'utilisateur"
        if request.user.get_departement():
            form.base_fields['prefecture'].queryset = Prefecture.objects.filter(arrondissement__departement=request.user.get_departement())
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form


@admin.register(Secouriste)
class SecouristeAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """
    list_display = ['pk', 'agent_name', 'user__is_active', 'association']
    list_filter = [('association__departement', RelationOnlyFieldListFilter), 'user__is_active']
    search_fields = ['user__username', 'user__last_name__unaccent', 'association__name']
    list_per_page = 50

    form = make_ajax_form(Secouriste, {'user': 'user'})

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(user__default_instance__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        form.base_fields['user'].help_text = "Recherche par nom ou/et prénom ou nom d'utilisateur"
        return form
