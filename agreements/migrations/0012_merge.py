# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-05-14 12:43
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0011_auto_20180305_1040'),
        ('agreements', '0011_auto_20180405_1629'),
    ]

    operations = [
    ]
