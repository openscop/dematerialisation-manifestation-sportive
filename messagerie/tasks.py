from __future__ import absolute_import, unicode_literals
from configuration.celery import app

from django.utils import timezone
from django.db.models import Q
from django.apps import apps

from core.models import User
from evenements.models.manifestation import Manif
from messagerie.models import Message, Enveloppe
from organisateurs.models import Organisateur
from administrative_division.models import Departement

@app.task(bind=True)
def envoi_msg_celery(self, msg_json, type_envoi):
    """
    Fonction pour envoyer un message via les pages de messagerie
    :param msg_json: msg_json = {
                    'expediteur': expediteur,
                    'msg': corps du messages,
                    'objet': objet,
                    'option': {
                        'doc_pk': objet_lie_pk,
                        'doc_objet': objet_lie_nature,
                    },
                    'type': type_msg",
                    'manif': pk de la manif,
                    'destinataire': destinataires,
                    'reponse': pk de la reponse
                }
    Tout doit être sous format str ou serialisable
    :param type_envoi: type du message à envoyer (actualite, destinataire_service, destinataire_user)
    """

    tab_desti = []
    expediteur = User.objects.get(pk=msg_json['expediteur'])
    manif = Manif.objects.get(pk=msg_json['manif']) if msg_json['manif'] else None
    reponse = msg_json.get('original', None)

    if type_envoi == 'actualite':
        # création de la liste destinataire

        # ici si l'user a choisi un departement precis ou non
        if msg_json['destinataire']['dep']:
            userindep = User.objects.none()
            for departement in msg_json['destinataire']['dep']:
                if type(departement) == 'str':
                    departement = int(departement)
                obj_dep = Departement.objects.filter(pk=departement)
                useradd = User.objects.filter(Q(default_instance__departement__in=obj_dep))
                # Cas où le departement reçoit une liste vide pour eviter les erreurs dans le second filtre
                userindep = userindep | useradd
        else:
            userindep = User.objects.all()

        # ici on va filtre ou non selon le role choisi
        if msg_json['destinataire']['role']:
            for user in userindep:
                for role in msg_json['destinataire']['role']:
                    if user.has_role(role):
                        tab_desti.append(user)
        else:
            for user in userindep:
                tab_desti.append(user)

        message = Message.objects.creer_et_envoyer(msg_json['type'], expediteur, tab_desti, msg_json['objet'],
                                                   msg_json['msg'])

    else:
        # cas où on envoi le message à une liste d'utilisateur
        if type_envoi == 'destinataire_user':
            users = [User.objects.get(pk=int(desti)) for desti in msg_json['destinataire']]

        # cas où on envoi le message à un rôle d'utilisateur
        elif type_envoi == "destinataire_role":
            dep = Departement.objects.get(pk=msg_json['destinataire']['pk'])
            users = [user for user in User.objects.filter(default_instance__departement=dep) if
                     user.has_role(msg_json['destinataire']['type'])]
            
        # cas où on envoi à un service en entier
        elif type_envoi == 'destinataire_service':

            if msg_json['destinataire']['type'] in ["new", "old"]:
                dep = Departement.objects.filter(pk=msg_json['destinataire']['pk'])
                dep = dep.get() if dep else None
                last_year = timezone.now() - timezone.timedelta(days=360)
                if msg_json['destinataire']['type'] == "new":
                    users = [orga.user for orga in Organisateur.objects.filter(
                        user__default_instance__departement=dep, user__last_login__gte=last_year)]
                else:
                    users = [orga.user for orga in Organisateur.objects.filter(
                        user__default_instance__departement=dep, user__last_login__lt=last_year)]
            else:
                if msg_json['destinataire']['type'] == 'Mairie':
                    app_select = 'administrative_division'
                    msg_json['destinataire']['type'] = 'Commune'
                elif msg_json['destinataire']['type'] == 'Federation':
                    app_select = 'sports'
                else:
                    app_select = 'administration'
                model_select = apps.get_model(app_select, msg_json['destinataire']['type'])
                service = model_select.objects.get(pk=msg_json['destinataire']['pk'])
                users = service.get_service_users()
        for user in users:
            if user.is_active:
                tab_desti.append(user)
        message = Message.objects.creer_et_envoyer(msg_json['type'], expediteur, tab_desti, msg_json['objet'],
                                                   msg_json['msg'], manifestation_liee=manif,
                                                   objet_lie_pk=msg_json['option']['doc_pk'],
                                                   objet_lie_nature=msg_json['option']['doc_objet'],
                                                   reponse_a_pk=reponse)

    # ici on va voir si l'utilisateur s'est envoyé lui même le message on le passera en lu
    if message and expediteur:
        env_auto_envoye = Enveloppe.objects.filter(expediteur_id=expediteur, destinataire_id=expediteur, corps=message)
        if env_auto_envoye:
            env_auto_envoye = env_auto_envoye.get()
            env_auto_envoye.lu_datetime = timezone.now()
            env_auto_envoye.save()
