# coding: utf-8
import factory

from administration.factories import ServiceFactory
from administrative_division.factories import CommuneFactory
from agreements.models import Avis
from agreements.models import CGAvis
from agreements.models import DDSPAvis
from agreements.models import EDSRAvis
from agreements.models import FederationAvis
from agreements.models import GGDAvis
from agreements.models import MairieAvis
from agreements.models import SDISAvis
from agreements.models import ServiceAvis
from authorizations.factories import ManifestationAuthorizationFactory


class AvisFactory(factory.django.DjangoModelFactory):
    """ Factory des avis """

    authorization = factory.SubFactory(ManifestationAuthorizationFactory)

    # Meta
    class Meta:
        model = Avis


class FederationAvisFactory(factory.django.DjangoModelFactory):
    """ Factory des avis fédération """

    authorization = factory.SubFactory(ManifestationAuthorizationFactory)

    # Meta
    class Meta:
        model = FederationAvis


class ServiceAvisFactory(factory.django.DjangoModelFactory):
    """ Factory avis service """

    authorization = factory.SubFactory(ManifestationAuthorizationFactory)
    service = factory.SubFactory(ServiceFactory)

    # Meta
    class Meta:
        model = ServiceAvis


class MairieAvisFactory(factory.django.DjangoModelFactory):
    """ Factory avis mairie """

    authorization = factory.SubFactory(ManifestationAuthorizationFactory)
    commune = factory.SubFactory(CommuneFactory)

    # Meta
    class Meta:
        model = MairieAvis


class SDISAvisFactory(factory.django.DjangoModelFactory):
    """ Factory avis SDIS """

    authorization = factory.SubFactory(ManifestationAuthorizationFactory)

    # Meta
    class Meta:
        model = SDISAvis


class DDSPAvisFactory(factory.django.DjangoModelFactory):
    """ Factory avis DDSP """

    authorization = factory.SubFactory(ManifestationAuthorizationFactory)

    # Meta
    class Meta:
        model = DDSPAvis


class EDSRAvisFactory(factory.django.DjangoModelFactory):
    """ Factory avis EDSR """

    authorization = factory.SubFactory(ManifestationAuthorizationFactory)

    # Meta
    class Meta:
        model = EDSRAvis


class CGAvisFactory(factory.django.DjangoModelFactory):
    """ Factory avis CG """

    authorization = factory.SubFactory(ManifestationAuthorizationFactory)

    # Meta
    class Meta:
        model = CGAvis


class GGDAvisFactory(factory.django.DjangoModelFactory):
    """ Factory avis GGD """

    authorization = factory.SubFactory(ManifestationAuthorizationFactory)

    # Meta
    class Meta:
        model = GGDAvis
