$(document).ready(function () {





    $('.btn_etat_1, .btn_etat_2, #btn_etat_all').click(function (e) {
        e.preventDefault();
        let url_type = url_tableau_organisateur_liste;

        url_type += "?";


        let btn_etat = $(this).attr('id');
        if (btn_etat==="btn_etat_demande"){
            url_type+='filtre_etat=demande';
        }
        if (btn_etat==="btn_etat_attente"){
            url_type+="filtre_etat=attente";
        }
        if (btn_etat==="btn_etat_autorise"){
            url_type+="filtre_etat=autorise";
        }
        if (btn_etat==="btn_etat_interdite"){
            url_type+="filtre_etat=interdite";
        }
        if (btn_etat==="btn_etat_annule"){
            url_type+="filtre_etat=annule";
        }
        if (btn_etat==="btn_etat_all"){
            url_type+="filtre_etat=all";
            $(this).after(' <i class="spinner attente_chargement"></i>')
            AfficherIcones()
        }

        console.log(url_type);
        $.get(url_type, function (data) {
            $('#block_tableaubord_liste').html(data);
            $('.attente_chargement').remove()
            $('tr.clickligne').click(function () {
                window.location=$(this).data("href");
            });
            AfficherIcones();
        })
    })



});