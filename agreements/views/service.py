# coding: utf-8
from django.contrib import messages
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView

from agreements.views.base import BaseAcknowledgeView, BaseResendView
from core.util.permissions import require_role
from ..models import *


class ServiceAvisDetail(DetailView):
    """ Vue de détail d'avis service """

    # Configuration
    model = ServiceAvis

    @method_decorator(require_role('serviceagent'))
    def dispatch(self, *args, **kwargs):
        """ N'autoriser les vues GET/POST que pour les agents service """
        return super(ServiceAvisDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.authorization.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class ServiceAvisAcknowledge(BaseAcknowledgeView):
    """ Vue de rendu d'avis service """

    # Configuration
    model = ServiceAvis

    @method_decorator(require_role('serviceagent'))
    def dispatch(self, *args, **kwargs):
        """ N'autoriser la vue qu'aux agents service """
        return super(ServiceAvisAcknowledge, self).dispatch(*args, **kwargs)


class ServiceAvisResend(BaseResendView):
    """ Vue de renvoie d'avis """

    # Configuration
    model = ServiceAvis

    def get(self, request, *args, **kwargs):
        """ Vue renvoyée lors d'une requête GET """
        instance = self.get_object()
        instance.notify_creation(agents=instance.service.serviceagents.all())
        instance.log_resend(recipient=instance.service.__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect(reverse('authorizations:authorization_detail', kwargs={'pk': instance.authorization.id}))
