from rest_framework import serializers

from organisateurs.models import Structure
from administrative_division.models import Commune


class CommuneField(serializers.RelatedField):
    """
    Modification de la représentation de l'objet
    """
    queryset = Commune.objects.all()
    label = 'Code INSEE et nom de la commune'

    def to_representation(self, value):
        return '%s, %s' % (value.code, value.name)


class StructureLightSerializer(serializers.ModelSerializer):

    structure = serializers.CharField(source='name')
    commune = CommuneField()
    # une autre façon : profiter d'une fonction de la classe
    # commune = serializers.CharField(source='commune.natural_key')

    class Meta:
        ref_name = None
        model = Structure
        fields = ('id', 'structure', 'commune')


class StructureSerializer(serializers.ModelSerializer):

    commune = CommuneField()

    class Meta:
        ref_name = 'Structure'
        model = Structure
        fields = (
            'id',
            'name',
            'address',
            'commune',
            'phone',
            'website',
        )
