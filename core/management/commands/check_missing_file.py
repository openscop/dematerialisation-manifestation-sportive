# coding: utf-8
"""
Verifie la précense de fichier inscrit dans la database sur le systeme de fichier
"""
import os
import json
from django.core.management.base import BaseCommand
from django.db import transaction
from evenements.models import Manif
from instructions.models import PieceJointeAvis


def check(file):
    # Todo : les accès disques doivent être écrits dans des Try Except
    if file:
        return False if os.path.exists(file) else True
    else:
        return False



class Command(BaseCommand):


    def handle(self, *args, **options):
        """
        Exécuter la commande
        Créer un fichier json qui affiche les resultats
        """
        tab_final = []
        for manif in Manif.objects.all():
            tab_manif = []
            cerfa = manif.cerfa
            tab_manif.append(cerfa.cartographie.name) if cerfa.cartographie and check(cerfa.cartographie.path) else ""
            tab_manif.append(cerfa.reglement_manifestation.name) if cerfa.reglement_manifestation and check(cerfa.reglement_manifestation.path) else ""
            tab_manif.append(cerfa.engagement_organisateur.name) if cerfa.engagement_organisateur and check(cerfa.engagement_organisateur.path) else ""
            tab_manif.append(cerfa.disposition_securite.name) if cerfa.disposition_securite and check(cerfa.disposition_securite.path) else ""
            tab_manif.append(cerfa.topo_securite.name) if cerfa.topo_securite and check(cerfa.topo_securite.path) else ""
            tab_manif.append(cerfa.presence_docteur.name) if cerfa.presence_docteur and check(cerfa.presence_docteur.path) else ""
            tab_manif.append(cerfa.certificat_assurance.name) if cerfa.certificat_assurance and check(cerfa.certificat_assurance.path) else ""
            tab_manif.append(cerfa.docs_additionels.name) if cerfa.docs_additionels and check(cerfa.docs_additionels.path) else ""
            tab_manif.append(cerfa.charte_dispense_site_n2k.name) if cerfa.charte_dispense_site_n2k and check(cerfa.charte_dispense_site_n2k.path) else ""
            tab_manif.append(cerfa.convention_police.name) if cerfa.convention_police and check(cerfa.convention_police.path) else ""
            if hasattr(cerfa, "carte_zone_public") and cerfa.carte_zone_public:
                tab_manif.append(cerfa.carte_zone_public.name) if check(cerfa.carte_zone_public.path) else ""
            if hasattr(cerfa, "commissaires") and cerfa.commissaires:
                tab_manif.append(cerfa.commissaires.name) if check(cerfa.commissaires.path) else ""
            if hasattr(cerfa, "certificat_organisateur_tech") and cerfa.certificat_organisateur_tech:
                tab_manif.append(cerfa.certificat_organisateur_tech.name) if check(cerfa.certificat_organisateur_tech.path) else ""
            if hasattr(cerfa, "itineraire_horaire") and cerfa.itineraire_horaire:
                tab_manif.append(cerfa.itineraire_horaire.name) if check(cerfa.itineraire_horaire.path) else ""
            if hasattr(cerfa, "participants") and cerfa.participants:
                tab_manif.append(cerfa.participants.name) if check(cerfa.participants.path) else ""
            if hasattr(cerfa, 'avis_federation_delegataire') and cerfa.avis_federation_delegataire:
                tab_manif.append(cerfa.avis_federation_delegataire.name) if check(cerfa.avis_federation_delegataire.path) else ""
            if hasattr(cerfa, 'plan_masse') and cerfa.plan_masse:
                tab_manif.append(cerfa.plan_masse.name) if check(cerfa.plan_masse.path) else ""
            if hasattr(cerfa, "liste_signaleurs") and cerfa.liste_signaleurs:
                tab_manif.append(cerfa.liste_signaleurs.name) if check(cerfa.liste_signaleurs.path) else ""
            if hasattr(cerfa, 'dossier_tech_cycl') and cerfa.dossier_tech_cycl:
                tab_manif.append(cerfa.dossier_tech_cycl.name) if check(cerfa.dossier_tech_cycl.path) else ""
            if manif.documentscomplementaires.all():
                for doc in manif.documentscomplementaires.all():
                    tab_manif.append(doc.document_attache.name) if doc.document_attache and check(doc.document_attache.path) else ""
            if hasattr(manif, 'instruction'):
                for doc in manif.instruction.documents.all():
                    tab_manif.append(doc.fichier.name) if doc.fichier and check(doc.fichier.path) else ""
            if hasattr(manif, 'instruction'):
                for avi in manif.instruction.avis.all():
                    for doc in PieceJointeAvis.objects.filter(avis=avi):
                        tab_manif.append(doc.fichier.name) if doc.fichier and check(doc.fichier.path) else ""
            if tab_manif:
                tab_final.append({"pk":manif.pk, "result": tab_manif})
        f = open("result.json", "w+")
        jsonresult = json.dumps(tab_final)
        f.write(str(jsonresult))
        f.close()







