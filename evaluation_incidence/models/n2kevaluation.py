# coding: utf-8
from django.db import models
from django.utils.encoding import smart_text
from django.core.exceptions import FieldDoesNotExist

from evaluation_incidence.models import EvaluationManif


class EvaluationN2K(EvaluationManif):
    """ Évaluation Natura 2000 """

    LISTE_CHAMPS_REQUIS = ['cout', 'Longueur_totale_parcours', 'distance_site', 'impact_estime']

    # Motivation
    pourquoi_instructeur = models.TextField("Motivation de l'instructeur", blank=True)
    pourquoi_organisateur = models.TextField("Motivation de l'organisateur", blank=True)
    # Description
    diurne = models.BooleanField("manifestation diurne", default=False)
    nocturne = models.BooleanField("manifestation nocturne", default=False)

    # Localisation
    terrestre = models.BooleanField("Milieu Terrestre", default=False)
    nautique = models.BooleanField("Milieu Nautique", default=False)
    aerien = models.BooleanField("Milieu Aérien", default=False)
    sensibilite = models.BooleanField("Zone de sensibilité", default=False,
                                      help_text='Consulter la couche "zone de sensibilité" dans l’outil cartographique')
    distance_site = models.PositiveIntegerField("distance par rapport au site NATURA 2000", blank=True, null=True)

    # Incidences potentielles
    pietinement = models.BooleanField("Destruction par piétinement", default=False)
    mesures_pietinement = models.TextField('Mesures engagées', blank=True)
    emissions_sonores = models.BooleanField("Dérangement par le bruit", default=False)
    mesures_emissions_sonores = models.TextField('Mesures engagées', blank=True)
    emissions_lumiere = models.BooleanField("Dérangement par émissions lumineuses nocturnes", default=False)
    mesures_emissions_lumiere = models.TextField('Mesures engagées', blank=True)
    pollution_eau = models.BooleanField("Pollution de milieux humides", default=False)
    mesures_pollution_eau = models.TextField('Mesures engagées', blank=True)
    pollution_terre = models.BooleanField("Pollution de milieux terrestre", default=False)
    mesures_pollution_terre = models.TextField('Mesures engagées', blank=True)
    sensibilisation = models.BooleanField("Sensibilisation", default=False)
    mesures_sensibilisation = models.TextField('Mesures engagées', blank=True)
    emprise_amenagement = models.BooleanField("Emprise des aménagements", default=False)
    mesures_emprise_amenagement = models.TextField('Mesures engagées', blank=True)
    public = models.BooleanField("Présence du public", default=False)
    mesures_public = models.TextField('Mesures engagées', blank=True)
    parking = models.BooleanField("Stationnement des véhicules", default=False)
    mesures_parking = models.TextField('Mesures engagées', blank=True)
    engins_aeriens = models.BooleanField("Présence d’engins aériens", default=False)
    mesures_engins_aeriens = models.TextField('Mesures engagées', blank=True)

    # Conclusion
    conclusion_natura_2000 = models.BooleanField(
        "Je déclare conclure à l'absence d'incidence significative sur "
        "le ou les sites NATURA 2000 concernés par la manifestation",
        default=False,
        help_text="Si laissé décoché, alors l'évaluation d'incidences doit se poursuivre. Contactez la "
                  "DDT ou la DREAL au plus vite.",
    )

    # Overrides
    def __str__(self):
        return smart_text(self.manif.__str__())

    def formulaire_n2k_complet(self):
        """ Indiquer si le formulaire est complet """
        for champ in self.get_liste_champs_requis():
            if getattr(self, champ) is None:
                return False
            elif isinstance(getattr(self, champ), str) and getattr(self, champ) == '':
                return False
        return True

    def get_liste_champs_requis(self):
        """ Retourne la liste des champs obligatoires pour la finalisation de l'incidence """
        liste_requis = list(self.LISTE_CHAMPS_REQUIS)
        for field in self._meta.get_fields():
            if type(field) is models.BooleanField:
                # Chercher les champs booléens
                champ = getattr(self, field.name)
                if champ:
                    # Si True, le champ mesures associé est obligatoire
                    champ_requis = "mesures_" + field.name
                    try:
                        self._meta.get_field(champ_requis)
                        # Le champ mesures associé existe
                        liste_requis.append(champ_requis)
                    except FieldDoesNotExist:
                        pass
        return liste_requis

    # Meta
    class Meta(EvaluationManif.Meta):
        verbose_name = "évaluation Natura 2000"
        verbose_name_plural = "évaluations Natura 2000"
        default_related_name = "n2kevaluation"
        app_label = "evaluation_incidence"
