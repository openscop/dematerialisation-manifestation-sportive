# coding: utf-8
from django.db import models

from administrative_division.mixins.divisions import DepartementableMixin


class RnrZone(models.Model):
    """ Réserve Naturelle Régionale """

    # Champs
    nom = models.CharField("nom", max_length=255, unique=True)
    departements = models.ManyToManyField(to='administrative_division.Departement', verbose_name='départements concernés')
    code = models.CharField("code", unique=True, max_length=255)

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.nom

    # Meta
    class Meta:
        verbose_name = "réserve naturelle régionale"
        verbose_name_plural = "réserves naturelles régionales"
        default_related_name = "rnrzones"
        app_label = "evaluation_incidence"


class RnrAdministrateur(DepartementableMixin):
    """ Administrateur RNR """

    # Champs
    nom_court = models.CharField("abréviation", max_length=10)
    nom = models.CharField("nom", max_length=255)
    adresse = models.CharField("adresse", max_length=255, null=True, blank=True)
    commune = models.ForeignKey('administrative_division.commune', verbose_name="commune", on_delete=models.CASCADE, null=True, blank=True)
    email = models.EmailField("e-mail", max_length=200)
    contacts = models.TextField(null=True, blank=True, help_text="prénom nom numéro de téléphone. Une ligne par contact")
    rnr = models.OneToOneField(RnrZone, related_name="administrateurrnr", verbose_name="Zone RNR", on_delete=models.CASCADE)

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.nom_court

    # Meta
    class Meta:
        verbose_name = "Gestionnaire RNR"
        verbose_name_plural = "Gestionnaires RNR"
        default_related_name = "rnradministrateurs"
        app_label = "evaluation_incidence"
