# coding: utf-8
from ajax_select.helpers import make_ajax_form
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin
from django.urls import reverse
from django.utils.html import format_html

from instructions.admin.instruction import InstructionInline
from core.util.admin import RelationOnlyFieldListFilter
from core.models import LogConsult
from evaluation_incidence.admin import N2kEvalInline, RNREvalInline
from evaluations.admin import Natura2000EvaluationInline, RNREvaluationInline
from evenements.admin.filter import EncoursFilter, CalendrierFilter, AVenirFilter, TypeManifFilter
from evenements.admin.inline import DocumentComplementaireInline
from evenements.models.dnm import Dnm, Dnmc, Dcnm, Dcnmc
from evenements.models.vtm import Dvtm, Avtm, Avtmcir
from evenements.models.manifestation import Manif
from evenements.models.sansinstance import SansInstanceManif

FIELDSET_GEN = {'fields': ('instance', 'nom', ('date_creation', 'date_debut', 'date_fin'), ('description', 'observation'),
                           ('structure', 'activite'), ('nb_organisateurs', 'nb_participants', 'nb_spectateurs'))}
FIELDSET_GEO = {'classes': ('collapse',),
                'fields': (('ville_depart', 'villes_traversees', 'departements_traverses'),
                           'parcours_openrunner', 'descriptions_parcours')}
FIELDSET_CAL = {'classes': ('collapse',),
                'fields': (('dans_calendrier', 'prive', 'cache', 'afficher_adresse_structure'))}
FIELDSET_BAL = {'classes': ('collapse',),
                'fields': ('convention_balisage', 'balisage')}
FIELDSET_CRE = {'classes': ('collapse',),
                'fields': ('emprise', 'competition', 'circuit_non_permanent', 'circuit_homologue')}
FIELDSET_NAT = {'classes': ('collapse',),
                'fields': ('gros_budget', 'delivrance_titre', 'lucratif', 'vtm_hors_circulation',
                           'signature_charte_dispense_site_n2k', 'sites_natura2000', 'lieux_pdesi',
                           'demande_homologation', 'zones_rnr')}
FIELDSET_CIR = {'classes': ('collapse',),
                'fields': (('depart_groupe', 'circul_groupe'), 'nb_vehicules_accompagnement',
                           ('vehicule_ouverture', 'vehicule_debut', 'vehicule_fin', 'vehicule_organisation'),
                           ('nb_signaleurs', 'nb_signaleurs_fixes', 'nb_signaleurs_autos', 'nb_signaleurs_motos'),
                           'police_municipale', 'detail_police_municipale', 'police_nationale', 'detail_police_nationale',
                           'respect_code_route', 'respect_code_route_detail', 'priorite_passage', 'priorite_passage_detail',
                           'usage_temporaire', 'usage_temporaire_detail', 'usage_privatif', 'usage_privatif_detail')}
FIELDSET_SEC = {'classes': ('collapse',),
                'fields': (('securite_nom', 'securite_prenom'), ('securite_tel', 'securite_email'))}
FIELDSET_TEC = {'classes': ('collapse',),
                'fields': (('technique_nom', 'technique_prenom'), ('technique_tel', 'technique_email'))}
FIELDSET_CON = {'classes': ('collapse',),
                'fields': (('prenom_contact', 'nom_contact', 'tel_contact'),)}
LISTE_FILES = ['cartographie', 'reglement_manifestation', 'disposition_securite', 'presence_docteur', 'topo_securite',
               'docs_additionels', 'engagement_organisateur', 'certificat_assurance', 'charte_dispense_site_n2k',
               'convention_police']


class AdminMixin(object):
    """ Mixin qui regroupe les méthodes partagées """

    list_display = ('__str__', 'date_debut', 'en_cours_instruction', 'soumise_natura2000', 'soumise_rnr',
                    'ville_depart__arrondissement__departement')
    list_filter = ['instruction__etat', ('ville_depart__arrondissement__departement', RelationOnlyFieldListFilter)]
    readonly_fields = ('date_creation',)
    search_fields = ['nom__unaccent', 'structure__name__unaccent', 'activite__name']
    inlines = [DocumentComplementaireInline, InstructionInline,
               RNREvaluationInline, Natura2000EvaluationInline,
               RNREvalInline, N2kEvalInline]
    list_per_page = 25
    ordering = ['-date_debut']

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(ville_depart__arrondissement__departement=request.user.get_departement())
        return queryset

    def save_formset(self, request, form, formset, change):
        # Surcharge pour enregistrer le user en référent pour tous changements
        if formset.model._meta.object_name == 'Instruction':
            for form in formset.forms:
                if not form.instance.referent:
                    form.instance.referent = request.user
                    # Si la partie instruction n'est pas modifiée, super n'enregistrera pas 'instruction'
                    form.instance.save()
        return super().save_formset(request, form, formset, change)


@admin.register(Manif)
class ManifAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des manifestations """

    # Configuration
    list_display = ('link_to_specialized', 'en_cours_instruction', 'date_creation', 'get_type_manif', 'nom',
                    'structure', 'activite__name', 'date_debut', 'ville_depart__arrondissement__departement', 'pk')
    list_filter = [TypeManifFilter, EncoursFilter, CalendrierFilter, AVenirFilter,
                   ('ville_depart__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['nom__unaccent', 'structure__name__unaccent', 'activite__name']
    date_hierarchy = 'date_debut'
    list_per_page = 25
    ordering = ['-date_creation']
    # Pas de configuration de détail, voir les manifestations spécialisées

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(ville_depart__arrondissement__departement=request.user.get_departement())
        return queryset

    def changelist_view(self, request, extra_context=None):
        """surcharge pour ajouter un log"""
        response = super().changelist_view(request, extra_context)
        LogConsult.objects.create_from_list(response, request)
        return response

    def link_to_specialized(self, obj):
        if obj.get_type_manif():
            link = reverse("admin:evenements_" + obj.get_type_manif() + "_change", args=[obj.get_cerfa().id])
            return format_html('<a href="{}">Consulter</a>', link)
        return '-'
    link_to_specialized.short_description = 'Dossier'


@admin.register(SansInstanceManif)
class SansInstanceManifAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des manifestations sans instance """

    # Configuration
    list_display = ('nom', 'structure', 'activite__name', 'date_debut', 'ville_depart__arrondissement__departement')
    list_filter = [('ville_depart__arrondissement__departement', RelationOnlyFieldListFilter)]
    readonly_fields = ['date_creation']
    search_fields = ['nom__unaccent', 'structure__name__unaccent', 'activite__name']
    list_per_page = 25
    ordering = ['-date_creation']

    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', FIELDSET_GEO),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(SansInstanceManif, {'ville_depart': 'commune', 'villes_traversees': 'commune',
                                              'departements_traverses': 'departement', 'structure': 'structure'})


@admin.register(Dnm)
class DnmAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des manifestations non motorisées soumises à déclaration """

    # Configuration
    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', FIELDSET_GEO),
        ('Calendrier', FIELDSET_CAL),
        ('Balisage', FIELDSET_BAL),
        ('Natura2000', FIELDSET_NAT),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Coordonateur Sécurité', FIELDSET_SEC),
        ('Circulation', FIELDSET_CIR),
        ('fichiers', {'classes': ('collapse',), 'fields': LISTE_FILES + ['itineraire_horaire']}),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Dnm, {'ville_depart': 'commune', 'villes_traversees': 'commune', 'lieux_pdesi': 'lieupdesi',
                                'departements_traverses': 'departement', 'sites_natura2000': 'siten2k',
                                'zones_rnr': 'zonernr', 'structure': 'structure'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(Dnmc)
class DnmcAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des manifestations cyclistes non motorisées soumises à déclaration """

    # Configuration
    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', FIELDSET_GEO),
        ('Calendrier', FIELDSET_CAL),
        ('Balisage', FIELDSET_BAL),
        ('Natura2000', FIELDSET_NAT),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Coordonateur Sécurité', FIELDSET_SEC),
        ('Circulation', FIELDSET_CIR),
        ('fichiers', {'classes': ('collapse',), 'fields': LISTE_FILES + ['itineraire_horaire', 'dossier_tech_cycl']}),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Dnmc, {'ville_depart': 'commune', 'villes_traversees': 'commune', 'lieux_pdesi': 'lieupdesi',
                                 'departements_traverses': 'departement', 'sites_natura2000': 'siten2k',
                                 'zones_rnr': 'zonernr', 'structure': 'structure'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(Dcnm)
class DcnmAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des manifestations compétitives non motorisées soumises à déclaration """

    # Configuration
    fieldsets = (
        (None, {'fields': ('instance', 'nom', ('date_creation', 'date_debut', 'date_fin'),
                           ('description', 'observation'), ('structure', 'activite'),
                           'affiliation', ('nb_participants', 'nb_spectateurs'))}),
        ('Géographie', FIELDSET_GEO),
        ('Calendrier', FIELDSET_CAL),
        ('Balisage', FIELDSET_BAL),
        ('Natura2000', FIELDSET_NAT),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Coordonateur Sécurité', FIELDSET_SEC),
        ('Circulation', FIELDSET_CIR),
        ('fichiers', {'classes': ('collapse',), 'fields': LISTE_FILES + ['liste_signaleurs', 'itineraire_horaire']}),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Dcnm, {'ville_depart': 'commune', 'villes_traversees': 'commune', 'lieux_pdesi': 'lieupdesi',
                                 'departements_traverses': 'departement', 'sites_natura2000': 'siten2k',
                                 'zones_rnr': 'zonernr', 'structure': 'structure'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(Dcnmc)
class DcnmcAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des manifestations cyclistes compétitives non motorisées soumises à déclaration """

    # Configuration
    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', FIELDSET_GEO),
        ('Calendrier', FIELDSET_CAL),
        ('Balisage', FIELDSET_BAL),
        ('Natura2000', FIELDSET_NAT),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Coordonateur Sécurité', FIELDSET_SEC),
        ('Circulation', FIELDSET_CIR),
        ('fichiers', {'classes': ('collapse',), 'fields': LISTE_FILES + ['liste_signaleurs', 'itineraire_horaire',
                                                                         'dossier_tech_cycl']}),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Dcnmc, {'ville_depart': 'commune', 'villes_traversees': 'commune', 'lieux_pdesi': 'lieupdesi',
                                  'departements_traverses': 'departement', 'sites_natura2000': 'siten2k',
                                  'zones_rnr': 'zonernr', 'structure': 'structure'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(Dvtm)
class DvtmAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des concentrations motorisées """

    # Configuration
    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', FIELDSET_GEO),
        ('Calendrier', FIELDSET_CAL),
        ('Balisage', FIELDSET_BAL),
        ('Natura2000', FIELDSET_NAT),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Organisateur Technique', FIELDSET_TEC),
        ('Circulation', {'classes': ('collapse',), 'fields': ('vehicules', 'nb_vehicules_accompagnement',
                                                              'nb_personnes_pts_rassemblement')}),
        ('fichiers', {'classes': ('collapse',), 'fields': LISTE_FILES + ['itineraire_horaire']}),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Dvtm, {'ville_depart': 'commune', 'villes_traversees': 'commune', 'lieux_pdesi': 'lieupdesi',
                                 'departements_traverses': 'departement', 'sites_natura2000': 'siten2k',
                                 'zones_rnr': 'zonernr', 'structure': 'structure'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(Avtm)
class AvtmAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des manifestations motorisées """

    # Configuration
    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', FIELDSET_GEO),
        ('Calendrier', FIELDSET_CAL),
        ('Balisage', FIELDSET_BAL),
        ('Natura2000', FIELDSET_NAT),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Organisateur Technique', FIELDSET_TEC),
        ('Circulation', {'classes': ('collapse',), 'fields': ('vehicules', 'nb_vehicules_accompagnement')}),
        ('fichiers', {'classes': ('collapse',), 'fields': LISTE_FILES + ['participants', 'avis_federation_delegataire',
                                                                         'commissaires', 'carte_zone_public',
                                                                         'itineraire_horaire', 'plan_masse',
                                                                         'certificat_organisateur_tech']}),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Avtm, {'ville_depart': 'commune', 'villes_traversees': 'commune', 'lieux_pdesi': 'lieupdesi',
                                 'departements_traverses': 'departement', 'sites_natura2000': 'siten2k',
                                 'zones_rnr': 'zonernr', 'structure': 'structure'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(Avtmcir)
class AvtmcirAdmin(ExportActionModelAdmin, AdminMixin, RelatedFieldAdmin):
    """ Administration des courses motorisées """

    # Configuration
    fieldsets = (
        (None, FIELDSET_GEN),
        ('Géographie', FIELDSET_GEO),
        ('Calendrier', FIELDSET_CAL),
        ('Balisage', FIELDSET_BAL),
        ('Natura2000', FIELDSET_NAT),
        ('Données d\'aiguillage', FIELDSET_CRE),
        ('Organisateur Technique', FIELDSET_TEC),
        ('Circulation', {'classes': ('collapse',), 'fields': ('vehicules', 'type_support')}),
        ('fichiers', {'classes': ('collapse',), 'fields': LISTE_FILES + ['plan_masse', 'certificat_organisateur_tech',
                                                                         'participants']}),
        ('Contact', FIELDSET_CON),
    )
    form = make_ajax_form(Avtmcir, {'ville_depart': 'commune', 'villes_traversees': 'commune',
                                    'departements_traverses': 'departement', 'sites_natura2000': 'siten2k',
                                    'lieux_pdesi': 'lieupdesi', 'zones_rnr': 'zonernr', 'structure': 'structure'})

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}
