# coding: utf-8
"""
Utilitaire pour les méthodes affichées dans l'admin

Le décorateur set_admin_info(attribute=value, ...) permet de simplifier l'écriture des
attributs de méthodes utilisés dans l'admin. Par exemple :

    def get_ancestor(self):
        return self.ancestor
    get_ancestor.short_description = "Ancêtre"
    get_ancestor.admin_order_field = 'ancestor'

peut être réécrit :

    @set_admin_info(short_desription="Ancêtre", admin_order_field='ancestor')
    def get_ancestor(self):
        return self.ancestor

"""
from django.contrib.admin.filters import RelatedFieldListFilter


def set_admin_info(**kwargs):
    """ Définir des attributs à une méthode ou fonction """

    def decorator(func):
        for key in kwargs:
            setattr(func, key, kwargs[key])
        return func

    return decorator


class RelationOnlyFieldListFilter(RelatedFieldListFilter):
    """
    Filtre admin affichant les champs liés existant dans la base de données

    ex. list_filter = [('first__other', RelationOnlyFieldListFilter)]
    """

    # Overrides
    def __init__(self, field, request, params, model, model_admin, field_path):
        self.field_path = field_path
        super().__init__(field, request, params, model, model_admin, field_path)

    def field_choices(self, field, request, model_admin):
        limit_choices_to = {'pk__in': set(model_admin.get_queryset(request).values_list(self.field_path, flat=True))}
        return field.get_choices(include_blank=False, limit_choices_to=limit_choices_to)
