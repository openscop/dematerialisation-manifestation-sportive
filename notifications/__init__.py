# coding: utf-8
"""
Application qui permet de notifier les agents lorsqu'un document change
d'état, par exemple. L'application permet aussi de consigner les actions dans
l'application.
"""
from django.apps import AppConfig


class NotificationsConfig(AppConfig):
    """ Configuration de l'application notifications """

    name = 'notifications'
    verbose_name = 'Notifications'


default_app_config = 'notifications.NotificationsConfig'
