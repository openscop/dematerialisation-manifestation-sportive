# coding: utf-8
from django.urls import re_path, path

from .views.n2k import SiteN2KListe, N2kEvalCreate, N2kEvalUpdate
from .views.rnr import RNRListe, RNREvalCreate, RNREvalUpdate
from .views.ajax import SitesN2kAJAXView, LieuxPdesiAJAXView, ZonesRnrAJAXView, N2kEvalAjaxCreate


app_name = 'evaluation_incidence'
urlpatterns = [

    # Listes des RNR et sites Natura2000
    re_path('RNR/(?P<dept>\d+)/', RNRListe.as_view(), name='rnr_liste'),
    path('RNR/', RNRListe.as_view(), name='rnr_liste_all'),
    re_path('SiteN2K/(?P<dept>\d+)/', SiteN2KListe.as_view(), name='siten2k_liste'),
    path('SiteN2K/', SiteN2KListe.as_view(), name='siten2k_liste_all'),
    re_path('n2keval/add/(?P<manif_pk>\d+)/', N2kEvalCreate.as_view(), name='n2keval_add'),
    re_path('n2keval/(?P<pk>\d+)/edit/', N2kEvalUpdate.as_view(), name='n2keval_update'),
    re_path('rnreval/add/(?P<manif_pk>\d+)/', RNREvalCreate.as_view(), name='rnreval_add'),
    re_path('rnreval/(?P<pk>\d+)/edit/', RNREvalUpdate.as_view(), name='rnreval_update'),
    path('ajax/sitesn2k/', SitesN2kAJAXView.as_view(), name='sitesn2k_widget'),
    path('ajax/zonesrnr/', ZonesRnrAJAXView.as_view(), name='zonesrnr_widget'),
    path('ajax/lieuxpdesi/', LieuxPdesiAJAXView.as_view(), name='lieuxpdesi_widget'),
    re_path('n2keval/add/ajax/(?P<manif_pk>\d+)/', N2kEvalAjaxCreate.as_view(), name='ajaxn2keval_add'),
]
