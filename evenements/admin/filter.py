# coding: utf-8
from django.contrib import admin
from django.db.models import Q
from django.utils import timezone


class EncoursFilter(admin.SimpleListFilter):
    title = 'Formulaire validé'
    parameter_name = 'encours'

    def lookups(self, request, model_admin):
        return ('True', 'Formulaire validé'), ('False', 'Formulaire non validé')

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(instruction__id__gt=0)
        if self.value() == 'False':
            return queryset.exclude(instruction__id__gt=0)


class CalendrierFilter(admin.SimpleListFilter):
    title = 'Présence dans le calendrier'
    parameter_name = 'Dans_calendrier'

    def lookups(self, request, model_admin):
        return ('True', 'Oui'), ('False', 'Non')

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(Q(prive=False) & Q(cache=False))
        if self.value() == 'False':
            return queryset.filter(Q(prive=True) | Q(cache=True))


class AVenirFilter(admin.SimpleListFilter):
    title = 'A venir'
    parameter_name = 'a_venir'

    def lookups(self, request, model_admin):
        return ('True', 'Oui'), ('False', 'Passée')

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(date_debut__gte=timezone.now())
        if self.value() == 'False':
            return queryset.filter(date_debut__lt=timezone.now())


class TypeManifFilter(admin.SimpleListFilter):
    title = 'Type de dossier'
    parameter_name = 'type_de_dossier'

    def lookups(self, request, model_admin):
        return ('dnm', 'dnm'), ('dnmc', 'dnmc'), ('dcnm', 'dcnm'), ('dcnmc', 'dcnmc'), ('dvtm', 'dvtm'), ('avtm', 'avtm'), ('avtmcir', 'avtmcir')

    def queryset(self, request, queryset):
        if self.value():
            lookup = {'{0}__isnull'.format(self.value()): False}
            return queryset.filter(**lookup)
        else:
            return queryset