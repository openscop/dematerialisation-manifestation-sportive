from datetime import timedelta
from post_office import mail

from django_cron import CronJobBase, Schedule
from django_cron.models import CronJobLog
from django.utils import timezone
from django.contrib.auth import get_user_model

from core.models import User
from messagerie.models import CptMsg
from configuration import settings


class UpdateCPT(CronJobBase):
    """
    Mise à jour des compteur de message non lu
    """
    RUN_EVERY_MINS = settings.MESSAGERIE_CPT_TIME_REFRESH  # à lancer toutes les minutes

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'messagerie.UpdateCPT'    # a unique code

    def do(self):

        # Purger les logs
        CronJobLog.objects.filter(code="messagerie.UpdateCPT").filter(is_success=True).filter(
            end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        log_retour = ""
        last_time = timezone.now() - timedelta(minutes=settings.INACTIVE_TIME)
        total = get_user_model().objects.filter(last_visit__gte=last_time).count()
        for user in get_user_model().objects.filter(last_visit__gte=last_time):

            cpt = CptMsg.objects.get(utilisateur=user)
            cpt.update_cpt()
        log_retour += str(total) + " compteurs mis à jour"
        return log_retour


class SendRecap(CronJobBase):
    """
    Cron pour envoyer un message récapitulatif des msg reçu
    """
    RUN_AT_TIME = ['7:05']  # à lancer à 7h05

    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'messagerie.SendRecap'  # a unique code

    def do(self):

        # Purger les logs
        CronJobLog.objects.filter(code="messagerie.SendRecap").filter(is_success=True).filter(
            end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        log_retour = ""

        total = 0
        temps = timezone.now()
        # on n'a qu'un nombre limité de mail par minute qu'on peut envoyer on va donc separer les envoi de 5 min
        for user in User.objects.filter(is_active=True).order_by('username'):
            if user.recap:
                cpt = CptMsg.objects.get(utilisateur=user)
                if cpt.action or cpt.conversation or cpt.info_suivi or cpt.news:
                    total_cpt = cpt.action + cpt.conversation + cpt.info_suivi + cpt.news
                    action = cpt.action
                    conv = cpt.conversation
                    info = cpt.info_suivi
                    nouv = cpt.news
                    context = {
                        "total": total_cpt,
                        "action": action,
                        "conv": conv,
                        "info": info,
                        "nouv": nouv,
                    }
                    send = user.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
                    mail.send(user.email, send, template="msg_recap", context=context, scheduled_time=temps)
                    total += 1
                    # Décaler l'envoi tous les 20
                    if total % 20 == 0:
                        temps = temps + timezone.timedelta(minutes=5)
        log_retour += str(total) + " mails envoyés"
        return log_retour
