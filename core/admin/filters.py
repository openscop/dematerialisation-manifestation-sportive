# coding: utf-8
from django.contrib.admin.filters import SimpleListFilter

from core.util.user import UserHelper
from core.models import Instance


class RoleFilter(SimpleListFilter):
    """ Filtre admin par rôle d'utilisateur """

    title = "Rôle"
    parameter_name = 'roles'

    def lookups(self, request, model_admin):
        """ Renvoyer les valeurs possibles du filtre """
        lookups = UserHelper.ROLE_CHOICES + [["", "aucun"]]
        return lookups

    def queryset(self, request, queryset):
        """ Renvoyer un queryset des utilisateurs correspondant à un rôle """
        if self.value() in UserHelper.AGENT_TYPES:
            lookup = {'agent__{0}__isnull'.format(self.value()): False}
            return queryset.filter(**lookup)
        elif self.value() in UserHelper.AGENTLOCAL_TYPES:
            lookup = {'agentlocal__{0}__isnull'.format(self.value()): False}
            return queryset.filter(**lookup)
        elif self.value() in UserHelper.OBSERVATEUR_TYPES:
            lookup = {'observateur__{0}__isnull'.format(self.value()): False}
            return queryset.filter(**lookup)
        elif self.value() in UserHelper.DIRECT_TYPES:
            lookup = {'{0}__isnull'.format(self.value()): False}
            return queryset.filter(**lookup)
        elif self.value() == "":
            return queryset.filter(tableau_role="")
        return queryset


class LogEntryInstanceFilter(SimpleListFilter):
    title = "instance de l'utilisateur"
    parameter_name = 'instance'

    def lookups(self, request, model_admin):
        if not request.user.has_group("Administrateurs technique") and not request.user.is_superuser:
            return None
        else:
            objets = Instance.objects.configured()
            return [(c.pk, str(c)) for c in objets]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(user__default_instance__pk=self.value())
        return queryset
