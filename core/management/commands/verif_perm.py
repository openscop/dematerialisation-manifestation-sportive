# coding: utf-8
"""
Permet de controler les droits par groupe et par appli
3 paramètres optionnels pour affiner la recherche
"""
from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group

from core.util.security import protect_code


class Command(BaseCommand):
    """ Nettoyer et charger les données par défaut de la base de données """
    args = ''
    help = 'Affiche les permissions pour un groupe donné et une appli donnée'

    def add_arguments(self, parser):
        parser.add_argument('--groupe', nargs='?', type=str, default='*', const='instance',
                            help="chaine contenue dans le nom du groupe recherché. '*' pour tous les groupes")
        parser.add_argument('--perm', nargs='?', type=str, default='*', const='delete',
                            choices=['add', 'delete', 'view', 'change', '*'], help="permission à examiner. '*' pour toutes les permissions")
        parser.add_argument('--appli', nargs='?', type=str, default='', const='',
                            help="chaine contenue dans le nom de l'application ou du modèle")

    def handle(self, *args, **options):
        """ Exécuter la commande """
        protect_code()
        print()
        groupe_str = options['groupe']
        perm_str = options['perm']
        appli_str = options['appli']
        if groupe_str != "*":
            if not Group.objects.filter(name__icontains=groupe_str).exists():
                print('Pas de groupes correspondant au paramètre.')
                return
            groupe = Group.objects.get(name__icontains=groupe_str)
            if not groupe.permissions.all():
                print('Pas de permissions ajoutées à ce groupe.')
                return
            if perm_str != "*":
                permissions = groupe.permissions.filter(codename__contains=perm_str)
            else:
                permissions = groupe.permissions.all()
            if not permissions:
                print('pas de permissions correspondant au paramètre')
                return
            if perm_str != "*":
                print("Liste des permissions \"" + perm_str + "\" du groupe \"" + groupe.name + "\" pour l'appli ou le mot clé \"" + appli_str + "\" :")
                print('---------------------------------------------------------------------------------------------------------------------')
            else:
                print("Liste des permissions du groupe \"" + groupe.name + "\" pour l'appli ou le mot clé \"" + appli_str + "\" :")
                print('----------------------------------------------------------------------------------------------------------')
            for perm in permissions:
                if appli_str in perm.__str__():
                    print(perm)
        else:
            if perm_str != "*":
                print("Liste des permissions \"" + perm_str + "\" de tous les groupes pour l'appli ou le mot clé \"" + appli_str + "\" :")
                print('-----------------------------------------------------------------------------------------------------------------')
            else:
                print("Liste des permissions de tous les groupes pour l'appli ou le mot clé \"" + appli_str + "\" :")
                print('--------------------------------------------------------------------------------------------')
            for groupe in Group.objects.all().order_by('name'):
                if perm_str != "*":
                    permissions = groupe.permissions.filter(codename__contains=perm_str)
                else:
                    permissions = groupe.permissions.all()
                # print(groupe + ' :')
                print(f'{groupe} :')
                for perm in permissions:
                    if appli_str in perm.__str__():
                        print(f'\t{perm}')
