# coding: utf-8
import hashlib
import logging

import slumber
from django.core.cache import cache
from django.core.exceptions import ImproperlyConfigured
from django.core.mail import mail_admins
from furl.furl import furl
from slumber import API, exceptions

from carto.models.openrunnerusers import OpenRunnerUsers
from configuration import OPENRUNNER_API_URL, OPENRUNNER_TOKEN, OPENRUNNER_FRONTEND_URL


logger = logging.getLogger('carto')


class OpenRunnerAPI(object):
    """ Helper pour l'API OpenRunner """

    ROLES = ['organizer', 'instructor', 'security_officer']

    # Initialisation
    def __init__(self):
        """
        Initialiser l'objet

        Note : les points d'accès de l'API n'ont pas de '/' final.
        Il ne faut pas faire d'appel d'API avec la méthode POST si l'on utilise
        un '/' à la fin de l'URL; Le serveur fera généralement automatiquement la
        redirection, mais avec la méthode GET. Ce qui provoque des erreurs 405.
        """
        self.ORAPI = API(OPENRUNNER_API_URL, append_slash=False, format='json')

    # Privé
    @staticmethod
    def _is_role_valid(name):
        """ Renvoyer si le nom de rôle utilisateur est accepté par l'API """
        return name in OpenRunnerAPI.ROLES

    # API
    def create_user(self, username, area, role, record=True, return_data_on_failure=False):
        """
        Créer un utilisateur avec un rôle particulier

        Attacher l'utilisateur à un département
        :param username: nom d'utilisateur à créer
        :param area: nom du département à attacher, 2 ou 3 caractères
        :param role: nom de rôle de l'utilisateur. Doit être un rôle défini dans ROLES
        :param record: Consigner le nouvel utilisateur connu dans la base de données
        :returns: Un booléen indiquant la réussite de l'opération
        :rtype: bool
        """
        if OpenRunnerUsers.objects.is_known(username):
            return True
        if self._is_role_valid(role):
            jsondata = ""
            try:
                jsondata = self.ORAPI.user.create.post({'api_token': OPENRUNNER_TOKEN, 'username': username, 'role': role, 'area': area})
            except slumber.exceptions.HttpServerError:
                mail_admins("Erreur API Openrunner", "create_user : Erreur d'appel à l'API Openrunner. Vérifiez vos paramètres de jeton OpenRunner")
                logger.error("create_user : Erreur serveur http de l'API Openrunner")
            except:
                logger.error("create_user : L'API Openrunner n'a pas renvoyé de réponse")
            if isinstance(jsondata, dict):
                if jsondata.get('success', False):
                    if record:
                        OpenRunnerUsers.objects.get_or_create(username=username)
                    return True
                else:
                    warning_text = "create_user : L'API Openrunner a renvoyé un message pour la paire {user}[{role}] : {error}"
                    if 'already used' in jsondata.get('message', "") and record:  # Si l'utilisateur existe déjà, l'insérer dans les données connues
                        OpenRunnerUsers.objects.get_or_create(username=username)
                    logger.warning(warning_text.format(user=username, role=role, error=jsondata.get('message') or jsondata.get('error', '---')))
                    return jsondata if return_data_on_failure else False
            else:
                if jsondata:
                    logger.error("L'API Openrunner a renvoyé une erreur pour {user} :\n{error}".format(user=username, error=jsondata))
        return False

    def get_user_routes(self, username, return_data_on_failure=False):
        """ Retourner les identifiants des routes créées par un utilisateur """
        jsondata = dict()
        try:
            jsondata = self.ORAPI.user.routes(format='json').get(api_token=OPENRUNNER_TOKEN, username=username)
        except slumber.exceptions.HttpServerError:
            mail_admins("Erreur API Openrunner", "get_user_routes : Erreur d'appel à l'API Openrunner. Vérifiez vos paramètres de jeton OpenRunner")
            logger.error("get_user_routes : Erreur serveur http de l'API Openrunner")
        except:
            logger.error("get_user_routes : L'API Openrunner n'a pas renvoyé de réponse")
        if isinstance(jsondata, dict):
            if jsondata.get('success', False):
                if type(jsondata['routelist']) is not dict:
                    logger.warning("get_user_routes : l'API a renvoyé une liste vide")
                    return dict(erreur="Problème : aucune cartographie de parcours récupérée.")
                return jsondata['routelist']
            if jsondata.get('code', 0) in [400, '400']:
                logger.warning("get_user_routes : l'API n'a pas accepté les paramètres d'entrée")
            elif jsondata.get('code', 0) in [404, '404']:
                logger.warning("get_user_routes : l'API n'a pas trouvé l'utilisateur")
            elif jsondata.get('error', False):
                logger.error("get_user_routes : L'API Openrunner a renvoyé une erreur pour {user} :\n{error}".format(user=username, error=jsondata))
        if return_data_on_failure:
            return jsondata
        return dict(erreur="Problème : nous ne parvenons pas à récupérer vos cartographies de parcours. Veuillez ré-essayez dans quelques instants")

    def get_user_login_token(self, username, role=None):
        """
        Retrouver le token de connexion à un compte utilisateur OpenRunner

        :param username: nom d'utilisateur Openrunner (miroir des users Manifsportive)
        :param role: facultatif, si une des valeurs de OpenRunnerAPI.ROLES, met également à jour
        le rôle de l'utilisateur OpenRunner (nouveau dans l'API du 10 février 2016)
        :param as_data: renvoyer les données JSON si True, ou le token en cas de succès. En
        cas d'échec, renvoyer une chaîne vide si False, sinon renvoyer les données JSON
        """
        cache_key = hashlib.md5("carto.token.{user}.{role}".format(user=username.lower(), role=role or "").encode('utf-8')).hexdigest()
        cached_data = cache.get(cache_key)
        if cached_data:  # On met en cache le token pour éviter un nombre élevé de requêtes successives
            return cached_data
        extra = {'role': role} if self._is_role_valid(role) else {'role': ''}  # Ajouter le paramètre rôle si passé, sinon passer une chaîne vide
        jsondata = ""
        try:
            jsondata = self.ORAPI.user.short_token.get(api_token=OPENRUNNER_TOKEN, username=username, **extra)
        except slumber.exceptions.HttpServerError:
            mail_admins("Erreur API Openrunner", "get_user_login_token : Erreur d'appel à l'API Openrunner. Vérifiez vos paramètres de jeton OpenRunner")
            logger.error("get_user_login_token : Erreur serveur http de l'API Openrunner")
        except:
            logger.error("get_user_login_token : L'API Openrunner n'a pas renvoyé de réponse")
        # Tester le type de retour d'appel à l'API, échouer et journaliser si incorrect
        if isinstance(jsondata, (bytes, bytearray)):
            # Si l'API renvoie une chaîne (flux au format bytes), alors logger l'erreur reçue
            error_text = "get_user_login_token : L'API Openrunner a renvoyé une erreur pour la paire {user}[{role}] : {error}"
            logger.error(error_text.format(user=username, role=role, error=jsondata))
        if isinstance(jsondata, dict):
            if jsondata.get('success', False):  # succès
                cache.set(cache_key, jsondata['jwt'], 240)  # timeout de 4 minutes, le timeout théorique sur le service Openrunner est de 5 minutes
                return jsondata['jwt']  # Retourner le token de connexion
            else:  # échec, ne pas retourner les données
                warning_text = "get_user_login_token : L'API Openrunner a renvoyé un message pour la paire {user}[{role}] : {error}"
                logger.warning(warning_text.format(user=username, role=role, error=jsondata.get('message', "---")))
        return None

    def get_url_with_login(self, url, username, role=None):
        """
        Ajouter le paramètre de connexion automatique d'un utilisateur à l'URL

        :param url: url à visiter. Si None ou "", utilise l'URL par défaut de la carto
        :param username: nom d'utilisateur à connecter
        :param role: rôle de l'utilisateur à connecter, facultatif
        """
        destination_url = furl(url or OPENRUNNER_FRONTEND_URL)
        connection_token = self.get_user_login_token(username, role)
        if connection_token:
            destination_url.args['token'] = connection_token
        else:
            logger.warning("get_url_with_login : La récupération d'URL de login Openrunner a échoué")
        return destination_url.url

    def create_from_user(self, user):
        """
        Créer un utilisateur OpenRunner

        :param user: utilisateur Django
        :type user: django.contrib.auth.models.AbstractUser
        """
        if user.has_role('organisateur'):
            # Essayer de créer l'utilisateur sur OpenRunner, au cas où
            self.create_user(user.username, user.get_instance().get_departement_name(), 'organizer')
        elif user.has_role('instructeur') or user.has_role('agent') or user.has_role('agentlocal'):
            # Essayer de créer l'utilisateur sur OpenRunner, au cas où
            self.create_user(user.username, user.get_instance().get_departement_name(), 'instructor')
        elif user.has_role('sdisagent'):
            # Essayer de créer l'utilisateur sur OpenRunner, au cas où
            self.create_user(user.username, user.get_instance().get_departement_name(), 'security_officer')


# Instance API
openrunner_api = OpenRunnerAPI()
