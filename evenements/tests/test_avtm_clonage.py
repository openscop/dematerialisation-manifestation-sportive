import re, os

from django.test import TestCase, override_settings
from django.contrib.auth.hashers import make_password as make

from allauth.account.models import EmailAddress
from post_office.models import EmailTemplate

from core.factories import UserFactory
from core.models import User, Instance
from administration.factories import InstructeurFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from organisateurs.factories import StructureFactory, OrganisateurFactory
from sports.factories import ActiviteFactory
from evenements.factories import AvtmFactory


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class Avmt_Clonage(TestCase):
    """
    Test du clonage d'une manifestation
        workflow_GGD : Avis GGD + préavis EDSR
        instruction par arrondissement
        openrunner false par défaut
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
            Création des objets nécessaires à la création de la manifestation
            Création des fichiers nécessaires à joindre à la création de la manifestation
        """
        print()
        print('========= Aiguillage organisateur =========')
        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__workflow_ggd=Instance.WF_GGD_SUBEDSR,
                                            instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)

        cls.organisateur = UserFactory.create(username='organisateur', password=make(123),
                                              default_instance=cls.dep.instance)
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        EmailAddress.objects.create(user=cls.organisateur, email='orga@test.fr', primary=True, verified=True)
        structure = StructureFactory(commune=cls.commune, organisateur=organisateur)
        activ = ActiviteFactory.create()
        cls.instructeur = UserFactory.create(username='instructeur', password=make(123),
                                             default_instance=cls.dep.instance, email='inst@test.fr')
        InstructeurFactory.create(user=cls.instructeur, prefecture=prefecture)

        EmailTemplate.objects.create(name='new_msg')
        for user in User.objects.all():
            user.optionuser.notification_mail = True
            user.optionuser.action_mail = True
            user.optionuser.tracabilite_mail = True
            user.optionuser.save()

        cls.manifestation = AvtmFactory.create(ville_depart=cls.commune, structure=structure, nom='Manifestation_Test',
                                               activite=activ)
        cls.manifestation.nb_participants = 150
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.descriptions_parcours = 'un gros parcours'
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_organisateurs = 5
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.vehicules = 150
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.save()

        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'liste_signaleurs',
                     'participants', 'certificat_organisateur_tech', 'commissaires', 'itineraire_horaire',
                     'topo_securite', 'dossier_tech_cycl', 'plan_masse', 'cartographie'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test clonage avtm")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Suppression des fichiers créés dans /tmp
        """
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'liste_signaleurs',
                     'participants', 'certificat_organisateur_tech', 'commissaires', 'itineraire_horaire',
                     'topo_securite', 'dossier_tech_cycl', 'plan_masse', 'cartographie'):
            os.remove(file+".txt")

        os.system('rm -R /tmp/maniftest/media/42/')

        super().tearDownClass()

    def joindre_fichiers(self, reponse, cas='d'):
        """
        Ajout des fichiers nécessaires à la manifestation
        :param reponse: reponse précédente
        :param cas: suivant cerfa considéré
        :return: reponse suivante
        """
        # appel de la vue pour joindre les fichiers
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        print('\t' + url_script.group('url'))

        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file12:
            reponse = self.client.post(url_script.group('url'), {'cartographie': file12}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        if cas == 'd':
            with open('/tmp/itineraire_horaire.txt') as file7:
                reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file7}, follow=True,
                                           HTTP_HOST='127.0.0.1:8000')
        if cas == 'c':
            with open('/tmp/dossier_tech_cycl.txt') as file6:
                self.client.post(url_script.group('url'), {'dossier_tech_cycl': file6}, follow=True,
                                 HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/itineraire_horaire.txt') as file7:
                reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file7}, follow=True,
                                           HTTP_HOST='127.0.0.1:8000')
        if cas == 'm':
            with open('/tmp/itineraire_horaire.txt') as file7:
                self.client.post(url_script.group('url'), {'itineraire_horaire': file7}, follow=True,
                                 HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/certificat_organisateur_tech.txt') as file8:
                self.client.post(url_script.group('url'), {'certificat_organisateur_tech': file8}, follow=True,
                                 HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/commissaires.txt') as file9:
                self.client.post(url_script.group('url'), {'commissaires': file9}, follow=True,
                                 HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/participants.txt') as file10:
                reponse = self.client.post(url_script.group('url'), {'participants': file10}, follow=True,
                                           HTTP_HOST='127.0.0.1:8000')
        if cas == 's':
            with open('/tmp/certificat_organisateur_tech.txt') as file8:
                self.client.post(url_script.group('url'), {'certificat_organisateur_tech': file8}, follow=True,
                                 HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/participants.txt') as file10:
                self.client.post(url_script.group('url'), {'participants': file10}, follow=True,
                                 HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/plan_masse.txt') as file11:
                reponse = self.client.post(url_script.group('url'), {'plan_masse': file11}, follow=True,
                                           HTTP_HOST='127.0.0.1:8000')
        return reponse

    def declarer_manif(self, reponse):
        """
        Soumettre la manifestation à l'instructeur
        :param reponse: reponse précédente
        :return: reponse suivante
        """
        # appel de la vue pour déclarer la manifestation
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(declar, 'group'))
        print('\t' + declar.group('url'))
        reponse = self.client.get(declar.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Demander l\'instruction', count=2)
        # Soumettre la déclaration
        reponse = self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        return reponse

    def test_clonage_avtm(self):
        """
        Test du clonage d'une manifestation
        """
        print('**** Complèter Avtm ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('1', nb_bloc.group('nb'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get('/tableau-de-bord-organisateur/liste?&filtre_etat=attente',
                                  HTTP_HOST='127.0.0.1:8000')
        detail1 = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail1, 'group'))
        reponse = self.client.get(detail1.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')

        reponse = self.joindre_fichiers(reponse, 'm')

        # reponse = self.declarer_manif(reponse)
        # retour à la vue de détail et test breadcrumbs
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        # etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Envoi de la demande', reponse.content.decode('utf-8'))
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+\\n.+Envoi de la demande', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        # self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')
        self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton vert Déclaration')

        print('**** Clonage Avtm ****')
        # Recherche de l'url de clonage
        cloner = re.search('href="(?P<url>(/[^"]+)).+Créer un nouveau dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(cloner, 'group'))
        print('\t' + cloner.group('url'))
        # Informations de clonage
        data = {}
        data['nom'] = 'Clonage_de_manifestation'
        data['date_debut'] = '11/06/2021 08:00'
        data['date_fin'] = '11/06/2021 20:00'

        # Clonage de la manifestation et redirect sur le détail de la nouvelle manifestation
        reponse = self.client.post(cloner.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        reponse = self.client.get(reponse.content.decode('utf-8'), HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Manifestation_Test')
        self.assertContains(reponse, 'Clonage_De_Manifestation')

        # Test breadcrumbs
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Préparation du dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape1, 'group'))
        self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+\\n.+Envoi de la demande', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(etape2, 'group'))
        self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Recherche du pk de la manifestation
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        el = action.group('url').split('/')
        self.assertTrue(el[2].isdecimal(), msg='pas un nombre')
        self.assertNotEqual(el[2], str(self.manifestation.pk), msg="le pk du clone est identique à l'original")

        print('**** Suppression modèle ****')
        reponse = self.client.get(detail1.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Supprimer ce dossier', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(action, 'group'))
        print('\t' + action.group('url'))
        reponse = self.client.post(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('1', nb_bloc.group('nb'))

        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get('/tableau-de-bord-organisateur/liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail2 = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail2, 'group'))
        reponse = self.client.get(detail2.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Clonage_De_Manifestation')
        # Si tous les fichiers sont présents
        self.assertContains(reponse, 'Envoyer la demande')


        # print(reponse.content.decode('utf-8'))
        # f = open('/var/log/manifsport/test_output.html', 'w', encoding='utf-8')
        # f.write(str(reponse.content.decode('utf-8')).replace('\\n',""))
        # f.close()
