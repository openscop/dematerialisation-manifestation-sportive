Description du projet Manifestation Sportive
=

Le projet manifestationsportive.fr est divisé en plusieurs applications distinctes permettant de décomposer
le projet en problématiques spécifiques. Voici la liste des applications :

1. **administration** : description des différentes entités administratives qui agissent dans le processus de
 validation d'un évenement sportif (préfecture, police, gendarmerie, etc...)
1. **emergencies** : définition des différentes associations de premiers secours ainsi que les différents
secours publics
1. **administrative_division** : description et définition des divisions administratives françaises
(départements, arrondissements et communes)
1. **contacts** : définition des différent contacts et adresses
1. **sports** : définition des différentes fédérations sportives, ainsi que des disciplines sportives
1. **events** : définition des différents types d'évenements sportifs (motorisés, sur voie publique, etc...)
1. **protected_areas** : définition des différents types de zones protégées (RNR et Natura2000) et
de leurs propriétés
1. **evaluations** : application permettant de gérer les évaluations naturelles d'un évenement
1. **declarations** : application permettant de gérer la délivrance d'un récépissé de déclaration d'évenement sportif
1. **authorizations** : gestion des autorisations d'évenements sportifs
1. **agreements** : gestion des avis nécessaires à la délivrance de l'autorisation d'une manifestation sportive
1. **sub_agreements** : gestion des pré-avis nécessaires à la délivrance d'un avis
1. **notifications** : gestion du journal des actions des utilisateurs, et des notifications par mail ou dans
le tableau de bord.
1. **carto** : gestion des cartographies de parcours des manifestations, et interfaçage avec le service OpenRunner
1. **organisateurs** : gestion des organisateurs d'événements, ainsi que leurs structures organisatrices
1. **core** : gestion des spécifités administratives des départements via configuration + Description des utilisateurs.
1. **portail** : interface d'accueil et pages non spécifiques.
1. **aide** : FAQ, Aide et définition des bulles d'aide contextuelle.



## Fichiers contenus dans une application

Chaque application du projet manifestationsportive.fr devrait contenir suivre des conventions de nommage spécifiques
à Python et Django.

- __locale__ : dossier, traductions de l'application (automatique)
- **migrations** : dossier, code de migration de schéma de base de données ; modification de tables, champs... (automatique)
- **management** : dossier, tâches d'administration exécutables manuellement, en ligne de commande ou via `crontab`
- **static** : dossier, fichiers statiques (css, js, images) de l'application
- **templates** : dossier, templates de l'application. Un template est un fichier qui permet à l'application
d'afficher son contenu, tout en laissant la possibilité d'y insérer des données (ex. "Je m'appelle {{prenom}}")
- **adapter** : module, permet de définir les redirections liées à l'authentification.
Par ex, redéfinir la redirection après le login d'un utilisateur.
Voir : https://django-allauth.readthedocs.org/en/latest/advanced.html#custom-redirects
- **admin** : module, définition des données visibles dans l'interface d'administration.
- **factories** : module, créer des données de tests qui seront instanciées au lancement de la suite de tests
- **forms** : module, décrit la structure de chaque formulaire de l'application
- **models** : module, modèles de données de l'application.
Toute méthode visant à interroger la base de données doit être définie ici.
- **signals** : module, signaux lancés par l'application. Ces signaux servent d'événements, reconnaissables
par Django, et qui peuvent déclencher un traitement depuis n'importe quelle autre application
- **tests** : module, suite de tests unitaires pour l'application
- **urls** : module, les urls exposées par l'application. Définissent à quelle URL afficher des vues
- **views** : module, code permettant d'afficher les pages de l'application

\* module : fichier .py, ou répertoire contenant un fichier \_\_init__.py

\* dossier : simple répertoire


