# coding: utf-8
from django.core.files.storage import default_storage
from django.db import models

from configuration.directory import UploadPath
from events.models.manifestation import Manifestation, ManifestationRelatedModel
from notifications.models.action import Action
from notifications.models.notification import Notification


class DocumentComplementaire(ManifestationRelatedModel):
    """ Document supplémentaire pour une manifestation """

    # Champs
    manifestation = models.ForeignKey(Manifestation, on_delete=models.CASCADE)
    desired_information = models.TextField("information requise")
    attached_document = models.FileField(upload_to=UploadPath('additional_data'), blank=True, null=True, verbose_name="Document", max_length=512)

    # Override
    def __str__(self):
        """ Renvoyer la représentation texte de l'objet """
        return self.desired_information

    # Action
    def log_data_provision(self):
        """ Enregistrer dans le journal la provision de données """
        action = "Documents complémentaires envoyés"
        Action.objects.log(self.manifestation.structure.organisateur, action, self.manifestation)

    def log_data_request(self, prefecture):
        """ Enregistrer dans le journal la demande de document """
        action = "Documents complémentaires requis"
        if not self.manifestation.crossed_cities.all() and (hasattr(self.manifestation, 'autorisationnm') or hasattr(self.manifestation, 'declarationnm')):
            # Instruction par la mairie
            commune = self.manifestation.departure_city
            recipients = commune.get_mairieagents()
        else:
            # Instruction par la préfecture
            recipients = prefecture.instructeurs.all()
        Action.objects.log(recipients, action, self.manifestation)

    def notify_data_provision(self, prefecture):
        """ Notifier la provision de document """
        subject = "Documents complémentaires envoyés"
        if not self.manifestation.crossed_cities.all() and (hasattr(self.manifestation, 'autorisationnm') or hasattr(self.manifestation, 'declarationnm')):
            # Instruction par la mairie
            commune = self.manifestation.departure_city
            recipients = list(commune.get_mairieagents()) + [prefecture]
        else:
            # Instruction par la préfecture
            recipients = list(prefecture.get_instructeurs()) + [prefecture]
        Notification.objects.notify_and_mail(recipients, subject, self.manifestation.structure, self.manifestation)

    def notify_data_request(self, prefecture):
        """ Notifier la demande de document """
        subject = "Documents complémentaires requis "
        recipients = self.manifestation.structure.organisateur
        Notification.objects.notify_and_mail(recipients, subject, prefecture, self.manifestation)

    def request_data(self):
        """ Consigner et notifier """
        prefecture = self.get_prefecture()
        self.notify_data_request(prefecture)
        self.log_data_request(prefecture)

    def provide_data(self):
        """ Consigner et notifier """
        prefecture = self.get_prefecture()
        self.notify_data_provision(prefecture)
        self.log_data_provision()

    # Getter
    def get_prefecture(self):
        """ Renvoyer la préfecture de la manifestation liée au document """
        return self.manifestation.get_concerned_prefecture()

    def exists(self):
        """ Renvoyer si le fichier de la pièce jointe existe sur le système de fichiers """
        field_populated = self.id and self.attached_document and self.attached_document.path
        file_exists = default_storage.exists(self.attached_document.name)
        return field_populated and file_exists

    # Meta
    class Meta:
        verbose_name = "document complémentaire"
        verbose_name_plural = "documents complémentaires"
        default_related_name = "documentscomplementaires"
        app_label = "events"
