# coding: utf-8

from datetime import date

from django.urls import reverse

from administration.factories import *
from agreements.tests.base import AvisTestsBase
from ..factories import *


class MairieAvisTests(AvisTestsBase):
    """ Tests des avis Mairie """

    # Configuration
    def setUp(self):
        super().setUp()

    # Tests
    def test_access_url(self):
        """ Tester les URLs d'raccès """
        avis = MairieAvisFactory.create(authorization=self.authorization)
        self.assertEqual(avis.get_absolute_url(), reverse('agreements:townhall_agreement_detail', kwargs={'pk': avis.pk}))

    def test_workflow(self):
        """ Tester la création des avis de Mairies """
        self.assertEqual(self.authorization.get_avis_count(), 1)  # Avis fédération par défaut
        self.authorization.concerned_cities.add(self.commune)  # Ajouter la commune aux villes consultées, crée automatiquement un avis Mairie
        self.assertEqual(self.authorization.get_avis_count(), 2)

    def test_life_cycle(self):
        """ Tester le cycle de validation d'un avis Fédération """
        # L'agent et l'instructeur sont créés après l'autorisation, il n'y aura pas de notification pour la création d'uatorisation
        MairieAgentFactory.create(commune=self.commune)  # créer un agent de la mairie
        InstructeurFactory(prefecture=self.prefecture)
        avis = MairieAvisFactory(commune=self.commune, authorization=self.authorization)

        # Vérifier que les agents (1 seul) ont reçu leur notification (pour l'avis Mairie)
        for mairie_agent in self.commune.get_mairieagents():
            self.assertEqual(mairie_agent.user.notifications.all().count(), 1)

        # Vérifier qu'une nouvelle notification n'est pas envoyée si l'avis est à nouveau sauvegardé
        avis.save()
        for mairie_agent in self.commune.get_mairieagents():
            self.assertEqual(mairie_agent.user.notifications.all().count(), 1)

        # Vérifier que l'avis est correctement rendu et à la date du jour
        avis.acknowledge()
        self.assertEqual(avis.reply_date, date.today())

        # Vérifier qu'une action "avis rendu" a bien été enregistrée pour les agents de la fédération
        for mairie_agent in self.commune.get_mairieagents():
            self.assertEqual(mairie_agent.user.actions.all().count(), 1)
            self.assertEqual(mairie_agent.user.actions.first().action, 'avis rendu')

        # Vérifier qu'aucune nouvelle notification n'existe pour les instructeurs
        for instructeur in self.prefecture.get_instructeurs():
            self.assertEqual(instructeur.user.notifications.all().count(), 0)  # pas pour la création d'autorisation mais pour le rendu d'avis

        # Vérifier que deux nouvelles notifications existent pour les instructeurs et les agents en mairie
        for agentmairie in self.commune.get_mairieagents():
            self.assertEqual(agentmairie.user.notifications.all().count(), 2)  # pas pour la création d'autorisation mais pour le rendu d'avis
            self.assertEqual(agentmairie.user.notifications.last().content_object,
                             self.commune)  # voir agreements.federation.acknowledge
