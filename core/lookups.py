# coding: utf-8
from ajax_select import register, LookupChannel
from django.db.models import Q
from unidecode import unidecode

from .models import User
from evenements.models import Manif


@register('user')
class UserLookup(LookupChannel):
    """ Lookup AJAX des utilisateurs """

    # Configuration
    model = User

    # Overrides
    def get_query(self, q, request):
        q = unidecode(q)
        try:
            dep = request.user.get_instance().get_departement()
            if request.user.is_superuser or not dep:
                filtre = Q()
            else:
                filtre = Q(default_instance__departement=dep)
        except:
            filtre = Q()
        list_q = q.split(' ')
        if len(list_q) != 1:
            # Recherche par nom et prénom ou l'inverse
            q = list_q[0]
            q1 = list_q[1]
            query = self.model.objects.filter(filtre).filter(
                Q(last_name__unaccent__istartswith=q) & Q(first_name__unaccent__istartswith=q1) |
                Q(first_name__unaccent__istartswith=q) & Q(last_name__unaccent__istartswith=q1)
            ).order_by('last_name', 'first_name')[:20]
        else:
            # Recherche générale sur nom, prénom et nom d'utilisateur
            query = self.model.objects.filter(filtre).filter(
                Q(last_name__unaccent__icontains=q) |
                Q(first_name__unaccent__icontains=q) |
                Q(username__unaccent__icontains=q)).order_by('last_name', 'first_name')[:20]
        return query

    def format_item_display(self, item):
        return u"<span class='tag'>%s %s</span>" % (item.last_name, item.first_name)

    def format_match(self, item):
        return "<span class='tag'>{firstname} {name} - {username}</span>".format(firstname=item.first_name, name=item.last_name, username=item.username)
