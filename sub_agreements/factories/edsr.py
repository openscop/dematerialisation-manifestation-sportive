# coding: utf-8
import factory

from administration.factories import EDSRFactory
from agreements.factories import EDSRAvisFactory
from sub_agreements.models import PreAvisEDSR


class PreAvisEDSRFactory(factory.django.DjangoModelFactory):
    """ Factory pour les préavis EDSR """

    avis = factory.SubFactory(EDSRAvisFactory)
    edsr = factory.SubFactory(EDSRFactory)

    class Meta:
        model = PreAvisEDSR
