# coding: utf-8
from django.conf import settings
from django.db.models.signals import post_save, m2m_changed
from django.dispatch import receiver

from administration.models import Commissariat
from agreements.models import DDSPAvis
from sub_agreements.models.commissariat import PreAvisCommissariat


if not settings.DISABLE_SIGNALS:

    @receiver(post_save, sender=PreAvisCommissariat)
    def notify_pre_avis_commissariat(created, instance, **kwargs):
        """ Notifier les agents de la création du préavis """
        if created:
            instance.notify_creation(agents=instance.get_agents(), content_object=instance.get_ddsp())


    @receiver(m2m_changed, sender=DDSPAvis.commissariats_concernes.through)
    def create_pre_avis_commissariats(instance, action, pk_set, **kwargs):
        """ Créer le préavis pour l'avis DDSP """
        if action == 'post_add':
            for pk in pk_set:
                PreAvisCommissariat.objects.get_or_create(avis=instance, commissariat=Commissariat.objects.get(pk=pk))
