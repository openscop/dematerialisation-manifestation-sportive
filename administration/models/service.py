# coding: utf-8
from django.db import models


from administrative_division.mixins.divisions import (OnePerDepartementMixin, ManyForDepartementMixin,
                                                      OnePerArrondissementMixin, ManyPerCommuneMixin,
                                                      OnePerDepartementQuerySetMixin, OnePerArrondissementQuerySetMixin,
                                                      ManyPerCommuneQuerySetMixin)


class GGD(OnePerDepartementMixin):
    """ Groupe départemental de gendarmerie """

    # Champs
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    objects = OnePerDepartementQuerySetMixin.as_manager()

    def __str__(self):
        return ' '.join(['GGD', self.get_departement().name])

    def get_ggdagents(self):  # Todo: éliminer les utilisations de cette méthode puis la supprimer
        """ Renvoyer les agents du GGD """
        return self.ggdagents.all()

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.ggdagents.all()]

    class Meta:
        verbose_name = "GGD"
        verbose_name_plural = "GGD"
        app_label = 'administration'
        # default_related_name = 'ggds'


class CG(OnePerDepartementMixin):
    """ Conseil départemental """

    # Champs
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    objects = OnePerDepartementQuerySetMixin.as_manager()

    def __str__(self):
        return ' '.join(['CD', self.get_departement().name])

    def get_cgsuperieurs(self):  # Todo: éliminer les utilisations de cette méthode puis la supprimer
        """ Renvoyer les agents n+1 du CG """
        return self.cgsuperieurs.all()

    def get_cgagents(self):  # Todo: éliminer les utilisations de cette méthode puis la supprimer
        """ Renvoyer les agents n du CG """
        return self.cgagents.all()

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.cgagents.all()] + [agent.user for agent in self.cgsuperieurs.all()]

    def get_service_users_superieur(self):
        """ Renvoyer les utilisateur superieur du service """
        return [agent.user for agent in self.cgsuperieurs.all()]

    class Meta:
        verbose_name = "CD"
        verbose_name_plural = "CD"
        app_label = 'administration'
        # default_related_name = 'cgs'


class DDSP(OnePerDepartementMixin):
    """ Direction départementale de sécurité publique """

    # Champs
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    objects = OnePerDepartementQuerySetMixin.as_manager()

    def __str__(self):
        return ' '.join(['DDSP', self.get_departement().name])

    def get_ddspagents(self):  # Todo: éliminer les utilisations de cette méthode puis la supprimer
        """ Renvoyer les agents DDSP """
        return self.ddspagents.all()

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.ddspagents.all()]

    class Meta:
        verbose_name = 'DDSP'
        verbose_name_plural = 'DDSP'
        app_label = 'administration'
        # default_related_name = 'ddsps'


class SDIS(OnePerDepartementMixin):
    """ Service départemental incendies et secourisme """

    # Champs
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    objects = OnePerDepartementQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return ' '.join(['SDIS', self.get_departement().name])

    # Getter
    def get_sdisagents(self):  # Todo: éliminer les utilisations de cette méthode puis la supprimer
        """ Renvoyer les agents SDIS """
        return self.sdisagents.all()

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.sdisagents.all()]

    class Meta:
        verbose_name = 'SDIS'
        verbose_name_plural = 'SDIS'
        app_label = 'administration'
        # default_related_name = 'sdiss'


class CODIS(OnePerDepartementMixin):
    """ SDIS : Centre d'opérations départemental incendies et secourisme """

    # Champs
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    objects = OnePerDepartementQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return ' '.join(['CODIS', self.get_departement().name])

    # Getter
    def get_codisagents(self):  # Todo: éliminer les utilisations de cette méthode puis la supprimer
        """ Renvoyer les agents codis """
        return self.codisagents.all()

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.codisagents.all()]

    class Meta:
        verbose_name = 'SDIS - CODIS'
        verbose_name_plural = 'SDIS - CODIS'
        app_label = 'administration'
        # default_related_name = 'codiss'


class EDSR(OnePerDepartementMixin):
    """ Escadron départemental de sécurité routière """

    # Champs
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    objects = OnePerDepartementQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return ' '.join(['EDSR', self.get_departement().name])

    # Getter
    def get_edsr_agents(self):  # Todo: éliminer les utilisations de cette méthode puis la supprimer
        """ Renvoyer les agents EDSR """
        return self.edsragents.all()

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.edsragents.all()] + [agent.user for agent in self.edsragentslocaux.all()]

    class Meta:
        verbose_name = 'EDSR'
        verbose_name_plural = 'EDSR'
        app_label = 'administration'
        # default_related_name = 'edsrs'


class Prefecture(OnePerArrondissementMixin):
    """ Préfécture (et sous-préfectures) """

    # Champs
    sous_prefecture = models.BooleanField("Sous-préfecture", default=False)
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    objects = OnePerArrondissementQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        typename = "sous-préfecture" if self.sous_prefecture else "préfecture"
        return ' '.join([typename.capitalize(), 'de', self.arrondissement.name])

    # Getter
    def get_instructeurs(self):
        """
        Renvoyer les instructeurs actifs pour la (sous-)préfecture
        
        Note : quand le mode d'instruction d'une instance/département est
        défini à 'Départemental', tous les instructeurs de toutes les
        sous-préfectures/arrondissements du département sont toujours concernés
        tous ensemble par les événements du département.
        """
        from administration.models import Instructeur
        return Instructeur.objects.by_prefecture(self)

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.instructeurs.all()]

    # Meta
    class Meta:
        verbose_name = 'Préfecture'
        verbose_name_plural = 'Préfectures'
        app_label = 'administration'
        abstract = False
        # default_related_name = 'prefectures'


class CGDQuerySet(models.QuerySet):
    """ Manager/Queryset """

    # Overrides
    def get_by_natural_key(self, code):
        return self.get(commune__code=code)


class CGD(models.Model):
    """ Compagnie de gendarmerie départementale """

    # Champs
    commune = models.OneToOneField('administrative_division.commune', verbose_name="Commune", on_delete=models.CASCADE, null=True)
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    objects = CGDQuerySet.as_manager()

    # Overrides
    def __str__(self):
        return 'CGD {0}'.format(self.commune.name)

    def natural_key(self):
        return self.commune.natural_key()

    # Getter
    def get_arrondissement(self):
        """ Renvoie l'arrondissement de l'objet """
        return self.commune.arrondissement
    get_arrondissement.short_description = "Arrondissement"

    def get_departement(self):
        """
        Renvoie le département de la commune de l'objet

        :rtype: administrative_division.models.departement.Departement
        """
        return self.commune.arrondissement.departement
    get_departement.short_description = "Département"

    def get_instance(self):
        """
        Renvoie l'instance du département de la commune de l'objet

        :rtype: core.models.instance.Instance
        """
        return self.get_departement().get_instance()
    get_instance.short_description = "Instance"

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.ggdagentslocaux.all()]

    class Meta:
        verbose_name = 'CGD'
        verbose_name_plural = 'CGD'
        app_label = 'administration'
        # default_related_name = 'cgds'


class Brigade(ManyPerCommuneMixin):
    """ Brigade de gendarmerie """

    # Constantes
    KIND_CHOICES = (('cob', 'Communauté de brigades (COB)'), ('bta', 'Brigade territoriale autonome (BTA)'),
                    ('bn', 'Brigade nautique (BN)'), ('bmo', 'Brigade motorisée (BMO)'),
                    ('pgm', 'Peloton de gendarmerie de montagne (PGM)'),
                    ('pghm', 'Peloton de gendarmerie de haute montagne (PGHM)'))

    # Champs
    kind = models.CharField('type', max_length=12, choices=KIND_CHOICES)
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    cgd = models.ForeignKey('administration.cgd', verbose_name='CGD', on_delete=models.CASCADE)
    objects = ManyPerCommuneQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return '{0} - {1}'.format(self.kind.upper(), self.commune.name)

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.brigadeagents.all()]

    # Méta
    class Meta:
        verbose_name = 'EDSR - Brigade'
        verbose_name_plural = 'EDSR - Brigades'
        app_label = 'administration'
        default_related_name = 'brigades'


class Compagnie(models.Model):
    """ Compagnie SDIS (Incendie et secourisme) """

    # Champs
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    sdis = models.ForeignKey('administration.sdis', verbose_name='SDIS', on_delete=models.CASCADE)
    number = models.CharField("Numéro de compagnie/groupement", max_length=12)

    # Overrides
    def __str__(self):
        return 'Compagnie/groupement {0}'.format(self.number)

    def get_instance(self):
        return self.sdis.get_instance()

    def natural_key(self):
        return self.sdis.departement.name, self.number

    # Getter
    def get_compagnieagentslocaux(self):
        """ Renvoyer les agents locaux """
        return self.compagnieagentslocaux.all()

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.compagnieagentslocaux.all()]

    # MEta
    class Meta:
        verbose_name = 'SDIS - Compagnie/Groupement'
        verbose_name_plural = 'SDIS - Compagnies/Groupements'
        ordering = ['number']
        app_label = 'administration'
        default_related_name = 'compagnies'


class CIS(ManyPerCommuneMixin):
    """ Caserne / Service feu """

    # Champs
    name = models.CharField("Nom", max_length=255, blank=True)
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    compagnie = models.ForeignKey('administration.compagnie', verbose_name='compagnie/groupement', on_delete=models.CASCADE)
    objects = ManyPerCommuneQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return ' '.join(filter(None, ['CIS', self.commune.name.capitalize(), self.name]))

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.cisagents.all()]

    # Méta
    class Meta:
        verbose_name = 'SDIS - CIS'
        verbose_name_plural = 'SDIS - CIS'
        app_label = 'administration'
        default_related_name = 'ciss'


class Commissariat(ManyPerCommuneMixin):
    """ Commissariat de police """

    # Champs
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    objects = ManyPerCommuneQuerySetMixin.as_manager()

    # Overrides
    def __str__(self):
        return 'Commissariat {0}'.format(self.commune.name)

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.commissariatagentslocaux.all()]

    # Meta
    class Meta:
        verbose_name = 'DDSP - Commissariat'
        verbose_name_plural = 'DDSP - Commissariats'
        app_label = 'administration'
        default_related_name = 'commissariats'


class Service(ManyForDepartementMixin):
    """ Service 'simple' """

    # Champs
    name = models.CharField("Nom", max_length=255, blank=False, unique=True)
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)

    # Overrides
    def __str__(self):
        return self.name

    # Getter
    def get_serviceagents(self):
        """ Renvoyer les agents """
        return self.serviceagents.all()

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.serviceagents.all()]

    # Meta
    class Meta:
        verbose_name = "Service simple"
        verbose_name_plural = "Services simples"
        app_label = 'administration'
        default_related_name = 'services'


class CGServiceQuerySet(models.QuerySet):
    """ Queryset """

    # Overrides
    def get_by_natural_key(self, pk, name):
        return self.get(id=pk, cg__departement__name=name)


class CGService(models.Model):
    """ Service de conseil départemental """

    # Constantes
    SERVICE_TYPE_CHOICES = (
        ('STD', 'Service technique départemental'),
        ('DTM', 'Direction des transports et de la mobilité'),
    )
    # Champs
    name = models.CharField("Nom", max_length=255)
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    cg = models.ForeignKey('administration.cg', verbose_name='CD', on_delete=models.CASCADE)
    service_type = models.CharField("type de service", max_length=3, choices=SERVICE_TYPE_CHOICES)
    objects = CGServiceQuerySet.as_manager()

    # Overrides
    def __str__(self):
        return '{0} - {1}'.format(self.service_type, self.name)

    def natural_key(self):
        return (self.pk,) + self.cg.natural_key()

    def get_instance(self):
        return self.cg.get_instance()

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.cgserviceagentslocaux.all()]

    # Meta
    class Meta:
        verbose_name = 'CD - Service'
        verbose_name_plural = 'CD - Services'
        app_label = 'administration'
        default_related_name = 'cgservices'
