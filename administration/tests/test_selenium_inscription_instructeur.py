from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from django.core import mail
from django.test import tag
import time

from administrative_division.factories import DepartementFactory, ArrondissementFactory
from core.models.instance import Instance
from core.models import User


class InscriptionInstructeur(StaticLiveServerTestCase):
    """
    Test de l'inscription d'un agent de préfecture
    """
    DELAY = 0.4

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
        """
        print()
        print('============ Inscription agent (Sel) =============')
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(600, 800)

        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__workflow_ggd=Instance.WF_GGD_SUBEDSR,
                                            instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        cls.arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        cls.prefecture = cls.arrondissement.prefecture

        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """
        cls.selenium.quit()
        super().tearDownClass()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.find_element_by_css_selector("a").click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    @tag('selenium')
    def test_inscription_instructeur(self):
        """
        Test complet de l'inscription d'un agent de préfecture
        """

        print('**** test 1 remplissage formulaire d\'inscription agent ****')

        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/agent'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Création d\'un compte Agent', self.selenium.page_source)

        prenom = self.selenium.find_element_by_id('id_first_name')
        prenom.send_keys('Jean')
        time.sleep(self.DELAY)

        nom = self.selenium.find_element_by_id('id_last_name')
        nom.send_keys('Dupont')
        time.sleep(self.DELAY)

        id_user = self.selenium.find_element_by_id('id_username')
        id_user.send_keys('jandup')
        time.sleep(self.DELAY)

        email = self.selenium.find_element_by_id('id_email')
        email.send_keys('jandup@manifestationsportive.fr')
        time.sleep(self.DELAY)

        pass1 = self.selenium.find_element_by_id('id_password1')
        pass1.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)

        pass2 = self.selenium.find_element_by_id('id_password2')
        pass2.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)

        self.chosen_select('id_instance_chosen', 'instance de test (42)')
        time.sleep(self.DELAY*2)

        self.chosen_select('id_service_chosen', 'Préfecture')
        time.sleep(self.DELAY*2)

        self.chosen_select('id_prefecture_chosen', 'Sous-préfecture de Montbrison')
        time.sleep(self.DELAY)

        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        print('**** test 2 vérification ****')

        self.assertIn('Vérifiez votre adresse email', self.selenium.page_source)
        self.assertIn('E-mail de confirmation envoyé à jandup@manifestationsportive.fr', self.selenium.page_source)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, '[example.com] Confirmer l\'adresse email')
        self.assertEqual(mail.outbox[0].to, ['jandup@manifestationsportive.fr'])
        url = mail.outbox[0].body.split()[-1]
        self.selenium.get(url)

        print('**** test 3 confirmation ****')

        self.assertIn('Confirmer une adresse email', self.selenium.page_source)
        self.assertIn('>jandup@manifestationsportive.fr</a> est bien une adresse email de jandup.', self.selenium.page_source)

        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY)

        self.assertIn('Adresse email confirmée', self.selenium.page_source)
        self.assertIn('Vous avez confirmé jandup@manifestationsportive.fr.', self.selenium.page_source)

        print('**** test 4 echec inscription ****')

        print('>>> email déjà utilisé')
        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/agent'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Création d\'un compte Agent', self.selenium.page_source)

        id_user = self.selenium.find_element_by_id('id_username')
        id_user.send_keys('jannotdup')
        time.sleep(self.DELAY)

        email = self.selenium.find_element_by_id('id_email')
        email.send_keys('jandup@manifestationsportive.fr')
        time.sleep(self.DELAY)

        pass1 = self.selenium.find_element_by_id('id_password1')
        pass1.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)
        self.assertIn('Un autre utilisateur utilise déjà cette adresse e-mail', self.selenium.page_source)

        print('>>> username déjà utilisé')
        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/organisateur'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Structure organisatrice', self.selenium.page_source)

        id_user = self.selenium.find_element_by_id('id_username')
        id_user.send_keys('jandup')
        time.sleep(self.DELAY)

        prenom = self.selenium.find_element_by_id('id_first_name')
        prenom.send_keys('Jeannot')
        time.sleep(self.DELAY)
        self.assertIn('Un utilisateur avec ce nom existe déjà', self.selenium.page_source)

        print('**** test 5 echec connexion ****')

        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element_by_name("login")
        pseudo_input.send_keys('jandup')
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.assertIn('Compte inactif', self.selenium.page_source)

        print('**** test 6 connexion réussie ****')

        user = User.objects.get()
        user.is_active = True
        user.save()

        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element_by_name("login")
        pseudo_input.send_keys('jandup')
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.assertIn('Tableau de bord instructeur', self.selenium.page_source)
        self.assertIn('Connexion avec jandup réussie.', self.selenium.page_source)
