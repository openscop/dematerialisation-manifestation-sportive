from django.core.management.base import BaseCommand
import csv
from evaluation_incidence.models import N2kSite, RnrZone


class Command(BaseCommand):
    """
    Importation des site natura et rnr
    necessite d'avoir un csv sans en tete avec un delimite , et en colones:
    fiche, nom, type(seulement pour natura)
    """

    help = 'Ajout des sites protégés (les anciens ne seront pas touchés)'

    def handle(self, *args, **options):

        chemin_n2k_sic = "evaluation_incidence/fixtures/SIC.csv"
        chemin_n2k_zps = "evaluation_incidence/fixtures/ZPS.csv"
        chemin_n2k_rnr = "evaluation_incidence/fixtures/RNR.csv"


        # on enregistre les SIC en verifiant que le code n'a pas été deja rentré
        with open(chemin_n2k_sic, newline='') as csvfichier:
            tableaucsv = csv.reader(csvfichier, delimiter=',')
            for row in tableaucsv:
                ancien = N2kSite.objects.filter(index=row[0])
                if not ancien:
                    nouveau = N2kSite(nom=row[1], site_type="s", index=row[0])
                    nouveau.save()
        print("SIP fini")
        # On rentre les ZPS
        with open(chemin_n2k_zps, newline='') as csvfichier:
            tableaucsv = csv.reader(csvfichier, delimiter=',')
            for row in tableaucsv:
                ancien = N2kSite.objects.filter(index=row[0])
                if not ancien:
                    ancien_sic = N2kSite.objects.filter(nom=row[1])
                    # ICI on traite le cas ou un ZPS a le même nom qu'un SIC on met donc un suffixe au deux enregistrements
                    if ancien_sic:
                        ancien_sic = ancien_sic.first()
                        if ancien_sic.site_type == "s":
                            ancien_sic.nom = ancien_sic.nom + " (SIC)"
                            ancien_sic.save()
                            nouveau = N2kSite(nom=row[1] + " (ZPS)", site_type="z", index=row[0])
                            nouveau.save()
                        else:
                            nouveau = N2kSite(nom=row[1], site_type="z", index=row[0])
                            nouveau.save()
                    else:
                        nouveau = N2kSite(nom=row[1], site_type="z", index=row[0])
                        nouveau.save()
        print("ZPS fini")
        # Ici on traite les rnr en n'enregistrant pas ceux dejà rentré
        with open(chemin_n2k_rnr, newline='') as csvfichier:
            tableaucsv = csv.reader(csvfichier, delimiter=',')
            for row in tableaucsv:
                ancien = RnrZone.objects.filter(code=row[0])
                if not ancien:
                    nouveau = RnrZone(nom=row[1], code=row[0])
                    nouveau.save()
        print("RNR fini")
        print("Tout fini")





