# coding: utf-8
from django.contrib import admin, messages
from django.utils.html import format_html
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from administrative_division.models import Departement, Arrondissement, Commune
from administration.models.service import (Prefecture, CGService, Service, EDSR, SDIS, DDSP, CG, GGD,
                                           CODIS, Brigade, CGD, Compagnie, Commissariat, CIS)
from core.util.admin import RelationOnlyFieldListFilter


class GetAgentsLinkMixin:
    def lien_agents(self, obj):
        return format_html(" , ".join(['<a href="/admin/core/user/' + str(user.pk) + '/change/">' + user.get_full_name() + '</a>' for user in obj.get_service_users()]))
    lien_agents.short_description = "Agents du service"

class DepartementMixin(admin.ModelAdmin):

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement() and not request.user.is_superuser:
            form.base_fields['departement'].queryset = Departement.objects.filter(id=request.user.get_departement().id)
            form.base_fields['departement'].widget.can_change_related = False
        return form


@admin.register(Prefecture)
class PrefectureAdmin(ExportActionModelAdmin, RelatedFieldAdmin, GetAgentsLinkMixin):
    """ Configuration admin """
    list_display = ['pk', 'arrondissement', 'get_departement', 'lien_agents']
    search_fields = ['email', 'arrondissement__departement__name__unaccent']
    list_filter = [('arrondissement__departement', RelationOnlyFieldListFilter)]
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(arrondissement__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement() and not request.user.is_superuser:
            form.base_fields['arrondissement'].queryset = Arrondissement.objects.filter(departement=request.user.get_departement())
            form.base_fields['arrondissement'].widget.can_change_related = False
            form.base_fields['arrondissement'].widget.can_add_related = False
        return form


@admin.register(Service)
class ServiceAdmin(ExportActionModelAdmin, GetAgentsLinkMixin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'get_departements', 'lien_agents']
    search_fields = ['name__unaccent']
    list_filter = [('departements', RelationOnlyFieldListFilter)]
    actions = ExportActionModelAdmin.actions + ['ajouter_mon_departement']
    list_per_page = 50
    list_display_links = ['pk', 'name']

    # Overrides
    def get_fieldsets(self, request, obj=None):
        if request.user.get_departement() and not request.user.is_superuser:
            return [(None, {'fields': ['name', 'email']}), ]
        return [(None, {'fields': ['departements', 'name', 'email']}), ]

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if request.user.get_departement():
            dept = request.user.get_departement()
            obj.departements.add(dept)

    def ajouter_mon_departement(self, request, queryset):
        if request.user.get_departement():
            dept = request.user.get_departement()
            for service in queryset:
                service.departements.add(dept)
            if len(queryset) > 1:
                self.message_user(request, "Département ajouté aux services")
            else:
                self.message_user(request, "Département ajouté à ce service")
        else:
            self.message_user(request, "Impossible d'ajouter de département, instance nationale !", level=messages.ERROR)
    ajouter_mon_departement.short_description = "Ajouter ce service dans mon département"



@admin.register(DDSP)
class DepartementAndEmailAdmin(DepartementMixin, ExportActionModelAdmin, GetAgentsLinkMixin):
    """ Configuration admin """
    list_display = ['pk', 'get_departement', 'email', 'lien_agents']
    list_filter = [('departement', RelationOnlyFieldListFilter)]
    list_per_page = 50

    def get_model_perms(self, request):
        if request.user.get_instance().active_ddsp:
            return super().get_model_perms(request)
        else:
            return {}

@admin.register(CG)
class DepartementAndEmailAdmin(DepartementMixin, ExportActionModelAdmin, GetAgentsLinkMixin):
    """ Configuration admin """
    list_display = ['pk', 'get_departement', 'email', 'lien_agents']
    list_filter = [('departement', RelationOnlyFieldListFilter)]
    list_per_page = 50

    def get_model_perms(self, request):
        if request.user.get_instance().active_cg:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(SDIS, CODIS)
class DepartementOnlyAdmin(DepartementMixin, ExportActionModelAdmin, GetAgentsLinkMixin):
    """ Configuration admin """
    list_display = ['pk', 'get_departement', 'lien_agents']
    list_filter = [('departement', RelationOnlyFieldListFilter)]
    list_per_page = 50

    def get_model_perms(self, request):
        if request.user.get_instance().active_sdis:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(GGD, EDSR)
class DepartementOnlyAdmin(DepartementMixin, ExportActionModelAdmin, GetAgentsLinkMixin):
    """ Configuration admin """
    list_display = ['pk', 'get_departement', 'lien_agents']
    list_filter = [('departement', RelationOnlyFieldListFilter)]
    list_per_page = 50

    def get_model_perms(self, request):
        if request.user.get_instance().active_ggd:
            return super().get_model_perms(request)
        else:
            return {}



@admin.register(Commissariat)
class CommuneOnlyAdmin(ExportActionModelAdmin, GetAgentsLinkMixin):
    """ Configuration admin """
    list_display = ['pk', 'get_commune', 'get_departement', 'lien_agents']
    list_filter = [('commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(commune__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement() and not request.user.is_superuser:
            form.base_fields['commune'].queryset = Commune.objects.filter(arrondissement__departement=request.user.get_departement())
            form.base_fields['commune'].widget.can_change_related = False
            form.base_fields['commune'].widget.can_add_related = False
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_ddsp:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(CIS)
class CISAdmin(ExportActionModelAdmin, RelatedFieldAdmin, GetAgentsLinkMixin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'get_commune', 'get_departement', 'compagnie', 'lien_agents']
    list_filter = ['compagnie', ('compagnie__sdis__departement', RelationOnlyFieldListFilter)]
    search_fields = ['name__unaccent']
    list_per_page = 50
    list_display_links = ['pk', 'name']

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(compagnie__sdis__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement() and not request.user.is_superuser:
            form.base_fields['commune'].queryset = Commune.objects.filter(arrondissement__departement=request.user.get_departement())
            form.base_fields['commune'].widget.can_change_related = False
            form.base_fields['commune'].widget.can_add_related = False
            form.base_fields['compagnie'].queryset = Compagnie.objects.filter(sdis__departement=request.user.get_departement())
            form.base_fields['compagnie'].widget.can_change_related = False
            form.base_fields['compagnie'].widget.can_add_related = False
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_sdis:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(Brigade)
class BrigadeAdmin(ExportActionModelAdmin, RelatedFieldAdmin, GetAgentsLinkMixin):
    """ Configuration admin """
    list_display = ['pk', 'get_commune', 'get_departement', 'kind', 'cgd', 'lien_agents']
    list_filter = ['kind', ('cgd__commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['commune__name__unaccent']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(cgd__commune__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement() and not request.user.is_superuser:
            form.base_fields['commune'].queryset = Commune.objects.filter(arrondissement__departement=request.user.get_departement())
            form.base_fields['commune'].widget.can_change_related = False
            form.base_fields['commune'].widget.can_add_related = False
            form.base_fields['cgd'].queryset = CGD.objects.filter(commune__arrondissement__departement=request.user.get_departement())
            form.base_fields['cgd'].widget.can_change_related = False
            form.base_fields['cgd'].widget.can_add_related = False
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_ggd:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(CGD)
class CGDAdmin(ExportActionModelAdmin, RelatedFieldAdmin, GetAgentsLinkMixin):
    """ Configuration admin """
    list_display = ['pk','commune', 'get_arrondissement', 'get_departement', 'lien_agents']
    list_filter = [('commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['commune__name__unaccent', 'commune__arrondissement__departement__name__unaccent']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(commune__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement() and not request.user.is_superuser:
            form.base_fields['commune'].queryset = Commune.objects.filter(arrondissement__departement=request.user.get_departement())
            form.base_fields['commune'].widget.can_change_related = False
            form.base_fields['commune'].widget.can_add_related = False
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_ggd:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(Compagnie)
class CompagnieAdmin(ExportActionModelAdmin, RelatedFieldAdmin, GetAgentsLinkMixin):
    """ Configuration admin """
    list_display = ['pk', 'sdis', 'number', 'lien_agents']
    list_filter = [('sdis__departement', RelationOnlyFieldListFilter)]
    search_fields = ['number']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(sdis__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement() and not request.user.is_superuser:
            form.base_fields['sdis'].queryset = SDIS.objects.filter(departement=request.user.get_departement())
            form.base_fields['sdis'].widget.can_change_related = False
            form.base_fields['sdis'].widget.can_add_related = False
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_sdis:
            return super().get_model_perms(request)
        else:
            return {}

@admin.register(CGService)
class CGServiceAdmin(ExportActionModelAdmin, RelatedFieldAdmin, GetAgentsLinkMixin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'cg', 'service_type', 'cg__departement', 'lien_agents']
    list_filter = ['service_type', ('cg__departement', RelationOnlyFieldListFilter)]
    search_fields = ['name']
    list_per_page = 50
    list_display_links = ['pk', 'name']

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(cg__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement() and not request.user.is_superuser:
            form.base_fields['cg'].queryset = CG.objects.filter(departement=request.user.get_departement())
            form.base_fields['cg'].widget.can_change_related = False
            form.base_fields['cg'].widget.can_add_related = False
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_cg:
            return super().get_model_perms(request)
        else:
            return {}
