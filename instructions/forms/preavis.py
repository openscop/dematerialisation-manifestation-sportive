# coding: utf-8
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout
from django import forms

from core.forms.base import GenericForm
from ..models import PreAvis


class PreAvisForm(GenericForm):
    """ Formulaire des préavis """

    prescriptions = forms.CharField(label="Contenu", required=False,
                                    widget=forms.Textarea(attrs={'placeholder': 'Précisez ici vos commentaires, prescriptions, recommandations...'}))

    # Overrides
    def __init__(self, *args, **kwargs):
        super(PreAvisForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'favorable',
            'prescriptions',
            FormActions(
                Submit('save', "Rendre le pré-avis")
            )
        )

    class Meta:
        model = PreAvis
        fields = ('favorable', 'prescriptions')


class NotifyCISForm(GenericForm):
    """ Notification CIS """

    cis_concernes = forms.ModelMultipleChoiceField(queryset=None, label="CIS concernés")

    def __init__(self, *args, **kwargs):
        super(NotifyCISForm, self).__init__(*args, **kwargs)
        self.fields['cis_concernes'].queryset = self.instance.destination_object.ciss.all()
        self.fields['cis_concernes'].widget.attrs = {'data-placeholder': "Choisissez un ou plusieurs CIS concernés"}
        self.helper.layout = Layout(
            'cis_concernes',
            FormActions(Submit('save', "Informer les CIS concernés"))
        )

    class Meta:
        model = PreAvis
        fields = ['cis_concernes']


class NotifyBrigadesForm(GenericForm):
    """ Notification brigade """

    brigades_concernees = forms.ModelMultipleChoiceField(queryset=None, label="brigades concernées")

    def __init__(self, *args, **kwargs):
        super(NotifyBrigadesForm, self).__init__(*args, **kwargs)
        self.fields['brigades_concernees'].queryset = self.instance.destination_object.brigades.all()
        self.fields['brigades_concernees'].widget.attrs = {'data-placeholder': "Choisissez une ou plusieurs brigades concernées"}
        self.helper.layout = Layout(
            'brigades_concernees',
            FormActions(
                Submit('save', "Informer les brigades concernées")
            )
        )

    class Meta:
        model = PreAvis
        fields = ['brigades_concernees']
