# coding: utf-8
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from administrative_division.mixins.divisions import ManyPerDepartementQuerySetMixin, DepartementableMixin


class Association1ersSecoursQuerySet(ManyPerDepartementQuerySetMixin):
    """ Queryset """
    pass


class Association1ersSecours(DepartementableMixin):
    """ Association Premiers secours """

    # Champs
    name = models.CharField('nom', max_length=200)
    website = models.URLField('site web', max_length=200, blank=True)
    email = models.EmailField('e-mail', max_length=200)
    address = GenericRelation('contacts.adresse', verbose_name="adresses")
    contact = GenericRelation('contacts.contact', verbose_name="contacts")
    objects = Association1ersSecoursQuerySet.as_manager()

    # Overrides
    def __str__(self):
        return self.name

    def natural_key(self):
        return self.address, self.name

    # Méta
    class Meta:
        verbose_name = "association premiers secours"
        verbose_name_plural = "associations premiers secours"
        app_label = 'emergencies'
