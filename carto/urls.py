# coding: utf-8
from django.urls import path, re_path

from carto.views.openrunner import route_create, route_view, route_view_instructor
from sports.views.ajax import ActiviteAJAXView, get_discipline_id_from_activite_name, get_activite_id_from_name


app_name = 'carto'
urlpatterns = [

    # Redirections
    re_path('route/create/(?P<role>.*)/', route_create, name='route_create'),
    re_path('route/view/(?P<route_ids>.+)/(?P<role>.*)/', route_view, name='route_view'),
    re_path('route/instructor/(?P<route_ids>.+)/(?P<role>.*)/', route_view_instructor, name='route_instructor'),

]
