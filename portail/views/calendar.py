# coding: utf-8
import json
from django.views import View
from django.db.models import Subquery
from django.views.generic import ListView
from django.shortcuts import redirect
from django.http import HttpResponse
from events.forms import *
from events.models import *
from sports.models import Activite
from evenements.models import Manif
from portail.forms import CalendrierForm


class Calendar(ListView):
    """ Calendrier des manifestations """

    # Configuration
    model = Manifestation
    template_name = 'portail/calendar.html'

    # Overrides
    def get_queryset(self):
        now = timezone.now()
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement()
        activite = self.kwargs.get('activite')
        filtre_activite = Q(activite=activite) if activite else Q()
        param_dep = self.request.GET.get('dep', None)
        param_com = self.request.GET.get('com', '0')
        if param_dep and param_dep != "0":
            if not param_dep.isdecimal():
                departement = None
            else:
                departement = Departement.objects.filter(pk=param_dep)
                departement = departement.get() if departement else None
        elif param_dep is not None:
            departement = None
        resultat = Manif.objects.filter(date_debut__gt=now, prive=False, cache=False).filter(
            Q(instruction__id__isnull=False) | Q(instance_id__isnull=True)
        ).exclude(instruction__etat__in=['interdite', 'annulée']).filter(filtre_activite)
        # Afficher les manifs du département, ou toute sur le domaine master
        if departement is not None:
            resultat = resultat.filter(ville_depart__arrondissement__departement=departement)
        if param_com != '0':
            resultat = resultat.filter(ville_depart__pk=param_com)

        return resultat.order_by('date_debut')

    def get_context_data(self, **kwargs):
        now = timezone.now()
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement()
        context = super(Calendar, self).get_context_data(**kwargs)
        param_dep = self.request.GET.get('dep', None)
        param_com = self.request.GET.get('com', '0')
        liste_activite = Activite.objects.filter(
            Q(manifs__instruction__id__isnull=False) | Q(manifs__instance_id__isnull=True),
            ~Q(manifs__instruction__etat__in=['interdite', 'annulée']),
            manifs__date_debut__gt=now, manifs__cache=False, manifs__prive=False)
        if param_dep and param_dep != "0":
            if not param_dep.isdecimal():
                departement = None
            else:
                departement = Departement.objects.filter(pk=self.request.GET['dep'])
                departement = departement.get() if departement else None
                if departement:
                    liste_activite = Activite.objects.filter(
                        Q(manifs__instruction__id__isnull=False) | Q(manifs__instance_id__isnull=True),
                        ~Q(manifs__instruction__etat__in=['interdite', 'annulée']),
                        manifs__date_debut__gt=now, manifs__cache=False, manifs__prive=False,
                        manifs__ville_depart__arrondissement__departement=departement)
        elif param_dep is not None:
            departement = "0"

        context['dep_actuel'] = departement
        context['com_actuel'] = int(param_com)
        context['activities'] = liste_activite.distinct('name').order_by('name')
        context['current_activite'] = int(self.kwargs.get('activite')) if self.kwargs.get('activite') else None
        context['dep'] = Departement.objects.filter(pk__in=Subquery(
            Manif.objects.filter(date_debut__gte=timezone.now()).values('ville_depart__arrondissement__departement__pk')))
        context['comm'] = Commune.objects.filter(arrondissement__departement=departement)
        context['form'] = CalendrierForm()
        return context

class CalendarAjaxView(View):
    """ Renvoyer le pk du département selectionné sur la map """

    def get(self, request):
        departement = self.request.GET.get('dep', None)
        if not departement:
            return redirect('home_page')
        else:
            param_dep = Departement.objects.filter(name__startswith=departement)
            dep_manifestation = Departement.objects.filter(
                        pk__in=Subquery(Manif.objects.filter(date_debut__gte=timezone.now()).values('ville_depart__arrondissement__departement__pk')))
            param_dep_pk = param_dep.get().pk if param_dep else ""
            # Test si le departement selectionné a des manifestations -> affiche une modale dans le template
            if dep_manifestation.filter(pk=param_dep_pk).exists():
                return HttpResponse(json.dumps({"pk": param_dep_pk}), content_type="application/json")
            else:
                return HttpResponse(json.dumps({"pk": 0}), content_type="application/json")
