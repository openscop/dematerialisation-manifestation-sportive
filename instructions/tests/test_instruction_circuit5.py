from django.test import TestCase
from django.contrib.auth.hashers import make_password as make
import re

from post_office.models import EmailTemplate, Email

from core.models import Instance, User
from evenements.factories import DcnmFactory, AvtmFactory
from instructions.factories import InstructionFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from organisateurs.factories import StructureFactory
from core.factories import UserFactory
from administration.factories import InstructeurFactory, MairieAgentFactory
from sports.factories import ActiviteFactory
from messagerie.models import Message


class InstructionCircuit5(TestCase):
    """
    Test du circuit d'instruction par arrondissement (circuit 5)
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========= Circuit 5 (Clt) ==========')
        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_EDSR,
                                        instance__instruction_mode=Instance.IM_ARRONDISSEMENT_COMPLEXE_5)
        arrond_1 = ArrondissementFactory.create(name='Montbrison', code='421', departement=dep, prefecture__sous_prefecture=False)
        arrond_2 = ArrondissementFactory.create(name='Roanne', code='422', departement=dep)
        arrond_3 = ArrondissementFactory.create(name='ST Etienne', code='420', departement=dep)
        cls.instance = dep.instance
        cls.prefecture1 = arrond_1.prefecture
        cls.prefecture2 = arrond_2.prefecture
        cls.prefecture3 = arrond_3.prefecture
        cls.commune1 = CommuneFactory(name='Bard', arrondissement=arrond_1)
        cls.autrecommune1 = CommuneFactory(name='Roche', arrondissement=arrond_1)
        cls.commune2 = CommuneFactory(name='Bully', arrondissement=arrond_2)
        cls.autrecommune2 = CommuneFactory(name='Régny', arrondissement=arrond_2)
        cls.commune3 = CommuneFactory(name='Villars', arrondissement=arrond_3)
        structure1 = StructureFactory(commune=cls.commune1)
        structure2 = StructureFactory(commune=cls.commune2)
        structure3 = StructureFactory(commune=cls.commune3)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        cls.instructeur1 = UserFactory.create(username='instructeur1', password=make(123),
                                              default_instance=dep.instance, email='instructeur1@test.fr')
        InstructeurFactory.create(user=cls.instructeur1, prefecture=cls.prefecture1)
        cls.agent_mairie1 = UserFactory.create(username='agent_mairie1', password=make(123),
                                               default_instance=dep.instance, email='agentmairie1@test.fr')
        MairieAgentFactory.create(user=cls.agent_mairie1, commune=cls.commune1)
        cls.instructeur2 = UserFactory.create(username='instructeur2', password=make(123),
                                              default_instance=dep.instance, email='instructeur1@test.fr')
        InstructeurFactory.create(user=cls.instructeur2, prefecture=cls.prefecture2)
        cls.agent_mairie2 = UserFactory.create(username='agent_mairie2', password=make(123),
                                               default_instance=dep.instance, email='agentmairie2@test.fr')
        MairieAgentFactory.create(user=cls.agent_mairie2, commune=cls.commune2)
        for user in User.objects.all():
            user.optionuser.notification_mail = True
            user.optionuser.action_mail = True
            user.optionuser.save()

        EmailTemplate.objects.create(name='new_msg')

        # Création des événements
        # Manif NM sur 1 commmune, arrondissement de préfecture
        cls.manifestation1 = DcnmFactory.create(ville_depart=cls.commune1, structure=structure1, activite=activ,
                                                nom='Manifestation_Test_1')
        # Manif NM sur 1 commmune, arrondissement de sous-préfecture
        cls.manifestation2 = DcnmFactory.create(ville_depart=cls.commune2, structure=structure2, activite=activ,
                                                nom='Manifestation_Test_2')
        # Manif NM sur 2 commmunes, même arrondissement de préfecture
        cls.manifestation3 = DcnmFactory.create(ville_depart=cls.commune1, structure=structure1, activite=activ,
                                                nom='Manifestation_Test_3', villes_traversees=(cls.autrecommune1,))
        # Manif NM sur 2 commmunes, même arrondissement de sous-préfecture
        cls.manifestation4 = DcnmFactory.create(ville_depart=cls.commune2, structure=structure2, activite=activ,
                                                nom='Manifestation_Test_4', villes_traversees=(cls.autrecommune2,))
        # Manif NM sur 2 commmunes, arrondissements différents, départ en préfecture
        cls.manifestation5 = DcnmFactory.create(ville_depart=cls.commune1, structure=structure1, activite=activ,
                                                nom='Manifestation_Test_5', villes_traversees=(cls.autrecommune2,))
        # Manif NM sur 2 commmunes, arrondissements différents, départ en sous-préfecture
        cls.manifestation6 = DcnmFactory.create(ville_depart=cls.commune2, structure=structure2, activite=activ,
                                                nom='Manifestation_Test_6', villes_traversees=(cls.autrecommune1,))
        # Manif VTM sur 1 commmune, arrondissement de sous-préfecture
        cls.manifestation7 = AvtmFactory.create(ville_depart=cls.commune2, structure=structure2, activite=activ,
                                                nom='Manifestation_Test_7')
        # Manif NM sur 1 commmune, arrondissement de sous-préfecture
        cls.manifestation8 = DcnmFactory.create(ville_depart=cls.commune3, structure=structure3, activite=activ,
                                                nom='Manifestation_Test_8')

    def test_Instruction_Circuit5(self):
        print('**** test 1 vérification des notifications ****')

        print('\t >>> Manifestation 1')
        self.instruction1 = InstructionFactory.create(manif=self.manifestation1)
        self.instruction1.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_mairie1.email])
        self.assertEqual(outbox[1].to, [self.commune1.email])
        self.assertEqual(outbox[1].subject, 'Nouveau dossier à instruire')
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "dossier")
        self.assertEqual(messages[0].message_enveloppe.first().type, "action")
        Email.objects.all().delete()

        print('\t >>> Manifestation 2')
        self.instruction2 = InstructionFactory.create(manif=self.manifestation2)
        self.instruction2.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_mairie2.email])
        self.assertEqual(outbox[1].to, [self.commune2.email])
        self.assertEqual(outbox[1].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        print('\t >>> Manifestation 3')
        self.instruction3 = InstructionFactory.create(manif=self.manifestation3)
        self.instruction3.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur1.email])
        self.assertEqual(outbox[1].to, [self.prefecture1.email])
        self.assertEqual(outbox[1].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        print('\t >>> Manifestation 4')
        self.instruction4 = InstructionFactory.create(manif=self.manifestation4)
        self.instruction4.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur2.email])
        self.assertEqual(outbox[1].to, [self.prefecture2.email])
        self.assertEqual(outbox[1].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        print('\t >>> Manifestation 5')
        self.instruction5 = InstructionFactory.create(manif=self.manifestation5)
        self.instruction5.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur1.email])
        self.assertEqual(outbox[1].to, [self.prefecture1.email])
        self.assertEqual(outbox[1].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        print('\t >>> Manifestation 6')
        self.instruction6 = InstructionFactory.create(manif=self.manifestation6)
        self.instruction6.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur1.email])
        self.assertEqual(outbox[1].to, [self.prefecture1.email])
        self.assertEqual(outbox[1].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        print('\t >>> Manifestation 7')
        self.instruction7 = InstructionFactory.create(manif=self.manifestation7)
        self.instruction7.notifier_creation()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur1.email])
        self.assertEqual(outbox[1].to, [self.prefecture1.email])
        self.assertEqual(outbox[1].subject, 'Nouveau dossier à instruire')
        Email.objects.all().delete()

        self.instruction8 = InstructionFactory.create(manif=self.manifestation8)

        print('**** test 2 instruction mairie 1 ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='agent_mairie1', password='123'))
        # Appel de la page
        url = '/instructions/tableaudebord/?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Arrondissements')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_specifique=instructionmairie&filtre_etat=atraiter',
                                  HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction3.manif)
        self.assertNotContains(reponse, self.instruction4.manif)
        self.assertNotContains(reponse, self.instruction5.manif)
        self.assertNotContains(reponse, self.instruction6.manif)
        self.assertNotContains(reponse, self.instruction7.manif)
        self.assertNotContains(reponse, self.instruction8.manif)

        print('**** test 3 instruction mairie 2 ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='agent_mairie2', password='123'))
        # Appel de la page
        url = '/instructions/tableaudebord/?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Arrondissements')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_specifique=instructionmairie&filtre_etat=atraiter',
                                  HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, self.instruction1.manif)
        self.assertContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction3.manif)
        self.assertNotContains(reponse, self.instruction4.manif)
        self.assertNotContains(reponse, self.instruction5.manif)
        self.assertNotContains(reponse, self.instruction6.manif)
        self.assertNotContains(reponse, self.instruction7.manif)
        self.assertNotContains(reponse, self.instruction8.manif)

        print('**** test 4 instruction sous-préfecture ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur2', password='123'))
        print('\t >>> Instruction en préfecture')
        # Appel de la page
        url = '/instructions/tableaudebord/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Arrondissements')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction3.manif)
        self.assertContains(reponse, self.instruction4.manif)
        self.assertNotContains(reponse, self.instruction5.manif)
        self.assertNotContains(reponse, self.instruction6.manif)
        self.assertNotContains(reponse, self.instruction7.manif)
        self.assertNotContains(reponse, self.instruction8.manif)

        print('\t >>> Instruction en mairie')
        # Appel de la page
        url = '/instructions/tableaudebord/?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Arrondissements')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_specifique=instructionmairie&filtre_etat=atraiter',
                                  HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, self.instruction1.manif)
        self.assertContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction3.manif)
        self.assertNotContains(reponse, self.instruction4.manif)
        self.assertNotContains(reponse, self.instruction5.manif)
        self.assertNotContains(reponse, self.instruction6.manif)
        self.assertNotContains(reponse, self.instruction7.manif)
        self.assertNotContains(reponse, self.instruction8.manif)

        print('**** test 5 instruction préfecture ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur1', password='123'))
        print('\t >>> Instruction en préfecture')
        # Appel de la page
        url = '/instructions/tableaudebord/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Arrondissements')
        #  Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('4', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)
        self.assertContains(reponse, self.instruction3.manif)
        self.assertNotContains(reponse, self.instruction4.manif)
        self.assertContains(reponse, self.instruction5.manif)
        self.assertContains(reponse, self.instruction6.manif)
        self.assertContains(reponse, self.instruction7.manif)
        self.assertNotContains(reponse, self.instruction8.manif)

        print('\t >>> Instruction en mairie')
        # Appel de la page
        url = '/instructions/tableaudebord/?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Arrondissements')
        #  Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        reponse = self.client.get(
            '/instructions/tableaudebord/list/?filtre_specifique=instructionmairie&filtre_etat=atraiter',
            HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction3.manif)
        self.assertNotContains(reponse, self.instruction4.manif)
        self.assertNotContains(reponse, self.instruction5.manif)
        self.assertNotContains(reponse, self.instruction6.manif)
        self.assertNotContains(reponse, self.instruction7.manif)
        self.assertNotContains(reponse, self.instruction8.manif)

        # print(reponse.content.decode('utf-8'))
