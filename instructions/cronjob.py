from django_cron import CronJobBase, Schedule
from django_cron.models import CronJobLog
from django.db.models import Q
from django.utils import timezone

from instructions.models import Avis
from evenements.models import Manif
from messagerie.models import Message, Enveloppe

import pytz



class RelanceAvis(CronJobBase):
    """
    Code pour envoyer des relances d'avis automatiques.
    """
    RUN_AT_TIME = ['7:05']  # à lancer à 7h05
    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'instructions.relanceavis'    # a unique code

    def do(self):

        # Purger les logs
        CronJobLog.objects.filter(code="instructions.relanceavis").filter(is_success=True).filter(
            end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        log_retour = ""
        avis = Avis.objects.en_cours().filter(~Q(etat='rendu'))
        for avi in avis:
            if not avi.instruction.etat == 'annulée':
                instance = avi.get_instance()
                utc = pytz.UTC
                debutlimite = avi.get_date_limite() - timezone.timedelta(days=instance.avis_delai_relance)
                # Comme on ne peut pas comparer un date et un datetime, on le localize le premier et on lui ajout une heure
                debutlimite = utc.localize(timezone.datetime.combine(debutlimite, timezone.datetime.min.time()))
                finlimite = utc.localize(timezone.datetime.combine(avi.get_date_limite(), timezone.datetime.min.time()))

                if timezone.now() < finlimite and timezone.now() > debutlimite:
                    avi.notifier_creation_avis(origine=avi.get_service(), agents=avi.get_agents(), non_agent_recipient=avi.get_service())
                    log_retour += str(avi) + " relancé"
        return log_retour


class RelanceAction(CronJobBase):
    """
    Cron qui permet de d'avertir l'instructeur si un message d'action n'a pas été lu par l'ensemble d'un service
    """
    RUN_AT_TIME = ['7:25']  # à lancer à 7h25
    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'instructions.relanceaction'

    def do(self):

        # Purger les logs
        CronJobLog.objects.filter(code="instructions.relanceaction").filter(is_success=True).filter(
            end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        log_retour = ""
        manifs = Manif.objects.filter(date_debut__gt=timezone.now()).exclude(instruction__etat='annulée')
        for manif in manifs:
            try:
                delai = manif.get_organisateur().user.get_instance().action_delai_relance
            except:
                delai = 14
            delta = timezone.now() - timezone.timedelta(days=delai)
            messages = Message.objects.filter(message_enveloppe__manifestation=manif,
                                              message_enveloppe__date__lt=delta,
                                              message_enveloppe__type="action").distinct()
            for message in messages:
                enveloppe = message.message_enveloppe.first()
                # ici on check s'il un des message envoyés à été lu ou pas si non on envoi un msg à l'insrtucteur
                env_avec_meme_messages = Enveloppe.objects.filter(corps=enveloppe.corps)\
                    .filter(lu_datetime__gte=enveloppe.date).filter(lu_requis=True)
                # ensuite on verifie qu'il n'y a pas de relance qui a été faite
                tab_rel = []
                for envel in message.message_enveloppe.all():
                    tab_rel.append(envel.pk)
                relance = Enveloppe.objects.filter(reponse__in=tab_rel)
                if not env_avec_meme_messages and not relance:
                    service = enveloppe.destinataire_id.get_service() if enveloppe.destinataire_id else enveloppe.destinataire_txt
                    Message.objects.creer_et_envoyer('info_suivi', None, [manif.instruction.referent], "Demande d'action non lue",
                                                     "En tant qu'instructeur référent, nous attirons votre attention sur le fait " +
                                                     "que le message ci-dessous adressé au service "+ str(service) + " il y a "
                                                     + str(delai) + " jours , n'a pas été encore vu."
                                                     , manifestation_liee=enveloppe.manifestation,
                                                     objet_lie_nature=enveloppe.doc_objet, reponse_a_pk=enveloppe.pk)
        return log_retour
