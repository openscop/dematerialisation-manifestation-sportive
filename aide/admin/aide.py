# coding: utf-8
from django.contrib import admin
from django.db.models import Q
from import_export.admin import ExportActionModelAdmin
from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin

from administrative_division.models.departement import Departement
from aide.forms import PageAideForm, PanneauPageAideForm, NotePageAideForm
from aide.models.aide import PageAide, OngletPageAide, PanneauPageAide, PanelTabs, NotePageAide
from core.util.admin import RelationOnlyFieldListFilter
from .filter import GroupeFilter


class OngletPageAideInLine(SortableInlineAdminMixin, admin.StackedInline):
    model = OngletPageAide
    extra = 1
    show_change_link = True


class PanelTabsInLine(SortableInlineAdminMixin, admin.StackedInline):
    model = PanelTabs
    extra = 0

    def get_formset(self, request, obj=None, **kwargs):
        # Pour supprimer l'icône "+" à la suite du champ
        formset = super().get_formset(request, obj, **kwargs)
        formset.form.base_fields['panel'].widget.can_add_related = False
        return formset


class NotePageAideInline(admin.StackedInline):
    """ Inline pour ajouter des notes aux pages d'aide """

    # Configuration
    model = NotePageAide
    fields = (('title', 'active'), 'content', 'role', 'groupe', 'departements')
    extra = 1
    form = NotePageAideForm

    # Overrides
    def get_queryset(self, request):
        """ Modifier le queryset selon l'utilisateur """
        if request.user.is_superuser:
            # Les superutilisateurs peuvent tout voir
            return super().get_queryset(request)
        else:
            # Les autres voient ce qui correspond à leur département
            departement = request.user.get_instance().get_departement()
            return super().get_queryset(request).filter(Q(departements=departement) | Q(departements__isnull=True))

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if request.user.is_superuser or db_field.name != 'departements':
            return super().formfield_for_foreignkey(db_field, request, **kwargs)
        else:
            departement = request.user.get_instance().get_departement()
            field = super().formfield_for_foreignkey(db_field, request, **kwargs)
            field.queryset = Departement.objects.filter(id=departement.pk)
            return field


@admin.register(PageAide)
class PageAideAdmin(ExportActionModelAdmin):
    """ Administration des pages d'aide """

    # Configuration
    list_select_related = True
    list_display = ['pk', 'title', 'path', 'active', 'get_tabs_count', 'get_panels_count', 'get_note_count', 'updated', 'nbr_vues']
    list_display_links = []
    list_filter = ['active', 'role', GroupeFilter, ('departements', RelationOnlyFieldListFilter)]
    list_editable = ['active']
    readonly_fields = ['get_contentbefore_html', 'get_contentafter_html', 'updated', 'nbr_vues']
    search_fields = ['title__unaccent', 'contentbefore__unaccent', 'contentafter__unaccent', 'path', 'slug']
    actions = []
    exclude = []
    form = PageAideForm
    inlines = [OngletPageAideInLine, NotePageAideInline]
    actions_on_top = True
    order_by = ['pk']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        """ Modifier le queryset selon l'utilisateur """
        if request.user.is_superuser:
            # Les superutilisateurs peuvent tout voir
            return super().get_queryset(request)
        else:
            # Les autres voient ce qui correspond à leur département
            departement = request.user.get_instance().get_departement()
            return super().get_queryset(request).filter(Q(departements=departement) | Q(departements__isnull=True))

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if request.user.is_superuser or db_field.name != 'departements':
            return super().formfield_for_foreignkey(db_field, request, **kwargs)
        else:
            departement = request.user.get_instance().get_departement()
            field = super().formfield_for_foreignkey(db_field, request, **kwargs)
            field.queryset = Departement.objects.filter(id=departement.pk)
            return field

    def get_fieldsets(self, request, obj=None):
        if [group in ['Page d\'aide', 'Administrateurs techniques'] for group in request.user.groups.values_list('name', flat=True)].count(True) > 0:
            return [(None, {'fields': ['title', 'path', 'active', 'updated', 'nbr_vues', 'authenticated_only',
                                       'contentbefore', 'contentafter', 'role', 'groupe', 'departements']}), ]
        return [(None, {'fields': ['title', 'path', 'active', 'updated', 'nbr_vues', 'authenticated_only',
                                   'get_contentbefore_html', 'get_contentafter_html',
                                   'role', 'groupe', 'departements']}), ]


@admin.register(OngletPageAide)
class OngletPageAideAdmin(SortableAdminMixin, ExportActionModelAdmin):
    """ Administration des pages d'aide """

    # Configuration
    inlines = [PanelTabsInLine]
    list_display = ('title', 'page')

    def has_module_permission(self, request):
        return False


@admin.register(PanneauPageAide)
class PanneauPageAideAdmin(ExportActionModelAdmin):
    """ Administration des pages d'aide """

    # Configuration
    model = PanneauPageAide
    list_display = ('title', 'get_tabs_with_page', 'get_note_count', 'updated', 'nbr_vues')
    search_fields = ('tabs__title', 'title')
    readonly_fields = ('updated', 'contenu', 'updated', 'nbr_vues')
    extra = 1
    inlines = [NotePageAideInline]
    form = PanneauPageAideForm

    # Overrides
    def get_queryset(self, request):
        """ Modifier le queryset selon l'utilisateur """
        if request.user.is_superuser:
            # Les superutilisateurs peuvent tout voir
            return super().get_queryset(request)
        else:
            # Les autres voient ce qui correspond à leur département
            departement = request.user.get_instance().get_departement()
            return super().get_queryset(request).filter(Q(departements=departement) | Q(departements__isnull=True))

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if request.user.is_superuser or db_field.name != 'departements':
            return super().formfield_for_foreignkey(db_field, request, **kwargs)
        else:
            departement = request.user.get_instance().get_departement()
            field = super().formfield_for_foreignkey(db_field, request, **kwargs)
            field.queryset = Departement.objects.filter(id=departement.pk)
            return field

    def has_module_permission(self, request):
        return True

    def get_fieldsets(self, request, obj=None):
        if [group in ['Page d\'aide', 'Administrateurs techniques'] for group in request.user.groups.values_list('name', flat=True)].count(True) > 0:
            return [(None, {'fields': [('title', 'active'), 'tabs', 'updated', 'nbr_vues', 'content', 'role', 'groupe', 'departements']}), ]
        return [(None, {'fields': [('title', 'active'), 'tabs', 'updated', 'nbr_vues', 'contenu', 'role', 'groupe', 'departements']}), ]
