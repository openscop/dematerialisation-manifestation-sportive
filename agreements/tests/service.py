# coding: utf-8

from datetime import date

from django.urls import reverse

from administration.factories import *
from agreements.tests.base import AvisTestsBase
from ..factories import *


class ServiceAvisTests(AvisTestsBase):
    """ Tests des avis de Service """

    # Tests
    def test_access_url(self):
        """ Tester l'URL d'accès """
        avis = ServiceAvisFactory.create(authorization=self.authorization)
        self.assertEqual(avis.get_absolute_url(), reverse('agreements:service_agreement_detail', kwargs={'pk': avis.pk}))

    def test_workflow(self):
        """ Tester la création des avis """
        self.assertEqual(self.authorization.get_avis_count(), 1)  # avis Fédération (tjs)
        self.service = ServiceFactory.create(departements=(self.departement,))
        self.authorization.concerned_services.add(self.service)  # Normalement un avis de service est automatiquement ajouté
        self.assertEqual(self.authorization.get_avis_count(), 2)  # avis Fédération + avis Service

    def test_life_cycle(self):
        """ Tester le cycle de validation d'un avis Service """
        # L'agent et l'instructeur sont créés après l'autorisation, il n'y aura pas de notification pour la création d'uatorisation
        MairieAgentFactory.create(commune=self.commune)  # créer un agent de la mairie
        InstructeurFactory(prefecture=self.prefecture)
        self.service = ServiceFactory.create(departements=(self.departement,))
        avis = ServiceAvisFactory(service=self.service, authorization=self.authorization)

        # Vérifier que les agents (1 seul) ont reçu leur notification (pour l'avis Service)
        for service_agent in self.service.get_serviceagents():
            self.assertEqual(service_agent.user.notifications.all().count(), 1)

        # Vérifier qu'une nouvelle notification n'est pas envoyée si l'avis est à nouveau sauvegardé
        avis.save()
        for service_agent in self.service.get_serviceagents():
            self.assertEqual(service_agent.user.notifications.all().count(), 1)

        # Vérifier que l'avis est correctement rendu et à la date du jour
        avis.acknowledge()
        self.assertEqual(avis.reply_date, date.today())

        # Vérifier qu'une action "avis rendu" a bien été enregistrée pour les agents du service
        for service_agent in self.service.get_serviceagents():
            self.assertEqual(service_agent.user.actions.all().count(), 1)
            self.assertEqual(service_agent.user.actions.first().action, 'avis rendu')

        # Vérifier qu'aucune nouvelle notification n'existe pour les instructeurs
        for instructeur in self.prefecture.get_instructeurs():
            self.assertEqual(instructeur.user.notifications.all().count(), 0)  # pas pour la création d'autorisation mais pour le rendu d'avis

        # Vérifier qu'une nouvelle notification existe pour les instructeurs en mairie
        for agentmairie in self.commune.get_mairieagents():
            self.assertEqual(agentmairie.user.notifications.all().count(), 1)  # pas pour la création d'autorisation mais pour le rendu d'avis
            self.assertEqual(agentmairie.user.notifications.last().content_object,
                             self.service)  # voir agreements.federation.acknowledge
