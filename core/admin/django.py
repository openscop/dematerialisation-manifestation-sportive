# coding: utf-8
from django import forms
from django.urls import reverse
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from django.contrib.admin.models import LogEntry
from django.contrib.flatpages.admin import FlatPageAdmin, FlatpageForm as BaseFlatpageForm
from django.contrib.flatpages.models import FlatPage

from django_cron.models import CronJobLog
from django_cron.admin import CronJobLogAdmin
from allauth.account.models import EmailAddress
from import_export.admin import ExportActionModelAdmin
from ckeditor_uploader.fields import RichTextUploadingFormField

from core.admin.filters import RoleFilter, LogEntryInstanceFilter
from core.models import User, Instance, OptionUser, LogConsult
from core.forms import CustomUserChangeForm, CustomUserCreateForm
from administration.models import Prefecture, Instructeur


class FlatpageForm(BaseFlatpageForm):
    """ Formulaire admin des flat pages """

    # Initialiser
    def __init__(self, *args, **kwargs):
        return super().__init__(*args, **kwargs)

    # Champs
    content = RichTextUploadingFormField()


class FlatPageAdmin(FlatPageAdmin):
    """ Administration des flatpages """

    # Configuration
    form = FlatpageForm


class EmailInline(admin.TabularInline):
    model = EmailAddress

    fieldsets = (
        (None,
         {'fields': ('email', 'verified', 'primary')}),
    )

    def get_fieldsets(self, request, obj=None):
        if request.user.has_group('Administrateurs d\'instance') or request.user.is_superuser:
            return ((None, {'fields': ('email', 'verified', 'primary', "renvoi_mail")}), )
        else:
            return ((None, {'fields': ('email', 'verified', 'primary')}), )

    def get_readonly_fields(self, request, obj=None):
        if request.user.has_group('Administrateurs d\'instance') or request.user.is_superuser:
            return ("renvoi_mail",)
        else:
            return ()

    # renvoi d'un mail de confirmation
    def renvoi_mail(self, obj):
        if obj.pk and not obj.verified:
            pk = obj.user.pk
            url = reverse("send_confirm_mail", args=[pk])
            return format_html('<a class="btn" href="{}">Renvoyer le mail de confirmation</a>', url)
        return '-'
    renvoi_mail.short_description = "Mail confirmation"

    def get_extra(self, request, obj=None, **kwargs):
        return 1


def supprimer_utilisateur_a_un_groupe(modeladmin, request, queryset):
    """ Ajoute une action pour supprimer des utilisateurs d'un groupe"""
    groupname = request.POST['nomGroupe']
    group = Group.objects.get(name=groupname)
    for user in queryset:
        group.user_set.remove(user)
        group.save()


supprimer_utilisateur_a_un_groupe.short_description = "Supprimer les utilisateurs sélectionnés d'un groupe..."


def ajouter_utilisateur_a_un_groupe(modeladmin, request, queryset):
    """ Ajoute une action pour ajouter des utilisateurs à un groupe"""
    groupname = request.POST['nomGroupe']
    group = Group.objects.get(name=groupname)
    for user in queryset:
        group.user_set.add(user)
        group.save()


ajouter_utilisateur_a_un_groupe.short_description = "Ajouter les utilisateurs sélectionnés dans un groupe..."


class UtilisateurAdmin(ExportActionModelAdmin, UserAdmin):
    """ Configuration de l'admin utilisateur """
    form = CustomUserChangeForm
    add_form = CustomUserCreateForm
    # Configuration
    list_display = ('agent_name', 'email', 'is_active', 'is_staff', 'date_joined', 'default_instance', 'get_role', 'get_service')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups', 'default_instance', RoleFilter)
    readonly_fields = ('get_service', 'get_role', 'last_change_password', 'lien_enveloppe')
    actions = ExportActionModelAdmin.actions + [ajouter_utilisateur_a_un_groupe, supprimer_utilisateur_a_un_groupe]
    ordering = ('-date_joined',)
    inlines = [EmailInline, ]
    fieldsets = (
        (None, {'fields': ('username', 'password', 'last_change_password',  ('default_instance', 'get_role', 'get_service'))}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'lien_enveloppe', 'recap')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    list_per_page = 25

    def lien_enveloppe(self, user_obj):
        if user_obj:
            link = reverse("admin:messagerie_enveloppe_changelist")
            return mark_safe('<a href="' + link + '?q=' + user_obj.username + '">Voir ses messages</a>')
        return '-'
    lien_enveloppe.short_description = "Voir les messages de l'utilisateur"
    lien_enveloppe.allow_tags = True

    def get_actions(self, request):
        actions = super().get_actions(request)
        choices = []
        groups = Group.objects.all()
        for group in groups:
            if request.user.has_group("Administrateurs techniques"):
                choices.append((group.name, group.name))
            elif request.user.has_group("Administrateurs d\'instance"):
                if group.name == "Instructeurs" or \
                        group.name == "Administrateur de sites protégés Réserve naturelle régionale" or \
                        group.name == "Administrateurs de sites protégés N2000":
                    choices.append((group.name, group.name))
        self.action_form.base_fields['nomGroupe'] = forms.ChoiceField(choices=choices, label='Nom du groupe :')
        return actions

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        # Les utilisateurs équipe ne peuvent voir que leur département
        if not request.user.is_superuser:
            queryset = queryset.filter(default_instance__departement=request.user.get_departement())
        return queryset

    def get_readonly_fields(self, request, obj=None):
        if request.user.has_group('Administrateurs techniques'):
            return ('get_service', 'get_role', 'lien_enveloppe')
        else:
            return ('get_service', 'get_role', 'groups', 'lien_enveloppe')

    def save_model(self, request, obj, form, change):
        """ Enregistrer le modèle """
        try:
            instance = request.user.get_instance()
            if not obj.default_instance and not instance.is_master():
                obj.default_instance = instance
        except (AttributeError, Exception):
            pass
        if request.user.groups.filter(name="Administrateurs d'instance") and request.user.groups.filter(name="_assistance"):
            if obj == request.user and obj.default_instance != request.user.default_instance:
                if request.user.has_role('instructeur'):
                    instructeur = request.user.instructeur
                    if not obj.default_instance.is_master():
                        instructeur.prefecture = Prefecture.objects.get(
                            sous_prefecture=False, arrondissement__departement=obj.default_instance.departement)
                        instructeur.save()
                    else:
                        instructeur.delete()
                else:
                    if not obj.default_instance.is_master():
                        pref = Prefecture.objects.get(
                            sous_prefecture=False, arrondissement__departement=obj.default_instance.departement)
                        Instructeur.objects.create(user=request.user, prefecture=pref)

        return super().save_model(request, obj, form, change)

    def delete_model(self, request, obj):
        # Si il y a cascade, c'est à dire que cette suppression va entrainer la suppression d'une ou plusieurs manifs,
        # et si l'utilisateur n'a pas les droits pour les suppressions en cascade, la méthode ne sera pas atteinte
        # Si il a les droits, alors il faudra en plus qu'il soit superuser ou du groupe "Administrateurs techniques
        if hasattr(obj, 'organisateur'):
            if obj.organisateur.structure.manifs.filter(instruction__isnull=False).exists():
                if not request.user.is_superuser and "Administrateurs techniques" not in request.user.groups.values_list('name', flat=True):
                    self.message_user(request, "Des manifestations vont être supprimées en cascade ! Vous n'avez pas les droits", level=messages.WARNING)
                    return
        if obj.actions.all():
            if not request.user.is_superuser and "Administrateurs techniques" not in request.user.groups.values_list('name', flat=True):
                self.message_user(request,
                                  "Cette opération n'est pas possible, car elle entrainerait des suppressions d'actions sur des dossiers de manifestation !",
                                  level=messages.WARNING)
                return

        super().delete_model(request, obj)

    def get_form(self, request, obj=None, **kwargs):
        """ Empêcher les non superuser de modifier les permissions des utilisateurs """
        if not request.user.is_superuser:
            # Vous ne voulez surtout pas donner accès à ces 3 champs
            self.exclude = ['user_permissions', 'is_superuser']
            self.fieldsets = (
                (None, {'fields': ('username', 'password', 'last_change_password', ('default_instance', 'get_role', 'get_service'))}),
                (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', "lien_enveloppe", 'recap')}),
                (_('Permissions'), {'fields': ('is_active', 'is_staff', 'groups')}),
                (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
            )
        form = super().get_form(request, obj, **kwargs)
        if request.user.get_departement():
            if hasattr(form.base_fields, 'default_instance'):
                form.base_fields['default_instance'].queryset = Instance.objects.filter(departement=request.user.get_departement())
                form.base_fields['default_instance'].initial = request.user.get_instance()
        return form

    def agent_name(self, user_obj):
        return user_obj.get_full_name_and_username()
    agent_name.short_description = "Nom complet et pseudo"

    def changelist_view(self, request, extra_context=None):
        """surcharge pour ajouter un log"""
        response = super().changelist_view(request, extra_context)
        LogConsult.objects.create_from_list(response, request)
        return response

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response


class CustomCronJobLogAdmin(CronJobLogAdmin):
    list_display = ('code', 'start_time', 'end_time', 'humanize_duration', 'is_success', 'extrait')

    def extrait(self, obj):
        return obj.message[0:20]


@admin.register(OptionUser)
class OptionUserAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(LogEntry)
class LogEntryAdmin(admin.ModelAdmin):

    list_display = ('action_time', 'user', 'content_type', 'object_repr', 'action_flag', 'change_message')
    search_fields = ('user__username', 'content_type__model', 'object_repr', 'change_message')
    date_hierarchy = 'action_time'
    readonly_fields = ('action_time', 'user', 'content_type', 'object_repr', 'action_flag', 'change_message')
    fields = ('action_time', 'user', 'content_type', 'object_repr', 'action_flag', 'change_message')
    list_filter = ('action_flag', LogEntryInstanceFilter)

    def get_queryset(self, request):
        queryset = super(LogEntryAdmin, self).get_queryset(request)
        if not request.user.has_group("Administrateurs technique") and not request.user.is_superuser:
            return queryset.filter(user__default_instance=request.user.get_instance())
        return queryset


@admin.register(LogConsult)
class LogConsultAdmin(admin.ModelAdmin):

    list_display = ('date', 'user', "model", 'type', "pk_affiche")
    search_fields = ('user__username', "model", 'pk_affiche')
    date_hierarchy = 'date'
    readonly_fields = ('date', 'user', "model", 'type', "pk_affiche")
    fields = ('date', 'user', "model", 'type', "pk_affiche")
    list_filter = ('type', 'model', LogEntryInstanceFilter)

    def get_queryset(self, request):
        queryset = super(LogConsultAdmin, self).get_queryset(request)
        if not request.user.has_group("Administrateurs technique") and not request.user.is_superuser:
            return queryset.filter(user__default_instance=request.user.get_instance())
        return queryset


admin.site.unregister(EmailAddress)

admin.site.unregister(CronJobLog)
admin.site.register(CronJobLog, CustomCronJobLogAdmin)

admin.site.unregister(FlatPage)

admin.site.register(User, UtilisateurAdmin)
admin.site.register(FlatPage, FlatPageAdmin)
