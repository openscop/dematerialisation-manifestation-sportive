from django.core.management.base import BaseCommand

from core.models import User
from core.util.user import UserHelper

ROLE_TYPES = UserHelper.ROLE_TYPES


class Command(BaseCommand):
    """
    commande pour vérifier les roles types de tout le monde
    """

    def handle(self, *args, **options):
        for user in User.objects.all():

            roles = []
            for role in ROLE_TYPES:
                # Tester les rôles dont le modèle est lié directement à User
                if hasattr(user, role):
                    roles.append(role)

                # Tester le nom de modèle pour user.agent (uniquement avec parent_link=True)
                try:
                    model_name = user.agent._meta.model_name
                    if model_name in ROLE_TYPES:
                        roles.append(model_name)
                except AttributeError:
                    pass
                # Tester le nom de modèle pour user.agentlocal (uniquement avec parent_link=True)
                try:
                    model_name = user.agentlocal._meta.model_name
                    if model_name in ROLE_TYPES:
                        roles.append(model_name)
                except AttributeError:
                    pass
                # Tester les rôles dont le modèle est lié directement à User.agent (déprécie depuis parent_link)
                try:
                    if getattr(user.agent, role) is not None:
                        roles.append(role)
                except AttributeError:
                    pass
                # Tester les rôles dont le modèle est lié directement à User.agentlocal (déprécié depuis parent_link)
                try:
                    if getattr(user.agentlocal, role) is not None:
                        roles.append(role)
                except AttributeError:
                    pass

            stringrole = ','.join(roles)
            if user.tableau_role != stringrole:
                print(str(user.pk) + " - " + user.username)
