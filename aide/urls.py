# coding: utf-8
from django.urls import path, re_path

from aide import views


app_name = 'aide'
urlpatterns = [
    # Aide : sommaire
    path('', views.page_aide, {"path": "sommaire"}, name="aide-sommaire"),

    # Aide : liste
    path('liste', views.liste_pages_aide, name="liste-page-aide"),

    # Aide contextuelle
    re_path('context/(?P<name>.+)', views.view_context_help, name="context"),

    # formulaire de demande
    path('contact', views.demande, name="contact"),

    # Aide : page
    re_path('vu/(?P<pk>\d+)/', views.AjoutVuePanelAjax, name="vu-panneau"),
    re_path('(?P<path>[a-zA-Z0-9-/_]+)/(?P<pk>\d+)', views.page_aide, name="page-aide-direct"),
    re_path('(?P<path>.+)', views.page_aide, name="page-aide"),
]
