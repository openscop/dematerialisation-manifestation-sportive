import re, os, time

from django.test import override_settings
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.hashers import make_password as make

from post_office.models import EmailTemplate
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.firefox.webdriver import WebDriver
from allauth.account.models import EmailAddress

from core.models import Instance, User
from core.factories import UserFactory
from evenements.factories import DcnmFactory
from organisateurs.factories import OrganisateurFactory, StructureFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import (InstructeurFactory, ServiceAgentFactory, ServiceFactory,FederationAgentFactory,
                                      GGDAgentFactory, EDSRAgentFactory, CGDAgentFactory, DDSPAgentFactory, CGDFactory,
                                      CommissariatAgentFactory, MairieAgentFactory, CGAgentFactory, BrigadeAgentFactory,
                                      CGSuperieurFactory, SDISAgentFactory, GroupementAgentFactory, CODISAgentFactory,
                                      CISAgentFactory, CGServiceAgentFactory)
from administration.factories import (BrigadeFactory, CommissariatFactory, CGServiceFactory, CompagnieFactory,
                                      CISFactory, EDSRAgentLocalFactory)
from sports.factories import ActiviteFactory, FederationFactory


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class SeleniumCommunClass(StaticLiveServerTestCase):
    """
    Méthodes communes aux tests Sélénium
    """
    DELAY = 0.35

    def init_setup(self):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets sur le 42
            Création des utilisateurs
            Création de l'événement
        """
        self.selenium = WebDriver()
        self.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        self.selenium.set_window_size(800, 1000)

        if not hasattr(self, 'dep'):
            dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__workflow_ggd=Instance.WF_EDSR,
                                            instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
            self.dep = dep
            arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
            self.prefecture = arrondissement.prefecture
            self.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
            self.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)
        else:
            dep = self.dep

        # Création des utilisateurs
        self.organisateur = UserFactory.create(username='organisateur', password=make(123), default_instance=dep.instance)
        organisateur = OrganisateurFactory.create(user=self.organisateur)
        EmailAddress.objects.create(user=self.organisateur, email='organisateur@example.com', primary=True, verified=True)
        structure = StructureFactory(commune=self.commune, organisateur=organisateur)

        self.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.instructeur, email='instructeur@example.com', primary=True, verified=True)
        InstructeurFactory.create(user=self.instructeur, prefecture=self.prefecture)
        self.agent_fede = UserFactory.create(username='agent_fede', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_fede, email='agent_fede@example.com', primary=True, verified=True)
        activ = ActiviteFactory.create()
        self.fede = FederationFactory.create()
        FederationAgentFactory.create(user=self.agent_fede, federation=self.fede)
        self.agent_ggd = UserFactory.create(username='agent_ggd', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_ggd, email='agent_ggd@example.com', primary=True, verified=True)
        GGDAgentFactory.create(user=self.agent_ggd, ggd=dep.ggd)
        self.agent_edsr = UserFactory.create(username='agent_edsr', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_edsr, email='agent_edsr@example.com', primary=True, verified=True)
        EDSRAgentFactory.create(user=self.agent_edsr, edsr=dep.edsr)
        self.agentlocal_edsr = UserFactory.create(username='agentlocal_edsr', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agentlocal_edsr, email='agentlocal_edsr@example.com', primary=True, verified=True)
        EDSRAgentLocalFactory.create(user=self.agentlocal_edsr, edsr=dep.edsr)
        self.cgd = CGDFactory.create(commune=self.commune)
        self.agent_cgd = UserFactory.create(username='agent_cgd', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_cgd, email='agent_cgd@example.com', primary=True, verified=True)
        CGDAgentFactory.create(user=self.agent_cgd, cgd=self.cgd)
        self.brigade = BrigadeFactory.create(kind='bta', cgd=self.cgd, commune=self.commune)
        self.agent_brg = UserFactory.create(username='agent_brg', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_brg, email='agent_brg@example.com', primary=True, verified=True)
        BrigadeAgentFactory.create(user=self.agent_brg, brigade=self.brigade)
        self.agent_ddsp = UserFactory.create(username='agent_ddsp', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_ddsp, email='agent_ddsp@example.com', primary=True, verified=True)
        DDSPAgentFactory.create(user=self.agent_ddsp, ddsp=dep.ddsp)
        self.commiss = CommissariatFactory.create(commune=self.commune)
        self.agent_commiss = UserFactory.create(username='agent_commiss', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_commiss, email='agent_commiss@example.com', primary=True, verified=True)
        CommissariatAgentFactory.create(user=self.agent_commiss, commissariat=self.commiss)
        self.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_mairie, email='agent_mairie@example.com', primary=True, verified=True)
        MairieAgentFactory.create(user=self.agent_mairie, commune=self.commune)
        self.agent_cg = UserFactory.create(username='agent_cg', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_cg, email='agent_cg@example.com', primary=True, verified=True)
        CGAgentFactory.create(user=self.agent_cg, cg=dep.cg)
        self.cgserv = CGServiceFactory.create(name='STD_test', cg=dep.cg, service_type='STD')
        self.agent_cgserv = UserFactory.create(username='agent_cgserv', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_cgserv, email='agent_cgserv@example.com', primary=True, verified=True)
        CGServiceAgentFactory.create(user=self.agent_cgserv, cg_service=self.cgserv)
        self.agent_cgsup = UserFactory.create(username='agent_cgsup', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_cgsup, email='agent_cgsup@example.com', primary=True, verified=True)
        CGSuperieurFactory.create(user=self.agent_cgsup, cg=dep.cg)
        self.agent_sdis = UserFactory.create(username='agent_sdis', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_sdis, email='agent_sdis@example.com', primary=True, verified=True)
        SDISAgentFactory.create(user=self.agent_sdis, sdis=dep.sdis)
        self.group = CompagnieFactory.create(sdis=dep.sdis, number=98)
        self.agent_group = UserFactory.create(username='agent_group', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_group, email='agent_group@example.com', primary=True, verified=True)
        GroupementAgentFactory.create(user=self.agent_group, compagnie=self.group)
        self.agent_codis = UserFactory.create(username='agent_codis', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_codis, email='agent_codis@example.com', primary=True, verified=True)
        CODISAgentFactory.create(user=self.agent_codis, codis=dep.codis)
        self.cis = CISFactory.create(name='CIS_test', compagnie=self.group, commune=self.commune)
        agent_cis = UserFactory.create(username='agent_cis', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_cis, email='agent_cis@example.com', primary=True, verified=True)
        CISAgentFactory.create(user=agent_cis, cis=self.cis)
        self.sncf = ServiceFactory.create(departements=(dep,))
        self.agent_sncf = UserFactory.create(username='agent_sncf', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=self.agent_sncf, email='agent_sncf@example.com', primary=True, verified=True)
        ServiceAgentFactory.create(user=self.agent_sncf, service=self.sncf)
        self.cg = dep.cg
        self.ddsp = dep.ddsp
        self.edsr = dep.edsr

        EmailTemplate.objects.create(name='new_msg')
        for user in User.objects.all():
            user.optionuser.notification_mail = True
            user.optionuser.action_mail = True
            user.optionuser.save()

        self.manifestation = DcnmFactory.create(ville_depart=self.commune, structure=structure, nom='Manifestation_Test',
                                               villes_traversees=(self.autrecommune,), activite=activ)
        self.manifestation.nb_participants = 1
        self.manifestation.description = 'une course qui fait courir'
        self.manifestation.descriptions_parcours = 'un gros parcours'
        self.manifestation.nb_spectateurs = 100
        self.manifestation.nb_signaleurs = 1
        self.manifestation.nb_vehicules_accompagnement = 0
        self.manifestation.nom_contact = 'durand'
        self.manifestation.prenom_contact = 'joseph'
        self.manifestation.tel_contact = '0605555555'
        self.manifestation.save()

        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'itineraire_horaire',
                     'cartographie', 'pj_1', 'pj_2', 'pj_3', 'pj_4'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test Sélénium")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """
        cls.selenium.quit()
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'itineraire_horaire',
                     'cartographie', 'pj_1', 'pj_2', 'pj_3', 'pj_4'):
            os.remove(file+".txt")

        os.system('rm -R /tmp/maniftest/media/' + cls.dep.name + '/')

        super().tearDownClass()


    def connexion(self, username):
        """ Connexion de l'utilisateur 'username' """
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element_by_name("login")
        pseudo_input.send_keys(username)
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('123')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.assertIn('Connexion avec ' + username + ' réussie', self.selenium.page_source)

    def deconnexion(self):
        """ Deconnexion de l'utilisateur """
        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_class_name('deconnecter').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    def vue_detail(self):
        """
        Appel de la vue de détail de la manifestation
        """
        self.selenium.execute_script("window.scroll(0, 700)")
        url = self.selenium.current_url
        self.selenium.find_element_by_xpath('//td/span[text()="Manifestation_Test"]').click()
        wait = WebDriverWait(self.selenium, 15)
        wait.until(lambda driver: self.selenium.current_url != url)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        time.sleep(self.DELAY)

    def aucune_action(self):
        """
        Test auncune action affichée dans la zone action de la dashboard
        """
        action = re.search(', aucune action nécessaire', self.selenium.page_source)
        attente = re.search(' devez attendre ', self.selenium.page_source)
        if not action and not attente:
            self.assertIsNotNone(action, msg="message non trouvé")

    def presence_avis(self, username, state):
        """
        Test de la présence et l'état de l'événement
        :param username: agent considéré
        :param state: couleur de l'événement
        """
        if state == 'none':
            if username not in ["agent_cis", "agent_codis", "agent_brg"]:
                bloc = self.selenium.find_element_by_xpath("//*[starts-with(@id, 'btn')][contains(@id, '_etat_demande')]")
                nb_class = "test_nb_atraiter"
            else:
                bloc = self.selenium.find_element_by_xpath("//*[starts-with(@id, 'btn')][contains(@id, '_etat_rendu')]")
                nb_class = "test_nb_rendu"
            bloc.click()
            self.assertEqual('0', bloc.find_element_by_class_name(nb_class).text)
            if username == 'agent_commiss' or username == 'agent_cgd' or username == 'agentlocal_edsr' or username == 'agent_cgserv' or username == 'agent_group':
                self.assertIn('Aucune demande', self.selenium.page_source)
            else:
                self.assertIn('Aucun avis', self.selenium.page_source)
        # Test du bloc concerné
        elif state == 'nouveau':
            bloc = self.selenium.find_element_by_xpath("//*[starts-with(@id, 'btn')][contains(@id, '_etat_demande')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element_by_class_name('test_nb_atraiter').text)
            declar = self.selenium.find_element_by_tag_name('tbody')
            self.assertIn('Manifestation_Test', declar.text)
            try:
                declar.find_element_by_class_name('table_afaire')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de nouveau dossier")
        elif state == 'encours':
            bloc = self.selenium.find_element_by_xpath("//*[starts-with(@id, 'btn')][contains(@id, '_etat_encours')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element_by_class_name('test_nb_encours').text)
            declar = self.selenium.find_element_by_tag_name('tbody')
            self.assertIn('Manifestation_Test', declar.text)
            try:
                declar.find_element_by_class_name('table_encours')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de dossier en cours")
        elif state == 'rendu':
            self.selenium.execute_script("window.scroll(0, 600)")
            bloc = self.selenium.find_element_by_xpath("//*[starts-with(@id, 'btn')][contains(@id, '_etat_rendu')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element_by_class_name('test_nb_rendu').text)
            declar = self.selenium.find_element_by_tag_name('tbody')
            self.assertIn('Manifestation_Test', declar.text)
            try:
                declar.find_element_by_class_name('table_termine')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de dossier terminé")
        elif state == 'autorise':
            self.selenium.execute_script("window.scroll(0, 600)")
            bloc = self.selenium.find_element_by_xpath(
                "//*[starts-with(@id, 'btn')][contains(@id, '_etat_autorise')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element_by_class_name('test_nb_auto').text)
            declar = self.selenium.find_element_by_tag_name('tbody')
            self.assertIn('Manifestation_Test', declar.text)
            try:
                declar.find_element_by_class_name('table_termine')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de manifestation autorisée")
        elif state == 'interdite':
            self.selenium.execute_script("window.scroll(0, 600)")
            bloc = self.selenium.find_element_by_xpath("//*[starts-with(@id, 'btn')][contains(@id, '_etat_interdite')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element_by_class_name('test_nb_inter').text)
            declar = self.selenium.find_element_by_tag_name('tbody')
            self.assertIn('Manifestation_Test', declar.text)
            try:
                declar.find_element_by_class_name('table_termine')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de manifestation interdite")
        elif state == 'annule':
            self.selenium.execute_script("window.scroll(0, 600)")
            bloc = self.selenium.find_element_by_xpath("//*[starts-with(@id, 'btn')][contains(@id, '_etat_annule')]")
            bloc.click()
            self.assertEqual('1', bloc.find_element_by_class_name('test_nb_annul').text)
            declar = self.selenium.find_element_by_tag_name('tbody')
            self.assertIn('Manifestation_Test', declar.text)
            try:
                declar.find_element_by_class_name('table_termine')
            except NoSuchElementException:
                self.assertTrue(False, msg="pas de manifestation annulée")
        else:
            self.assertEqual('0', '1', 'paramètre inconnu')
