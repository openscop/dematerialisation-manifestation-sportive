from django.contrib import admin
from django.db.models import Q
from django.urls import reverse
from django.utils.safestring import mark_safe
from related_admin import RelatedFieldAdmin

from messagerie.models import *


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    exclude = ["corps"]
    readonly_fields = ["corps_html", "content_object", "content_type", "object_id"]

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}

    def corps_html(self, obj):
        return mark_safe(obj.corps)

    def reponse(self, obj):


            if obj.reponse:
                old = Enveloppe.objects.filter(Q(reponse=obj.reponse) | Q(pk=obj.reponse)).filter(
                    date__lt=obj.date).order_by("date")
                new = Enveloppe.objects.filter(reponse=obj.reponse).filter(date__gt=obj.date).order_by("date")
            else:
                new = Enveloppe.objects.filter(reponse=obj.pk).order_by("date")
                old = []
            if not new and not old:
                html = "<p>Aucun message en réponse à celui ci</p>"
            else:
                html = ''
                if old:
                    html += "<p>Cette enveloppe répond à : <ul style='margin-left:1em;'>"
                    for enve in old:
                        link = reverse("admin:messagerie_enveloppe_change", args=[enve.id])
                        html += "<li><a href='" + link + "'>" + str(enve.objet) + "</a></li>"
                    html += "</ul></p>"
                if new:
                    html += "<p>Réponse(s) à cette enveloppe :<ul style='margin-left:1em;'>"
                    for enve in new:
                        link = reverse("admin:messagerie_enveloppe_change", args=[enve.id])
                        html += "<li><a href='" + link + "'>" + str(enve.objet) + "</a></li>"
                    html += "</ul></p>"
            return mark_safe(html)


@admin.register(CptMsg)
class CptMsgAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(Filtre)
class FiltreAdmin(admin.ModelAdmin):
    list_display = ('titre', 'utilisateur',)


@admin.register(Enveloppe)
class EnveloppeAdmin(RelatedFieldAdmin):

    list_display = ('date', 'expediteur_txt', 'destinataire_txt', 'manifestation', 'type', "doc_objet",  'objet',)
    search_fields = ["manifestation__nom", "expediteur_txt", "destinataire_txt", 'destinataire_id__username__unaccent',
                     "expediteur_id__username__unaccent", "objet"]
    exclude = ['corps']
    date_hierarchy = 'date'
    list_filter = ("type", "doc_objet", "instance")
    list_per_page = 500
    readonly_fields = ['manifestation', "expediteur_id","expediteur_txt", "destinataire_id", "destinataire_txt",
                       "nombre_destinataire", "objet", "doc_objet","corps_du_message" ,  "type", "date", "manif_terminee",
                       "reponse", "lu_requis", "lu_datetime", "action_traitee", "affichage_messagerie",
                       "email_lie", "notif_bureau", "instance"]

    fieldsets = (
        ("Utilisateurs", {'fields': ("expediteur_id","expediteur_txt", "destinataire_id", "destinataire_txt",
                       "nombre_destinataire", "instance")}),
        ("Corps",
         {'fields': ("objet", "doc_objet", "corps_du_message", "type", "date",
                     )}),
        ("Manifestation",
         {'fields': ('manifestation', "manif_terminee")}),
        ("Notification",
         {'fields': ( "lu_requis", "lu_datetime", "action_traitee", "affichage_messagerie",
                     "email_lie", "notif_bureau")}),
    )

    def get_queryset(self, request):
        queryset = super(EnveloppeAdmin, self).get_queryset(request)
        if not request.user.has_group("Administrateurs technique") and not request.user.is_superuser:
            queryset = queryset.filter(instance=request.user.get_instance()).exclude(type='conversation')
        return queryset

    def corps_du_message(self, obj):
        if obj.type=='conversation':
            link = reverse("messagerie:message", args=[obj.id])  # model name has to be lowercase
            return mark_safe(u'<a href="%s" target="_blank">Voir la conversation dans une nouvelle fenêtre</a>' % (link))
        else:
            return mark_safe('<p>%s<p/>' % (obj.corps.corps))
    corps_du_message.short_description = "Contenu du message"
    corps_du_message.allow_tags = True

    def email_lie(self, obj):
        """
        Ameliorer l'affichage de l'objet email liè à un message
        """
        if not obj.envoi_courriel:
            html = "<p>Aucun</p>"
        else:
            if obj.envoi_courriel.status == 0:
                html = "<p>Email envoyé avec succès à " + obj.envoi_courriel.to[0] + "</p>"
            elif obj.envoi_courriel.status == 1:
                log = obj.envoi_courriel.logs.last().message
                html = "<p> L' email pour " + obj.envoi_courriel.to[0] + " n'a pas pu être envoyé pour la raison suivante : <br>" + \
                                                                        log + "</p>"
            else:
                html = "<p> L' email pour " + obj.envoi_courriel.to[0] + " est en cours d'envoi</p>"
        return mark_safe(html)
    email_lie.short_description = "Email envoyé"
    email_lie.allow_tags = True


