# coding: utf-8
from django.http.response import HttpResponse

from clever_selects.views import ChainedSelectChoicesView
from sports.models.sport import Activite, Discipline


class ActiviteAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les activités selon le champ parent (Discipline) sélectionné """
        data = list(Activite.objects.filter(discipline=self.parent_value if self.parent_value else None).values_list('id', 'name'))
        data = [[item[0], item[1].capitalize()] for item in data]
        return data


def get_aide_from_discipline_id(request):
    """
    Vue AJAX pour renvoyer l'url de la paige d'aide concerné par la discipline
    Renvoie une chaine de caaractère vide dans la cas contraire
    """
    pk = request.GET.get('pk')
    aide = Discipline.objects.get(pk=pk).aide
    if aide:
        return HttpResponse(aide.get_absolute_url())
    else:
        return HttpResponse('')


def get_aide_from_activite_id(request):
    """
    Vue AJAX pour renvoyer l'url de la paige d'aide concerné par la discipline
    Renvoie une chaine de caaractère vide dans la cas contraire
    """
    pk = request.GET.get('pk')
    aide = Activite.objects.get(pk=pk).discipline.aide
    if aide:
        return HttpResponse(aide.get_absolute_url())
    else:
        return HttpResponse('')


def get_discipline_id_from_activite_name(request):
    """
    Vue 'AJAX' pour renvoyer l'ID de la discipline qui correspond au

    nom de l'activité passé en paramètre
    """
    name = request.GET.get('name')
    response = HttpResponse(Discipline.objects.get_by_activite_name(name).pk)
    return response


def get_activite_id_from_name(request):
    """
    Vue 'AJAX' pour renvoyer l'ID de l'activité qui correspond au

    nom passé en paramètre
    """
    name = request.GET.get('name')
    response = HttpResponse(Activite.objects.get_by_name(name).pk)
    return response
