# coding: utf-8
from django.urls import path, re_path

from events.views.autorisationnm import AutorisationNMFilesUpdate
from events.views.declarationnm import DeclarationNMFilesUpdate
from events.views.motorizedconcentration import MotorizedConcentrationFilesUpdate
from events.views.motorizedmanifestation import MotorizedEventFilesUpdate
from events.views.motorizedrace import MotorizedRaceFilesUpdate
from .views import ManifestationStartChoose
from .views import Dashboard
from .views import UnsupportedManifestationCreate
from .views import ManifestationInstructeurUpdate
from .views import AutorisationNMCreate
from .views import AutorisationNMDetail
from .views import AutorisationNMUpdate
from .views import CreateRoute
from .views import DeclarationNMChoose
from .views import DeclarationNMCreate
from .views import DeclarationNMDetail
from .views import DeclarationNMUpdate
from .views import DocumentComplementaireProvideView
from .views import DocumentComplementaireRequestView
from .views import MailToOrganisateursView
from .views import MailToTownHallsView
from .views import ManifestationChoose
from .views import ManifestationPublicDetail
from .views import MotorizedConcentrationCreate
from .views import MotorizedConcentrationDetail
from .views import MotorizedConcentrationUpdate
from .views import MotorizedEventChoose
from .views import MotorizedEventCreate
from .views import MotorizedEventDetail
from .views import MotorizedEventUpdate
from .views import MotorizedRaceCreate
from .views import MotorizedRaceDetail
from .views import MotorizedRaceUpdate
from .views import NonMotorizedEventChoose
from .views import PublicRoadMotorizedEventChoose
from organisateurs.views import StructureCreate


app_name = 'events'
urlpatterns = [

    # Tableau de bord principal
    path('dashboard/', Dashboard.as_view(), name="dashboard"),

    path('mail_to_promoters/', MailToOrganisateursView.as_view(), name='mail_to_promoters'),
    path('mail_to_townhalls/', MailToTownHallsView.as_view(), name='mail_to_townhalls'),
    path('structure/add', StructureCreate.as_view(), name='structure_add'),

    path('events/choose_start/', ManifestationStartChoose.as_view(), name='manifestation_start_choose'),
    path('events/create_route/', CreateRoute.as_view(), name='create_route'),
    path('events/choose/', ManifestationChoose.as_view(), name='manifestation_choose'),
    path('motorizedevents/choose', MotorizedEventChoose.as_view(), name="motorizedevent_choose"),
    path('nonmotorizedevents/choose', NonMotorizedEventChoose.as_view(), name="nonmotorizedevent_choose"),
    path('DeclarationNM/choose', DeclarationNMChoose.as_view(), name="declarationnm_choose"),
    path('publicroadmotorizedevents/choose', PublicRoadMotorizedEventChoose.as_view(), name="publicroadmotorizedevent_choose"),

    # Manifestation sans type (non supporté)
    path('unsupported/add/', UnsupportedManifestationCreate.as_view(), name="unsupported_manifestation_add"),

    # Modifications instructeur
    re_path('event/(?P<pk>\d+)/instruction/', ManifestationInstructeurUpdate.as_view(), name="manifestation_instructeur"),
    re_path('event/(?P<manifestation_pk>\d+)/additional_data/add', DocumentComplementaireRequestView.as_view(), name="additional_data_add"),
    re_path('event/(?P<pk>\d+)/', ManifestationPublicDetail.as_view(), name="manifestation_detail"),

    # DeclarationNM urls
    path('DeclarationNM/add/', DeclarationNMCreate.as_view(), name="declarationnm_add"),
    re_path('DeclarationNM/(?P<pk>\d+)/edit/', DeclarationNMUpdate.as_view(), name="declarationnm_update"),
    re_path('DeclarationNM/(?P<pk>\d+)/edit_files/', DeclarationNMFilesUpdate.as_view(), name="declarationnm_files_update"),
    re_path('DeclarationNM/(?P<pk>\d+)/', DeclarationNMDetail.as_view(), name="declarationnm_detail"),

    # AutorisationNM urls
    path('AutorisationNM/add/', AutorisationNMCreate.as_view(), name="autorisationnm_add"),
    re_path('AutorisationNM/(?P<pk>\d+)/edit/', AutorisationNMUpdate.as_view(), name="autorisationnm_update"),
    re_path('AutorisationNM/(?P<pk>\d+)/edit_files/', AutorisationNMFilesUpdate.as_view(), name="autorisationnm_files_update"),
    re_path('AutorisationNM/(?P<pk>\d+)/', AutorisationNMDetail.as_view(), name="autorisationnm_detail"),

    # MotorizedConcentration urls
    path('motorizedconcentrations/add/', MotorizedConcentrationCreate.as_view(), name="motorizedconcentration_add"),
    re_path('motorizedconcentrations/(?P<pk>\d+)/edit/', MotorizedConcentrationUpdate.as_view(), name="motorizedconcentration_update"),
    re_path('motorizedconcentrations/(?P<pk>\d+)/edit_files/', MotorizedConcentrationFilesUpdate.as_view(), name="motorizedconcentration_files_update"),
    re_path('motorizedconcentrations/(?P<pk>\d+)/', MotorizedConcentrationDetail.as_view(), name="motorizedconcentration_detail"),

    # MotorizedEvent urls
    path('motorizedevents/add/', MotorizedEventCreate.as_view(), name="motorizedevent_add"),
    re_path('motorizedevents/(?P<pk>\d+)/edit/', MotorizedEventUpdate.as_view(), name="motorizedevent_update"),
    re_path('motorizedevents/(?P<pk>\d+)/edit_files/', MotorizedEventFilesUpdate.as_view(), name="motorizedevent_files_update"),
    re_path('motorizedevents/(?P<pk>\d+)/', MotorizedEventDetail.as_view(), name="motorizedevent_detail"),

    # MotorizedRace urls
    path('motorizedraces/add/', MotorizedRaceCreate.as_view(), name="motorizedrace_add"),
    re_path('motorizedraces/(?P<pk>\d+)/edit/', MotorizedRaceUpdate.as_view(), name="motorizedrace_update"),
    re_path('motorizedraces/(?P<pk>\d+)/edit_files/', MotorizedRaceFilesUpdate.as_view(), name="motorizedrace_files_update"),
    re_path('additional_data/(?P<pk>\d+)/', DocumentComplementaireProvideView.as_view(), name="additional_data_provide"),
    re_path('motorizedraces/(?P<pk>\d+)/', MotorizedRaceDetail.as_view(), name="motorizedrace_detail"),

]
