from .models.message import Enveloppe
from core.models import User


def get_carnet(request, manif=None):
    moi = request.user
    carnet = []

    # dans le cas d'un organisateur on prend les instructeurs de ses manifestations
    if hasattr(moi, 'organisateur') and moi.organisateur:
        if manif:
            instructeurs = manif.get_instructeurs_manif()
            for user in instructeurs:
                if isinstance(user, User) and user.is_active:
                    carnet.append(user)

    # puis on rajoute les personnes qui ont envoyé un message à l'user
    messagerecus = Enveloppe.objects.filter(destinataire_id=moi).distinct('expediteur_id').all()
    for messagerecu in messagerecus:
        if messagerecu.expediteur_id and messagerecu.expediteur_id not in carnet and messagerecu.expediteur_id.is_active:
            carnet.append(messagerecu.expediteur_id)

    return carnet
