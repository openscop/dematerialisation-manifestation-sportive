# coding: utf-8
from django.urls import path

from administrative_division.views.ajax import CommuneAJAXView


app_name = 'administrative_division'
urlpatterns = [

    # Widget AJAX
    path('ajax/commune/', CommuneAJAXView.as_view(), name='commune_widget'),

]
