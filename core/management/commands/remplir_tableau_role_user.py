from django.core.management.base import BaseCommand
from django.db import transaction

from core.models import User
from core.util.user import UserHelper

ROLE_TYPES = UserHelper.ROLE_TYPES


class Command(BaseCommand):
    """
    commande pour remplir les roles types de tout le monde
    ils seront dans la forme d'un tableau en string dans le modèle user
    """

    def handle(self, *args, **options):
        with transaction.atomic():
            allusers = User.objects.all()

            for user in allusers:

                roles = []
                for role in ROLE_TYPES:
                    # Tester les rôles dont le modèle est lié directement à User

                    if hasattr(user, role):
                        roles.append(role)

                    # Tester le nom de modèle pour user.agent (uniquement avec parent_link=True)
                    try:
                        model_name = user.agent._meta.model_name
                        if model_name in ROLE_TYPES:
                            roles.append(model_name)
                    except AttributeError:
                        pass
                    # Tester le nom de modèle pour user.agentlocal (uniquement avec parent_link=True)
                    try:
                        model_name = user.agentlocal._meta.model_name
                        if model_name in ROLE_TYPES:
                            roles.append(model_name)
                    except AttributeError:
                        pass
                    # Tester les rôles dont le modèle est lié directement à User.agent (déprécie depuis parent_link)
                    try:
                        if getattr(user.agent, role) is not None:
                            roles.append(role)
                    except AttributeError:
                        pass
                    # Tester les rôles dont le modèle est lié directement à User.agentlocal (déprécié depuis parent_link)
                    try:
                        if getattr(user.agentlocal, role) is not None:
                            roles.append(role)
                    except AttributeError:
                        pass

                stringrole = ','.join(roles)
                user.tableau_role = stringrole
                user.save()
