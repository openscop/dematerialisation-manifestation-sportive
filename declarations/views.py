# coding: utf-8
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView

from core.util.permissions import require_role
from core.util.user import UserHelper
from events.models import Manifestation
from .forms import DeclarationForm
from .forms import DeclarationPublishReceiptForm
from .models import ManifestationDeclaration
from authorizations.models import ManifestationAutorisation


class Dashboard(ListView):
    """ Tableau de bord instructeur pour les manifestations instruites par les mairies"""

    # Configuration
    model = ManifestationDeclaration
    template_name = 'declarations/dashboard_mairieagent.html'

    # Overrides
    @method_decorator(require_role(['instructeur', 'mairieagent']))
    def dispatch(self, *args, **kwargs):
        return super(Dashboard, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        if UserHelper.has_role(self.request.user, 'instructeur'):
            instructeur = self.request.user.instructeur
        else:  # 2 accès possibles : si pas instructeur => mairieagent
            instructeur = self.request.user.agent.mairieagent
        return ManifestationDeclaration.objects.to_process().display_mairie(instructeur).closest_first()

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        if UserHelper.has_role(self.request.user, 'instructeur'):
            instructeur = self.request.user.instructeur
        else:  # 2 accès possibles : si pas instructeur => mairieagent
            instructeur = self.request.user.agent.mairieagent
        context['autorisations'] = ManifestationAutorisation.objects.to_process().display_mairie(instructeur).closest_first()
        return context


class Archives(ListView):
    """ Tableau de bord instructeur pour les manifestations passées"""

    # Configuration
    model = ManifestationDeclaration
    template_name = 'declarations/archives_mairieagent.html'

    # Overrides
    @method_decorator(require_role(['instructeur', 'mairieagent']))
    def dispatch(self, *args, **kwargs):
        return super(Archives, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        if UserHelper.has_role(self.request.user, 'instructeur'):
            instructeur = self.request.user.instructeur
        else:  # 2 accès possibles : si pas instructeur => mairieagent
            instructeur = self.request.user.agent.mairieagent
        return ManifestationDeclaration.objects.finished().display_mairie(instructeur).last_first()

    def get_context_data(self, **kwargs):
        context = super(Archives, self).get_context_data(**kwargs)
        if UserHelper.has_role(self.request.user, 'instructeur'):
            instructeur = self.request.user.instructeur
        else:  # 2 accès possibles : si pas instructeur => mairieagent
            instructeur = self.request.user.agent.mairieagent
        context['autorisations'] = ManifestationAutorisation.objects.finished().display_mairie(instructeur).last_first()
        return context


class DeclarationCreateView(CreateView):
    """ Vue de création de déclaration de manifestation """

    # Configuration
    model = ManifestationDeclaration
    form_class = DeclarationForm

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        manifestation = get_object_or_404(Manifestation, pk=self.kwargs['manifestation_pk'])
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if ManifestationDeclaration.objects.filter(manifestation=manifestation).exists():
            return redirect('events:declarationnm_detail', pk=manifestation.pk)
        return super(DeclarationCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        manifestation = get_object_or_404(Manifestation, pk=self.kwargs['manifestation_pk'])
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if ManifestationDeclaration.objects.filter(manifestation=manifestation).exists():
            return redirect(self.get_success_url())
        # Sinon enregister la nouvelle déclaration
        form.instance.manifestation = manifestation
        return super().form_valid(form)

    def get_success_url(self):
        try:
            self.object.manifestation.declarationnm
        except (ObjectDoesNotExist, AttributeError):
            return reverse('events:motorizedconcentration_detail', kwargs={'pk': self.object.manifestation.pk})
        else:
            return reverse('events:declarationnm_detail', kwargs={'pk': self.object.manifestation.pk})


class DeclarationDetailView(DetailView):
    """ Vue de la déclaration de manifestation """

    # Configuration
    model = ManifestationDeclaration

    # Overrides
    @method_decorator(require_role(['instructeur', 'mairieagent']))
    def dispatch(self, *args, **kwargs):
        return super(DeclarationDetailView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class DeclarationPublishReceiptView(UpdateView):
    """ Vue de publication de récepissé """

    # Configuration
    model = ManifestationDeclaration
    form_class = DeclarationPublishReceiptForm
    template_name_suffix = '_publish_form'

    # Overrides
    @method_decorator(require_role(['instructeur', 'mairieagent']))
    def dispatch(self, *args, **kwargs):
        return super(DeclarationPublishReceiptView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.publish()
        return super(DeclarationPublishReceiptView, self).form_valid(form)

    def get_success_url(self):
        if UserHelper.has_role(self.request.user, 'instructeur'):
            return reverse('authorizations:dashboard')
        else:
            return reverse('declarations:dashboard')
