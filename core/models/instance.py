# coding: utf-8
""" Configuration """
from datetime import timedelta
from smtplib import SMTPException

import logging
from django.conf import settings
from django.core.mail import get_connection, send_mail
from django.db import models
from django.db.utils import IntegrityError

from administrative_division.models.departement import Departement
from core.util.admin import set_admin_info
from core.util.application import get_current_subdomain


logger = logging.getLogger('core')
mail_logger = logging.getLogger('smtp')


class InstanceManager(models.Manager):
    """ Manager pour les instances """

    # Getter
    def get_for_request(self, request):
        """ Renvoyer l'objet d'instance pour l'utilisateur """
        subdomain = get_current_subdomain(request)
        try:
            number = subdomain.strip()
            instance = self.get(departement__name=number)
            return instance
        except (ValueError, Instance.DoesNotExist):
            # Si le sous-domaine n'est pas valide, utiliser l'instance de l'utilisateur
            if hasattr(request, 'user') and request.user.is_authenticated:
                instance = request.user.get_instance()
                return instance
            return self.get_master()
        except:
            return None

    def get_master(self):
        """ Renvoyer l'instance par défaut """
        masters = self.filter(departement__isnull=True)
        if masters.exists():
            return masters.first()
        return self.create(departement=None, name="(Master)")

    def not_master(self):
        """ Renvoyer les instances non master """
        return self.exclude(departement__isnull=True).order_by("name")

    def configured(self):
        """ Renvoyer toutes les instances non master """
        return self.not_master()

    @staticmethod
    def get_for_user(user):
        """ Renvoyer l'instance de configuration utilisée par l'utilisateur """
        return user.get_instance()

    def get_by_natural_key(self, departement):
        return self.get(departement=departement)

    # Setter
    def set_for_departement(self, number):
        """ Inscrire un département : assigner à une nouvelle instance (et/ou renvoyer l'instance) """
        try:
            departement = Departement.objects.db_manager(self.db).get(name=number)
            instance = self.get_or_create(departement=departement)[0]
            if not instance.name:
                instance.name = "Configuration {num}".format(num=number)
                instance.color = "f2f2f2"
                instance.save()
            return instance
        except IntegrityError:
            return self.get(departement__name=number)


class Instance(models.Model):
    """ Configuration de l'application par département """

    # Constantes
    AGREEMENT_DELAY = 21  # délai en jour pour les avis
    AGREEMENT_FEDERATION_DELAY = 28  # délai en jours pour les avis fédération
    WORKFLOW_GGD_CHOICES = [[0, "Avis EDSR"], [1, "Avis GGD + Préavis EDSR"], [2, "Avis GGD"]]
    WORKFLOW_DDSP_CHOICES = [[0, "DDSP"]]
    WORKFLOW_SDIS_CHOICES = [[0, "SDIS"]]
    WORKFLOW_CG_CHOICES = [[0, "CD"]]
    WF_EDSR, WF_GGD_SUBEDSR, WF_GGD_EDSR = 0, 1, 2
    WORKFLOW_NAMES = {'WORKFLOW_EDSR': 0, 'WORKFLOW_GGD_SUBEDSR': 1, 'WORKFLOW_GGD_EDSR': 2}
    WF_DDSP = 0
    WF_SDIS = 0
    WF_CG = 0
    INSTRUCTION_MODES = [[0, "Par arrondissement"], [1, "Par département"], [2, "Par arrondissement (circuit 5)"]]
    IM_ARRONDISSEMENT, IM_DEPARTEMENT, IM_ARRONDISSEMENT_COMPLEXE_5 = 0, 1, 2
    AC_ARRONDISSEMENT = 0
    ACCES_ARRONDISSEMENT_CHOICES = [[0, "Non"], [1, "Oui en lecture seule"], [2, "Oui en Lecture ET Instruction"]]
    EMAIL_HELP = """Paramètres SMTP au format host;port;user;password. Une chaîne vide utilise les paramètres par défaut."""

    # Champs
    name = models.CharField(max_length=96, blank=False, verbose_name="Nom")
    departement = models.OneToOneField('administrative_division.departement', null=True, related_name='instance', verbose_name="Département", on_delete=models.SET_NULL)
    or_map_enabled = models.BooleanField(default=False, verbose_name="Cartes Openrunner activées")
    or_map_required = models.BooleanField(default=False, verbose_name="Cartes Openrunner requises")
    color = models.CharField(max_length=8, blank=True, help_text="ex. FF0000", verbose_name="Code couleur HTML")
    # Workflow
    workflow_ggd = models.SmallIntegerField(default=WF_GGD_SUBEDSR, choices=WORKFLOW_GGD_CHOICES, verbose_name="Workflow GGD")
    workflow_ddsp = models.SmallIntegerField(default=WF_DDSP, choices=WORKFLOW_DDSP_CHOICES, verbose_name="Workflow DDSP")
    workflow_sdis = models.SmallIntegerField(default=WF_SDIS, choices=WORKFLOW_SDIS_CHOICES, verbose_name="Workflow SDIS")
    workflow_cg = models.SmallIntegerField(default=WF_CG, choices=WORKFLOW_CG_CHOICES, verbose_name="Workflow CD")

    # Activation des services
    active_ggd = models.BooleanField(default=True, verbose_name="GGD activé")
    active_ddsp = models.BooleanField(default=True, verbose_name="DDSP activé")
    active_sdis = models.BooleanField(default=True, verbose_name="SDIS activé")
    active_cg = models.BooleanField(default=True, verbose_name="CD activé")

    # Possibilité de rendu direct des avis pour les services complexes
    avis_direct_ggd = models.BooleanField(default=False, verbose_name="autorisé à rendre un avis GGD sans demande de préavis")
    avis_direct_ddsp = models.BooleanField(default=False, verbose_name="autorisé à rendre un avis DDSP sans demande de préavis")
    avis_direct_sdis = models.BooleanField(default=False, verbose_name="autorisé à rendre un avis SDIS sans demande de préavis")
    avis_direct_cg = models.BooleanField(default=False, verbose_name="autorisé à rendre un avis CD sans demande de préavis")

    # Possibilité de rendre l'avis sans attendre la réponse aux demandes de préavis pour les services complexes
    preavis_non_bloquant_ggd = models.BooleanField(default=False, verbose_name="autorisé à rendre un avis GGD sans attendre les réponses aux demandes de préavis")
    preavis_non_bloquant_ddsp = models.BooleanField(default=False, verbose_name="autorisé à rendre un avis DDSP sans attendre les réponses aux demandes de préavis")
    preavis_non_bloquant_sdis = models.BooleanField(default=False, verbose_name="autorisé à rendre un avis SDIS sans attendre les réponses aux demandes de préavis")
    preavis_non_bloquant_cg = models.BooleanField(default=False, verbose_name="autorisé à rendre un avis CD sans attendre les réponses aux demandes de préavis")

    # Modes d'instruction (territoire)
    instruction_mode = models.SmallIntegerField(default=IM_ARRONDISSEMENT, choices=INSTRUCTION_MODES, verbose_name="Mode d'instruction", help_text="L'option \"Par département\" permet à tous les instructeurs de toutes les préfecture & sous-préfectures du département d'être concernés par toutes les manifestations du département. L'option \"Par arrondissement\" oriente les manifestations vers la préfecture ou sous-préfectures de l'arrondissement.")
    acces_arrondissement = models.SmallIntegerField(default=AC_ARRONDISSEMENT, choices=ACCES_ARRONDISSEMENT_CHOICES, verbose_name="Accès aux autres arrondissements", help_text="Attention: Cet élément n'est pris en compte que si l'option \"Par arrondissement\" est sélectionnée dans le choix \"Mode d'instruction\"")

    # Délais légaux par défaut
    manifestation_delay = models.SmallIntegerField(default=30, verbose_name="Délai manifestation")
    manifestation_anm_1dept_delay = models.SmallIntegerField(default=60, verbose_name="Délai autorisé non motorisé sur 1 dépt.")
    manifestation_anm_ndept_delay = models.SmallIntegerField(default=90, verbose_name="Délai autorisé non motorisé sur 2+ dépt.")
    manifestation_dvtm_delay = models.SmallIntegerField(default=60, verbose_name="Délai déclaration véhicules terrestres motorisés")
    manifestation_avtmc_delay = models.SmallIntegerField(default=90, verbose_name="Délai autorisation course de véhicules terrestres motorisés")
    manifestation_avtm_vp_delay = models.SmallIntegerField(default=90, verbose_name="Délai autorisation véhicules terrestres motorisés sur voie publique")
    manifestation_avtm_vn_delay = models.SmallIntegerField(default=90, verbose_name="Délai autorisation véhicules terrestres motorisés hors voie publique")
    manifestation_avtm_homolog_delay = models.SmallIntegerField(default=60,
                                                                verbose_name="Délai autorisation véhicules terrestres motorisés sur circuit homologué")
    avis_delai_federation = models.SmallIntegerField(default=28, verbose_name="Délai de réponse des avis pour les fédérations")
    avis_delai_service = models.SmallIntegerField(default=21, verbose_name="Délai de réponse des avis pour les services")
    avis_delai_relance = models.SmallIntegerField(default=5, verbose_name="Délai d'envoi des relances d'avis")

    action_delai_relance = models.SmallIntegerField(default=14, verbose_name="Délai d'envoi des relance aux instructeurs en cas d'action non lue")

    # Email d'expéditeur pour les notifications de manifestations
    sender_email = models.CharField(max_length=128, default="Manifestationsportive.fr <plateforme@manifestationsportive.fr>", verbose_name="Email d'expéditeur")
    email_settings = models.CharField(max_length=192, blank=True, default='', help_text=EMAIL_HELP, verbose_name="Configuration email")

    objects = InstanceManager()

    # Getter
    def is_master(self):
        """
        Renvoie si cette instance sert de modèle lorsqu'une information est indisponible

        dans une instance de département.
        """
        return self.departement is None

    def get_email(self):
        """ Renvoie l'email d'expéditeur de l'instance """
        return self.sender_email

    @set_admin_info(short_description="Openrunner ?")
    def uses_openrunner(self):
        """ Renvoie si les cartes OpenRunner sont activées sur l'instance """
        return self.or_map_enabled

    @set_admin_info(short_description="Openrunner requis ?")
    def is_openrunner_map_required(self):
        """ Renvoyer si le département nécessite des cartes OR dans les manifs """
        return self.or_map_required

    @set_admin_info(short_description="Couleur")
    def get_default_color(self):
        """ Renvoyer une couleur qui correspond au nom de l'objet """
        return "#{0}".format(hex(hash(self.name.lower()))[-6:])

    def get_workflow_ggd(self):
        """ Renvoyer le workflow de validation """
        return self.workflow_ggd

    def get_workflow_ddsp(self):
        """ Renvoyer le workflow de validation """
        return self.workflow_ddsp

    def get_workflow_sdis(self):
        """ Renvoyer le workflow de validation """
        return self.workflow_sdis

    def get_workflow_cg(self):
        """ Renvoyer le workflow de validation """
        return self.workflow_cg

    @set_admin_info(short_description="Mode d'instruction")
    def get_instruction_mode(self):
        """ Renvoyer le mode d'instruction """
        return self.instruction_mode

    def get_departement(self):
        """ Renvoyer le département """
        return self.departement

    def get_departement_name(self):
        """ Renvoyer le nom du département, ou le territoire """
        name = self.departement.name if self.departement else "Nationale"
        return name.upper()

    def get_departement_friendly_name(self):
        """ Renvoyer le nom entier du département """
        name = self.departement.get_name_display() if self.departement else "France"
        return name

    @staticmethod
    def get_avis_delay(extra_days=0):
        """ Renvoyer le délai standard pour les avis """
        return timedelta(days=Instance.AGREEMENT_DELAY + extra_days)

    @staticmethod
    def get_federation_avis_delay(extra_days=0):
        """ Renvoyer le délai standard pour les avis Fédération """
        return timedelta(days=Instance.AGREEMENT_FEDERATION_DELAY + extra_days)

    def get_manifestation_delay(self):
        """ Renvoyer le délai ou celui par défaut """
        return self.manifestation_delay or Instance.objects.get_master().manifestation_delay

    def get_manifestation_anm_ldept_delay(self):
        """ Renvoyer le délai ou celui par défaut (Non motorisé + Autorisation) """
        return self.manifestation_anm_1dept_delay or Instance.objects.get_master().manifestation_anm_ldept_delay

    def get_manifestation_anm_ndept_delay(self):
        """ Renvoyer le délai ou celui par défaut (Non motorisé + Autorisation, n>1 départements) """
        return self.manifestation_anm_ndept_delay or Instance.objects.get_master().manifestation_anm_ndept_delay

    def get_manifestation_dvtm_delay(self):
        """ Renvoyer le délai ou celui par défaut """
        return self.manifestation_dvtm_delay or Instance.objects.get_master().manifestation_dvtm_delay

    def get_manifestation_avtmc_delay(self):
        """ Renvoyer le délai ou celui par défaut """
        return self.manifestation_avtmc_delay or Instance.objects.get_master().manifestation_avtmc_delay

    def get_manifestation_avtm_vp_delay(self):
        """ Renvoyer le délai ou celui par défaut """
        return self.manifestation_avtm_vp_delay or Instance.objects.get_master().manifestation_avtm_vp_delay

    def get_manifestation_avtm_vn_delay(self):
        """ Renvoyer le délai ou celui par défaut """
        return self.manifestation_avtm_vn_delay or Instance.objects.get_master().manifestation_avtm_vn_delay

    def get_manifestation_avtm_homolog_delay(self):
        """ Renvoyer le délai ou celui par défaut """
        return self.manifestation_avtm_homolog_delay or Instance.objects.get_master().manifestation_avtm_homolog_delay

    def get_email_settings(self):
        """ Renvoyer les paramètres d'email pour l'instance """
        values = self.email_settings
        if values.strip() == '' or len(values.split(';')) < 4:
            values = "{host};{port};{user};{pwd}".format(host=settings.EMAIL_HOST, port=int(settings.EMAIL_PORT), user=settings.EMAIL_HOST_USER,
                                                         pwd=settings.EMAIL_HOST_PASSWORD)
        values = values.split(';')
        config = {'host': values[0], 'port': int(values[1]), 'username': values[2], 'password': values[3]}
        return config

    def get_color(self):
        """ Renvoyer la couleur de l'instance """
        code = self.color.replace('#', '')
        return "#{0}".format(code or "FAFAFA")

    # Actions
    def send_test_mail(self):
        """ Envoyer un mail de test avec les paramètres de l'instance """
        sender_email = self.get_email() or settings.DEFAULT_FROM_EMAIL
        email_config = self.get_email_settings()
        connection = get_connection(use_tls=True, **email_config)  # utiliser le EMAIL_BACKEND
        try:
            send_mail(subject="[Manifestation Sportive] Message de test Manifestationsportive.fr", message="Ceci est un message de test", from_email=sender_email,
                      recipient_list=['destinataire@aa.aa'], connection=connection)
        except SMTPException:
            mail_logger.exception("core.instance.send_test_mail: failure")
        return True

    # Override
    def save(self, *args, **kwargs):
        if self.pk is None and self.departement is None:
            # Forcer l'unicité de l'instance master
            count = Instance.objects.filter(departement=None).count()
            if count != 0:
                raise IntegrityError("Une seule instance master (number=None) peut exister")
        return super(Instance, self).save(*args, **kwargs)

    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        name = self.name or self.departement.get_name_display() if self.departement else "Master"
        departement_name = self.departement.name if self.departement else "Nationale"
        return "{name} ({dept})".format(name=name, dept=departement_name)

    def natural_key(self):
        return self.departement,

    # Métadonnées
    class Meta:
        verbose_name = "configuration d'instance"
        verbose_name_plural = "configurations d'instance"
        app_label = 'core'
