# coding: utf-8
from crispy_forms.layout import Layout, Fieldset, Field, HTML
from django.template.loader import render_to_string
from django import forms

from core.forms.base import GenericForm
from ..models.dnm import Dnm, Dnmc
from .manif import ManifForm, FIELDS_INITIALS, FIELDS_MANIFESTATION, FIELDS_FILES, FIELDS_MARKUP, FIELDS_NATURA2000, FORM_WIDGETS, FIELDS_CONTACT


class DnmForm(ManifForm):
    """ Formulaire """
    FORM_WIDGETS['detail_police_municipale'] = forms.Textarea(attrs={'rows': 1})
    FORM_WIDGETS['detail_police_nationale'] = forms.Textarea(attrs={'rows': 1})
    FORM_WIDGETS['respect_code_route_detail'] = forms.Textarea(attrs={'rows': 1, 'placeholder': 'nom de la voie et créneau horaire de passage'})
    FORM_WIDGETS['priorite_passage_detail'] = forms.Textarea(attrs={'rows': 1, 'placeholder': 'nom de la voie et créneau horaire de passage'})
    FORM_WIDGETS['usage_temporaire_detail'] = forms.Textarea(attrs={'rows': 1, 'placeholder': 'nom de la voie et créneau horaire de passage'})
    FORM_WIDGETS['usage_privatif_detail'] = forms.Textarea(attrs={'rows': 1, 'placeholder': 'nom de la voie et créneau horaire de passage'})

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML("<p>Vous pouvez enregistrer ce formulaire à tout moment puis le reprendre ultérieurement.</p><hr>"),
                                    Fieldset(*(FIELDS_INITIALS)),
                                    Fieldset(*(FIELDS_MANIFESTATION + ['depart_groupe', 'circul_groupe'])),
                                    Fieldset("Coordonnateur Sécurité", 'securite_nom', 'securite_prenom', 'securite_tel', 'securite_email'),
                                    Fieldset("Régime demandé en matière de circulation publique",
                                             Field('respect_code_route', css_class='traffic'), 'respect_code_route_detail',
                                             Field('priorite_passage', css_class='traffic'), 'priorite_passage_detail',
                                             Field('usage_temporaire', css_class='traffic'), 'usage_temporaire_detail',
                                             Field('usage_privatif', css_class='traffic'), 'usage_privatif_detail'),
                                    Fieldset("Informations sur le dispositif de sécurité de la manifestation",
                                             Fieldset("Véhicules d'accompagnement :", 'nb_vehicules_accompagnement', 'vehicule_ouverture', 'vehicule_debut', 'vehicule_fin', 'vehicule_organisation', css_class='special_fieldset'),
                                             Fieldset("Signaleurs :", 'nb_signaleurs', 'nb_signaleurs_fixes', 'nb_signaleurs_autos', 'nb_signaleurs_motos', css_class='special_fieldset'),
                                             Fieldset("Forces de l'ordre :", 'police_municipale', 'detail_police_municipale', 'police_nationale', 'detail_police_nationale', css_class='special_fieldset')
                                             ),
                                    Fieldset(*FIELDS_MARKUP),
                                    Fieldset(*(FIELDS_NATURA2000[:-1])),
                                    Fieldset(*FIELDS_CONTACT))
        self.ajouter_css_class_requis_aux_champs_etape_0()

    # Meta
    class Meta:
        model = Dnm
        exclude = ('descriptions_parcours', 'parcours_openrunner', 'structure', 'delivrance_titre', 'demande_homologation', 'instance')
        help_texts = {'engagement_organisateur': render_to_string('evenements/forms/help/dnm_engagement_organisateur.txt')}
        widgets = FORM_WIDGETS


class DnmFilesForm(GenericForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(Fieldset(*(FIELDS_FILES + ['itineraire_horaire'])))
        self.helper.form_tag = False

    # Meta
    class Meta:
        model = Dnm
        fields = FIELDS_FILES[1:] + ['itineraire_horaire']


class DnmcForm(DnmForm):
    """ Formulaire """

    # Meta
    class Meta:
        model = Dnmc
        exclude = ('descriptions_parcours', 'parcours_openrunner', 'structure', 'delivrance_titre', 'demande_homologation', 'instance')
        help_texts = {'engagement_organisateur': render_to_string('evenements/forms/help/mc_engagement_organisateur.txt')}
        widgets = FORM_WIDGETS


class DnmcFilesForm(GenericForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(Fieldset(*(FIELDS_FILES + ['dossier_tech_cycl', 'itineraire_horaire'])))
        self.helper.form_tag = False

    # Meta
    class Meta:
        model = Dnmc
        fields = FIELDS_FILES[1:] + ['dossier_tech_cycl', 'itineraire_horaire']
