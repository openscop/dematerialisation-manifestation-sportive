import re, time

from django.test import tag
from post_office.models import Email

from selenium.common.exceptions import NoSuchElementException

from .test_base_selenium import SeleniumCommunClass
from instructions.models import Avis
from messagerie.models import Message


class InstructionAjoutPjTests(SeleniumCommunClass):
    """
    Test d'ajout de PJ avec sélénium pour une Dcnm
        workflow_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    Test sur les services simples avec transfert des PJ en document officiel
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ Pièces Jointes Services Simples (Sel) =============')
        SeleniumCommunClass.init_setup(cls)
        cls.avis_nb = 3
        super().setUpClass()

    @tag('selenium')
    def test_Ajout_PJ(self):
        """
        Test d'ajout de PJ pour les différentes étapes du circuit EDSR pour une autorisationNM
        """

        print('**** test 1 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        reponse = self.client.get('/tableau-de-bord-organisateur/liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file4:
            self.client.post(url_script.group('url'), {'cartographie': file4}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/itineraire_horaire.txt') as file5:
            reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file5}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 2 distribution avis ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes d\'avis', self.selenium.page_source)
        # Distribuer les demandes d'avis de l'événement
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes d\'avis').click()
        time.sleep(self.DELAY*4)
        self.assertIn('Choix des avis', self.selenium.page_source)
        self.chosen_select('id_villes_concernees_chosen', 'Bard')
        self.chosen_select('id_services_concernes_chosen', 'sncf')
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('submit-id-save').click()
        time.sleep(self.DELAY * 6)

        # Vérifier le passage en encours et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1200)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)

        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 3 agent fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.connexion('agent_fede')
        self.presence_avis('agent_fede', 'nouveau')
        self.assertIn('Délai : 28 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # Valider l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.assertIn('pouvez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Aucune', self.selenium.page_source)  # Pièces jointes
        self.selenium.execute_script("window.scroll(0, 500)")
        # Ajouter une pièce jointe
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_1.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence de la pièce jointe
        pj = self.selenium.find_element_by_partial_link_text('.txt')
        nom_pj = pj.get_attribute('text')
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_fede', 'rendu')
        self.deconnexion()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[1].to, [self.prefecture.email])
        self.assertEqual(messages[0].corps, 'Pièce jointe ajoutée à un avis au sujet de la manifestation Manifestation_Test')
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_concerne='federation').id)          # Avis Fédération
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")

        print('**** test 4 instructeur transfert pj ****')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.vue_detail()
        # Avis de la fédération
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']")
        icones = avis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            avis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 200)")
        # Déplier l'avis et transférer en document officiel
        avis.click()
        self.selenium.execute_script("window.scroll(0, 800)")
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']/following::*")
        time.sleep(self.DELAY)
        # Tester le bouton de transfert
        self.assertIn('Transférer en document officiel', carte.text)
        self.selenium.find_element_by_partial_link_text('Transférer en document officiel').click()
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.selenium.find_element_by_xpath("//select[@name='nature']/option[text()='Arrêté de circulation']").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        self.selenium.execute_script("window.scroll(0, 200)")
        # Reprendre l'avis et tester plus de bouton de transfert
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']").click()
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']/following::*")
        self.assertNotIn('Transférer en document officiel', carte.text)
        self.selenium.execute_script("window.scroll(0, 0)")
        self.assertIn('Documents officiels (1)', self.selenium.page_source)
        self.selenium.find_element_by_partial_link_text('Documents officiels (1)').click()
        pan = self.selenium.find_element_by_xpath("//div[@id='officialdata']")
        self.assertIn('Arrêté de circulation', pan.text)
        self.assertIn('Déposé le', pan.text)
        self.assertIn(nom_pj, pan.text)
        self.deconnexion()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 8)
        self.assertEqual(outbox[0].to, [self.organisateur.email])
        self.assertEqual(outbox[1].to, [self.prefecture.email])
        self.assertEqual(outbox[2].to, [self.agent_fede.email])
        self.assertEqual(outbox[3].to, [self.fede.email])
        self.assertEqual(outbox[4].to, [self.agent_mairie.email])
        self.assertEqual(outbox[5].to, [self.commune.email])
        self.assertEqual(outbox[6].to, [self.agent_sncf.email])
        self.assertEqual(outbox[7].to, [self.sncf.email])
        self.assertEqual(messages[0].corps, 'Arrêté de circulation publié pour la manifestation Manifestation_Test')
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "arrete")

        print('**** test 5  agent mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en nouveau
        self.connexion('agent_mairie')
        self.presence_avis('agent_mairie', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # Valider l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.assertIn('pouvez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Aucune', self.selenium.page_source)  # Pièces jointes
        self.selenium.execute_script("window.scroll(0, 500)")
        # Ajouter une pièce jointe
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_2.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence de la pièce jointe
        pj = self.selenium.find_element_by_partial_link_text('.txt')
        nom_pj = pj.get_attribute('text')
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_mairie', 'rendu')
        self.deconnexion()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 2)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[1].to, [self.prefecture.email])
        self.assertEqual(messages[0].corps, 'Pièce jointe ajoutée à un avis au sujet de la manifestation Manifestation_Test')
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_concerne='mairie').id)          # Avis Mairie
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")

        print('**** test 6 instructeur transfert pj ****')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.vue_detail()
        # Avis de la fédération
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'Mairie')][@data-toggle='collapse']")
        icones = avis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            avis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 200)")
        # Déplier l'avis et transférer en document officiel
        avis.click()
        self.selenium.execute_script("window.scroll(0, 800)")
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Mairie')][@data-toggle='collapse']/following::*")
        # Tester le bouton de transfert
        self.assertIn('Transférer en document officiel', carte.text)
        self.selenium.find_element_by_partial_link_text('Transférer en document officiel').click()
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.selenium.find_element_by_xpath("//select[@name='nature']/option[text()='Arrêté de circulation']").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        self.selenium.execute_script("window.scroll(0, 200)")
        # Reprendre l'avis et tester plus de bouton de transfert
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath("//div[contains(string(),'Mairie')][@data-toggle='collapse']").click()
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Mairie')][@data-toggle='collapse']/following::*")
        self.assertNotIn('Transférer en document officiel', carte.text)
        self.selenium.execute_script("window.scroll(0, 0)")
        self.assertIn('Documents officiels (2)', self.selenium.page_source)
        self.selenium.find_element_by_partial_link_text('Documents officiels (2)').click()
        pan = self.selenium.find_element_by_xpath("//div[@id='officialdata']")
        self.assertIn('Arrêté de circulation', pan.text)
        self.assertIn('Déposé le', pan.text)
        self.assertIn(nom_pj, pan.text)
        self.deconnexion()

        print('**** test 7  agent sncf ****')
        # Instruction de l'avis par le service SNCF, vérification de la présence de l'événement en nouveau
        self.connexion('agent_sncf')
        self.presence_avis('agent_sncf', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # Valider l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.assertIn('pouvez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Aucune', self.selenium.page_source)  # Pièces jointes
        self.selenium.execute_script("window.scroll(0, 500)")
        # Ajouter une pièce jointe
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_3.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence de la pièce jointe
        pj = self.selenium.find_element_by_partial_link_text('.txt')
        nom_pj = pj.get_attribute('text')
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_sncf', 'rendu')
        self.deconnexion()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[1].to, [self.prefecture.email])
        self.assertEqual(messages[0].corps, 'Pièce jointe ajoutée à un avis au sujet de la manifestation Manifestation_Test')
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_concerne='service').id)          # Avis Service
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")

        print('**** test 8 instructeur transfert pj ****')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.vue_detail()
        # Avis de la fédération
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'Service')][@data-toggle='collapse']")
        icones = avis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            avis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 200)")
        # Déplier l'avis et transférer en document officiel
        avis.click()
        self.selenium.execute_script("window.scroll(0, 800)")
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Service')][@data-toggle='collapse']/following::*")
        # Tester le bouton de transfert
        self.assertIn('Transférer en document officiel', carte.text)
        self.selenium.find_element_by_partial_link_text('Transférer en document officiel').click()
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.selenium.find_element_by_xpath('//select[@name="nature"]/option[text()="Déclaration d\'annulation"]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        self.selenium.execute_script("window.scroll(0, 200)")
        time.sleep(self.DELAY)
        # Reprendre l'avis et tester plus de bouton de transfert
        self.selenium.find_element_by_xpath("//div[contains(string(),'Service')][@data-toggle='collapse']").click()
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Service')][@data-toggle='collapse']/following::*")
        self.assertNotIn('Transférer en document officiel', carte.text)
        self.selenium.execute_script("window.scroll(0, 0)")
        self.assertIn('Documents officiels (3)', self.selenium.page_source)
        self.selenium.find_element_by_partial_link_text('Documents officiels (3)').click()
        pan = self.selenium.find_element_by_xpath("//div[@id='officialdata']")
        self.assertIn('Déclaration d\'annulation', pan.text)
        self.assertIn('Déposé le', pan.text)
        self.assertIn(nom_pj, pan.text)
        # Vérifier le passage en autorise
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'annule')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.deconnexion()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 8)
        self.assertEqual(outbox[0].to, [self.organisateur.email])
        self.assertEqual(outbox[1].to, [self.prefecture.email])
        self.assertEqual(outbox[2].to, [self.agent_fede.email])
        self.assertEqual(outbox[3].to, [self.fede.email])
        self.assertEqual(outbox[4].to, [self.agent_mairie.email])
        self.assertEqual(outbox[5].to, [self.commune.email])
        self.assertEqual(outbox[6].to, [self.agent_sncf.email])
        self.assertEqual(outbox[7].to, [self.sncf.email])
        self.assertEqual(messages[0].corps, 'déclaration d\'annulation publiée pour la manifestation Manifestation_Test')
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "arrete")
