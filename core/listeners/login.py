# coding: utf-8

from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.dispatch.dispatcher import receiver

from core.models.user import User
from messagerie.models import CptMsg


@receiver(user_logged_in, sender=User)
def login_actions(sender, request, user, **kwargs):
    """ Traiter la connexion d'un utilisateur """
    # Si l'utilisateur se connecte, ce n'est plus la peine de le marquer comme ayant changé de login
    from messagerie.models import Message
    Message.objects.creer_et_envoyer('tracabilite', None, [user], 'Connexion à la plateforme', 'Connexion à la plateforme')
    cpt = CptMsg.objects.get(utilisateur=user)
    cpt.update_cpt()
    if user.status == User.USERNAME_CHANGED:
        user.status = User.NORMAL
        user.save()

@receiver(user_logged_out, sender=User)
def logout_actions(sender, request, user, **kwargs):
    # envoyer un msg de traçabilité à al deco
    destinataire = User.objects.get(username=user)
    from messagerie.models import Message
    Message.objects.creer_et_envoyer('tracabilite', None, [destinataire], 'Déconnexion de la plateforme', 'Déconnexion de la plateforme')