# coding: utf-8
from django.views.generic import ListView

from protected_areas.models import RNR, SiteN2K
from administrative_division.models.departement import Departement

class RNRList(ListView):
    """ Afficher la liste des réserves naturelles régionales """

    # Configuration
    model = RNR

    def get_queryset(self):
        if Departement.objects.filter(name=self.kwargs['dept']).count() == 0:
            return RNR.objects.all()
        return RNR.objects.filter(departement__name=self.kwargs['dept'])


class SiteN2KList(ListView):
    """ Afficher la liste des sites Natura 2000 """

    # Configuration
    model = SiteN2K

    def get_queryset(self):
        if Departement.objects.filter(name=self.kwargs['dept']).count() == 0:
            return SiteN2K.objects.all()
        return SiteN2K.objects.filter(departement__name=self.kwargs['dept'])
