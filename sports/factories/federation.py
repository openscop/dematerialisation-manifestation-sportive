# coding: utf-8
import factory

from sports.models import Federation, Discipline


class FederationFactory(factory.django.DjangoModelFactory):
    """ Factory fédération sportive """

    # Champs
    name = factory.Sequence(lambda n: 'Federation{0}'.format(n))
    email = factory.Sequence(lambda n: 'fede{0}@federation.fr'.format(n))
    level = Federation.NATIONAL

    @factory.post_generation
    def disciplines(self, create, extracted, **kwargs):
        for discipline in Discipline.objects.all():
            self.save()
            self.disciplines.add(discipline)

    # Meta
    class Meta:
        model = Federation
