import re, time

from django.test import tag

from .test_base_selenium import SeleniumCommunClass


class InstructionCircuit_EDSRTests(SeleniumCommunClass):
    """
    Test du circuit d'instance EDSR avec sélénium pour une Dnm
        workflow_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ WF_EDSR (Sel) =============')
        SeleniumCommunClass.init_setup(cls)
        cls.avis_nb = 6
        super().setUpClass()

    @tag('selenium')
    def test_Circuit_EDSR(self):
        """
        Test des différentes étapes du circuit EDSR pour une autorisationNM
        """

        print('**** test 0 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        reponse = self.client.get('/tableau-de-bord-organisateur/liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file4:
            self.client.post(url_script.group('url'), {'cartographie': file4}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/itineraire_horaire.txt') as file5:
            reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file5}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 1 vérification avis; 0 pour tous sauf la fédération ****')
        # Vérification des avis des divers agents
        # GGD
        self.connexion('agent_ggd')
        self.presence_avis('agent_ggd', 'none')
        self.deconnexion()
        # EDSR
        self.connexion('agent_edsr')
        self.presence_avis('agent_edsr', 'none')
        self.deconnexion()
        # Mairie
        self.connexion('agent_mairie')
        self.presence_avis('agent_mairie', 'none')
        self.deconnexion()
        # CG
        self.connexion('agent_cg')
        self.presence_avis('agent_cg', 'none')
        self.deconnexion()
        # CGSuperieur
        self.connexion('agent_cgsup')
        self.presence_avis('agent_cgsup', 'none')
        self.deconnexion()
        # SDIS
        self.connexion('agent_sdis')
        self.presence_avis('agent_sdis', 'none')
        self.deconnexion()

        print('**** test 2 instructeur - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes d\'avis', self.selenium.page_source)
        # Distribuer les demandes d'avis de l'événement
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes d\'avis').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('Choix des avis', self.selenium.page_source)
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis DDSP requis')]/..").click()
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis EDSR requis')]/..").click()
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis SDIS requis')]/..").click()
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis Conseil Dép')]/..").click()
        # time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1200)")

        self.chosen_select('id_villes_concernees_chosen', 'Bard')
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('submit-id-save').click()
        time.sleep(self.DELAY * 8)

        # Vérifier le passage en encours et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1600)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 3 avis fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        self.connexion('agent_fede')
        self.presence_avis('agent_fede', 'nouveau')
        self.assertIn('Délai : 28 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Valider l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_fede', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 4 vérification avis; 0 pour GGD, CGD et Brigade ****')
        # Vérification des avis des divers agents
        # EDSR
        self.connexion('agent_ggd')
        self.presence_avis('agent_ggd', 'none')
        self.deconnexion()
        # CGD
        self.connexion('agent_cgd')
        self.presence_avis('agent_cgd', 'none')
        self.deconnexion()
        # Brigade
        self.connexion('agent_brg')
        self.presence_avis('agent_brg', 'none')
        self.deconnexion()

        print('**** test 5 avis edsr - distribution ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en nouveau
        self.connexion('agent_edsr')
        self.presence_avis('agent_edsr', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_cgd_concerne_chosen', self.cgd.__str__())
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_edsr', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 6 preavis cgd ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en nouveau
        self.connexion('agent_cgd')
        self.presence_avis('agent_cgd', 'nouveau')
        print('\t >>> Informer brigade')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Informer les brigades', self.selenium.page_source)
        # informer les brigades de l'événement
        self.selenium.find_element_by_partial_link_text('Informer les brigades').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('Choisissez les brigades concernées', self.selenium.page_source)
        self.chosen_select('id_brigades_concernees_chosen', 'BTA - Bard')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cgd', 'encours')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        print('\t >>> Rendre préavis')
        # Vérifier l'action disponible
        self.assertIn('Rendre le pré-avis', self.selenium.page_source)
        # Rendre le préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre le pré-avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cgd', 'rendu')
        self.deconnexion()
        print('\t >>> Vérifier brigade')
        self.connexion('agent_brg')
        self.presence_avis('agent_brg', 'encours')
        self.deconnexion()

        print('**** test 7 avis edsr - formattage ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en encours
        self.connexion('agent_edsr')
        self.presence_avis('agent_edsr', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Mettre en forme l\'avis', self.selenium.page_source)
        # Formater l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Mettre en forme l\'avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis favorable')]/..").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # vérification de la présence de l'événement en encours
        self.selenium.execute_script("window.scroll(0, 600)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_edsr', 'rendu')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.deconnexion()

        print('**** test 8 avis ggd ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en encours
        self.connexion('agent_ggd')
        self.presence_avis('agent_ggd', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Rendre l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # vérification de la présence de l'événement en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ggd', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 9 vérification avis; 0 pour Commissariat ****')
        # Vérification des avis des divers agents
        # CGD
        self.connexion('agent_commiss')
        self.presence_avis('agent_commiss', 'none')
        self.deconnexion()

        print('**** test 10 avis ddsp - distribution ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_commissariats_concernes_chosen', 'Commissariat Bard')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérifier le passage en encours
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ddsp', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 11 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        self.connexion('agent_commiss')
        self.presence_avis('agent_commiss', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre le pré-avis', self.selenium.page_source)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        self.selenium.find_element_by_partial_link_text('Rendre le pré-avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_commiss', 'rendu')
        self.deconnexion()

        print('**** test 12 avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en encours
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Rendre l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ddsp', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 13 avis mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en nouveau
        self.connexion('agent_mairie')
        self.presence_avis('agent_mairie', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Valider l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_mairie', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 14 vérification avis; 0 pour CGService ****')
        # Vérification des avis des divers agents
        # CGService
        self.connexion('agent_cgserv')
        self.presence_avis('agent_cgserv', 'none')
        self.deconnexion()

        print('**** test 15 avis cg - distribution ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en nouveau
        self.connexion('agent_cg')
        self.presence_avis('agent_cg', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_services_concernes_chosen', 'STD - STD_test')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérifier le passage en encours
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cg', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 16 preavis cgservice ****')
        # Instruction du préavis par le cgservice, vérification de la présence de l'événement en nouveau
        self.connexion('agent_cgserv')
        self.presence_avis('agent_cgserv', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre le pré-avis', self.selenium.page_source)
        # Rendre le préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre le pré-avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cgserv', 'rendu')
        self.deconnexion()

        print('**** test 17 vérification avis; 0 pour CGSupérieur ****')
        # Vérification des avis des divers agents
        # CGSuperieur
        self.connexion('agent_cgsup')
        self.presence_avis('agent_cgsup', 'none')
        self.deconnexion()

        print('**** test 18 avis cg - formattage ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en encours
        self.connexion('agent_cg')
        self.presence_avis('agent_cg', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Mettre en forme l\'avis', self.selenium.page_source)
        # Mettre en forme l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Mettre en forme l\'avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis favorable')]/..").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # vérification de la présence de l'événement en encours
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cg', 'rendu')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.deconnexion()

        print('**** test 19 agent cgsup ****')
        # Instruction de l'avis par le cgsup, vérification de la présence de l'événement en encours
        self.connexion('agent_cgsup')
        self.presence_avis('agent_cgsup', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Rendre l'avis de l'événementn
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cgsup', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 20 vérification avis; 0 pour Groupement, codis et cis ****')
        # Vérification des avis des divers agents
        # CIS
        self.connexion('agent_cis')
        self.presence_avis('agent_cis', 'none')
        self.deconnexion()
        # CODIS
        self.connexion('agent_codis')
        self.presence_avis('agent_codis', 'none')
        self.deconnexion()
        # SDIS groupement
        self.connexion('agent_group')
        self.presence_avis('agent_group', 'none')
        self.deconnexion()

        print('**** test 21 avis sdis - distribution ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en nouveau
        self.connexion('agent_sdis')
        self.presence_avis('agent_sdis', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_compagnies_concernees_chosen', 'Compagnie/groupement 98')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_sdis', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 22 preavis groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en nouveau
        self.connexion('agent_group')
        self.presence_avis('agent_group', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre le pré-avis', self.selenium.page_source)
        # Rendre le préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre le pré-avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_group', 'rendu')
        self.deconnexion()

        print('**** test 23 avis sdis ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en encours
        self.connexion('agent_sdis')
        self.presence_avis('agent_sdis', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Rendre l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_sdis', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 24 avis codis ****')
        # Vérification de la présence de l'événement
        self.connexion('agent_codis')
        self.presence_avis('agent_codis', 'rendu')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Tester aucune action disponible
        self.aucune_action()
        self.deconnexion()

        print('**** test 25 notification cis du groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en encours
        self.connexion('agent_group')
        self.presence_avis('agent_group', 'encours')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Informer les CIS', self.selenium.page_source)
        # Informer les CIS de l'événement
        self.selenium.find_element_by_partial_link_text('Informer les CIS').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('Choisissez les CIS concernés', self.selenium.page_source)
        self.chosen_select('id_cis_concernes_chosen', 'CIS Bard CIS_test')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_group', 'rendu')
        self.deconnexion()

        print('**** test 26 avis cis ****')
        # Vérification de la présence de l'événement
        self.connexion('agent_cis')
        self.presence_avis('agent_cis', 'rendu')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Tester aucune action disponible
        self.aucune_action()
        self.deconnexion()
