from django.db import models


class LogConsultManager(models.Manager):

    def create_from_list(self, response, request):
        pk_affiche = []
        if hasattr(response, 'context_data') and 'cl' in response.context_data:
            for instance in response.context_data.get('cl').result_list:
                pk_affiche.append(str(instance.pk))
            pk_affiche = ','.join(pk_affiche)
            model_affiche = response.context_data.get('cl').model.__name__
            LogConsult(user=request.user, model=model_affiche, pk_affiche=pk_affiche, type='liste').save()

    def create_from_unique(self, response, request):
        if response.context_data.get('object_id'):
            pk_affiche = response.context_data.get('object_id')
            model_affiche = response.context_data.get('original').__class__.__name__
            LogConsult(user=request.user, model=model_affiche, pk_affiche=pk_affiche, type='unique').save()


class LogConsult(models.Model):
    """Log des consultation de model"""
    TYPE = (('liste', 'Liste'), ('unique', 'Unique'))

    #Champs
    user = models.ForeignKey('core.user', verbose_name='Utilisateur', related_name='logconsult', on_delete=models.CASCADE)
    model = models.CharField(max_length=150, verbose_name='Model')
    pk_affiche = models.CharField(max_length=500, verbose_name='Pk affiché')
    type = models.CharField(max_length=50, choices=TYPE, verbose_name='Type de consultation')
    date = models.DateTimeField(auto_now_add=True)

    objects = LogConsultManager()

    def __str__(self):
        return str(self.date) + self.model

    class Meta:
        app_label = 'core'
        verbose_name = "entrées d'historique de consultation"
