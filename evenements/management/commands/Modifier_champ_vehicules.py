# coding: utf-8
from django.core.management.base import BaseCommand
from django.db import transaction

from evenements.models import Avtm, Avtmcir, Dvtm


class Command(BaseCommand):
    """
    Remplir les modèles de l'application "evaluation_incidence" avec ceux de "protected_areas"
    """
    args = ''
    help = "Préparer le champ 'véhicules' des manifestations VTM pour changer de Text à Integer"

    def add_arguments(self, parser):
        parser.add_argument('--no-input', action='store_false', dest='interactive', default=True,
                            help="Ne pas demander de validation de l'utilisateur")

    def handle(self, *args, **options):
        """ Exécuter la commande """
        interactive = options.get('interactive')
        reply = ''

        if interactive is True:
            reply = input("Cette commande va modifier le champ 'véhicules' pour préparer le migrate qui change le type de Text à Integer. Continuer ? [oui/*]\n")

        if reply.lower() in ["oui", "o"] or not interactive:
            with transaction.atomic():
                query_liste = [Avtm.objects.all(), Avtmcir.objects.all(), Dvtm.objects.all()]
                total = 0
                valeur_defaut = 0
                valeur_unique = 0
                valeur_calculee = 0
                for query in query_liste:
                    total += len(query)
                    for vtm in query:
                        # Sortir du champ, les chiffres isolés qui ne sont pas une année
                        liste = vtm.vehicules.split()
                        num_liste = [el for el in liste if el.isdecimal() and not 1880 < int(el) < 2020]
                        if num_liste:
                            if len(num_liste) == 1:
                                vtm.vehicules = num_liste[0]
                                valeur_unique += 1
                            else:
                                # Plusieurs valeurs
                                calcul = 0
                                for num in num_liste:
                                    if liste.index(num) + 1 < len(liste) and \
                                            liste[liste.index(num) + 1] not in ['et', 'à', 'ans', 'ans.', 'cm3', 'cm3.',
                                                                                'cm3)', 'cc', 'temps', 'Temps', 'temps)', 'roues']:
                                        calcul += int(num)
                                    if liste.index(num) + 1 == len(liste):
                                        calcul += int(num)
                                if calcul:
                                    vtm.vehicules = str(calcul)
                                    valeur_calculee += 1
                                else:
                                    vtm.vehicules = "1"
                                    valeur_defaut += 1
                        else:
                            vtm.vehicules = "1"
                            valeur_defaut += 1
                        vtm.save()

                print(str(total) + " entrées à traitées")
                print(str(valeur_defaut) + " valeurs mises par défaut")
                print(str(valeur_unique) + " valeurs uniques trouvées")
                print(str(valeur_calculee) + " valeurs calculées")

        else:
            print("Commande abandonnée.")
