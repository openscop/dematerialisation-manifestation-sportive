# coding: utf-8
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from agreements.models import GGDAvis
from core.models.instance import Instance
from sub_agreements.models.edsr import PreAvisEDSR


if not settings.DISABLE_SIGNALS:

    @receiver(post_save, sender=PreAvisEDSR)
    def notify_preavis_edsr(sender, instance=None, created=None, **kwargs):
        """ Envoyer une notification lorsque le préavis EDSR est créé """
        if created:
            instance.notify_creation(agents=instance.get_agents(), content_object=instance.get_ggd())


    @receiver(post_save, sender=GGDAvis)
    def create_preavis_edsr(sender, instance=None, created=None, **kwargs):
        """ Créer un sous-avis EDSR pour un nouvel avis GGD """
        # Ne créer le préavis EDSR avec l'avis GGD que dans la configuration AgentLocalEDSR
        if instance.get_instance() and instance.concerned_edsr:
            if instance.get_instance().get_workflow_ggd() == Instance.WF_GGD_SUBEDSR:
                avis, _ = PreAvisEDSR.objects.get_or_create(avis=instance, edsr=instance.concerned_edsr)
