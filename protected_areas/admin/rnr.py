# coding: utf-8
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from contacts.admin import ContactInline, AddressInline
from core.util.admin import RelationOnlyFieldListFilter
from ..models import RNR, AdministrateurRNR
from administrative_division.models import Departement


@admin.register(AdministrateurRNR)
class AdministrateurRNRAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    list_display = ['pk', 'name', 'rnr']
    list_filter = ['rnr', ('departement', RelationOnlyFieldListFilter)]
    search_fields = ['name__unaccent', 'rnr__name__unaccent']
    inlines = [AddressInline, ContactInline]
    list_per_page = 25

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(rnr__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['rnr'].queryset = RNR.objects.filter(departement=request.user.get_departement())
        return form


@admin.register(RNR)
class RNRAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration RNR """

    list_display = ['code', 'name', 'departement']
    list_filter = [('departement', RelationOnlyFieldListFilter)]
    search_fields = ['name__unaccent']
    list_per_page = 25

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['departement'].queryset = Departement.objects.filter(id=request.user.get_departement().id)
            form.base_fields['departement'].initial = request.user.get_departement()
        return form
