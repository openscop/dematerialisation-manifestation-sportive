# coding: utf-8
from django.apps import AppConfig


class AdministrationConfig(AppConfig):
    """ Configuration de l'application Administration """

    name = 'administration'
    verbose_name = "Agents et services"

    def ready(self):
        """ Installer les récepteurs de signaux (listeners) """
        from administration import listeners


default_app_config = 'administration.AdministrationConfig'
