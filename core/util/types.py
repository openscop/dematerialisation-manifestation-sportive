# coding: utf-8
""" Utilitaires liés aux types Python """
import re

from django.db.models import QuerySet


def make_iterable(value):
    """
    Renvoyer une liste contenant l'objet, ou l'objet converti en liste si itérable

    :rtype: list
    """
    if not isinstance(value, (list, tuple, set, QuerySet)):
        return [value]
    return list(value)


def one_line(value):
    """ Renvoyer le texte, mais sur une seule ligne """
    output = re.sub(r"\r?\n", " ", value).strip()
    output = output.strip('\n').strip('\r')
    return output
