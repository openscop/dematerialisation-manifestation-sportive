import os, time

from django.test import override_settings
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.hashers import make_password as make
from django.utils.timezone import timedelta, datetime

from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from core.models import Instance
from core.factories import UserFactory
from administration.factories import InstructeurFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from organisateurs.factories import StructureFactory, OrganisateurFactory
from sports.factories import ActiviteFactory
from contacts.factories import ContactFactory


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class OrganisateurTestANM(StaticLiveServerTestCase):
    """
    Test du circuit organisateur avec sélénium
        workflow_GGD : Avis GGD + préavis EDSR
        instruction par arrondissement
        openrunner false par défaut

    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets nécessaires au formulaire de création de manifestation
            Création des fichiers nécessaires à joindre à la création de la manifestation
        """
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(600, 800)

        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__workflow_ggd=Instance.WF_GGD_SUBEDSR,
                                            instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)

        cls.organisateur = UserFactory.create(username='organisateur',
                                              password=make(123),
                                              default_instance=cls.dep.instance)
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        cls.instructeur = UserFactory.create(username='instructeur',
                                             password=make(123),
                                             default_instance=cls.dep.instance)
        InstructeurFactory.create(user=cls.instructeur, prefecture=prefecture)

        cls.structure = StructureFactory(commune=cls.commune, organisateur=organisateur)
        cls.activ = ActiviteFactory.create()
        cls.contact = ContactFactory.create()

        os.chdir("/tmp")
        for file in ('manifestation_rules', 'organisateur_commitment', 'safety_provisions', 'signalers_list',
                     'tech_organisateur_certificate', 'commissioners', 'hourly_itinerary', 'rounds_safety'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()

        super(OrganisateurTestANM, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Suppression des fichiers créés dans /tmp
            Arrêt du driver sélénium
        """
        os.chdir("/tmp")
        for file in ('manifestation_rules', 'organisateur_commitment', 'safety_provisions', 'signalers_list',
                     'tech_organisateur_certificate', 'commissioners', 'hourly_itinerary', 'rounds_safety'):
            os.remove(file+".txt")

        os.system('rm -R /tmp/maniftest/media/42/')

        cls.selenium.quit()
        super(OrganisateurTestANM, cls).tearDownClass()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.find_element_by_css_selector("a").click()
        time.sleep(self.DELAY)
        #WebDriverWait(self.selenium, 10).until( EC.presence_of_element_located((By.TAG_NAME, 'li')))
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()
        #WebDriverWait(self.selenium, 10).until( EC.text_to_be_present_in_element((By.CSS_SELECTOR, 'div#'+id_chosen+' a'), value))


    def test_autorisationNM(self):
        """
        Circuit organisateur pour une autorisation sans véhicules
        :return:
        """
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element_by_name("login")
        pseudo_input.send_keys('organisateur')
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element_value((By.NAME, "login"), 'organisateur'))
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('123')
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element_value((By.NAME, "password"), '123'))
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element((By.ID, "main-content"), 'Connexion avec organisateur réussie'))

        self.selenium.find_element_by_link_text('Déclarer une nouvelle manifestation').click()
        self.selenium.find_element_by_link_text('42 - Loire').click()
        self.selenium.find_element_by_partial_link_text('Manifestation sportive sans').click()
        self.selenium.find_element_by_partial_link_text('Oui').click()
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element((By.ID, "main-content"), 'Organisation d\'une manifestation sportive'))

        name = self.selenium.find_element_by_id('id_name')
        name.send_keys('La grosse course')
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element_value((By.ID, "id_name"), 'La grosse course'))

        begin_date = self.selenium.find_element_by_id('id_begin_date')
        begin_date.clear()
        debut = str(datetime.now() + timedelta(days=100))
        begin_date.send_keys(debut)
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element_value((By.ID, "id_begin_date"), debut))

        end_date = self.selenium.find_element_by_id('id_end_date')
        end_date.clear()
        fin = str(datetime.now() + timedelta(days=100, hours=8))
        end_date.send_keys(fin)
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element_value((By.ID, "id_end_date"), fin))

        description = self.selenium.find_element_by_id('id_description')
        description.send_keys('une grosse course qui déchire')
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element_value((By.ID, "id_description"), 'une grosse course qui déchire'))

        self.chosen_select('id_discipline_chosen', 'Discipline1')
        time.sleep(self.DELAY)

        self.chosen_select('id_activite_chosen', 'Activite1')
        time.sleep(self.DELAY)

        self.chosen_select('id_departure_city_chosen', 'Bard')
        time.sleep(self.DELAY)

        entries = self.selenium.find_element_by_id('id_number_of_entries')
        entries.send_keys('50')
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element_value((By.ID, "id_number_of_entries"), '50'))

        audience = self.selenium.find_element_by_id('id_max_audience')
        audience.send_keys('100')
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element_value((By.ID, "id_max_audience"), '100'))

        contact = self.selenium.find_element_by_xpath("//fieldset[6]")
        champs = contact.find_elements_by_tag_name("input")
        for champ in champs:
            name = champ.get_attribute("id").split('-')[-1]
            if name.lower() == name and name != '' and name != 'id':
                champ.send_keys(getattr(self.contact, name))
                time.sleep(self.DELAY)

        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element((By.ID, "main-content"), 'La Grosse Course'))
        boutons = self.selenium.find_elements_by_css_selector('.page-header li')
        try:
            boutons[0].find_element_by_class_name('fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="Le bouton Etape1 n\'est pas vert")
        try:
            boutons[1].find_element_by_class_name('fa-warning')
        except NoSuchElementException:
            self.assertTrue(False, msg="Le bouton Etape2 n\'est pas rouge")

        files = self.selenium.find_elements_by_css_selector('.bg-primary li')
        for file in files:
            if file.text not in ('Réglement de la manifestation',
                                 'Déclaration et engagement de l\'organisateur',
                                 'Dispositions prises pour la sécurité',
                                 'Cartographie de parcours', 'Liste des signaleurs'):
                self.assertTrue(False, msg="le fichier "+file.text+" n\'est pas dans la liste")

        self.selenium.find_element_by_link_text('Joindre des documents').click()

        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element((By.ID, "main-content"), 'Fichiers attachés'))

        file1 = self.selenium.find_element_by_id('id_manifestation_rules')
        file1.send_keys('/tmp/manifestation_rules.txt')
        file2 = self.selenium.find_element_by_id('id_organisateur_commitment')
        file2.send_keys('/tmp/organisateur_commitment.txt')
        file3 = self.selenium.find_element_by_id('id_safety_provisions')
        file3.send_keys('/tmp/safety_provisions.txt')
        file4 = self.selenium.find_element_by_id('id_signalers_list')
        file4.send_keys('/tmp/signalers_list.txt')
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()

        self.selenium.find_element_by_link_text('Demander l\'autorisation').click()

        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element((By.ID, "main-content"), 'La Grosse Course'))
        boutons = self.selenium.find_elements_by_css_selector('.page-header li')
        try:
            boutons[0].find_element_by_class_name('fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="Le bouton Etape1 n\'est pas vert")
        try:
            boutons[1].find_element_by_class_name('fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="Le bouton Etape2 n\'est pas vert")

        self.selenium.find_element_by_class_name('navbar-toggler').click()
        self.selenium.find_element_by_partial_link_text('Déconnexion').click()
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()

        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element_by_name("login")
        pseudo_input.send_keys('instructeur')
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element_value((By.NAME, "login"), 'instructeur'))
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('123')
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element_value((By.NAME, "password"), '123'))
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element((By.ID, "main-content"), 'Connexion avec instructeur réussie'))
        declar = self.selenium.find_element_by_class_name('authorizations-requested')
        try:
            declar.find_element_by_partial_link_text('La Grosse Course')
            declar.find_element_by_class_name('list-group-item-danger')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en rouge")
