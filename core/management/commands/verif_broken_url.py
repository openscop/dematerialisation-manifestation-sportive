# coding: utf-8
"""
verifier tout les liens mort des pages d'(aide
"""
from django.core.management.base import BaseCommand
from aide.models.aide import PageAide, PanneauPageAide, NotePageAide

import requests
import json


class Command(BaseCommand):

    def handle(self, *args, **options):
        """ la commande va créer un fichier json avec toutes les erreurs. """
        result = []
        resulttemp = []
        # note d'aide
        for aide in NotePageAide.objects.all():
            contenu = aide.content.split('href="')
            count = 0
            provi = []
            for text in contenu:
                if count:
                    deb = text.split('"')[0]
                    url = ""
                    if deb[0] == "/":
                        url = "http://manifestationsportive.fr"+deb
                    elif deb.startswith('http'):
                        url = deb
                    if url:
                        print(url)
                        try:
                            response = requests.get(url)
                            print(response.status_code)
                            if not response.status_code == 200 and not response.status_code == 403:
                                meh = {"url": url, "statut": response.status_code}
                                provi.append(meh)
                        except:
                            meh = {"url": url, "statut": "erreur"}
                            provi.append(meh)
                count += 1
            if len(provi):
                aze = {"nom": aide.title, "result": provi}
                resulttemp.append(aze)
        result.append({"helpnote": resulttemp})
        resulttemp = []
        # panneaux de page d'aide
        for aide in PanneauPageAide.objects.all():
            contenu = aide.content.split('href="')
            count = 0
            provi = []
            for text in contenu:
                if count:
                    deb = text.split('"')[0]
                    url = ""
                    if deb[0] == "/":
                        url = "http://manifestationsportive.fr"+deb
                    elif deb.startswith('http'):
                        url = deb
                    if url:
                        print(url)
                        try:
                            response = requests.get(url)
                            print(response.status_code)
                            if not response.status_code == 200 and not response.status_code == 403:
                                meh = {"url": url, "statut": response.status_code}
                                provi.append(meh)
                        except:
                            meh = {"url": url, "statut": "erreur"}
                            provi.append(meh)
                count += 1
            if len(provi):
                aze = {"nom": aide.title, "result": provi}
                resulttemp.append(aze)
        result.append({"helpaccordionpanel": resulttemp})
        resulttemp = []
        # page d'aide
        for aide in PageAide.objects.all():
            contenu = aide.contentbefore.split('href="')
            count = 0
            provi = []
            for text in contenu:
                if count:
                    deb = text.split('"')[0]
                    url = ""
                    if deb[0] == "/":
                        url = "http://manifestationsportive.fr"+deb
                    elif deb.startswith('http'):
                        url = deb
                    if url:
                        print(url)
                        try:
                            response = requests.get(url)
                            print(response.status_code)
                            if not response.status_code == 200 and not response.status_code == 403:
                                meh = {"url": url, "statut": response.status_code}
                                provi.append(meh)
                        except:
                            meh = {"url": url, "statut": "erreur"}
                            provi.append(meh)
                count += 1
            contenu = aide.contentafter.split('href="')
            count = 0
            provi = []
            for text in contenu:
                if count:
                    deb = text.split('"')[0]
                    url = ""
                    if deb[0] == "/":
                        url = "http://manifestationsportive.fr" + deb
                    elif deb.startswith('http'):
                        url = deb
                    if url:
                        print(url)
                        try:
                            response = requests.get(url)
                            print(response.status_code)
                            if not response.status_code == 200 and not response.status_code == 403:
                                meh = {"url": url, "statut": response.status_code}
                                provi.append(meh)
                        except:
                            meh = {"url": url, "statut": "erreur"}
                            provi.append(meh)
                count += 1
            if len(provi):
                aze = {"nom": aide.title, "result": provi}
                resulttemp.append(aze)
        result.append({"helpaccordionpage": resulttemp})

        f = open("result.json", "w+")
        jsonresult = json.dumps(result)
        f.write(str(jsonresult))
        f.close()
