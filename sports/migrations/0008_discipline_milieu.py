# -*- coding: utf-8 -*-
# Generated by Django 1.9.11 on 2016-12-13 07:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sports', '0007_federation_region'),
    ]

    operations = [
        migrations.AddField(
            model_name='discipline',
            name='milieu',
            field=models.SmallIntegerField(choices=[[0, 'Nautique'], [1, 'Terrestre'], [2, 'Aérien']], default=1, verbose_name='Milieu'),
        ),
    ]
