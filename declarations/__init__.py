# coding: utf-8
from django.apps.config import AppConfig


class DeclarationsConfig(AppConfig):
    """ Configuration de l'application Déclarations """

    name = 'declarations'
    verbose_name = "Déclarations de manifestations"

    # Django est prêt
    def ready(self):
        """ Installer les récepteurs de signaux (listeners) """
        from declarations import listeners


default_app_config = 'declarations.DeclarationsConfig'
