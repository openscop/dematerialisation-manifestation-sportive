# coding: utf-8
from django.db import models

from administrative_division.mixins.divisions import ManyPerArrondissementMixin


class CommuneQuerySet(models.QuerySet):
    """ Queryset des communes """

    # Getter
    def by_arrondissement(self, arrondissement):
        """
        Renvoyer toutes les communes d'un arrondissement

        :type arrondissement: administrative_division.models.Arrondissement
        """
        return self.filter(arrondissement=arrondissement)

    def by_departement(self, departement):
        """
        Renvoyer toutes les communes d'un département

        :type departement: administrative_division.models.Departement
        """
        return self.filter(arrondissement__departement=departement)

    def by_departement_name(self, name):
        """
        Renvoyer toutes les communes d'un département portant le nom passé en argument

        :param name: nom du département
        :type name: str
        :return: un Queryset de communes
        """
        return self.filter(arrondissement__departement__name=name)

    def get_by_natural_key(self, code, name):
        return self.get(name=name, code=code)


class Commune(ManyPerArrondissementMixin):
    """ Division administrative de niveau 4 (commune) """

    # Champs
    code = models.CharField("Code INSEE", unique=True, max_length=5)
    name = models.CharField("Nom", max_length=255)
    zip_code = models.CharField("Code postal", max_length=5)
    email = models.EmailField("e-mail", blank=True, null=True, max_length=200)
    # Position géographie (en coordonnées WGS84)
    latitude = models.DecimalField("Latitude", max_digits=8, decimal_places=5, null=True, blank=True)
    longitude = models.DecimalField("Longitude", max_digits=8, decimal_places=5, null=True, blank=True)
    objects = CommuneQuerySet.as_manager()

    # Overrides
    def __str__(self):
        return self.name

    def natural_key(self):
        return self.code, self.name

    # Getter
    def get_full_name(self):
        """ Renvoyer un nom complet formaté pour la commune """
        return "{code} {name} ({dept})".format(code=self.code, name=self.name, dept=self.get_departement().name)

    def get_zipcoded_name(self):
        """ Renvoyer un nom complet formaté pour la commune """
        return "{code} {name}".format(code=self.zip_code, name=self.name)

    def get_mairieagents(self):
        """ Renvoyer les agents de mairie pour la commune """
        return self.mairieagents.all()

    def get_service_users(self):
        """ Renvoyer les agents de mairie pour la commune """
        return [agent.user for agent in self.mairieagents.all()]

    def get_prefecture(self):
        """ Renvoyer la préfecture de la commune """
        return self.arrondissement.prefecture
    get_prefecture.short_description = "préfecture"

    def get_instance(self):
        """ Renvoyer l'instance du département de la commune """
        return self.arrondissement.departement.get_instance()

    def get_service_users(self):
        """ Renvoyer les agents du service """
        return [agent.user for agent in self.mairieagents.all()]

    # Meta
    class Meta:
        verbose_name = 'commune'
        app_label = 'administrative_division'
        ordering = ['name']
        default_related_name = 'communes'
