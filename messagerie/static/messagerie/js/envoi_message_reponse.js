$(function() { showcontexthelps(); });

// envoi du mail si tout est ok
var tentative_envoi = 0;
$('#formmail_rep').submit(function (e) {
    e.preventDefault();
    $("#id_corps").val(CKEDITOR.instances.id_corps.getData())
   if (!$('#msg_objet').val()){
        $('#msg_objet').css('border', "red 1px solid")
    }

    else {
        send_le_mail();
    }

})


// envoi du mail, le problème étant que ckeditor, et l'ajax c'est pas ça.
// le premier envoi donnera un formulaire incomplet. on le renvoi donc une seconde fois.
// si la seconde fois n'est pas passé on arrete l'erreur n'étant plus celà
function send_le_mail() {
    var data = $('#formmail_rep').serialize();
    $.ajax({
        url: url_envoi_reponse+"?pk="+enve_pk,
        type: 'POST',
        data : data
    }).done(function (response) {
        $('#msg_envoi_alert').html("<div class='alert alert-success'>Message envoyé</div>")
        $('#envoi_reponse_div').fadeOut().empty()
        $('#show_mail').trigger( "click" );
    }).fail(function (response) {
        if (response.status===403 && tentative_envoi<2){
            tentative_envoi++;
            send_le_mail();
        }
        else{
            $('#msg_envoi_alert').html("<div class='alert alert-Kdanger'>Erreur d'envoi du message</div>")
        }
    })
}
