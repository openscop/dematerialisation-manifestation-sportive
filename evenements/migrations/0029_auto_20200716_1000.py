# Generated by Django 2.2.12 on 2020-07-16 08:00

import configuration.directory
import core.FileTypevalidator
import django.core.validators
from django.db import migrations, models
import re


class Migration(migrations.Migration):

    dependencies = [
        ('evenements', '0028_auto_20200708_1514'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dcnm',
            name='detail_police_nationale',
            field=models.TextField(blank=True, default='', verbose_name='Précisez les moyens affectés'),
        ),
        migrations.AlterField(
            model_name='dcnmc',
            name='detail_police_nationale',
            field=models.TextField(blank=True, default='', verbose_name='Précisez les moyens affectés'),
        ),
        migrations.AlterField(
            model_name='dnm',
            name='detail_police_nationale',
            field=models.TextField(blank=True, default='', verbose_name='Précisez les moyens affectés'),
        ),
        migrations.AlterField(
            model_name='dnmc',
            name='detail_police_nationale',
            field=models.TextField(blank=True, default='', verbose_name='Précisez les moyens affectés'),
        ),
    ]
