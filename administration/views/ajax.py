# coding: utf-8
import json

from django.http import HttpResponse

from clever_selects.views import ChainedSelectChoicesView, View

from administration.models.service import *
from administrative_division.models.commune import Commune
from administrative_division.models.departement import LISTE_DEPARTEMENT
from core.models import Instance
from sports.models import Federation
from emergencies.models import Association1ersSecours

class ServiceComplexeAJAXView(View):
    """ pour pour donner la liste des services complexe en fonction du département"""

    def get(self, request):
        if not request.GET.get('pk', None):
            return HttpResponse('{}')
        instance = Instance.objects.get(pk=int(request.GET.get('pk', 1)))
        service_liste = [
            ['', '---------'],
            ['Prefecture', 'Préfecture'],
            ['Commune', 'Mairie'],
            ['Federation', 'Fédération sportive'],
            ['CG', 'Conseil Départemental'],
            ['CGn+1', 'Conseil Départemental n+1'],
            ['CG_Service', 'Services du Conseil Départemental'],
            ['GGD', 'Groupement de Gendarmerie Départemental'],
            ['CGD', 'Compagnie de Gendarmerie Départementale'],
            ['EDSR', 'Escadron Départemental de Sécurité Routière'],
            ['Brigade', 'Brigade de gendarmerie'],
            ['DDSP', 'Direction Départementale de Sécurité Publique'],
            ['Commissariat', 'Commissariat de Police'],
            ['SDIS', 'Service Départemental Incendie et Secours'],
            ['CODIS', 'Center d\'Opérations Départemental Incendie et Secours'],
            ['Compagnie', 'Compagnie SDIS'],
            ['CIS', 'Caserne / Service feu'],
            ['Association1ersSecours', 'Associations de premiers secours'],
            ['Service', 'Autres services...'],
        ]
        if not instance.active_ddsp:
            service_liste.remove(['DDSP', 'Direction Départementale de Sécurité Publique'])
            service_liste.remove(['Commissariat', 'Commissariat de Police'])
        if not instance.active_ggd:
            service_liste.remove(['GGD', 'Groupement de Gendarmerie Départemental'])
            service_liste.remove(['EDSR', 'Escadron Départemental de Sécurité Routière'])
            service_liste.remove(['CGD', 'Compagnie de Gendarmerie Départementale'])
            service_liste.remove(['Brigade', 'Brigade de gendarmerie'])
        if not instance.active_sdis:
            service_liste.remove(['SDIS', 'Service Départemental Incendie et Secours'])
            service_liste.remove(['CODIS', 'Center d\'Opérations Départemental Incendie et Secours'])
            service_liste.remove(['Compagnie', 'Compagnie SDIS'])
            service_liste.remove(['CIS', 'Caserne / Service feu'])
        if not instance.active_cg:
            service_liste.remove(['CG', 'Conseil Départemental'])
            service_liste.remove(['CGn+1', 'Conseil Départemental n+1'])
            service_liste.remove(['CG_Service', 'Services du Conseil Départemental'])
        txt = json.dumps(service_liste)
        return HttpResponse(txt)

class ServiceAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les activités selon le champ parent (Discipline) sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = list(Service.objects.filter(departements=int(self.parent_value)).values_list('id', 'name'))
        elif isinstance(self.parent_value, (list, tuple)):
            result = list(Service.objects.filter(departements__in=self.parent_value).values_list('id', 'name'))
        else:
            result = []
        return result


class CommuneAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les communes selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = Commune.objects.filter(arrondissement__departement=departement)
            for commune in liste:
                result.append((str(commune.id), commune.name + ' - ' + commune.zip_code))
        else:
            result = []
        return result


class PrefectureAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les préfectures selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = Prefecture.objects.filter(arrondissement__departement=departement)
            for prefecture in liste:
                result.append((str(prefecture.id), prefecture.__str__()))
        else:
            result = []
        return result


class CGServiceAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            result = list(CGService.objects.filter(cg__departement=departement).values_list('id', 'name'))
        else:
            result = []
        return result


class CGDAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = CGD.objects.filter(commune__arrondissement__departement=departement)
            for cgd in liste:
                result.append((str(cgd.id), cgd.__str__()))
        else:
            result = []
        return result


class BrigadeAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = Brigade.objects.filter(cgd__commune__arrondissement__departement=departement).order_by('commune__name')
            for brigade in liste:
                result.append((str(brigade.id), brigade.__str__()))
        else:
            result = []
        return result


class CommissariatAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = Commissariat.objects.filter(commune__arrondissement__departement=departement)
            for commissariat in liste:
                result.append((str(commissariat.id), commissariat.__str__()))
        else:
            result = []
        return result


class CompagnieAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = Compagnie.objects.filter(sdis__departement=departement)
            for compagnie in liste:
                result.append((str(compagnie.id), compagnie.__str__()))
        else:
            result = []
        return result


class CISAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = CIS.objects.filter(compagnie__sdis__departement=departement)
            for cis in liste:
                result.append((str(cis.id), cis.__str__()))
        else:
            result = []
        return result


class AutreServiceAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = Service.objects.filter(departements=departement).order_by('name')
            for autre in liste:
                result.append((str(autre.id), autre.__str__()))
        else:
            result = []
        return result


class FederationAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les fédérations selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste = Federation.objects.filter(level=Federation.DEPARTEMENTAL, departement=departement)
            for fede in liste:
                result.append((str(fede.id), fede.name))
        else:
            result = []
        return result


class SecoursAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les services de secours selon le champ parent Instance sélectionné """
        if isinstance(self.parent_value, (str, int)):
            result = []
            departement = Instance.objects.get(pk=int(self.parent_value)).departement
            liste1 = Association1ersSecours.objects.filter(departement=departement)
            # TODO : A vérifier si normal que des objets ne soit pas rattachés à un département
            liste2 = Association1ersSecours.objects.filter(departement__isnull=True)
            liste = liste2 | liste1
            for secours in liste:
                result.append((str(secours.id), secours.__str__()))
        else:
            result = []
        return result
