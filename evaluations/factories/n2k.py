# coding: utf-8

import factory

from evaluations.models import N2KEvaluation
from events.factories import DeclarationNMFactory
from evenements.factories import DnmFactory


class Natura2000EvaluationFactory(factory.django.DjangoModelFactory):
    """ Factory Evaluation Natura2000 """

    # Champs
    range_from_site = 5
    track_total_length = 1
    manifestation = factory.SubFactory(DeclarationNMFactory)

    # Meta
    class Meta:
        model = N2KEvaluation


class Natura2000EvalManifFactory(factory.django.DjangoModelFactory):
    """ Factory Evaluation Natura2000 """

    # Champs
    range_from_site = 5
    track_total_length = 1
    manif = factory.SubFactory(DnmFactory)

    # Meta
    class Meta:
        model = N2KEvaluation
