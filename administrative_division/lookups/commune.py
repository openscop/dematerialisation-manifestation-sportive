# coding: utf-8
from ajax_select import register, LookupChannel
from django.db.models import Q
from unidecode import unidecode

from administrative_division.models.commune import Commune
from evenements.models import Manif
from instructions.models.instruction import Instruction


@register('commune')
class CommuneLookup(LookupChannel):
    """ Lookup AJAX des communes """

    # Configuration
    model = Commune
    min_length = 2

    # Overrides
    def get_query(self, q, request):
        q = unidecode(q)
        try:
            object_id = int(request.META['HTTP_REFERER'].split('/')[-3])
            if request.META['HTTP_REFERER'].split('/')[-4] == 'instruction':
                manif = Instruction.objects.get(pk=object_id).manif
            else:
                manif = Manif.objects.get(pk=object_id)
            dept = manif.get_departements_traverses()
            filtre = Q(arrondissement__departement__in=dept)
        except:
            filtre = Q()
        return self.model.objects.filter(filtre).filter(
            Q(name__unaccent__icontains=q) | Q(code__icontains=q)).order_by('name')[:20]

    def format_item_display(self, item):
        return "<span class='tag'>{code} - {name}</span>".format(code=item.code, name=item.name)

    def format_match(self, item):
        return "<span class='tag'>{code} - {name}</span>".format(code=item.code[:2], name=item.name)

    def check_auth(self, request):
        return True
