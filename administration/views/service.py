from django.views.generic import TemplateView
from django.shortcuts import render
from django.db.models import Count, Q

from administration.models import *
from administrative_division.models.departement import Departement, LISTE_DEPARTEMENT
from administrative_division.models.commune import Commune
from sports.models import Federation
from evaluation_incidence.models import N2kSite, RnrZone
from core.util.user import UserHelper


class ServiceList(TemplateView):

    template_name = "administration/liste_services.html"

    def dispatch(self, request, *args, **kwargs):
        if UserHelper.has_role(request.user, 'instructeur') or \
                "Administrateurs d'instance" in request.user.groups.values_list('name', flat=True):
            return super().dispatch(request, *args, **kwargs)
        return render(request, 'core/access_restricted.html', status=403)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        liste_dept = []
        for d in Departement.objects.configured():
            liste_dept.append((d.name, d))
        context['liste_dept'] = liste_dept

        prefectures = Prefecture.objects.order_by('arrondissement__departement').annotate(
            nb_user_actif=Count('instructeurs', filter=Q(instructeurs__user__is_active=True))).annotate(
            nb_user_nonactif=Count('instructeurs', filter=~Q(instructeurs__user__is_active=True)))

        mairies = Commune.objects.order_by('arrondissement__departement').annotate(
            nb_user_actif=Count('mairieagents', filter=Q(mairieagents__user__is_active=True))).annotate(
            nb_user_nonactif=Count('mairieagents', filter=~Q(mairieagents__user__is_active=True)))

        context['groupes_federations'] = ['Nationales', 'Régionales', 'Départementales']
        federations = Federation.objects.order_by('departement').annotate(
            nb_user_actif=Count('federationagents', filter=Q(federationagents__user__is_active=True))).annotate(
            nb_user_nonactif=Count('federationagents', filter=~Q(federationagents__user__is_active=True)))

        context['groupes_services'] = ['CD', 'Services CD', 'DDSP', 'Commissariats', 'GGD', 'EDSR', 'CGD', 'Brigades',
                                       'SDIS', 'CODIS', 'Compagnies/groupements', 'CIS', '\'Autres\' services']
        cg = CG.objects.order_by('departement').annotate(
            nb_user_actif=Count('cgagents', filter=Q(cgagents__user__is_active=True), distinct=True)).annotate(
            nb_user_nonactif=Count('cgagents', filter=~Q(cgagents__user__is_active=True), distinct=True)).annotate(
            nb_usersup_actif=Count('cgsuperieurs', filter=Q(cgsuperieurs__user__is_active=True), distinct=True)).annotate(
            nb_usersup_nonactif=Count('cgsuperieurs', filter=~Q(cgsuperieurs__user__is_active=True), distinct=True)).annotate(
            relation=Count('cgservices', distinct=True))
        cgservices = CGService.objects.order_by('cg__departement').annotate(
            nb_user_actif=Count('cgserviceagentslocaux', filter=Q(cgserviceagentslocaux__user__is_active=True))).annotate(
            nb_user_nonactif=Count('cgserviceagentslocaux', filter=~Q(cgserviceagentslocaux__user__is_active=True)))
        ddsp = DDSP.objects.order_by('departement').annotate(
            nb_user_actif=Count('ddspagents', filter=Q(ddspagents__user__is_active=True), distinct=True)).annotate(
            nb_user_nonactif=Count('ddspagents', filter=~Q(ddspagents__user__is_active=True), distinct=True)).annotate(
            relation=Count('departement__arrondissements__communes__commissariats', distinct=True))
        commissariats = Commissariat.objects.order_by('commune__arrondissement__departement').annotate(
            nb_user_actif=Count('commissariatagentslocaux', filter=Q(commissariatagentslocaux__user__is_active=True))).annotate(
            nb_user_nonactif=Count('commissariatagentslocaux', filter=~Q(commissariatagentslocaux__user__is_active=True)))
        ggd = GGD.objects.order_by('departement').annotate(
            nb_user_actif=Count('ggdagents', filter=Q(ggdagents__user__is_active=True), distinct=True)).annotate(
            nb_user_nonactif=Count('ggdagents', filter=~Q(ggdagents__user__is_active=True), distinct=True)).annotate(
            relation=Count('departement__arrondissements__communes__cgd', distinct=True) + Count('departement__edsr', distinct=True))
        edsr = EDSR.objects.order_by('departement').annotate(
            nb_user_actif=Count('edsragents', filter=Q(edsragents__user__is_active=True), distinct=True)).annotate(
            nb_user_nonactif=Count('edsragents', filter=~Q(edsragents__user__is_active=True), distinct=True)).annotate(
            nb_localuser_actif=Count('edsragentslocaux', filter=Q(edsragentslocaux__user__is_active=True), distinct=True)).annotate(
            nb_localuser_nonactif=Count('edsragentslocaux', filter=~Q(edsragentslocaux__user__is_active=True), distinct=True)).annotate(
            relation=Count('departement__arrondissements__communes__cgd', distinct=True))
        cgd = CGD.objects.order_by('commune__arrondissement__departement').annotate(
            nb_user_actif=Count('ggdagentslocaux', filter=Q(ggdagentslocaux__user__is_active=True), distinct=True)).annotate(
            nb_user_nonactif=Count('ggdagentslocaux', filter=~Q(ggdagentslocaux__user__is_active=True), distinct=True)).annotate(
            relation=Count('brigades', distinct=True))
        brigades = Brigade.objects.order_by('commune__arrondissement__departement').annotate(
            nb_user_actif=Count('brigadeagents', filter=Q(brigadeagents__user__is_active=True))).annotate(
            nb_user_nonactif=Count('brigadeagents', filter=~Q(brigadeagents__user__is_active=True)))
        sdis = SDIS.objects.order_by('departement').annotate(
            nb_user_actif=Count('sdisagents', filter=Q(sdisagents__user__is_active=True), distinct=True)).annotate(
            nb_user_nonactif=Count('sdisagents', filter=~Q(sdisagents__user__is_active=True), distinct=True)).annotate(
            relation=Count('compagnies', distinct=True))
        codis = CODIS.objects.order_by('departement').annotate(
            nb_user_actif=Count('codisagents', filter=Q(codisagents__user__is_active=True))).annotate(
            nb_user_nonactif=Count('codisagents', filter=~Q(codisagents__user__is_active=True)))
        compagnies = Compagnie.objects.order_by('sdis__departement').annotate(
            nb_user_actif=Count('compagnieagentslocaux', filter=Q(compagnieagentslocaux__user__is_active=True), distinct=True)).annotate(
            nb_user_nonactif=Count('compagnieagentslocaux', filter=~Q(compagnieagentslocaux__user__is_active=True), distinct=True)).annotate(
            relation=Count('ciss', distinct=True))
        cis = CIS.objects.order_by('commune__arrondissement__departement').annotate(
            nb_user_actif=Count('cisagents', filter=Q(cisagents__user__is_active=True))).annotate(
            nb_user_nonactif=Count('cisagents', filter=~Q(cisagents__user__is_active=True)))
        services = Service.objects.order_by('name').annotate(
            nb_user_actif=Count('serviceagents', filter=Q(serviceagents__user__is_active=True))).annotate(
            nb_user_nonactif=Count('serviceagents', filter=~Q(serviceagents__user__is_active=True)))

        n2k = N2kSite.objects.order_by('departements').annotate(admin=Count('operateursiten2k'))
        rnr = RnrZone.objects.order_by('departements').annotate(admin=Count('administrateurrnr'))

        dept = self.request.GET.get('dept')
        dept_obj = Departement.objects.get_by_name(dept)
        if self.request.user.get_departement():
            dept_obj = self.request.user.get_departement()
            context['dept_user'] = dept_obj
            dept = dept_obj.name
        if dept:
            context['prefectures'] = prefectures.filter(arrondissement__departement__name=dept)
            context['mairies'] = mairies.filter(arrondissement__departement__name=dept).order_by('name')
            region = [dep[2] for dep in LISTE_DEPARTEMENT if dep[0] == dept]
            context['federations'] = federations.filter(Q(departement__name=dept) | Q(level=2) | Q(region__in=region))
            cg = cg.filter(departement__name=dept)
            cgservices = cgservices.filter(cg__departement__name=dept)
            ddsp = ddsp.filter(departement__name=dept)
            commissariats = commissariats.filter(commune__arrondissement__departement__name=dept)
            ggd = ggd.filter(departement__name=dept)
            edsr = edsr.filter(departement__name=dept)
            cgd = cgd.filter(commune__arrondissement__departement__name=dept)
            brigades = brigades.filter(commune__arrondissement__departement__name=dept)
            sdis = sdis.filter(departement__name=dept)
            codis = codis.filter(departement__name=dept)
            cis = cis.filter(commune__arrondissement__departement__name=dept)
            compagnies = compagnies.filter(sdis__departement__name=dept)
            services = services.filter(departements__name=dept)
            context['services'] = [cg, cgservices, ddsp, commissariats, ggd, edsr, cgd, brigades, sdis, codis,
                                   compagnies, cis, services]
            context['n2k'] = n2k.filter(departements__name=dept)
            context['rnr'] = rnr.filter(departements__name=dept)
            context['groupes_services'] = ['CD', 'Services CD', 'DDSP', 'Commissariats', 'GGD', 'EDSR', 'CGD',
                                           'Brigades',
                                           'SDIS', 'CODIS', 'Compagnies/groupements', 'CIS', '\'Autres\' services']
            if not dept_obj.get_instance().active_ddsp:
                context['groupes_services'].remove('DDSP')
                context['groupes_services'].remove('Commissariats')
                context['services'].remove(ddsp)
                context['services'].remove(commissariats)
            if not dept_obj.get_instance().active_ggd:
                context['groupes_services'].remove('GGD')
                context['groupes_services'].remove('EDSR')
                context['groupes_services'].remove('CGD')
                context['groupes_services'].remove('Brigades')
                context['services'].remove(ggd)
                context['services'].remove(edsr)
                context['services'].remove(cgd)
                context['services'].remove(brigades)
            if not dept_obj.get_instance().active_sdis:
                context['groupes_services'].remove('SDIS')
                context['groupes_services'].remove('CODIS')
                context['groupes_services'].remove('Compagnies/groupements')
                context['groupes_services'].remove('CIS')
                context['services'].remove(sdis)
                context['services'].remove(codis)
                context['services'].remove(compagnies)
                context['services'].remove(cis)
            if not dept_obj.get_instance().active_cg:
                context['groupes_services'].remove('CD')
                context['groupes_services'].remove('Services CD')
                context['services'].remove(cg)
                context['services'].remove(cgservices)
        else:
            context['prefectures'] = prefectures
            context['mairies'] = mairies.none()
            context['federations'] = federations
            context['services'] = [cg, cgservices, ddsp, commissariats, ggd, edsr, cgd, brigades,
                                   sdis, codis, compagnies, cis, services]
            context['n2k'] = n2k
            context['rnr'] = rnr

        return context
