from django.db import models
from administrative_division.models import Departement



class AbstractInstructionConfig(models.Model):
    """ Configuration des cerfas """

    LISTE_MODE_CONSULT = {"Sans consultation": 0, "Consultation optionnelle": 1, "Consultation obligatoire": 2}
    # Il s'agit des champs et des fichiers requis pendant l'étape pour passer à l'étape suivante
    LISTE_CHAMPS_ETAPE_0 = ['nom', 'date_debut', 'date_fin', 'description', 'descriptions_parcours', 'nb_spectateurs',
                            'nb_organisateurs', 'nom_contact', 'prenom_contact', 'tel_contact']
    LISTE_FICHIERS_ETAPE_0 = ['reglement_manifestation', 'engagement_organisateur', 'disposition_securite']   # parcours_openrunner est traité comme un fichier alors qu'il s'agit d'un élément du formulaire
    LISTE_FICHIERS_ETAPE_1 = []
    LISTE_FICHIERS_ETAPE_2 = ['certificat_assurance', ]

    refCerfa = ""
    consultFedeRequis = True
    modeConsultServices = 0
    delaiDepot = 60  # Délai pour le dépôt de la demande de manifestation
    delaiDepot_ndept = 90  # Délai pour le dépôt de la demande de manifestation sur plusieurs départements
    delaiDocComplement1 = 21  # Délai #1 pour le dépôt de documents requis pour la demande de manifestation
    delaiDocComplement2 = 6  # Délai #2 pour le dépôt de documents requis pour la demande de manifestation

    def get_liste_champs_etape_0(self, departement_pk = None ):
        """ Retourne la liste des champs obligatoire à l'étape 0 de l'instruction, c'est à dire pour être envoyé aux instructeurs """

        # on considère que self est un dossier qui permet de récupérer l'instance de configuration
        # on le prend donc comme objet de travail
        obj = self

        # si self ne permet pas de récupérer l'instance de configuration : cela est le cas lors du formulaire de création
        # et qu'un département est précisé
        if not self.get_instance() and departement_pk:
            # alors utiliser le département (pour récupérer l'instance de configuration)
            obj = Departement.objects.get(pk=departement_pk)

        # si instance de configuration peut être récupérer et openrunner activé et requis
        if obj.get_instance() and obj.get_instance().uses_openrunner() and obj.get_instance().is_openrunner_map_required():
            # ajouter openrunner au champ requis et renvoyer la liste des champs
            return self.LISTE_CHAMPS_ETAPE_0 + ['parcours_openrunner']
        else:
            # sinon renvoyer la liste des champs sur la base du type de dossier
            return self.LISTE_CHAMPS_ETAPE_0

    def get_liste_fichier_etape_0(self, departement_pk=None):
        """ Retourne la liste des fichiers obligatoire à l'étape 0 de l'instruction, c'est à dire pour être envoyé aux instructeurs """

        # on considère que self est un dossier qui permet de récupérer l'instance de configuration
        # on le prend donc comme objet de travail
        obj = self
        liste = self.LISTE_FICHIERS_ETAPE_0[:]
        if self.signature_charte_dispense_site_n2k:
            liste.append('charte_dispense_site_n2k')

        if hasattr(self.cerfa, 'police_nationale') and self.cerfa.police_nationale:
            liste.append('convention_police')

        # si self ne permet pas de récupérer l'instance de configuration : cela est le cas lors du formulaire de création
        # et qu'un département est précisé
        if not self.get_instance() and departement_pk:
            # alors utiliser le département (pour récupérer l'instance de configuration)
            obj = Departement.objects.get(pk=departement_pk)

        # si instance de configuration peut être récupérer et openrunner n'est activé
        if obj.get_instance() and not obj.get_instance().uses_openrunner():
            # ajouter un fichier cartographie aux fichiers requis et renvoyer la liste des fichiers
            return liste + ['cartographie']
        else:
            # sinon renvoyer la liste des fichiers sur la base du type de dossier
            return liste

    # Meta
    class Meta:
        abstract = True
