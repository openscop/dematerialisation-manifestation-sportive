# Fonctionnement de la version #4

Deux nouvelles applications ont été ajoutés :
1. L'application "instructions" qui regroupe les applications "declarations",
"autorizations", "agreements" et "sub_agreements"
2. L'application "evenements" qui remplace l'application "events".

Pour faire cohabiter les versions, il a fallu modifier les applications "notifications"
et "evaluations" et inclure dans les modèles la possibilité de les lier
au nouveau modèle créé.<br>
De plus, quelques bouts de code ont été modifiées pour prendre en compte le nouveau modèle
comme, par exemple, "by_cities" dans le manager de "département"
et un nouveau décorateur dans "organisateur"

### Application *instructions*

##### Les modèles :

- **Instruction**<br>
On retrouve les 7 types de service qui peuvent être consultés.<br>
Parmi ceux-ci, 5 sont uniques par département, un booléen suffit à engager l'avis du service,<br>
2 sont multiples, les mairies et les services divers, une relation plusieurs à plusieurs est nécessaire.<br>
L'état de l'instruction est enregistré grâce une variable créée avec l'application Django-FSM,
Le décorateur **@transition** permet d'utiliser l'appel à une méthode pour faire évoluer l'état.<br>
Les valeurs sont les suivantes :
1. **demandée** quand l'instruction est créée par l'organisateur qui envoie sa demande.
1. **distribuée** quand l'instructeur envoie les demandes d'avis ; méthode envoyerDemandeAvis().
1. **autorisée** quand un document officiel d'autorisation est déposé ; méthode ajouter_autorisation().
1. **interdite** quand un document officiel d'interdiction est déposé ; méthode ajouter_interdiction().
1. **annulée** quand un document officiel d'annulation est déposé ; méthode ajouter_annulation().

- **DocumentOfficiel**<br>
Un seul document était accepté auparavent. Avec ce modèle, il est possible de déposer plusieurs documents officiels.<br>
Il y a cinq types définis, arrêté d'autorisation, arrêté d'interdiction, arrêté d'annulation, arrêté de circulation et récépissé d'autorisation.<br>
La plateforme prends, en compte la nature du document pour afficher l'état des manifestations.

- **Avis**<br>
Plus de spécialisation, ce modèle a donc besoin d'un champ pour renseigner le type de service concerné en remplacement
des modèles dédiés. Il permet d'accèder aux avis par type plus facilement.<br>
Il faut aussi un champ pour stocker le service désigné.
Surtout utile pour les services mairies et divers où plusieurs avis peuvent être demandés, (pour les autres, un seul avis possible),
chaque avis a son service mémorisé dans ce champ pour une question d'homogénéité.<br>
Ce champ est construit grâce à un content_type qui permet de lier à un modèle non défini à l'avance.<br>
Variable d'état de l'avis :
1. **demandé** quand l'avis est créée par l'instructeur qui envoie les demandes.
1. **distribué** quand le service concerné envoie les demandes de préavis ; méthode log_distribution().
1. **transmis** lorsque le traitement d'un avis est delégué  ; méthode notifier_passage_avis().
1. **formaté** quand un avis est formaté ; méthode formatterAvis().
1. **rendu** quand un avis est rendu ; méthode rendreAvis().

- **Preavis**<br>
Plus de spécialisation, ce modèle a donc, comme le modèle AVIS, un champ pour renseigner le type de service concerné
et un champ pour stocker le service.<br>
Variable d'état du préavis :
1. **demandé** quand le préavis est créée par l'agent qui envoie les demandes.
1. **notifié** quand un avis est notifié ; méthode notifier_brigades().
1. **rendu** quand un préavis est rendu ; méthode rendrePreavis().

- **AutorisationAcces**<br>
Ce modèle permet de donner accès  à un avis à un service qui n'a pas à rendre d'avis.<br>
Ce modèle est nécessaire dans le cas du workflow SDIS, il permet de donner accès à l'avis aux CIS qui ont été notifiés.<br>
L'utilisateur qui a créé l'accès est aussi enregistré.

**Gestion des tableaux de bord**

***Pour les utilisateurs de la chaine d'instruction***

Il n'y a qu'un seul tableau de bord pour l'ensemble des utilisateurs de la chaine d'instruction. Il s'adapte en fonction du rôle de l'utilisateur qui est passé en élément d'url après /tableaudebord/\<role> (ceci est géré dans le menu).
Les données sont ensuite recherchées et filtrées en fonction du rôle demandé dans l'url tout en vérifiant que l'utilisateur possède réellement ce rôle.

Il y a une particularité pour les agents en mairie qui ont 2 déclinaisons du tableau de bord :
- en tant que *instructeur* pour les dossiers qui requierent une instruction en mairie (manifestation non motorisé sur une seule commune),
- en tant que *service consulté* pour les dossiers instruits en préfecture pour lesquels une demande d'avis a été envoyé à la mairie.

3 cas spécifiques sont également pris en compte :
- les instructeurs (en préfecture) peuvent accéder aux dossiers des instructions en mairie de l'ensemble de leur territoire (département ou arrondissement). `filtre_specifique == 'instructionmairie'`
- tous les agents peuvent accéder à leurs archives `temporalite == "archive"` 
- en combinant les 2 cas précédent, les instructeurs (en préfecture) peuvent accéder aux archives des instructions en mairie.

NB : un lien est proposé pour accéder aux tableaux de bord de l'ancienne réglementation (avant 2018) qui renvoit vers les applications Django de la version 2 et 3 (declaration et authorization). 

Cf : 
- instructions/views/tableaudebord.py
- instructions/templates/instructions/tableaudebord.html
- portail/templates/base.html


***Pour les organisateurs***

Le tableau des organisateurs affiche tous leurs dossiers.

NB : un lien est proposé pour accéder aux tableaux de bord de l'ancienne réglementation (avant 2018) qui renvoit vers les applications Django de la version 2 et 3 (declaration et authorization).

Cf : 
- evenements/views/dashboard.py
- evenements/templates/evenements/dashboard_organisateur.html
- portail/templates/base.html

### Application *evenements*

##### Les modèles :

- **Manif**<br>
Similaire au modèle de "events".

- **DocumentComplementaire**<br>
Similaire au modèle de "events".

- **SansInstanceManif**<br>
Similaire au modèle de "events".

- **AbstractInstructionConfig**<br>
Classe regroupant les constantes propres à chaque type de manifestations.<br>
On y retrouve les différents délais, le numéro de Cerfa, un booléen pour la consultation de la fédération
et un choix pour la consultation des services entre trois (sans, optionnelle, obligatoire).<br>
Le choix "sans" enlève simplement le bouton d'envoie des demandes d'avis.<br>
Le choix "obligatoire" oblige à tester si des avis ont été demandés quand un document officiel de type arrêté d'autorisation est déposé.

- **AbstractDnm**<br>
Classe mère pour les modèles **Dnm**, **Dnmc**, **Dcnm**, **Dcnmc**.<br>
Elle hérite de **AbstractInstructionConfig** pour incorporer les constantes pour chaque modèle final.<br>
Elle regroupe les champs et méthodes communes.

- **AbstractVtm**<br>
Classe mère pour les modèles **Dvtm**, **Avtm**, **Avtmcir**.<br>
Elle hérite de **AbstractInstructionConfig** pour incorporer les constantes pour chaque modèle final.<br>
Elle regroupe les champs et méthodes communes.

- ****Dnm**, **Dnmc**, **Dcnm**, **Dcnmc**, **Dvtm**, **Avtm**, **Avtmcir**<br>
Classes correspondant aux Cerfas de l'administration.<br>
Elles hérite de **AbstractDnm** ou **AbstractVtm** d'une part et de **Manif** d'autre part.