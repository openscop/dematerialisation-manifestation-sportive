# coding: utf-8
from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from import_export.admin import ExportActionModelAdmin
from ajax_select.helpers import make_ajax_form

from instructions.models.instruction import Instruction
from instructions.admin.avis import AvisInline
from administration.models.service import Service
from core.models import User, LogConsult


class InstructionInline(admin.TabularInline):
    """ Inline admin pour les autorisations """

    # Configuration
    model = Instruction
    extra = 0
    max_num = 1
    can_delete = False
    view_on_site = False
    readonly_fields = ['date_creation', 'etat', 'referent', 'date_consult', 'doc_verif', 'edition_instruction']
    fieldsets = (
        (None, {'fields': ('date_creation', 'etat', 'date_consult', 'referent', 'doc_verif', 'edition_instruction')}),
    )

    def edition_instruction(self, obj):
        if obj.pk:
            url = reverse("admin:instructions_instruction_change", args=[obj.pk])
            return format_html('<a class="btn" href="{}">Instruction</a>', url)
        return '-'
    edition_instruction.short_description = "Voir l'instruction"


@admin.register(Instruction)
class InstructionAdmin(ExportActionModelAdmin, admin.ModelAdmin):
    """ Configuration admin pour l'instruction """

    #configuration
    model = Instruction
    readonly_fields = ['date_creation', 'manif']
    inlines = [AvisInline]
    list_display = ['__str__', 'date_creation', 'etat', 'referent', 'date_consult', 'doc_verif']
    list_filter = ['etat']
    search_fields = ['manif__nom__unaccent', 'referent__username__unaccent', 'manif__description__unaccent']
    form = make_ajax_form(Instruction, {'villes_concernees': 'commune'})
    fieldsets = (
        (None, {'fields': ('manif', ('referent', 'etat'), ('date_creation', 'date_consult'), 'doc_verif')}),
        ("Distribution", {'fields': ('services_concernes', 'villes_concernees', 'ddsp_concerne', 'edsr_concerne',
                             'ggd_concerne', 'sdis_concerne', 'cg_concerne')}),
    )

    def get_field_queryset(self, db, db_field, request):
        instruction_id = int(request.resolver_match.kwargs['object_id'])
        if instruction_id:
            dept = Instruction.objects.get(pk=instruction_id).get_instance().departement
            if db_field.name == 'services_concernes':
                return Service.objects.filter(departements=dept)
            if db_field.name == 'referent':
                return User.objects.by_role('instructeur').filter(default_instance__departement=dept)
        super().get_field_queryset(db, db_field, request)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        referent = form.base_fields['referent']
        service = form.base_fields['services_concernes']

        referent.widget.can_add_related = False
        service.widget.can_add_related = False
        referent.widget.can_delete_related = False
        referent.widget.can_change_related = False

        return form

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(manif__ville_depart__arrondissement__departement=request.user.get_departement())
        return queryset

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        response = super().render_change_form(request, context, add, change, form_url, obj)
        LogConsult.objects.create_from_unique(response, request)
        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}