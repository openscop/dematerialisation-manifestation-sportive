# coding: utf-8
"""
Remplacer tous les mots de passe par 123.
Utilisable uniquement en mode DEBUG.
"""
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db import transaction

from core.util.security import protect_code


class Command(BaseCommand):
    """ Nettoyer et charger les données par défaut de la base de données """
    args = ''
    help = 'Remplace tous les emails des utilisateurs par le paramètre passé ; defaut, elineda@elineda.ovh'

    def add_arguments(self, parser):
        parser.add_argument('email', nargs='?', type=str, default='elineda@elineda.ovh')

    def handle(self, *args, **options):
        """ Exécuter la commande """
        protect_code()

        if settings.DEBUG:
            with transaction.atomic():
                confirm = ''
                email = options['email']
                count=0
                if (email != '123'):
                    print('Les emails seront remplacés par : "' + str( email) + '".')
                    print('Entrez Y pour continuer.')
                    confirm = input()
                if (confirm == 'Y' or confirm == 'y' or email == '123'):
                    count = get_user_model().objects.count()
                    for user in get_user_model().objects.all():
                        user.email=str(count) + email
                        count+=1
                        user.save()
                    print("Les emails de {0} utilisateurs ont été mis à jour.".format(count))
                else:
                    print('Abandon')
        else:
            print("Échec : Par sécurité, vous ne pouvez lancer cette commande qu'en mode DEBUG.")
