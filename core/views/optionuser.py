import json

from django.views.generic import View
from core.models.optionuser import OptionUser
from core.forms.optionuser import OptionUserForm
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect
from webpush.models import PushInformation, SubscriptionInfo


class ParametreNotificationMessagerie(View):
    """
    Modif des paramètres de notification
    """
    # Overrides
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ParametreNotificationMessagerie, self).dispatch(*args, **kwargs)

    def get(self, request):
        option = OptionUser.objects.get(user=request.user)
        form = OptionUserForm(instance=option)
        pushs = PushInformation.objects.filter(user=request.user).all()
        endpoints = []
        for push in pushs:
            endpoint = push.subscription.endpoint
            endpoints.append(endpoint)
        end = json.dumps({"point": endpoints})
        if pushs:
            pushactif = 1
        else:
            pushactif = 0
        return render(request, 'core/parametre_notification_messagerie.html', {'form': form, 'pushactif': pushactif, 'end': end})

    def post(self, request):
        option = OptionUser.objects.get(user=request.user)
        form = OptionUserForm(request.POST, instance=option)

        if form.is_valid():
            form.save()
            if self.request.POST.get('recap'):
                self.request.user.recap=True
            else:
                self.request.user.recap = False
            self.request.user.save()
            return redirect('profile')
        else:
            return redirect('profile')
