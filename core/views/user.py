# coding: utf-8
import logging
import html

from django.http import HttpResponse
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, TemplateView, View
from django.views.generic.edit import UpdateView
from allauth.account.views import SignupView, PasswordChangeView, PasswordResetView
from allauth.account.utils import send_email_confirmation
from allauth.account.forms import ResetPasswordForm
from django.template.loader import render_to_string
from django.core.mail import send_mail, get_connection
from django.contrib.auth import get_user_model

from configuration import settings
from core.models import User
from core.util.user import UserHelper
from core.forms import UserUpdateForm, SignupAgentForm, SignupOrganisateurForm
from messagerie.models import Message
from django.shortcuts import render
from messagerie.models.cptmsg import CptMsg

mail_logger = logging.getLogger('smtp')


class RenvoiMailConfirm(View):
    """
    Vue utilisée par l'admin pour renvoyer un mail de confirmation
    """
    def get(self, request, pk):
        if request.user.has_group('Administrateurs d\'instance') or request.user.is_superuser:
            user = get_object_or_404(User, pk=pk)
            send_email_confirmation(request, user)
            return redirect("admin:core_user_change", pk)
        else:
            return redirect("admin:core_user_change", pk)


class ProfileDetailView(DetailView):
    """ Vue du profil utilisateur """

    # Configuration
    model = get_user_model()

    # Overrides
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProfileDetailView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        role = UserHelper.get_role_instance(self.request.user)
        context['role'] = role
        context['user_is_admin'] = True
        context['user_is_owner'] = True

        return context


class ProfilPublicView(DetailView):
    """ Vue du profil publique utilisateur """

    model = User

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or request.user.has_role('organisateur'):
            return redirect('home_page')
        return super(ProfilPublicView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        user_is_admin = True if user in User.objects.filter(groups__name__contains="Administrateurs d'instance") or \
                                user in User.objects.filter(groups__name__contains="Administrateurs technique") else False
        user_profile = User.objects.get(pk=self.kwargs['pk'])
        cpt_msg = CptMsg.objects.get(utilisateur=user_profile)
        role = UserHelper.get_role_instance(user_profile)
        nb_actif = 0
        nb_inactif = 0
        liste_agents = user_profile.get_service().get_service_users()
        for agent in liste_agents:
            if agent.is_active:
                nb_actif += 1
            else:
                nb_inactif += 1
        context['nb_inactif'] = nb_inactif
        context['nb_actif'] = nb_actif
        context['user_is_admin'] = user_is_admin
        context['role'] = role
        context['cpt_msg'] = cpt_msg
        context['user'] = user
        return context


class PasswordChangeCustomView(PasswordChangeView):
    """
    Evite de rester sur la même vue après un changement de mot de passe
    L'url est interceptée avant transmission à allauth
    """
    success_url = reverse_lazy('profile')


class PasswordResetCustomView(PasswordResetView):
    """
    Class pour ajouter un message de traçabilité à la demande d'un nouveau mot de passe
    """

    def form_valid(self, form):
        response = super(PasswordResetCustomView, self).form_valid(form)
        if form.users:
            user = form.users[0]
            Message.objects.creer_et_envoyer(
                'tracabilite', None, [user], "Demande de nouveau mot de passe",
                "Vous avez sollicité la réinitialisation de votre mot de passe. Pour cela, un lien a été envoyé par email.<br>"
                "Veuillez vous assurer que vous êtes en capacité de recevoir des emails sur cette adresse et que "
                "<strong>le domaine manifestationsportive.fr fait partie des domaines autorisés</strong> par votre messagerie.<br>"
                "Si vous ne savez pas comment faire, consultez cette <a href='/aide/probleme-reception-email' target='_blank'>page d'aide</a>.")
        return response


class PasswordExpired(View):
    """
    Si le mot de passe est perimé on l'efface
    """
    def get(self, request):
        # le mot de passe va être purgé et on va envoyé un mail de reset
        request.user.set_unusable_password()
        request.user.save()
        Message.objects.creer_et_envoyer(
            'tracabilite', None, [request.user], 'Mot de passe expiré',
            "Votre mot de passe a expiré. Celui n'est plus opérationnel. Un nouveau mot de passe doit être défini pour réaccéder à la plateforme.")
        reset_password_form = ResetPasswordForm(data={'email': request.user.email})
        if reset_password_form.is_valid():
            reset_password_form.save(request=request)

            # bof Avertir les administrateurs d'instance
            instance = request.user.get_instance()
            admin_instance_list = User.objects.get_email_admin_instance(instance)
            if admin_instance_list:
                try:
                    sender_email = request.user.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
                    email_config = request.user.get_instance().get_email_settings()
                    connection = get_connection(use_tls=True, **email_config)
                    subject = "Mot de passe expiré"
                    msg = render_to_string('notifications/mail/password_expired.txt', {'username': request.user.username,
                                                                                       'email': request.user.email})
                    send_mail(subject=html.unescape(subject), message=html.unescape(msg),
                              from_email=sender_email,
                              recipient_list=list(admin_instance_list), connection=connection)
                except:
                    pass
            # eof Avertir les administrateurs d'instance

            logout(request)
            return render(request, "core/password_expired.html", status=400)
        else:
            return render(request, "403.html", status=400)


class UserUpdateView(UpdateView):
    """ Modification du profil utilisateur """

    # Configuration
    model = get_user_model()
    form_class = UserUpdateForm
    success_url = '/accounts/profile/'

    # Overrides
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user


class SignupAgentView(SignupView):
    form_class = SignupAgentForm
    template_name = "account/signupagent.html"
    view_name = 'signupagentview'

    def get_context_data(self, **kwargs):
        ret = super(SignupAgentView, self).get_context_data(**kwargs)
        ret.update(self.kwargs)
        return ret


signupagentview = SignupAgentView.as_view()


class SignupOrganisateurView(SignupView):
    form_class = SignupOrganisateurForm
    view_name = 'signuporganisateurview'

    def get_context_data(self, **kwargs):
        ret = super(SignupOrganisateurView, self).get_context_data(**kwargs)
        ret.update(self.kwargs)
        return ret


signuporganisateurview = SignupOrganisateurView.as_view()

class EmailConfirmedView(TemplateView):
    template_name = "account/email_confirmed.html"

    def get_context_data(self, **kwargs):
        context = super(EmailConfirmedView, self).get_context_data(**kwargs)
        context['valid'] = True if self.request.GET.get('valid') == 'true' else False
        return context


@method_decorator(login_required, name='dispatch')
class NomPrenomChange(View):

    def post(self, request):
        last_name = request.POST['nom']
        first_name = request.POST['prenom']
        user = request.user
        user.last_name = last_name
        user.first_name = first_name
        user.save()
        return HttpResponse(200)
