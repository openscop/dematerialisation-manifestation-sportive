import logging

from django.utils import timezone
from django.db.models import Q
from bootstrap_datepicker_plus import DateTimePickerInput
from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from django_filters.rest_framework import DjangoFilterBackend, ModelChoiceFilter, DateTimeFilter, NumberFilter, FilterSet
from drf_yasg.utils import swagger_auto_schema

from api.serializer.manif import (ManifPublicSerializer, ManifestationSerializer,
                                  ManifPJSerializer, DocCompSerializer, DocOfficielSerializer,
                                  DnmSerializer, DnmcSerializer, DcnmSerializer, DcnmcSerializer,
                                  DvtmSerializer, AvtmSerializer, AvtmcirSerializer,
                                  RnrevaluationSerilizer, N2kevaluationSerilizer)
from api.permissions import IsInGroupApi, IsInvolvedInInstruction
from administrative_division.models import Departement, Arrondissement, Commune
from evenements.models import Manif
from sports.models import Activite

api_logger = logging.getLogger('api')


class ManifFilter(FilterSet):
    """
    A utiliser dans les vues Instruction
    """
    departement = ModelChoiceFilter(field_name="manif__ville_depart__arrondissement__departement",
                                    label='departement',
                                    queryset=Departement.objects.all())
    arrondissement = ModelChoiceFilter(field_name="manif__ville_depart__arrondissement",
                                       label='arrondissement',
                                       queryset=Arrondissement.objects.all())
    commune = ModelChoiceFilter(field_name="manif__ville_depart",
                                label='commune',
                                queryset=Commune.objects.all())
    activite = ModelChoiceFilter(field_name="manif__activite",
                                 label='activite',
                                 queryset=Activite.objects.all())
    date_debut = DateTimeFilter(field_name="manif__date_debut",
                                label='date de début de la manifestation',
                                widget=DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'locale': 'fr'}),
                                lookup_expr='gte')
    date_fin = DateTimeFilter(field_name="manif__date_fin",
                              label='date de fin de la manifestation',
                              widget=DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'locale': 'fr'}),
                              lookup_expr='lte')
    annee = NumberFilter(field_name="manif__date_debut",
                         label='année',
                         initial=timezone.now().year,
                         lookup_expr='year')

    # def __init__(self, data=None, *args, **kwargs):
    #     # filterset is bound, use initial values as defaults
    #     if data is not None:
    #         data = data.copy()
    #
    #         # year, begin_date and end_date are either missing or empty, use initial as default for year
    #         if not data.get('annee') and not data.get('date_debut') and not data.get('date_fin'):
    #             data['annee'] = self.base_filters['annee'].extra['initial']
    #
    #     super().__init__(data, *args, **kwargs)


class ManifViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Base pour les vues des manifestations
    """
    permission_classes = (IsAuthenticated, IsInGroupApi, IsInvolvedInInstruction)
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter,)
    filter_fields = ('activite', 'ville_depart', 'instance')
    search_fields = ('nom', 'description',)
    ordering_fields = ('date_debut', 'activite', 'ville_depart')

    """
    Utilisation de la fonction finalize_response pour bénéficier des variables request et response
    dans lesquelles se trouvent les infos à logger
    """
    def finalize_response(self, request, response, *args, **kwargs):
        if 'count' in response.data:
            count = response.data['count']
        else:
            count = 'none'
        api_logger.info("Utilisateur %s | %s de %s | %s | réponse %s, %s | items : %s" %
                        (request.user, request.method, self.request.get_host(), self.request.get_full_path(),
                         response.status_code, response.status_text, count
                         ))
        return super().finalize_response(request, response, *args, **kwargs)


class ManifPublicViewSet(ManifViewSet):
    """
    Vue publique de toutes les manifestations à venir\n
    Accès avec restrictions identiques au calendrier public
    sans tenir compte des restrictions d'affichage manuelles dans le calendrier
    """
    authentication_classes = ()
    permission_classes = ()
    queryset = Manif.objects.filter(
        date_fin__gt=timezone.now()).filter(
        Q(instruction__id__isnull=False) | Q(instance_id__isnull=True)).exclude(
        instruction__etat__in=['interdite', 'annulée']).order_by('-date_debut')
    serializer_class = ManifPublicSerializer

    def retrieve(self, request, *args, **kwargs):
        """
        Détail d'une manifestation\n
        Accès avec restrictions identiques au calendrier public
        sans tenir compte des restrictions d'affichage manuelles dans le calendrier
        """
        return super().retrieve(request, *args, **kwargs)



class ManifestationViewSet(ManifViewSet):
    """
    Vue complète des manifestations
    """
    queryset = Manif.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return DnmSerializer
        if self.action == 'pieceJointe':
            return ManifPJSerializer
        if self.action == 'docComp':
            return DocCompSerializer
        if self.action == 'docOfficiel':
            return DocOfficielSerializer
        if self.action == 'evaluationRnr':
            return RnrevaluationSerilizer
        if self.action == 'evaluationN2k':
            return N2kevaluationSerilizer
        return super().get_serializer_class()

    @swagger_auto_schema(auto_schema=None)
    def list(self, request, *args, **kwargs):
        return Response({"detail": "pas de vue de liste, vous devez fournir un numéro de dossier"}, status=403)

    def retrieve(self, request, *args, **kwargs):
        """
        Détail du dossier d'une manifestation\n
        Réservé aux instructeurs pour les maniestations de leur département
        et aux agents des services consultés via une demande d'avis
        La liste des champs de données varie selon le type de dossier (indiqué dans le champs type)
        """
        manif = self.get_object()
        if manif.get_type_manif() == 'dnm':
            serializer = DnmSerializer(manif.cerfa)
        elif manif.get_type_manif() == 'dnmc':
            serializer = DnmcSerializer(manif.cerfa)
        elif manif.get_type_manif() == 'dcnm':
            serializer = DcnmSerializer(manif.cerfa)
        elif manif.get_type_manif() == 'dcnmc':
            serializer = DcnmcSerializer(manif.cerfa)
        elif manif.get_type_manif() == 'dvtm':
            serializer = DvtmSerializer(manif.cerfa)
        elif manif.get_type_manif() == 'avtm':
            serializer = AvtmSerializer(manif.cerfa)
        elif manif.get_type_manif() == 'avtmcir':
            serializer = AvtmcirSerializer(manif.cerfa)
        else:
            serializer = ManifestationSerializer(manif)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def pieceJointe(self, request, pk=None, *args, **kwargs):
        """
        Obtenir les urls des pièces jointes du dossier\n
        {id} est le numéro de la manifestation.
        Réservé aux instructeurs et agents des services suivant leur secteur d'instruction
        """
        manif = self.get_object()
        serializer = ManifPJSerializer(manif.cerfa)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def docComp(self, request, pk=None, *args, **kwargs):
        """
        Obtenir les urls des documents complémentaires du dossier\n
        {id} est le numéro de la manifestation.
        Réservé aux instructeurs et agents des services suivant leur secteur d'instruction
        """
        manif = self.get_object()
        doc_list = manif.documentscomplementaires.all()
        serializer = DocCompSerializer(doc_list, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def docOfficiel(self, request, pk=None, *args, **kwargs):
        """
        Obtenir les urls des documents officiels du dossier\n
        {id} est le numéro de la manifestation.
        Réservé aux instructeurs et agents des services suivant leur secteur d'instruction
        """
        manif = self.get_object()
        if not hasattr(manif, 'instruction'):
            return Response({"detail": "Intruction non trouvée."}, status=404)
        doc_list = manif.instruction.documents.all()
        serializer = DocOfficielSerializer(doc_list, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def evaluationRnr(self, request, pk=None, *args, **kwargs):
        """
        Détail de l'évaluation RnR associée au dossier\n
        {id} est le numéro de la manifestation.
        Réservé aux instructeurs et agents des services suivant leur secteur d'instruction
        """
        manif = self.get_object()
        if not hasattr(manif, 'rnrevaluation'):
            return Response({"detail": "Evaluation non trouvée."}, status=404)
        eval = manif.rnrevaluation
        serializer = RnrevaluationSerilizer(eval)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def evaluationN2k(self, request, pk=None, *args, **kwargs):
        """
        Détail de l'évaluation Natura 2000 associée au dossier\n
        {id} est le numéro de la manifestation.
        Réservé aux instructeurs et agents des services suivant leur secteur d'instruction
        """
        manif = self.get_object()
        if not hasattr(manif, 'n2kevaluation'):
            return Response({"detail": "Evaluation non trouvée."}, status=404)
        eval = manif.n2kevaluation
        serializer = N2kevaluationSerilizer(eval)
        return Response(serializer.data)
