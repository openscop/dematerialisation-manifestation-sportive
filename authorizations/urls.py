# coding: utf-8
from django.urls import path, re_path

from .views import *


app_name = 'authorizations'
urlpatterns = [
    # Dashboard
    path('dashboard/', Dashboard.as_view(), name="dashboard"),
    path('archives/', Archives.as_view(), name="archives"),

    # Autres vues
    re_path('add/(?P<manifestation_pk>\d+)', AuthorizationCreateView.as_view(), name='authorization_add'),
    re_path('(?P<pk>\d+)/publish/', AuthorizationPublishBylawView.as_view(), name='authorization_publish'),
    re_path('(?P<pk>\d+)/edit/', AuthorizationUpdateView.as_view(), name="authorization_update"),
    re_path('(?P<pk>\d+)/', AuthorizationDetailView.as_view(), name="authorization_detail"),
]
