# coding: utf-8
import datetime

from django.urls import reverse
from django.db import models
from django_fsm import transition

from notifications.models.action import Action
from notifications.models.notification import Notification
from sub_agreements.models.abstract import PreAvis, PreAvisQuerySet


class PreAvisCompagnie(PreAvis):
    """ Préavis Compagnie """

    # Champs
    preavis_ptr = models.OneToOneField("sub_agreements.preavis", parent_link=True, related_name='preaviscompagnie', on_delete=models.CASCADE)
    compagnie = models.ForeignKey("administration.compagnie", verbose_name="compagnie", on_delete=models.CASCADE)
    concerned_cis = models.ManyToManyField("administration.cis", verbose_name="CIS concerné")
    objects = PreAvisQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        manifestation = self.avis.get_manifestation()
        compagnie = self.compagnie
        return ' - '.join([str(manifestation), str(compagnie)])

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'objet """
        url = 'sub_agreements:company_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_sdis(self):
        """ Renvoyer le SDIS du préavis """
        return self.compagnie.sdis

    def get_agents(self):
        """ Renvoyer les agents concernés par le préavis """
        return self.compagnie.compagnieagentslocaux.all()

    # Action
    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        """ Rendre le préavis """
        self.reply_date = datetime.date.today()
        self.save()
        self.notify_ack(agents=self.get_sdis().sdisagents.all(), content_object=self.compagnie)
        self.log_ack(agents=self.get_agents())

    @transition(field='state', source='acknowledged', target='acknowledged')
    def notify_cis(self):
        """ Notifier les agents du CIS """
        recipients = [agent for cis in self.concerned_cis.all() for agent in cis.cisagents.all()]
        Notification.objects.notify_and_mail(recipients, "Prenez connaissance des informations", self.compagnie, self.avis.get_manifestation())
        Action.objects.log(self.get_agents(), "CIS informé", self.avis.get_manifestation())

    # Méta
    class Meta:
        verbose_name = "pré-avis compagnie"
        verbose_name_plural = "pré-avis compagnies"
        default_related_name = "preaviscompagnie_set"
        app_label = "sub_agreements"
