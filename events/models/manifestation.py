# coding: utf-8
import datetime
from datetime import date

from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import validate_comma_separated_integer_list
from django.urls import reverse
from django.db import models
from django.db.models import Q
from model_utils.managers import InheritanceQuerySet

from carto.consumer.openrunner import openrunner_api
from configuration.directory import UploadPath
from contacts.models import Contact
from core.models.instance import Instance
from core.util.admin import set_admin_info
from sports.models.federation import Federation


class ManifestationQuerySet(InheritanceQuerySet):
    """ Queryset des manifestations """

    # Getter
    def by_instance(self, request=None, instances=None):
        """
        Renvoyer les manifestations pour l'utilisateur ou les instances

        :param request: requête HTTP
        :param instances: liste d'instances
        :type instances: list<core.instance>
        """
        if request and not request.user.is_anonymous:
            # Renvoyer les manifestations pour l'utilisateur s'il est renseigné
            return self.filter(instance=request.user.get_instance())
        elif instances:
            # Renvoyer les manifestations pour les instances si elles sont renseignées
            return self.filter(instance__in=instances)
        else:
            return self

    def by_organisateur(self, organisateur):
        """ Renvoyer les manifestations créées par l'organisateur """
        return self.filter(structure__organisateur=organisateur)

    def supported(self):
        """ Renvoyer les manifestations prises en charge """
        return self.filter(instance__departement__isnull=False)

    def unsupported(self):
        """ Renvoyer les manifestations non prises en charge """
        return self.filter(instance__departement__isnull=True)

    # Overrides
    def get_by_natural_key(self, name):
        """ Clé naturelle """
        return self.get(name=name)


class Manifestation(models.Model):
    """ Manifestation sportive """

    # Texte d'aide et descriptions
    HELP_DESCRIPTION = "précisez les modalités d'organisation et les caractéristiques de la manifestation"
    HELP_OBSERVATION = "précisez les observations sur la manifestation, destinées aux services d'instruction"
    HELP_ROUTE_DESCRIPTION = "précisez pour chaque parcours : nb km, nb participants, heure de départ, départ groupé ou échelonné..."
    DESC_REGISTEREDCAL = "la manifestation est inscrite sur le calendrier de la fédération délégataire de la discipline"
    DESC_DEPARTEMENTS = "autres départements traversés par la manifestation"
    HELP_MULTISELECT = "maintenez appuyé « Ctrl », ou « Commande (touche pomme) » sur un Mac, pour en sélectionner plusieurs."
    HELP_NENTRIES = "renseigner le nombre de participants de la précedente édition ou le nombre de participants estimé si première édition"
    DESC_MARKUP_CONVENTION = "vous êtes signataire de la charte du balisage temporaire"
    HELP_MARKUP_CONVENTION = ""
    HELP_ONEWAY = "veuillez lister les voies publiques à emprunter en sens unique lors de la manifestation"
    HELP_CLOSED_ROADS = "veuillez lister les voies publiques à fermer lors de la manifestation"
    DESC_BIGBUDGET = "le budget de la manifestation excède 100 000 €"
    DESC_BIGTITLE = "manifestation donnant lieu à la délivrance d'un titre national ou international"
    DESC_LUCRATIVE = "manifestation présentant un caractère lucratif et regroupant sur un même site plus de 1500 personnes"
    DESC_MOTOR_CLOSED_ROAD = "manifestation avec engagement de véhicules terrestres à moteur se déroulant en dehors des voies ouvertes à la circulation"
    DESC_MOTOR_N2K = "manifestation avec engagement de véhicules terrestres à moteur se déroulant sur des voies ouvertes à la circulation et à l'intérieur " \
                     "d'un périmètre Natura 2000"
    DESC_APPROVAL_REQUEST = "manifestation avec demande d'homologation d'un circuit de sports motorisés"
    HELP_COMMITMENT = "un modèle de cette déclaration est disponible <a target='_blank' href='/aide/engagement_organisateur/autorisation'>ici</a>"
    DESC_COMMITMENT = "déclaration et engagement de l'organisateur"
    HELP_INSURANCE = "attestation de la police d'assurance souscrite par l'organisateur et couvrant sa responsabilité civile ainsi que celle des " \
                     "participants à la manifestation et de toute personne, nommément désignée par l'organisateur, prêtant son concours à l'organisation" \
                     "de la manifestation"
    DESC_SAFETYP = "dispositions prises pour la sécurité"
    HELP_SAFETY_PROVISION = "recenser l'ensemble des dispositions assurant la sécurité et la protection des pratiquants et des tiers ainsi que les mesures " \
                            "prises par l'organisateur pour garantir la tranquilité publique pendant toute la durée de la manifestation (attestation " \
                            "secouristes, ambulance, extincteurs, dépanneuse...)"
    HELP_ROUND_SAFETY = "recenser l'ensemble des dispositions assurant la sécurité et la protection des pratiquants et des tiers ainsi que les mesures " \
                        "prises par l'organisateur pour garantir la tranquilité publique pendant toute la durée de la manifestation (carte détaillée " \
                        "et photos des zones de protection du public)"
    DESC_DOCTOR_ATTENDING = "attestation de présence de médecin(s)"

    # Chemins d'upload
    UP_CARTOGRAPHY = UploadPath('cartographie')
    UP_RULES = UploadPath('reglements')
    UP_COMMIT = UploadPath('engagements')
    UP_ICERT = UploadPath('attestation_assurance')
    UP_SPRO = UploadPath('securite')
    UP_DOC = UploadPath('attestations_presence_medecin')
    UP_RNDS = UploadPath('securisation_epreuves')
    UP_EXTRA = UploadPath('doc_complementaires')

    instance = models.ForeignKey('core.instance', null=True, verbose_name="Configuration d'instance", on_delete=models.SET_NULL)
    creation_date = models.DateField("date de création", default=date.today, editable=False, blank=True, null=True)
    structure = models.ForeignKey('organisateurs.structure', verbose_name="structure", on_delete=models.CASCADE)
    name = models.CharField(verbose_name="nom de la manifestation", max_length=255, unique=True)
    begin_date = models.DateTimeField("date de début")
    end_date = models.DateTimeField("date de fin")
    description = models.TextField("description", help_text=HELP_DESCRIPTION)
    observation = models.TextField("observation", default='', blank=True, help_text=HELP_OBSERVATION)
    activite = models.ForeignKey('sports.activite', verbose_name="activité", on_delete=models.CASCADE)
    registered_calendar = models.BooleanField(verbose_name=DESC_REGISTEREDCAL, default=False)
    departure_city = models.ForeignKey('administrative_division.commune', verbose_name="commune de départ", on_delete=models.CASCADE)
    openrunner_route = models.CharField("cartographie de parcours", validators=[validate_comma_separated_integer_list], max_length=255, blank=True)
    route_descriptions = models.TextField("description des parcours", default='', help_text=HELP_ROUTE_DESCRIPTION)
    contact = GenericRelation(Contact, verbose_name="personne à contacter sur place")
    number_of_entries = models.PositiveIntegerField("nombre de participants", help_text=HELP_NENTRIES)
    max_audience = models.PositiveIntegerField("nombre max. de spectateurs")
    crossed_cities = models.ManyToManyField('administrative_division.commune', related_name="%(app_label)s_%(class)s_crossed_by",
                                            verbose_name="communes traversées", blank=True, help_text="Ne pas sélectionner la commune de départ.")
    other_departments_crossed = models.ManyToManyField('administrative_division.departement', verbose_name=DESC_DEPARTEMENTS, blank=True)

    # Informations de publication
    private = models.BooleanField(default=False, verbose_name="Ne pas publier la manifestation dans le calendrier")  # Organisateurs
    hidden = models.BooleanField(default=False, verbose_name="Ne pas publier la manifestation dans le calendrier")  # Instructeurs
    show_structure_address = models.BooleanField(default=True, verbose_name="Afficher les coordonnées de la structure dans le calendrier")  # Organisateur

    # markup info
    markup_convention = models.BooleanField(verbose_name=DESC_MARKUP_CONVENTION, help_text=HELP_MARKUP_CONVENTION, default=False)
    rubalise_markup = models.BooleanField("balisage rubalise", default=False)
    bio_rubalise_markup = models.BooleanField("balisage rubalise biodégradable", default=False)
    chalk_markup = models.BooleanField("balisage à la craie", default=False)
    paneling_markup = models.BooleanField("balisage panneaux (sur piquets ou fixés sur supports naturels)", default=False)
    ground_markup = models.BooleanField("balisage signalétique collé au sol", default=False)
    sticker_markup = models.BooleanField("balisage autocollant", default=False)
    lime_markup = models.BooleanField("balisage à la chaux", default=False)
    plaster_markup = models.BooleanField("balisage au plâtre", default=False)
    confetti_markup = models.BooleanField("balisage confettis", default=False)
    paint_markup = models.BooleanField("balisage à la peinture (aérosol)", default=False)
    acrylic_markup = models.BooleanField("balisage à la peinture acrylique", default=False)
    withdrawn_markup = models.BooleanField("balisage retiré dans les 48h", default=False)
    oneway_roads = models.TextField("voies publiques en sens unique", blank=True, help_text=HELP_ONEWAY)
    closed_roads = models.TextField(verbose_name="fermeture de voies publiques", blank=True, help_text=HELP_CLOSED_ROADS)

    # natura 2000 related questions
    big_budget = models.BooleanField(verbose_name=DESC_BIGBUDGET, default=False)
    big_title = models.BooleanField(verbose_name=DESC_BIGTITLE, default=False)
    lucrative = models.BooleanField(verbose_name=DESC_LUCRATIVE, default=False)
    motor_on_closed_road = models.BooleanField(verbose_name=DESC_MOTOR_CLOSED_ROAD, default=False)
    motor_on_natura2000 = models.BooleanField(verbose_name=DESC_MOTOR_N2K, default=False)
    approval_request = models.BooleanField(verbose_name=DESC_APPROVAL_REQUEST, default=False)
    is_on_rnr = models.BooleanField("sur périmètre RNR", default=False, help_text="*RNR : Réserve Naturelle Régionale")

    # attached files
    cartography = models.FileField(upload_to=UP_CARTOGRAPHY, blank=True, null=True, verbose_name="cartographie", max_length=512)
    manifestation_rules = models.FileField(upload_to=UP_RULES, blank=True, null=True, verbose_name="réglement de la manifestation", max_length=512)
    safety_provisions = models.FileField(upload_to=UP_SPRO, blank=True, null=True, verbose_name=DESC_SAFETYP, help_text=HELP_SAFETY_PROVISION, max_length=512)
    doctor_attendance = models.FileField(upload_to=UP_DOC, blank=True, null=True, verbose_name=DESC_DOCTOR_ATTENDING, max_length=512)
    additional_docs = models.FileField(upload_to=UP_EXTRA, blank=True, null=True, verbose_name="documents complémentaires", max_length=512)
    organisateur_commitment = models.FileField(upload_to=UP_COMMIT, blank=True, null=True, verbose_name=DESC_COMMITMENT, help_text=HELP_COMMITMENT,
                                               max_length=512)
    insurance_certificate = models.FileField(upload_to=UP_ICERT, blank=True, null=True, verbose_name="attestation d'assurance", help_text=HELP_INSURANCE,
                                             max_length=512)
    rounds_safety = models.FileField(upload_to=UP_RNDS, blank=True, null=True, verbose_name="sécurisation des épreuves", help_text=HELP_ROUND_SAFETY,
                                     max_length=512)
    objects = ManifestationQuerySet.as_manager()

    # Overrides
    def __str__(self):
        return self.name.title()

    def as_str(self):
        return str(self)

    def save(self, *args, **kwargs):
        # Définir l'instance à celle de la ville de départ
        if self.departure_city and not self.instance and not self.departure_city.get_instance().is_master():
            self.instance = self.departure_city.get_departement().get_instance()
        super().save(*args, **kwargs)

    def natural_key(self):
        """ Clé naturelle """
        return self.name,

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de consultation """
        url = ''.join(['events:', ContentType.objects.get_for_model(self).model, '_detail'])
        return reverse(url, kwargs={'pk': self.pk})

    def get_instance(self):
        """
        Renvoyer l'instance associée à la manifestation

        :rtype: core.models.Instance
        """
        return self.instance

    def get_departure_departement_name(self):
        """ Renvoyer le nom simple du département de départ """
        return self.departure_city.get_departement().name

    def get_federation(self):
        """ Renvoyer la fédération pour le département et pour la discipline """
        return Federation.objects.get_for_departement(self.activite.discipline, self.departure_city.get_departement())

    def get_organisateur(self):
        """ Renvoyer l'organisateur """
        """ Renvoyer l'organisateur """
        return self.structure.organisateur

    def get_autorisation(self):
        """ Renvoyer l'autorisation si disponible """
        try:
            return self.manifestationautorisation
        except (AttributeError,):
            return None

    def get_crossed_departements(self):
        """ Renvoyer les départements traversés """
        return list(set(list(self.other_departments_crossed.all()) + [self.departure_city.get_departement()]))

    def get_crossed_communes(self):
        """
        Renvoyer les communes traversées

        :returns: toutes les communes traversées (dont celle de départ)
        """
        return list(set(list(self.crossed_cities.all()) + [self.departure_city]))

    def get_crossed_departement_names(self):
        """ Renvoyer les noms (01, 42, 78 etc.) des départements traversés """
        return [departement.name for departement in self.get_crossed_departements()]

    def is_unsupported(self):
        """ Renvoyer si la manifestation n'est pas prise en charge """
        return self.instance is None or self.instance.departement is None

    @set_admin_info(boolean=True, short_description="Dans le calendrier")
    def is_visible_in_calendar(self):
        """ Renvoyer l'état de visibilité dans le calendrier """
        return not (self.private or self.hidden)

    def get_default_federation(self):
        """ Renvoyer la fédération disponible qui correspond le mieux à cette manif """
        return Federation.objects.get_for_departement(self.activite.discipline, self.departure_city.get_departement())

    @set_admin_info(boolean=True, short_description="est soumise à évaluation natura 2000")
    def has_natura2000evaluation(self):
        """ Renvoyer si la manifestation est soumise à une éval N2K """
        return hasattr(self, 'natura2000evaluations')

    @set_admin_info(boolean=True, short_description="est soumise à évaluation RNR")
    def has_rnrevaluation(self):
        """ Renvoyer si la manifestation est soumise à une éval RNR """
        return hasattr(self, 'rnrevaluations')

    def legal_delay(self):
        """ Renvoie le délai légal en jours pour déclarer ou envoyer la demande d'autorisation """
        return self.instance.get_manifestation_delay()

    def get_limit_date(self):
        """ Returns limit date for declaring or sending authorization request """
        return (self.begin_date - datetime.timedelta(days=self.legal_delay())).replace(hour=23, minute=59)

    def get_concerned_prefecture(self):
        """ Renvoyer la préfécture selon la ville de départ """
        return self.departure_city.arrondissement.prefecture

    def two_weeks_left(self):
        """ Returns true or false according to the number of weeks left (under 2 weeks or more than 2 weeks) """
        return self.get_limit_date().date() < (datetime.date.today() + datetime.timedelta(weeks=2))

    def delay_exceeded(self):
        """ If official delay to declare or send the authorization request is passed, return True, else, return False """
        return datetime.date.today() > self.get_limit_date().date()

    def get_manifestation_routes(self):
        """
        Renvoyer les parcours OpenRunner sélectionnés pour la manifestation

        Si l'on considère les ensembles get_manifestation_routes et get_available_routes,
        l'intersection entre les deux est égale à get_manifestation_routes. Pour faire clair,
        get_manifestation_routes renvoie toujours tout ou partie des parcours renvoyés
        par get_available_routes.
        :returns: un dictionnaire id parcours:nom parcours
        :rtype: dict
        """
        ids = [route for route in self.openrunner_route.strip(',').split(',') if route]
        available_routes = self.get_available_routes()
        routes = {key: available_routes[key] for key in ids if key in available_routes}
        return routes

    def get_available_routes(self):
        """ Renvoyer les parcours Openrunner disponibles pour la manifestation """
        return openrunner_api.get_user_routes(self.get_organisateur().user.get_username())

    def get_same_day_manifestations(self, include_self=False):
        """
        Renvoyer les manifestations qui ont lieu aux mêmes dates que cette manifestation

        (et qui bien entendu, ne sont pas cette même manifestation)

        Dans la pratique, si cette manifestation est nommée e1, et la manifestation à
        comparer est nommée e2, les deux manifestations coïncident si :
        - (e2.debut <= e1.debut ET e2.fin >= e1.debut) OU
        - (e2.debut <= e1.fin ET e2.fin >= e1.fin) OU
        - (e2.debut >= e1.debut ET e2.fin <= e1.fin)

        Et plus simplement, elles coïncident si :
        - NON (e2.debut > e1.fin OU e2.fin < e1.debut)
        """
        exclusion = {} if include_self else {'name': self.name}
        return Manifestation.objects.exclude(Q(begin_date__gt=self.end_date) | Q(end_date__lt=self.begin_date)).exclude(**exclusion).distinct()

    def get_same_day_same_town_manifestations(self, include_self=False):
        """
        Renvoyer les manifestations qui passent dans les mêmes villes

        que la manifestation actuelle.
        """
        communes = self.get_crossed_communes()
        return self.get_same_day_manifestations(include_self=include_self).filter(Q(departure_city__in=communes) | Q(crossed_cities__in=communes)).distinct()

    def get_conflicts_map_routes(self, include_self=True):
        """
        Renvoyer les ids des parcours probablement en conflit

        Les ids de parcours de la manifestation en cours sont inclus.
        Pour vérifier si un conflit existe, préférer
        has_conflicts_map_routes

        :rtype: list
        """
        possible_conflicts = self.get_same_day_same_town_manifestations(include_self=include_self)
        route_ids = []
        for manifestation in possible_conflicts:
            routes = manifestation.get_manifestation_routes()
            for key in routes.keys():  # Récupérer les ids
                route_ids.append(key)
        route_ids = list(set(route_ids))  # dédupliquer
        return route_ids

    def get_conflicts_map_routes_hyphen_separated(self, include_self=True):
        """ Renvoyer les ids des parcours en conflit, mais sous forme de chaîne séparée par des tirets """
        route_ids = self.get_conflicts_map_routes(include_self=include_self)
        return '-'.join([str(route) for route in route_ids])

    def has_conflicts_map_routes(self):
        """
        Renvoyer les ids des parcours probablement en conflit

        Les ids de parcours de la manifestation en cours sont inclus.
        Pour vérifier si un conflit existe, préférer
        has_conflicts_map_routes

        :rtype: list
        """
        return bool(self.get_conflicts_map_routes(include_self=False))

    def conflicts_maybe_incomplete(self):
        """ Renvoyer les noms de manifs du même jour/même ville qui n'ont pas de route OpenRunner """
        result = []
        possible_conflicts = self.get_same_day_same_town_manifestations()
        for manifestation in possible_conflicts:
            if not manifestation.openrunner_route:
                result.append(manifestation.name)
        return result

    def display_rnr_eval_panel(self):
        """ Renvoyer si l'encart d'évaluation RNR doit être affiché """
        if self.is_on_rnr and not hasattr(self, 'rnrevaluations'):
            return True
        return False

    def display_natura2000_eval_panel(self):
        """ Renvoyer si l'encart d'évaluation Natura2000 doit être affiché """
        if not hasattr(self, 'natura2000evaluations') and (self.big_budget or self.lucrative):
            return True
        return False

    def get_breadcrumbs(self):
        """ Renvoie les breadcrumbs indiquant l'avancement de l'instruction """
        breadcrumbs = [["description", 1]]

        if self.display_rnr_eval_panel():
            breadcrumbs.append(["évaluation RNR", 0])
        elif hasattr(self, 'rnrevaluations'):
            breadcrumbs.append(["évaluation RNR", 1])
        if self.display_natura2000_eval_panel():
            breadcrumbs.append(["évaluation Natura 2000", 0])
        elif hasattr(self, 'natura2000evaluations'):
            breadcrumbs.append(["évaluation Natura 2000", 1])
        final, exists = self.get_final_breadcrumb()

        if exists:
            breadcrumbs.append(final)
        return breadcrumbs

    def ready_for_instruction(self):
        """ Indiquer si le dossier pour la manifestation peut être instruit """
        ready = True
        if not self.manifestation_rules or not self.organisateur_commitment or not self.safety_provisions:
            ready = False
        if not self.openrunner_route and self.get_instance().is_openrunner_map_required():
            ready = False
        return ready

    @set_admin_info(boolean=True, short_description="Formulaire validé")
    def processing(self):
        """
        Renvoyer si une spécialisation de manifestation existe

        En d'autres mots, renvoyer si le formulaire principal est validé.
        """
        try:
            self.manifestationautorisation
        except AttributeError:
            try:
                self.manifestationdeclaration
            except AttributeError:
                return False
            return True
        return True

    def missing_additional_data(self):
        """ Renvoyer les documents complémentaires dont le fichier n'est pas renseigné """
        return self.documentscomplementaires.filter(attached_document='')

    @set_admin_info(short_description="Type de demande")
    def get_manifestation_type_name(self):
        """
        Renvoyer l'attribut qui définit le type de manifestation

        :rtype: str
        """
        for name in ['autorisationnm', 'declarationnm', 'motorizedconcentration', 'motorizedevent', 'motorizedrace']:
            try:
                getattr(self, name)
                return name
            except (AttributeError, ObjectDoesNotExist):
                pass
        return 'unsupportedmanifestation'

    def get_manifestation_specialized(self):
        """
        Renvoyer l'objet manifestation spécialisé

        :rtype: AutorisationNM [ DeclarationNM | MotorizedRace | MotorizedEvent | MotorizedConcentration
        :returns: Une instance d'une classe héritant de Manifestation
        """
        name = self.get_manifestation_type_name()
        if name:
            try:
                return getattr(self, name)
            except (AttributeError, ObjectDoesNotExist):
                pass
        return self

    def get_contacts(self):
        """
        Renvoyer les contacts attachés à cette manifestation

        Note : les Contacts ne sont jamais attachés à des instances de
        Manifestation mais à des instances de modèles héritant de
        Manifestation.
        Utiliser seulement self.contact.all() ne renverrait rien
        """
        specialized = self.get_manifestation_specialized()
        contacts = specialized.contact.all()
        return contacts

    # Meta
    class Meta:
        verbose_name = "manifestation"
        verbose_name_plural = "manifestations"
        default_related_name = "manifestations"
        app_label = "events"


class ManifestationRelatedModel(models.Model):
    """
    Tous les modèles liés à manifestation peuvent hériter de cette classe

    afin de bénéficier de méthodes liées aux informations de manifestation
    """

    # Getter
    def get_manifestation(self):
        """ Renvoyer la manifestation de l'objet """
        return self.manifestation

    def get_instance(self):
        """ Renvoyer l'instance liée à la manifestation liée à l'objet """
        return self.get_manifestation().get_instance() or Instance.objects.get_master()

    # Méta
    class Meta:
        abstract = True
