# coding: utf-8
import logging
import sys
import os
from collections import OrderedDict
import yaml
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.celery import CeleryIntegration

from core.util.migrations import DisableMigrations
from core.util.dynamic_static import get_static_url_now
from configuration.directory import Paths
from configuration import thirdparty

# Get the local settings of the server
server_settings = {}
SETTINGS_FILE = 'SETTINGS_FILE' in os.environ and os.environ['SETTINGS_FILE'] or '/etc/django/settings-manifestationsportive.yaml'
try:
    with open(SETTINGS_FILE, 'r') as f:
        server_settings = yaml.load(f, Loader=yaml.FullLoader)
except FileNotFoundError:
    print('No local settings.')
    pass


# DJANGO BASE SETTINGS
# =========================
# Sécurité
SECRET_KEY = 'SECRET_KEY' in server_settings and server_settings['SECRET_KEY'] or 'olqk+0l-a0yh@!yd%+#6-0n5l8k8&_^ncmo_4$q==2l$+@knq0'
ALLOWED_HOSTS = 'ALLOWED_HOSTS' in server_settings and server_settings['ALLOWED_HOSTS'] or ['0.0.0.0', '127.0.0.1', 'localhost', '.manifestationsportive.fr']
INTERNAL_IPS = ['127.0.0.1']
ADMIN_INSTANCE_IP = 'ADMIN_INSTANCE_IP' in server_settings  and server_settings['ADMIN_INSTANCE_IP'] or ['127.0.0.1']

# Debug
DEBUG = 'DEBUG' in server_settings and server_settings['DEBUG'] or False
TEMPLATE_DEBUG = 'DEBUG' in server_settings and server_settings['DEBUG'] or False


# WSGI et routage
ROOT_URLCONF = 'configuration.urls'
WSGI_APPLICATION = 'configuration.wsgi.application'

# Traduction et Régionalisation (https://docs.djangoproject.com/en/1.6/topics/i18n/)
TIME_ZONE = 'Europe/Paris'
LANGUAGE_CODE = 'fr'  # bootstrap3_datetime nécessite un code en 2 lettres
USE_I18N = True
USE_L10N = True
USE_TZ = True
DATETIME_INPUT_FORMATS = ['%d/%m/%Y %H:%M', '%d/%m/%Y %H:%M:%s']
DATE_INPUT_FORMATS = ['%d/%m/%Y']
DATE_FORMAT = "d/m/Y"
DATETIME_FORMAT = "d F Y H:i"

# Framework Sites (https://docs.djangoproject.com/en/1.6/ref/contrib/sites/)
SITE_ID = 1

# Fichiers statiques et fichiers uploadés (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
# https://docs.djangoproject.com/en/1.6/topics/files/
STATIC_ROOT_URL = '/static/'
STATIC_ROOT = 'STATIC_ROOT' in server_settings and server_settings['STATIC_ROOT'] or '/tmp/static/'
STATIC_URL = get_static_url_now(STATIC_ROOT_URL, True, SECRET_KEY) # permet de generer une adresse aléatoire pour les statics
STATICFILES_DIRS = []
MEDIA_ROOT = 'MEDIA_ROOT' in server_settings and server_settings['MEDIA_ROOT'] or '/tmp/media/'
MEDIA_URL = '/media/'

# Limite mémoire d'upload de fichier dans Django (mais pas dans Apache/Nginx)
FILE_UPLOAD_MAX_MEMORY_SIZE = 10485760  # 10 Mégaoctets

# Email backend (https://docs.djangoproject.com/en/1.6/topics/email/#email-backends)
EMAIL_BACKEND = ('EMAIL_BACKEND' in server_settings and server_settings['EMAIL_BACKEND'] == 'smtp') and 'django.core.mail.backends.smtp.EmailBackend' or 'post_office.EmailBackend'
EMAIL_HOST = 'EMAIL_HOST' in server_settings and server_settings['EMAIL_HOST'] or ''
EMAIL_PORT = 587
EMAIL_HOST_USER = 'EMAIL_HOST_USER' in server_settings and server_settings['EMAIL_HOST_USER'] or ''
EMAIL_HOST_PASSWORD = 'EMAIL_HOST_PASSWORD' in server_settings and server_settings['EMAIL_HOST_PASSWORD'] or ''
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'DEFAULT_FROM_EMAIL' in server_settings and server_settings['DEFAULT_FROM_EMAIL'] or 'plateforme@manifestationsportive.fr'

POST_OFFICE = {
    'BACKENDS': {
        'default': 'core.SMTPCustomBackend.SMTPCustomBackend',
    },
    "THREADS_PER_PROCESS": 1 # éviter que postoffice s'y perde entre differents threads au niveau des connections smtp, mis en place de plusieurs process lors de l'execution de la commande
}

# Utilisé par mail_admins. surcharge de la valeur défaut qui génère une erreur smtp
SERVER_EMAIL = 'DEFAULT_FROM_EMAIL' in server_settings and server_settings['DEFAULT_FROM_EMAIL'] or 'plateforme@manifestationsportive.fr'
ADMINS = [('Notif', 'notif@openscop.fr'), ]

# Vue CSRF utilisé en cas d'erreur
CSRF_FAILURE_VIEW = "core.views.csrf.csrf_failure"

# DATABASE SETTINGS
# =================
DATABASES = OrderedDict([
    ['default', {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'DATABASE_NAME' in server_settings and server_settings['DATABASE_NAME'] or 'default',
        'USER': 'DATABASE_USER' in server_settings and server_settings['DATABASE_USER'] or 'postgres',
        'PASSWORD': 'DATABASE_PASSWORD' in server_settings and server_settings['DATABASE_PASSWORD'] or '',
        'HOST': 'localhost',
        'PORT': 5432,
        'NUMBER': 42
    }]
])

# celery
CELERY_ENABLED = True
CELERY_BROKER = 'CELERY_BROKER' in server_settings and server_settings['CELERY_BROKER'] or 'amqp://guest:guest@localhost:5672'
CELERY_BACKEND = 'CELERY_BACKEND' in server_settings and server_settings['CELERY_BACKEND'] or 'amqp://guest:guest@localhost:5672'

FILE_UPLOAD_PERMISSIONS = 0o644

# CACHE SETTINGS
# ==============
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

# DJANGO USER AND AUTH SETTINGS
# =============================
# Cookie
# SESSION_COOKIE_DOMAIN=".manifestationsportive.fr"

# Modèle utilisateur utilisé par Django (1.5+)
AUTH_USER_MODEL = 'core.user'

# Authentification (https://docs.djangoproject.com/en/1.6/ref/settings/#authentication-backends)
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

# Password hashers (https://docs.djangoproject.com/en/1.6/ref/settings/#password-hashers)
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'core.util.hashers.DrupalPasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.UnsaltedMD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
)

# Validateur de mot de passe pour empecher des mot de passe peu sûr
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 9,
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# TEMPLATES & MIDDLEWARE
# ======================
# Configuration du moteur de templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            Paths.get_root_dir('portail', 'templates'),
            Paths.get_root_dir('templates'),
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'core.processors.core.core',
                'aide.processors.contexthelp.contexthelps'
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'debug': DEBUG,
        },
    },
]

# Middleware
MIDDLEWARE_MINI = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'core.LastvisitMiddleware.LastvisitMiddleware',
)

MIDDLEWARE_DEV = (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

MIDDLEWARE_PROD = ()

MIDDLEWARE = ('DEVELOPMENT_MODE' in server_settings and server_settings['DEVELOPMENT_MODE']) and (MIDDLEWARE_MINI + MIDDLEWARE_DEV) or (MIDDLEWARE_MINI + MIDDLEWARE_PROD)


# APPLICATIONS
# ============
INSTALLED_APPS_MINI = (
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.postgres',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django_extensions',
    'django_filters',
    'django_jenkins',
    'django_cron',
    'adminsortable2',
    'bootstrap_datepicker_plus',
    'ajax_select',
    'allauth',
    'allauth.account',
    'ckeditor',
    'ckeditor_uploader',
    'crispy_forms',
    'import_export',
    'fixture_magic',
    'oauth2_provider',
    'rest_framework',
    'drf_yasg',
    'administration',
    'administrative_division',
    'agreements',
    'aide',
    'authorizations',
    'carto',
    'contacts',
    'core',
    'declarations',
    'emergencies',
    'evaluations',
    'evaluation_incidence',
    'events',
    'evenements',
    'instructions',
    'notifications',
    'nouveautes',
    'organisateurs',
    'portail',
    'protected_areas',
    'sports',
    'sub_agreements',
    'clever_selects',
    'messagerie',
    'post_office',
    'webpush',
    "celery.contrib.testing.tasks",
)
WEBPUSH_SETTINGS ={
    "VAPID_PUBLIC_KEY": "VAPID_PUBLIC_KEY" in server_settings and server_settings['VAPID_PUBLIC_KEY'] or "BMzJJa177GqcxhaQD1zn9a8k1BQO3wKGewH8j0HNTtF8sPNTCmI6rzSdhI6nvij95VR0KGiTXh3jn7AFUhDxrHA",
    "VAPID_PRIVATE_KEY": "VAPID_PRIVATE_KEY" in server_settings and server_settings['VAPID_PRIVATE_KEY'] or "JOP3unX8MDb1J9f8EQ0tlEHDrpPw_BSS1ths26Ug-Tc",
    "VAPID_ADMIN_EMAIL": "VAPID_ADMIN_EMAIL" in server_settings and server_settings['VAPID_ADMIN_EMAIL'] or "admin@example.com"
}

INSTALLED_APPS_DEV = (
    'django_custom_user_migration',
    'debug_toolbar',
)
INSTALLED_APPS_PROD = (
)

INSTALLED_APPS = ('DEVELOPMENT_MODE' in server_settings and server_settings['DEVELOPMENT_MODE']) and (INSTALLED_APPS_MINI + INSTALLED_APPS_DEV) or (INSTALLED_APPS_MINI + INSTALLED_APPS_PROD)

# LOGGING
# ==================================

LOG_ROOT = 'LOG_ROOT' in server_settings and server_settings['MEDIA_ROOT'] or '/tmp/log/manifsport/'
if not os.path.exists(LOG_ROOT):
    os.makedirs(LOG_ROOT)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'carto_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': LOG_ROOT + 'carto.debug.log',
            'formatter': 'verbose'
        },
        'smtp_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': LOG_ROOT + 'smtp.debug.log',
            'formatter': 'verbose'
        },
        'api_file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': LOG_ROOT + 'api.info.log',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
            'email_backend': 'django.core.mail.backends.smtp.EmailBackend',
        },
    },
    'formatters': {
        'verbose': {'format': '%(levelname)s %(asctime)s %(module)s (pid %(process)d) %(message)s'},
        'simple': {'format': '%(levelname)s | %(asctime)s | %(message)s'},
    },
    'loggers': {
        'carto': {
            'handlers': ['carto_file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'smtp': {
            'handlers': ['smtp_file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'api': {
            'handlers': ['api_file'],
            'level': 'INFO',
            'propagate': True,
        },
        'django': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
    },
}


# APPLICATION MANIFESTATION SPORTIVE
# ==================================
# URL principale du site.
MAIN_URL = 'MAIN_URL' in server_settings and server_settings['MAIN_URL'] or 'http://localhost:8000/'


# Paramètre de maintenance : désactiver la création auto d'avis etc.
DISABLE_SIGNALS = False

# Alias de formulaire pour la validation auto
FORM_ALIASES = {
    'manifestation': ['events.forms.ManifestationForm'],
    'manifestation_autorisationnm': ['events.forms.AutorisationNMForm'],
    'manifestation_declarationnm': ['events.forms.DeclarationNMForm'],
    'manifestation_motorizedconcentration': ['events.forms.MotorizedConcentrationForm'],
    'manifestation_motorizedevent': ['events.forms.MotorizedEventForm'],
    'manifestation_motorizedrace': ['events.forms.MotorizedRaceForm'],
    'umanifestation': ['events.forms.UnsupportedManifestationForm'],
    'organisateur_signup': ['core.forms.SignupOrganisateurForm'],
    'agent_signup': ['core.forms.SignupAgentForm'],
    'manif': ['evenements.forms.manif.ManifForm'],
    'umanif': ['evenements.forms.sansinstance.SansInstanceManifForm']
}

# Gestion des dates pour campagne (définie manuellement) de modification des mots de passe
DATE_CHANGE_PASSWORD = "17/06/2020"  # date à partir de laquelle le mot de passe doit être changé (affichage formulaire nouveau mot de passe).
DATE_INIT_PASSWORD = "30/06/2020"  # date à laquelle le mot de passe sera changé (envoi par mail du lien pour nouveau mot de passe.

# TESTS
# =====
TESTS_IN_PROGRESS = False
if 'test' in sys.argv[1:] or 'jenkins' in sys.argv[1:]:
    logging.disable(logging.CRITICAL)
    DEBUG = True  # Todo: ce réglage fait que Travis effectue les tests avec le Debug actif. Ceci nous éloigne de la configuration de la production. Etudier une manière différente.
    TEMPLATE_DEBUG = False
    TESTS_IN_PROGRESS = True
    MIGRATION_MODULES = DisableMigrations()
    DATABASES = {k: v for k, v in DATABASES.items() if k is 'default'}
    DISABLE_SIGNALS = False
    DATE_CHANGE_PASSWORD = ""
    CELERY_ENABLED = False


# FUSION des bases / LEGACY
# =========================
if 'full_merge' in sys.argv[1:] or 'import_external' in sys.argv[1:]:
    DISABLE_SIGNALS = True  # Ne surtout pas conserver le comportement des signaux lors de l'importation

# PROTECTION de base du code sensible
# ===================================
PROTECT_CODE = False  # Certaines commandes ne peuvent pas s'exécuter si == True

# SETTINGS RUNSERVER
# ==================
for arg in sys.argv[1:]:
    if 'runserver' in arg:
        DISABLE_SIGNALS = False
        DATE_CHANGE_PASSWORD = ""

# Empêcher l'effacement de .thirdparty lors du nettoyage automatique des imports
for key in thirdparty.__dict__:
    if key == key.upper():
        locals()[key] = getattr(thirdparty, key)

# CRISPY
# ======
CRISPY_TEMPLATE_PACK = 'bootstrap4'

# crontab classes
CRON_CLASSES = [
    "evenements.cronjob.CompletudeDossier",  # 6h45
    "messagerie.cronjob.UpdateCPT",                # toutes les minutes
    "messagerie.cronjob.SendRecap",                # 7h45
    "evenements.cronjob.UpdateDelai",              # 6h25
    "instructions.cronjob.RelanceAvis",            # 7h05
    "instructions.cronjob.RelanceAction",          # 7h25
    "core.cronjob.DeleteFlagFile",                 # toutes les heures
    "core.cronjob.DeleteExportationFile",          # toutes les heures
    "core.cronjob.EmailAdresseValide",             # 6h25
]

MESSAGERIE_CPT_TIME_REFRESH = 1  # minute paramètre interne

SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_AGE = 4 * 60 * 60  # 4 heures
INACTIVE_TIME = 20  # minutes, paramètre interne

# Exceptions logging :
# https://sentry.io/Openscop/prodmanifsport3/
SENTRY_CONFIG = 'SENTRY_CONFIG' in server_settings and server_settings['SENTRY_CONFIG'] or '',
if SENTRY_CONFIG:
    sentry_sdk.init(
        dsn=SENTRY_CONFIG[0],
        integrations=[DjangoIntegration(), CeleryIntegration()],
    )

# OPENRUNNER settings
# ======
OPENRUNNER_HOST = 'https://carto.manifestationsportive.fr/'
OPENRUNNER_ROUTE_DISPLAY = 'orservice/serviceDDCS.php?idr='
OPENRUNNER_MULTI_ROUTES_DISPLAY = 'orservice/serviceDDCS.php?idr='
if 'OPENRUNNER_API_URL' in os.environ:                  # prendre en compte une variable d'environnement
    OPENRUNNER_API_URL = os.environ['OPENRUNNER_API_URL']
else:
    OPENRUNNER_API_URL = 'OPENRUNNER_API_URL' in server_settings and server_settings['OPENRUNNER_API_URL'] or ''
OPENRUNNER_FRONTEND_URL = 'https://carto.manifestationsportive.fr/'
if 'OPENRUNNER_TOKEN' in os.environ:                    # prendre en compte une variable d'environnement
    OPENRUNNER_TOKEN = os.environ['OPENRUNNER_TOKEN']
else:
    OPENRUNNER_TOKEN = 'OPENRUNNER_TOKEN' in server_settings and server_settings['OPENRUNNER_TOKEN'] or ''

# django-ajax-selects
# ======
# Empêcher le chargement de dépendances jQuery et jQueryUI obsolètes
AJAX_SELECT_BOOTSTRAP = False


# Configuration de l'API
# =======================
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
            'rest_framework.renderers.JSONRenderer',
            'rest_framework.renderers.BrowsableAPIRenderer',
        ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
        'rest_framework.authentication.SessionAuthentication',
        ),
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAuthenticated', ),
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend', ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 30
}
OAUTH2_PROVIDER = {
    # this is the list of available scopes
    'OAUTH2_BACKEND_CLASS': 'oauth2_provider.oauth2_backends.JSONOAuthLibCore',
    'SCOPES': {'read': 'Read scope', 'write': 'Write scope', 'groups': 'Access to your groups'}
}
SWAGGER_SETTINGS = {
    'DEFAULT_MODEL_RENDERING': 'example',
   'SECURITY_DEFINITIONS': {
      'Bearer': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
      }
   }
}