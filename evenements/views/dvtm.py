# coding: utf-8
from .manif import ManifDetail, ManifCreate, ManifUpdate, ManifFilesUpdate, ManifDelete
from ..forms.dvtm import DvtmForm, DvtmFilesForm
from ..models.vtm import Dvtm


class DvtmDetail(ManifDetail):
    """ Détail de la manifestation """

    # Configuration
    model = Dvtm


class DvtmCreate(ManifCreate):
    """ Création de la manifestation """

    # Configuration
    model = Dvtm
    form_class = DvtmForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dvtm'
        return context


class DvtmUpdate(ManifUpdate):
    """ Modification de la manifestation """

    # Configuration
    model = Dvtm
    form_class = DvtmForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dvtm'
        return context


class DvtmFilesUpdate(ManifFilesUpdate):
    """ Ajout de fichiers à une manif """

    # Configuration
    model = Dvtm
    form_class = DvtmFilesForm


class DvtmDelete(ManifDelete):
    """ Supprimer une manifestation """

    # Configuration
    model = Dvtm