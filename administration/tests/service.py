# coding: utf-8
from django.test import TestCase
from unidecode import unidecode

from administrative_division.factories import CommuneFactory


class PrefectureTests(TestCase):
    """ Tester les fonctionnalités du modèle Préfecture """

    def test_prefecture_string(self):
        """ Teste que la méthode __str__ indique bien le type de préfecture """
        commune_a = CommuneFactory.build(arrondissement__prefecture__sous_prefecture=True)
        commune_b = CommuneFactory.build(arrondissement__prefecture__sous_prefecture=False)
        # La sous-préfecture doit contenir le nom 'sous-préfecture'
        self.assertIn("sous-prefecture", unidecode(str(commune_a.arrondissement.prefecture).lower()))
        # La préfecture contient préfecture mais pas sous-préfecture
        self.assertIn("prefecture", unidecode(str(commune_b.arrondissement.prefecture).lower()))
        self.assertNotIn("sous-prefecture", unidecode(str(commune_b.arrondissement.prefecture).lower()))
