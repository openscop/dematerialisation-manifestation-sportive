import factory

from evenements.factories import DcnmFactory
from .models import Instruction, Avis, PreAvis, DocumentOfficiel


class InstructionFactory(factory.django.DjangoModelFactory):
    """ Factory pour les instructions """

    # Champs
    manif = factory.SubFactory(DcnmFactory)

    # Meta
    class Meta:
        model = Instruction


class AvisInstructionFactory(factory.django.DjangoModelFactory):
    """ Factory des avis """

    instruction = factory.SubFactory(InstructionFactory)

    # Meta
    class Meta:
        model = Avis


class PreAvisInstructionFactory(factory.django.DjangoModelFactory):
    """ Factory des avis """

    avis = factory.SubFactory(AvisInstructionFactory)

    # Meta
    class Meta:
        model = PreAvis


class DocOfficielFactory(factory.django.DjangoModelFactory):
    """ Factory des documents officiels """

    instruction = factory.SubFactory(InstructionFactory)

    # Meta
    class Meta:
        model = DocumentOfficiel
