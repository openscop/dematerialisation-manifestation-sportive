import logging
import os
from django_cron import CronJobBase, Schedule
from django_cron.models import CronJobLog
from django.conf import settings
from django.utils import timezone
from core.models import User
from messagerie.models import Enveloppe
from evenements.models import Manif

mail_logger = logging.getLogger('smtp')


class DeleteFlagFile(CronJobBase):
    """
    Preview Manager n'arrive parfois pas à traiter des fichiers, il laisse des fichiers .flag derrière lui
    Ce qui peut le faire planter au second passage et remplir inutilement le dossier.
    Ce cron nettoie donc le dossier media des fichiers flag qui s'y trouve.
    """
    RUN_EVERY_MINS = 60  # à lancer toutes les heures

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'core.deleteflagfile'

    def do(self):

        # Purger les logs
        CronJobLog.objects.filter(code="core.deleteflagfile").filter(is_success=True).filter(
            end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        os.system('cd ' + settings.MEDIA_ROOT + ' && find . -name "*.*_flag" -type f -delete')


class DeleteExportationFile(CronJobBase):
    """
    Ici on va supprimer le dossier des fichiers temporaires de l'exportations
    """
    RUN_EVERY_MINS = 60  # à lancer toutes les heures

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'core.deleteexportationfile'

    def do(self):

        # Purger les logs
        CronJobLog.objects.filter(code="core.deleteexportationfile").filter(is_success=True).filter(
            end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        os.system('rm ' + settings.MEDIA_ROOT + 'exportation/* -R')


class EmailAdresseValide(CronJobBase):
    """
    Clore les comptes qui :
    - n'ont aucune adresse validée
    - sont organisateur
    - ne sont associé à aucune manif
    - n'ont aucun message
    - ce sont inscrit il y a plus de 15 jours
    """
    RUN_AT_TIME = ['6:25']  # à lancer à 6h25

    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'core.emailadressevalide'    # a unique code

    def do(self):

        # Purger les logs sans message
        CronJobLog.objects.filter(code="core.emailadressevalide").filter(is_success=True).filter(
            message__isnull=True).filter(end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        effacer = '' # chaine de caractères renvoyer avec la liste des comptes supprimés
        for user in User.objects.all():
            enveloppes = Enveloppe.objects.filter(destinataire_id=user).exclude(type='news')
            manif = Manif.objects.filter(structure__organisateur__user=user)
            email = user.emailaddress_set.filter(verified=True)
            delta = timezone.now() - timezone.timedelta(days=15)
            if not enveloppes and not manif and not email and hasattr(user, 'organisateur') and user.date_joined < delta:
                effacer += 'Username : ' + str(user.username) + ' | Prénom : ' + str(user.first_name) + ' | Nom : ' + str(user.last_name) + ' | pk : ' + str(user.pk) + chr(10)
                user.delete()
        return effacer
