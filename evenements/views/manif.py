# coding: utf-8
import copy, shutil, os
from django.forms.fields import IntegerField
from django.forms.models import ModelChoiceField
from django.forms.widgets import HiddenInput
from django.http import HttpResponseRedirect, Http404, HttpResponseForbidden, HttpResponse
from django.shortcuts import render, reverse, get_object_or_404
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views.generic import DetailView, CreateView, DeleteView, View
from django.views.generic.edit import UpdateView
from django.forms.models import model_to_dict
from django.conf import settings
from django.db.models.fields.files import FileField

from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from evenements.forms.manif import FIELDS_FILES_optionals, ManifCartoForm
from core.util.user import UserHelper
from core.util.permissions import require_role
from organisateurs.decorators import verifier_proprietaire
from instructions.decorators import verifier_secteur_instruction
from carto.consumer.openrunner import openrunner_api
from ..forms.manif import ManifInstructeurUpdateForm, ManifCloneForm
from ..models import Manif
from core.tasks import creation_thumbail_manif
from messagerie.models import Message


@method_decorator(verifier_proprietaire, name='dispatch')
class ManifDetail(DetailView):
    """ Détail d'une manifestation """

    template_name = 'evenements/evenement_detail.html'

    # Overrides
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['manquants'] = context['declarer'] = context['toutbon'] = context['formulaire_manquant'] = context['carto_manquant'] = False
        context['type'] = self.object.get_type_manif()
        context['last_action'] = self.object.message_manifestation.filter(type='action').last()
        # Pour la barre de progression
        if self.object.en_cours_instruction() and self.object.instruction.get_nb_avis() != 0:
            context['progres'] = int(self.object.instruction.get_nb_avis_rendus() / self.object.instruction.get_nb_avis() * 100)
        else:
            context['progres'] = 0
        # Pour afficher l'instructeur
        if self.object.en_cours_instruction() and self.object.instruction.referent:
            context['referent'] = self.object.instruction.referent.get_full_name()
        else:
            context['referent'] = ''
        # Pour gérer l'affichage dans le bloc Actions
        if self.object.dossier_complet():
            if not self.object.get_docs_complementaires_manquants().exists():
                if not self.object.delai_depasse() and not hasattr(self.object, 'instruction'):
                    context['declarer'] = True
                else:
                    context['toutbon'] = True
        else:
            if hasattr(self.object, 'n2kevaluation'):
                if not self.object.n2kevaluation.formulaire_n2k_complet():
                    context['n2k_manquant'] = True
            if self.object.afficher_panneau_eval_n2000():
                context['cause'] = self.object.afficher_panneau_eval_n2000(cause=True)
            if hasattr(self.object, 'rnrevaluation'):
                if not self.object.rnrevaluation.formulaire_rnr_complet():
                    context['rnr_manquant'] = True
            if not self.object.formulaire_complet():
                context['formulaire_manquant'] = True
            if not self.object.carto_complet():
                context['carto_manquant'] = True
            if self.object.liste_manquants():
                context['manquants'] = True
                # Afficher le retard pour avertissement
                if self.object.etape_en_cours() == 'etape 2':
                    # Pendant l'étape 2, seul les fichiers à fournir à l'étape 1 peuvent être en retard
                    for fichier in self.object.LISTE_FICHIERS_ETAPE_1:
                        # Si un seul fichier est en retard, on passe le retard dans le contexte
                        if not getattr(self.object, fichier):
                            context['delta'] = (timezone.now().date() - self.object.get_date_etape_1().date()).days
                if self.object.etape_en_cours() == 'etape 3':
                    # Pendant l'étape 3, tous les manquants sont en retard
                    context['delta'] = (timezone.now().date() - self.object.get_date_etape_2().date()).days
        return context


@method_decorator(verifier_proprietaire, name='dispatch')
class ManifClonageAjax(UpdateView):
    """ Clonage de manifestation """
    model = Manif
    form_class = ManifCloneForm
    template_name = 'evenements/evenement_clone_form.html'

    def form_valid(self, form):
        manif = self.get_object()
        # Copie du cerfa et modification des données
        new_manif = copy.deepcopy(manif.cerfa)
        new_manif.nom = form.cleaned_data['nom']
        new_manif.date_debut = form.cleaned_data['date_debut']
        new_manif.date_fin = form.cleaned_data['date_fin']
        # Suppression du pk et id pour créer une nouvelle manifestation
        new_manif.id = None
        new_manif.pk = None
        new_manif.save()
        # Gestion des pièces jointes
        path = settings.MEDIA_ROOT + new_manif.get_departement_depart_nom() + '/'
        if os.path.isdir(path + str(manif.cerfa.pk)):
            # Un dossier de PJs existe, copie du dossier
            shutil.copytree(path + str(manif.cerfa.pk), path + str(new_manif.pk))
            for field in new_manif._meta.fields:
                if type(field) is FileField:
                    file = getattr(new_manif, field.attname)
                    if file:
                        # Si le champ est un fichier et qu'il est rempli, modification du chemin
                        path_list = file.name.split('/')
                        path_list[1] = str(new_manif.pk)
                        setattr(new_manif, field.attname, '/'.join(path_list))
            new_manif.save()

        # envoi des messages de traçabilité
        # traçabilité sur l'ancienne manif
        Message.objects.creer_et_envoyer('tracabilite', None, [self.request.user],
                                         'Dossier cloné',
                                         'Ce dossier a été cloné vers la manifestation <em>' + new_manif.nom + '</em> (' + str(new_manif.pk) + ')',
                                         manifestation_liee=manif)
        # traçabilité sur la nouvelle manif
        Message.objects.creer_et_envoyer('tracabilite', None, [self.request.user],
                                         'Dossier cloné',
                                         'Ce dossier a été créé par un clonage de la manifestation <em>' + manif.nom + '</em> (' + str(manif.pk) + ')',
                                         manifestation_liee=new_manif)
        return HttpResponse(new_manif.get_absolute_url())

    def form_invalid(self, form):
        reponse = super().form_invalid(form)
        reponse.status_code = 400
        return render(self.request, 'evenements/evenement_clone_form.html', {'form' : form}, status=400)


class ManifCreate(CreateView):
    """ Création de manifestation """

    # Configuration
    template_name = 'evenements/evenement_form.html'

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        if 'activite' not in self.request.session or not self.request.GET.get('dept'):
            return render(self.request, "core/access_restricted.html",
                          {'message': "Demande incorrecte. Veuillez utiliser l'outil de <a href=\"/evenement/creation/?dept=\">recherche de formulaire</a>."}, status=403)
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.instance = self.object.ville_depart.get_instance()
        self.object.structure = self.request.user.organisateur.structure
        self.object.emprise = self.request.session['emprise']
        self.object.competition = self.request.session['competition']
        self.object.circuit_non_permanent = self.request.session['circuit_non_permanent']
        self.object.circuit_homologue = self.request.session['circuit_homologue']
        self.object.save()
        form.save_m2m()
        self.request.session.pop('activite')
        delai = self.object.get_cerfa().delai_en_cours()
        if not delai == 21:
            self.object.delai_cours = str(delai)
        else:
            depot = self.object.get_cerfa().delaiDepot
            self.object.delai_cours = str(depot) + '-21'
        self.object.save()
        self.object.notifier_creation()
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        request_departement = self.request.GET.get('dept')
        kwargs = super().get_form_kwargs()
        kwargs['extradata'] = request_departement
        self.request.session['extradata'] = request_departement
        self.request.session.save()
        if request_departement:
            kwargs['initial'].update({'departement_depart': Departement.objects.get_by_name(request_departement).pk})
        return kwargs

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.target = form.instance

        request_departement = self.request.session['extradata']
        form.fields['activite'].initial = self.request.session['activite']
        form.fields['vtm_hors_circulation'].initial = self.request.session.get('vtm_hors_circulation', False)
        form.fields['nb_participants'].initial = self.request.session['nb_participants']
        form.fields['departements_traverses'].queryset = Departement.objects.exclude(name=request_departement)
        form.fields['ville_depart'] = ModelChoiceField(required=True, queryset=Commune.objects.by_departement_name(request_departement),
                                                       label="Commune de départ", disabled=True)
        form.fields['ville_depart'].initial = self.request.session['commune']
        form.fields['departement_depart'] = IntegerField(widget=HiddenInput())
        form._meta.exclude += ('departement_depart',)
        return form

    def get_initial(self):
        initial = super().get_initial()
        if self.request.GET.get('manif', default=None):
            ancien_manif = Manif.objects.get(pk=self.request.GET.get('manif'))
            initial = model_to_dict(ancien_manif)
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = self.model._meta.model_name
        context['cerfa'] = self.model.refCerfa
        context['verbose'] = self.model._meta.verbose_name
        if self.request.GET.get('manif', default=None):
            context['manif'] = Manif.objects.get(pk=self.request.GET.get('manif'))
        return context


class ManifUpdate(UpdateView):
    """ Modifier une manifestation """

    # Configuration
    template_name = 'evenements/evenement_form.html'

    # Overrides
    @method_decorator(verifier_proprietaire)
    def dispatch(self, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['pk'])
        # En accès direct par url; si la manifestation a été envoyée, il est trop tard pour la modifier
        if manif.en_cours_instruction():
            return render(self.request, "core/access_restricted.html",
                          {'message': "Cette manifestation ne peut plus être modifiée !"}, status=403)
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        request_departement = self.request.GET.get('dept')
        kwargs = super().get_form_kwargs()
        kwargs['extradata'] = request_departement
        self.request.session['extradata'] = request_departement
        self.request.session.save()
        kwargs['initial'].update({'departement_depart': self.object.ville_depart.get_departement().pk})
        # Pour afficher les communes traversées du département de départ, il faut ajouter le département de départ à la liste des département traversés
        # Cela doit venir de clever-select
        # le kwarg écrase la valeur initiale tirée de la DB d'où l'ajout à la liste
        dept_trav = list(self.object.departements_traverses.all())
        dept_trav.append(self.object.ville_depart.get_departement())
        kwargs['initial'].update({'departements_traverses': dept_trav})
        return kwargs

    def get_form(self, form_class=None):
        request_departement = self.request.GET.get('dept')
        form = super().get_form(form_class)
        form.target = form.instance
        form.request = self.request

        # Remplacer les champs commune de départ et département si le département est passé dans l"URL
        if request_departement:
            form.fields['departements_traverses'].queryset = Departement.objects.exclude(name=request_departement)
            form.fields['ville_depart'] = ModelChoiceField(required=True, queryset=Commune.objects.by_departement_name(request_departement),
                                                           label="Commune de départ", disabled=True)
            form.fields['departement_depart'] = IntegerField(widget=HiddenInput())
            form._meta.exclude += ('departement_depart',)
        return form

    def form_valid(self, form):
        if form.instance.signature_charte_dispense_site_n2k:
            if form.instance.sites_natura2000.all().count() == 1:
                if hasattr(form.instance, 'n2kevaluation') and not (form.instance.n2kevaluation.pourquoi_instructeur or
                                                                    form.instance.n2kevaluation.pourquoi_organisateur):
                    form.instance.n2kevaluation.delete()
        return super().form_valid(form)


class ManifCartoUpdate(UpdateView):
    """
    Modifier la carto d'une manifestation
    """

    template_name = 'evenements/evenement_carto_form.html'
    model = Manif
    form_class = ManifCartoForm

    # Overrides
    @method_decorator(verifier_proprietaire)
    def dispatch(self, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['pk'])
        # En accès direct par url; si la manifestation a été envoyée, il est trop tard pour éditer la carto
        if manif.en_cours_instruction():
            return render(self.request, "core/access_restricted.html",
                          {'message': "Cette manifestation ne peut plus être editée !"}, status=403)
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        return self.object.get_cerfa().get_absolute_url()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['routes'] = openrunner_api.get_user_routes(self.request.user.username)
        # On transforme le texte des routes/parcours en liste*
        if kwargs['instance'].__dict__['parcours_openrunner']:
            kwargs['instance'].__dict__['parcours_openrunner'] = [int(route) for route in kwargs['instance'].__dict__[
                'parcours_openrunner'].strip(',').split(',')]
        return kwargs

    def get_form(self, form_class=None):
        request_departement = self.request.GET.get('dept')
        form = super().get_form(form_class)
        form.target = form.instance
        form.request = self.request

        # Remplacer les champs commune de départ et département si le département est passé dans l"URL
        if request_departement:
            departement = Departement.objects.get_by_name(request_departement)
            instance = departement.get_instance()
            if not instance.uses_openrunner():
                del form.fields['parcours_openrunner']
        return form


@method_decorator(verifier_proprietaire, name='dispatch')
class ManifFilesUpdate(UpdateView):
    """ Modifier une manifestation """

    # Configuration
    template_name = 'evenements/evenement_file_form.html'

    # Overrides
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        return kwargs

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.target = form.instance
        for field in FIELDS_FILES_optionals:
            if field not in form.instance.get_liste_fichier_etape_0() and field in form.fields:
                del form.fields[field]
        # En fonction de l'étape en cours, on complète la liste des docs requis.
        # Si le champ est rempli et dans la liste, alors plus de changement possible
        etape = form.instance.manifestation_ptr.etape_en_cours()
        liste_doc_requis = []
        liste_doc_abloquer = []
        if hasattr(form.instance, 'instruction'):
            liste_doc_abloquer += form.instance.get_liste_fichier_etape_0()
        else:
            liste_doc_requis += form.instance.get_liste_fichier_etape_0()
        if etape == "etape 1":
            liste_doc_requis += form.instance.LISTE_FICHIERS_ETAPE_1
        elif etape == "etape 2":
            liste_doc_abloquer += form.instance.LISTE_FICHIERS_ETAPE_1
            liste_doc_requis += form.instance.LISTE_FICHIERS_ETAPE_1 + form.instance.LISTE_FICHIERS_ETAPE_2
        elif etape == "etape 3":
            liste_doc_abloquer += form.instance.LISTE_FICHIERS_ETAPE_1 + form.instance.LISTE_FICHIERS_ETAPE_2
            liste_doc_requis += form.instance.LISTE_FICHIERS_ETAPE_1 + form.instance.LISTE_FICHIERS_ETAPE_2
        for field in form.fields:
            if field in liste_doc_abloquer:
                if form.initial[field]:
                    form.fields[field].disabled = True
            if field in liste_doc_requis:
                if not form.initial[field]:
                    css = form.fields[field].widget.attrs.get('class', '')
                    form.fields[field].widget.attrs['class'] = 'adonner' + css
        return form

    def form_valid(self, form):
        """ Le formulaire est valide, gérer la réponse HTTP """
        if hasattr(form.instance, 'instruction') and form.changed_data:
            for change in form.changed_data:
                if form.cleaned_data[change]:
                    form.instance.manifestation_ptr.ajouter_document(form.fields[change].label)
                    form.instance.instruction.doc_verif = False
                    form.instance.instruction.save()
        redirect = super().form_valid(form)
        creation_thumbail_manif.delay(form.instance.pk)
        return redirect

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Pour afficher un message d'alerte à coté du fichier en retard
        if self.object.liste_manquants():
            context['files2'], context['files3'] = [], []
            if self.object.etape_en_cours() in ['etape 2', 'etape 3']:
                #  Pendant l'étape 2, seul les fichiers à fournir à l'étape 1 peuvent être en retard
                for fichier in self.object.LISTE_FICHIERS_ETAPE_1:
                    # Si un fichier est en retard, on passe le retard dans le contexte et on le met dans la liste
                    if not getattr(self.object, fichier):
                        context['delta2'] = (timezone.now().date() - self.object.get_date_etape_1().date()).days
                        context['files2'].append(fichier)
            if self.object.etape_en_cours() == 'etape 3':
                #  Pendant l'étape 3, seul les fichiers à fournir à l'étape 2 peuvent être en retard
                for fichier in self.object.LISTE_FICHIERS_ETAPE_2:
                    # Si un fichier est en retard, on passe le retard dans le contexte et on le met dans la liste
                    if not getattr(self.object, fichier):
                        context['delta3'] = (timezone.now().date() - self.object.get_date_etape_2().date()).days
                        context['files3'].append(fichier)
        return context


class ManifDelete(DeleteView):
    """ Supprimer une manifestation """

    # Configuration
    template_name = 'evenements/evenement_suppression.html'
    success_url = '/tableau-de-bord-organisateur/'

    # Overrides
    @method_decorator(verifier_proprietaire)
    def dispatch(self, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['pk'])
        # En accès direct par url; si la manifestation a été envoyée, il est trop tard pour la supprimer
        if manif.en_cours_instruction():
            return render(self.request, "core/access_restricted.html",
                          {'message': "Cette manifestation ne peut plus être supprimée !"}, status=403)
        return super().dispatch(*args, **kwargs)

    def delete(self, request, *args, **kwargs):
        from django.db.models.fields.files import FileField
        manif = self.get_object()
        for field in manif._meta.fields:
            if type(field) is FileField:
                file = getattr(manif, field.attname)
                file.delete(save=False)
        return super().delete(request, *args, **kwargs)


@method_decorator(verifier_secteur_instruction(arr_wrt=True), name='dispatch')
class ManifInstructeurUpdate(UpdateView):
    """ Formulaire instructeur """

    model = Manif
    form_class = ManifInstructeurUpdateForm
    template_name = 'evenements/evenement_form.html'

    # Overrides
    def get_success_url(self):
        """ URL lors de la validation du formulaire """
        return reverse("instructions:instruction_detail", kwargs={'pk': self.object.instruction.id})

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.target = form.instance
        form.fields['afficher_adresse_structure'].required = False
        return form


class ManifPublicDetail(DetailView):
    """ Détails pyblics d'une manifestation """

    # Configuration
    model = Manif
    template_name = 'evenements/evenementpublic_detail.html'

    def dispatch(self, request, *args, **kwargs):
        instance = self.get_object()
        if not instance.visible_dans_calendrier():
            if not UserHelper.has_role(request.user, 'instructeur'):
                if instance.structure.organisateur.user != request.user:
                    return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)


class OpenrunnerAjax(View):
    def get(self, request):
        idmanif = request.GET.get('idmanif', None)
        if idmanif:
            manif = Manif.objects.get(pk=idmanif)
            return render(request, 'evenements/panneaux/openrunner_ajax.html', context={"object": manif})
        else:
            return HttpResponseForbidden


class ManifUrlGen(View):
    def get(self, request, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=kwargs.get('pk'))
        if request.user.is_authenticated:
            if request.user.has_role('organisateur'):
                if request.user == manif.get_organisateur().user:
                    url_name = 'evenements:' + manif.get_type_manif() + '_detail'
                    return HttpResponseRedirect(reverse(url_name, kwargs={'pk': manif.pk}))
                return HttpResponseRedirect(reverse('evenements:manif_detail', kwargs={'pk': manif.pk}))
            if request.user.has_role('instructeur'):
                if manif.en_cours_instruction():
                    return HttpResponseRedirect(reverse('instructions:instruction_detail', kwargs={'pk': manif.instruction.pk}))
                raise Http404("Pas d'instruction pour cette manifestation")
            if request.user.has_role('agent'):
                if manif.en_cours_instruction():
                    avis = manif.instruction.get_avis_user(request.user)
                    if avis:
                        return HttpResponseRedirect(reverse('instructions:avis_detail', kwargs={'pk': avis.pk}))
                    elif request.user.has_role('mairieagent'):
                        return HttpResponseRedirect(reverse('instructions:instruction_detail', kwargs={'pk': manif.instruction.pk}))
                raise Http404("Pas d'avis pour cette manifestation")
            if request.user.has_role('agentlocal'):
                if manif.en_cours_instruction():
                    preavis = manif.instruction.get_preavis_user(request.user)
                    if preavis:
                        return HttpResponseRedirect(reverse('instructions:preavis_detail', kwargs={'pk': preavis.pk}))
                raise Http404("Pas de préavis pour cette manifestation")
            raise Http404("Pas de role utilisateur pour orienter la vue de détail")
        else:
            return render(request, 'core/access_restricted.html', status=403)

