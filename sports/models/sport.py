# coding: utf-8
from django.db import models
from django.db.models.query_utils import Q
from django.utils import timezone
from django.utils.html import format_html

from core.util.admin import set_admin_info


class DisciplineQuerySet(models.QuerySet):
    """ Queryset Disciplines """

    # Overrides
    def get_by_natural_key(self, name):
        return self.get(name=name)

    # Getter
    def get_by_name(self, name):
        """
        Renvoyer la discipline correspondant à un nom

        :returns: Une discipline
        :rtype: Discipline
        """
        return self.get(name__iexact=name)

    def get_by_activite_name(self, name):
        """
        Renvoyer la discipline correspondant à un nom d'activité

        :param name: nom de l'activité complet
        :type name: str
        """
        return self.get(activites__name__iexact=name)

    def motorise(self):
        """ Renvoyer les disciplines motorisées """
        return self.filter(motorise=True)

    def non_motorise(self):
        """ Renvoyer les disciplines motorisées """
        return self.filter(motorise=False)


class Discipline(models.Model):
    """ Discipline sportive """

    # Constantes
    MILIEU_CHOICES = [[0, "Nautique"], [1, "Terrestre"], [2, "Aérien"]]
    NAUTIQUE, TERRESTRE, AERIEN = 0, 1, 2

    # Champs
    name = models.CharField("nom", max_length=200)
    milieu = models.SmallIntegerField(default=TERRESTRE, choices=MILIEU_CHOICES, verbose_name="Milieu")
    motorise = models.BooleanField("motorisé", default=False)
    aide = models.ForeignKey('aide.PageAide', null=True, blank=True, on_delete=models.SET_NULL, related_name='discipline', verbose_name="Page d'aide",
                             help_text="Autocomplète sur le titre de page d'aide. Ne fonctionne qu'avec des page d'aide 'simple'.")
    objects = DisciplineQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.name.capitalize()

    def natural_key(self):
        """ Renvoyer une clé naturelle """
        return self.name,

    # Getter
    def get_federations(self):
        """ Renvoyer la fédération de la discipline"""
        return self.federations.all()

    def get_activites(self):
        """ Renvoyer les activités de la discipline """
        return self.activites.all()

    @set_admin_info(short_description="Activités")
    def get_activite_count(self):
        """ Renvoyer le nombre d'activités de la discipline """
        return self.get_activites().count()

    @set_admin_info(short_description="Page d'aide")
    def get_aide_page(self):
        """ Renvoyer la page d'aide affectée à la discipline """
        if self.aide:
            return format_html('<a href="/admin/aide/helppage/' + str(self.aide.pk) + '/change/">' + self.aide.title + '</a>')
        return None

    # Méta
    class Meta:
        verbose_name = "discipline"
        default_related_name = "disciplines"
        ordering = ['name']
        app_label = "sports"


class ActiviteQuerySet(models.QuerySet):
    """ Queryset activités """

    # Overrides
    def get_by_natural_key(self, name):
        return self.get(name=name)

    # Getter
    def get_by_name(self, name):
        """ Renvoyer l'activité correspondant à un nom """
        return self.get(name__iexact=name)

    def motorise(self):
        """ Renvoyer les disciplines motorisées """
        return self.filter(discipline__motorise=True)

    def non_motorise(self):
        """ Renvoyer les disciplines non motorisées """
        return self.filter(discipline__motorise=False)


class Activite(models.Model):
    """ Activité sportive (catégorie de discipline sportive) """

    # Champs
    name = models.CharField("nom", max_length=200, unique=True)
    discipline = models.ForeignKey("sports.discipline", related_name='activites', verbose_name="discipline", on_delete=models.CASCADE)
    objects = ActiviteQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représqentation de l'objet """
        return self.name.capitalize()

    def natural_key(self):
        """ Clé naturelle """
        return self.name,

    # Getter
    def get_federations(self):
        """ Revnoyer la fédération de l'activité """
        return self.discipline.get_federations()

    def get_incoming_manifestation_count(self, request):
        """
        Renvoyer le nombre de manifestations à venir

        :param request: Ajoute la contrainte du département de l'instance
        en cours pour filtrer les manifestations.
        :returns: int
        """
        from core.models import Instance
        from events.models import Manifestation
        now = timezone.now()
        criteria = {'end_date__gt': now, 'activite': self}
        if request:
            instance = Instance.objects.get_for_request(request)
            departement = instance.get_departement()
            criteria['departure_city__arrondissement__departement'] = departement

        manifestations = Manifestation.objects.supported().filter(
            ~Q(autorisationnm__manifestationautorisation__id=None) |
            ~Q(declarationnm__manifestationdeclaration__id=None) |
            ~Q(motorizedevent__manifestationautorisation__id=None) |
            ~Q(motorizedrace__manifestationautorisation__id=None) |
            ~Q(motorizedconcentration__manifestationautorisation__id=None) |
            ~Q(motorizedconcentration__manifestationdeclaration__id=None),
            begin_date__gt=now, **criteria
        )
        return manifestations.count()

    # Méta
    class Meta:
        verbose_name = "activité"
        verbose_name_plural = "activités"
        default_related_name = "activites"
        ordering = ['name']
        app_label = "sports"
