# coding: utf-8
from django.contrib import admin
from django.utils.html import format_html
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin, getter_for_related_field
from ajax_select.helpers import make_ajax_form

from administration.forms.agent import MairieAgentForm
from administration.models.service import SDIS, CODIS, CIS, Brigade, CG, DDSP, GGD, EDSR, Service
from administration.models.agent import CGAgent, GGDAgent, DDSPAgent, EDSRAgent, BrigadeAgent, SDISAgent, CODISAgent,\
                                        CISAgent, ServiceAgent, FederationAgent, MairieAgent, CGSuperieur
from administrative_division.models.departement import Departement
from sports.models.federation import Federation
from core.util.admin import set_admin_info, RelationOnlyFieldListFilter
from core.models import User


class UserFullNameAndUsernameMixin():
    def agent_name(self, user_obj):
        return user_obj.user.get_full_name_and_username()
    agent_name.short_description = "Nom complet et pseudo"

    def link_to_admin(self, obj):
        return obj.user.get_link_to_admin()
    link_to_admin.short_description = "Lien vers l'utilisateur"


@admin.register(CGAgent)
class CGAgentAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin CGAgent """

    # Configuration
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'cg', 'cg__departement', 'link_to_admin']
    list_filter = [('cg__departement', RelationOnlyFieldListFilter), 'agent__user__is_active']
    search_fields = ['user__username', 'user__last_name__unaccent']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agent__user__is_active', short_description='actif')

    form = make_ajax_form(CGAgent, {'user': 'user'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(cg__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        dept = request.user.get_departement()
        form.base_fields['user'].help_text = "Recherche par nom ou/et prénom ou nom d'utilisateur"
        if dept:
            form.base_fields['cg'].queryset = CG.objects.filter(departement=dept)
            if hasattr(dept, 'cg'):
                form.base_fields['cg'].initial = dept.cg
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_cg:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(CGSuperieur)
class CGSuperieurAdmin(CGAgentAdmin):
    """ Configuration admin CGSupérieur """

    form = make_ajax_form(CGSuperieur, {'user': 'user'})


@admin.register(GGDAgent)
class GGDAgentAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'ggd', 'ggd__departement', 'link_to_admin']
    list_filter = [('ggd__departement', RelationOnlyFieldListFilter), 'agent__user__is_active']
    search_fields = ['user__username', 'user__last_name__unaccent']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agent__user__is_active', short_description='actif')

    form = make_ajax_form(GGDAgent, {'user': 'user'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(ggd__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        dept = request.user.get_departement()
        form.base_fields['user'].help_text = "Recherche par nom ou/et prénom ou nom d'utilisateur"
        if dept:
            form.base_fields['ggd'].queryset = GGD.objects.filter(departement=dept)
            if hasattr(dept, 'ggd'):
                form.base_fields['ggd'].initial = dept.ggd
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_ggd:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(EDSRAgent)
class EDSRAgentAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'edsr', 'edsr__departement', 'link_to_admin']
    list_filter = [('edsr__departement', RelationOnlyFieldListFilter), 'agent__user__is_active']
    search_fields = ['user__username', 'user__last_name__unaccent']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agent__user__is_active', short_description='actif')

    form = make_ajax_form(EDSRAgent, {'user': 'user'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(edsr__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        dept = request.user.get_departement()
        form.base_fields['user'].help_text = "Recherche par nom ou/et prénom ou nom d'utilisateur"
        if dept:
            form.base_fields['edsr'].queryset = EDSR.objects.filter(departement=dept)
            if hasattr(dept, 'edsr'):
                form.base_fields['edsr'].initial = dept.edsr
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_ggd:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(DDSPAgent)
class DDSPAgentAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'ddsp', 'ddsp__departement', 'link_to_admin']
    list_filter = [('ddsp__departement', RelationOnlyFieldListFilter), 'agent__user__is_active']
    search_fields = ['user__username', 'user__last_name__unaccent']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agent__user__is_active', short_description='actif')

    form = make_ajax_form(DDSPAgent, {'user': 'user'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(ddsp__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        dept = request.user.get_departement()
        form.base_fields['user'].help_text = "Recherche par nom ou/et prénom ou nom d'utilisateur"
        if dept:
            form.base_fields['ddsp'].queryset = DDSP.objects.filter(departement=dept)
            if hasattr(dept, 'ddsp'):
                form.base_fields['ddsp'].initial = dept.ddsp
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_ddsp:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(SDISAgent)
class SDISAgentAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'sdis', 'sdis__departement', 'link_to_admin']
    list_filter = [('sdis__departement', RelationOnlyFieldListFilter), 'agent__user__is_active']
    search_fields = ['user__username', 'user__last_name__unaccent']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agent__user__is_active', short_description='actif')

    form = make_ajax_form(SDISAgent, {'user': 'user'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(sdis__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        dept = request.user.get_departement()
        form.base_fields['user'].help_text = "Recherche par nom ou/et prénom ou nom d'utilisateur"
        if dept:
            form.base_fields['sdis'].queryset = SDIS.objects.filter(departement=dept)
            if hasattr(dept, 'sdis'):
                form.base_fields['sdis'].initial = dept.sdis
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_sdis:
            return super().get_model_perms(request)
        else:
            return {}
    #
    # def agent_name(self, SDISAgent):
    #     return SDISAgent.user.get_full_name_and_username()


@admin.register(CODISAgent)
class CODISAgentAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'codis', 'codis__departement', 'link_to_admin']
    list_filter = [('codis__departement', RelationOnlyFieldListFilter), 'agent__user__is_active']
    search_fields = ['user__username', 'user__last_name__unaccent']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agent__user__is_active', short_description='actif')

    form = make_ajax_form(CODISAgent, {'user': 'user'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(codis__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        dept = request.user.get_departement()
        form.base_fields['user'].help_text = "Recherche par nom ou/et prénom ou nom d'utilisateur"
        if dept:
            form.base_fields['codis'].queryset = CODIS.objects.filter(departement=dept)
            if hasattr(dept, 'codis'):
                form.base_fields['codis'].initial = dept.codis
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_sdis:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(CISAgent)
class CISAgentAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'cis', 'cis__commune__arrondissement__departement', 'link_to_admin']
    list_filter = [('cis__commune__arrondissement__departement', RelationOnlyFieldListFilter), 'agent__user__is_active']
    search_fields = ['user__username', 'user__last_name__unaccent', 'cis__commune__name']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agent__user__is_active', short_description='actif')

    form = make_ajax_form(CISAgent, {'user': 'user'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(cis__commune__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        form.base_fields['user'].help_text = "Recherche par nom ou/et prénom ou nom d'utilisateur"
        if request.user.get_departement():
            form.base_fields['cis'].queryset = CIS.objects.filter(commune__arrondissement__departement=request.user.get_departement())
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_sdis:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(ServiceAgent)
class ServiceAgentAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'service', 'link_to_admin']
    list_filter = [('service__departements', RelationOnlyFieldListFilter), 'agent__user__is_active']
    search_fields = ['user__username', 'user__last_name__unaccent', 'service__name']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agent__user__is_active', short_description='actif')

    form = make_ajax_form(ServiceAgent, {'user': 'user'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(service__departements=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        form.base_fields['user'].help_text = "Recherche par nom ou/et prénom ou nom d'utilisateur"
        if request.user.get_departement():
            form.base_fields['service'].queryset = Service.objects.filter(departements=request.user.get_departement())
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form


@admin.register(FederationAgent)
class FederationAgentAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'federation', 'link_to_admin']
    list_filter = ['agent__user__is_active', ('federation__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username', 'user__last_name__unaccent', 'federation__name']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agent__user__is_active', short_description='actif')

    form = make_ajax_form(FederationAgent, {'user': 'user'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(federation__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        form.base_fields['user'].help_text = "Recherche par nom ou/et prénom ou nom d'utilisateur"
        if request.user.get_departement():
            form.base_fields['federation'].queryset = Federation.objects.filter(departement=request.user.get_departement())
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form


@admin.register(MairieAgent)
class MairieAgentAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'commune', 'get_departement', 'link_to_admin']
    list_filter = [('commune__arrondissement__departement', RelationOnlyFieldListFilter), 'agent__user__is_active']
    search_fields = ['user__username', 'user__last_name__unaccent', 'commune__name']
    form = MairieAgentForm
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agent__user__is_active', short_description='actif')

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(commune__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        form.base_fields['user'].help_text = "Recherche par nom ou/et prénom ou nom d'utilisateur"
        if request.user.get_departement() and not request.user.is_superuser:
            form.base_fields['departement'].queryset = Departement.objects.filter(id=request.user.get_departement().id)
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form


@admin.register(BrigadeAgent)
class BrigadeAgentAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'brigade', 'get_departement', 'link_to_admin']
    list_filter = [('brigade__commune__arrondissement__departement', RelationOnlyFieldListFilter), 'agent__user__is_active', 'brigade__kind']
    search_fields = ['user__username', 'user__last_name__unaccent', 'brigade__commune__name']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agent__user__is_active', short_description='actif')

    form = make_ajax_form(BrigadeAgent, {'user': 'user'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(brigade__commune__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        form.base_fields['user'].help_text = "Recherche par nom ou/et prénom ou nom d'utilisateur"
        if request.user.get_departement():
            form.base_fields['brigade'].queryset = Brigade.objects.filter(commune__arrondissement__departement=request.user.get_departement())
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_ggd:
            return super().get_model_perms(request)
        else:
            return {}

    # Getter
    @set_admin_info(admin_order_field='brigade__commune__arrondissement__departement', short_description="Département")
    def get_departement(self, obj):
        return obj.brigade.commune.arrondissement.departement
