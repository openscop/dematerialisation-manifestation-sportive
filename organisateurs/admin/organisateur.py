# coding: utf-8
from django.contrib import admin, messages
from django.core.exceptions import ObjectDoesNotExist
from django.utils.html import format_html
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from core.util.admin import set_admin_info, RelationOnlyFieldListFilter
from organisateurs.models import Organisateur
from core.models.user import User

@admin.register(Organisateur)
class OrganisateurAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Admin des organisateurs """

    # Configuration
    list_display = ['pk', 'lien_user', 'lien_structure', 'structure__commune__arrondissement__departement']
    list_filter = ['cgu', ('structure__commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username', 'user__first_name', 'user__last_name', 'user__email', 'structure__name__unaccent']
    inlines = []
    actions = ['create_compte_openrunner']

    def lien_user(self, obj):
        return format_html('<a href="/admin/core/user/' + str(obj.user.pk) + '/change/">' + obj.user.get_full_name() + '</a>')
    lien_user.short_description = "Utilisateur"

    def lien_structure(self, obj):
        return format_html('<a href="/admin/organisateurs/structure/' + str(obj.structure.pk) + '/change/">' + obj.structure.name + '</a>')
    lien_structure.short_description = "Structure"

    # Actions
    @set_admin_info(short_description="Créer un compte openrunner pour chaque organisateur sélectionné")
    def create_compte_openrunner(self, request, queryset):
        """ Créer un compte OpenRunner pour l'organistaeur """
        created = 0
        for instance in queryset:
            try:
                if instance.create_openrunner_user() is True:
                    created += 1
            except (AttributeError, ObjectDoesNotExist):
                pass
        self.message_user(request, "{count} organisateurs ont désormais un compte OpenRunner.".format(count=created))

    # Overrides
    def lookup_allowed(self, lookup, value):
        if lookup.startswith('structure__commune__arrondissement__departement'):
            return True
        return super().lookup_allowed(lookup, value)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(structure__commune__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def delete_model(self, request, obj):
        # Si il y a cascade, c'est à dire que cette suppression va entrainer la suppression d'une ou plusieurs manifs,
        # et si l'utilisateur n'a pas les droits pour les suppressions en cascade, la méthode ne sera pas atteinte
        # Si il a les droits, alors il faudra en plus qu'il soit superuser ou du groupe "Administrateurs techniques
        if hasattr(obj, 'structure'):
            if obj.structure.manifs.filter(instruction__isnull=False).exists():
                if not request.user.is_superuser and \
                        not "Administrateurs techniques" in request.user.groups.values_list('name', flat=True):
                    self.message_user(request, "Cette opération n'est pas possible, car elle entrainerait des"
                                               " suppressions de dossiers de manifestation !", level=messages.WARNING)
                    return
        super().delete_model(request, obj)
