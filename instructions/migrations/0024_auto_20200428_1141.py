# Generated by Django 2.2.1 on 2020-04-28 09:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('instructions', '0023_creation_autorisation_access'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documentofficiel',
            name='nature',
            field=models.SmallIntegerField(choices=[(9, '--------'), (0, "Arrêté d'interdiction"), (1, "Arrêté d'autorisation"), (2, 'Arrêté de circulation'), (3, 'Récépissé de déclaration'), (4, "Déclaration d'annulation")], verbose_name='nature du document déposé'),
        ),
    ]
