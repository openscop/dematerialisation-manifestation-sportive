# coding: utf-8
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from core.util.admin import RelationOnlyFieldListFilter
from administrative_division.models import Departement
from ..models import PdesiLieu


@admin.register(PdesiLieu)
class PdesiLieuAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration Natura 2000 """

    list_display = ['pk', 'nom']
    list_filter = [('departement', RelationOnlyFieldListFilter)]
    search_fields = ['nom__unaccent']
    list_per_page = 25

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['departement'].queryset = Departement.objects.filter(id=request.user.get_departement().id)
            form.base_fields['departement'].initial = request.user.get_departement()
        return form
