import logging
from django_filters.rest_framework import FilterSet
from django_filters.rest_framework import DjangoFilterBackend, ModelChoiceFilter

from rest_framework import viewsets, filters

from api.serializer.structure import StructureLightSerializer, StructureSerializer
from api.permissions import IsInGroupApi
from administrative_division.models import Departement, Arrondissement
from organisateurs.models import Structure

api_logger = logging.getLogger('api')


class OrganisateurFilter(FilterSet):
    departement = ModelChoiceFilter(field_name="commune__arrondissement__departement",
                                    label='departement',
                                    queryset=Departement.objects.all())
    arrondissement = ModelChoiceFilter(field_name="commune__arrondissement",
                                       label='arrondissement',
                                       queryset=Arrondissement.objects.all())

    class Meta:
        model = Structure
        fields = ('departement', 'arrondissement', 'commune')


class OrganisateurViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vue des organisateurs basée sur le modèle Structure\n
    Accessible par les utilisateurs du groupe API
    Recherche possible sur les champs 'organisateur' et 'structure' avec le bouton 'filtres'
    """
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter,)
    queryset = Structure.objects.all()
    serializer_class = StructureLightSerializer
    permission_classes = (IsInGroupApi, )
    filter_class = OrganisateurFilter
    detail_serializer_class = StructureSerializer
    search_fields = ('name', 'organisateur__user__username')
    ordering_fields = ('name', 'commune')

    """
    Utilisation de la fonction finalize_response pour bénéficier des variables request et response
    dans lesquelles se trouvent les infos à logger
    """
    def finalize_response(self, request, response, *args, **kwargs):
        if 'count' in response.data:
            count = response.data['count']
        else:
            count = 'none'
        api_logger.info("Utilisateur %s | %s de %s | %s | réponse %s, %s | items : %s" %
                        (request.user, request.method, self.request.get_host(), self.request.get_full_path(),
                         response.status_code, response.status_text, count
                         ))
        return super().finalize_response(request, response, *args, **kwargs)

    def get_serializer_class(self):
        if self.action == 'retrieve':
            if hasattr(self, 'detail_serializer_class'):
                return self.detail_serializer_class

        return super().get_serializer_class()

    def retrieve(self, request, *args, **kwargs):
        """
        Détail d'une structure d'organisateur.\n
        Accessible par les utilisateurs du groupe API
        """
        return super().retrieve(request, *args, **kwargs)
