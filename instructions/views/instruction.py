# coding: utf-8
import  logging, html

from smtplib import SMTPRecipientsRefused, SMTPException
from post_office import mail
from django.contrib import messages
from django.db.models import Q
from django.conf import settings
from django.http.response import HttpResponseRedirect
from django import forms
from django.urls import reverse
from django.shortcuts import get_object_or_404, render, redirect
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, UpdateView

from administrative_division.models import Departement
from core.models.instance import Instance
from core.models.user import User
from core.util.permissions import require_role
from instructions.decorators import verifier_secteur_instruction
from core.util.user import UserHelper
from evenements.models import Manif, AbstractInstructionConfig
from instructions.forms.instruction import InstructionDispatchForm, InstructionForm, InstructionPublishForm, InstructionPublierForm
from instructions.models.instruction import Instruction, DocumentOfficiel
from instructions.models.avis import Avis, PieceJointeAvis
from messagerie.models import Message


class InstructionCreateView(CreateView):

    # Configuration
    model = Instruction
    form_class = InstructionForm

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        self.manif = manif
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if Instruction.objects.filter(manif=manif).exists():
            return redirect('evenements:manif_detail', pk=manif.pk)
        if manif.structure.organisateur.user == self.request.user:
            return super().dispatch(*args, **kwargs)
        return render(self.request, 'core/access_restricted.html', status=403)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        from sports.models import Federation
        if self.manif.get_cerfa().consultFederation:
            fede = self.manif.get_federation()
            if fede.level == Federation.DEPARTEMENTAL:
                ajout = " (définie au niveau de votre département) "
            elif fede.level == Federation.REGIONAL:
                ajout = " (définie au niveau de votre région) "
            else:
                if fede.name == Federation.DEFAULT_FEDERATION_NAME:
                    ajout = " (pas de fédération définie > renvoi par défaut) "
                else:
                    ajout = " (définie au niveau national) "
            texte = form.helper.layout.fields[0].html.replace(':<br>', ':<br>' + str(fede) + ajout + '<i class="ctx-help-federation_definie"></i>')
            form.helper.layout.fields[0].html = texte
        return form

    def form_valid(self, form):
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        # si le dossier est imcomplet on redirige vers une mage d'erreur
        if manif.etape_en_cours() != "etape 0":
            return render(self.request, '400.html', context={"message": "Dossier hors délai réglementaire"}, status=403)
        if not manif.cerfa.dossier_complet():
            return render(self.request, '400.html', context={"message": "Dossier incomplet"}, status=403)
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if Instruction.objects.filter(manif=manif).exists():
            return redirect(self.get_success_url())
        # Sinon enregister la nouvelle déclaration
        form.instance.manif = manif
        response = super().form_valid(form)
        # A faire après : on ne doit pas lier un objet qui n'est pas enregistré (instruction)
        if form.instance.manif.get_cerfa().consultFederation:
            avis = Avis.objects.create(service_concerne='federation', instruction=form.instance, destination_object=form.instance.manif.get_federation())
            avis.notifier_creation_avis(origine=None, agents=avis.get_agents(), non_agent_recipient=avis.get_service())
        form.instance.notifier_creation()

        # envoi de mail de confirmation
        subject = "Accusé de réception : "+manif.nom
        data = {
            "date": timezone.now(),
            "structure": self.request.user.organisateur.structure,
            "user": self.request.user,
            "manif": manif,
            "instru": form.instance,
            "url": self.request.META['HTTP_HOST']+self.get_success_url(),
        }
        message = render_to_string('notifications/mail/message_organisateur_envoi_manif.txt', data)
        recipient = self.request.user
        mail_logger = logging.getLogger('smtp')
        if hasattr(recipient, 'email'):
            try:
                sender_email = manif.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL  # utiliser le EMAIL_BACKEND
                mail.send(recipient.email, sender_email,  subject=html.unescape(subject), message=html.unescape(message), html_message=html.unescape(message))
                message = render_to_string('notifications/mail/message_organisateur_envoi_manif_message.txt', data)
                Message.objects.creer_et_envoyer("info_suivi", None, [self.request.user], "Accusé de réception (dépôt de dossier)", message, manifestation_liee=manif)
            except SMTPRecipientsRefused:
                # En cas d'échec où l'adresse n'existe pas (ou similaire), notifier les instructeurs du dossier
                # À noter que l'échec SMTP ne peut se produire qu'en situation de production
                # donc à surveiller.
                mail_logger.exception(
                    "notifications.notification.notify_and_mail: SMTP Recipients refused, trying to send to instructors")
            except SMTPException:
                # Si le serveur SMTP pose un autre type de problème, logger l'erreur
                exception_text = """
                notifications.notification.notify_and_mail: Une exception SMTP non gérée vient de se produire
                instance: {instance}
                destinataires: {recipients}
                manifestation: {manifestation}
                cible: {target}
                """
                mail_logger.exception(exception_text.format(instance=manif.get_instance(),
                                                            recipients=recipient, manifestation=manif,
                                                            target=manif.structure))
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['creer'] = True
        return context

    def get_success_url(self):
        """ Renvoyer l'URL de retour lorsque le formulaire est valide """

        type_name = self.object.manif.get_type_manif()

        if type_name == 'dnm':
            return reverse('evenements:dnm_detail', kwargs={'pk': self.object.manif.pk})
        elif type_name == 'dnmc':
            return reverse('evenements:dnmc_detail', kwargs={'pk': self.object.manif.pk})
        elif type_name == 'dcnm':
            return reverse('evenements:dcnm_detail', kwargs={'pk': self.object.manif.pk})
        elif type_name == 'dcnmc':
            return reverse('evenements:dcnmc_detail', kwargs={'pk': self.object.manif.pk})
        elif type_name == 'dvtm':
            return reverse('evenements:dvtm_detail', kwargs={'pk': self.object.manif.pk})
        elif type_name == 'avtm':
            return reverse('evenements:avtm_detail', kwargs={'pk': self.object.manif.pk})
        elif type_name == 'avtmcir':
            return reverse('evenements:avtmcir_detail', kwargs={'pk': self.object.manif.pk})
        else:
            return reverse('portail:home_page')


@method_decorator(verifier_secteur_instruction, name='dispatch')
class InstructionDetailView(DetailView):

    # Configuration
    model = Instruction

    # Overrides
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user_is_instructeur'] = bool(self.request.user.has_role('instructeur') or self.request.user.has_role('mairieagent'))   # test inutile car en principe, seuls les instructeurs peuvent passer dans cette view
        context['last_action'] = self.object.manif.message_manifestation.filter(type='action').last()
        # Pour afficher l'instructeur
        if self.object.manif.en_cours_instruction() and self.object.manif.instruction.referent:
            context['referent'] = self.object.referent.get_full_name()
        else:
            context['referent'] = ''
        # Pour contrôler la possibilité d'action pour un instructeur hors de son arrondissement
        # le décorateur a controlé l'accès en lecture, il faut tester l'accès en écriture
        if self.request.user.has_role('instructeur') and \
                self.request.user not in self.object.get_instructeurs_prefecture(arr_wrt=True):
            context['en_lecture'] = True
        return context


@method_decorator(verifier_secteur_instruction(arr_wrt=True), name='dispatch')
class InstructionUpdateView(UpdateView):
    # Configuration

    model = Instruction
    form_class = InstructionDispatchForm
    template_name = 'instructions/instruction_dispatch_form.html'

    # Overrides
    def form_valid(self, form):
        form.instance.referent = self.request.user
        form.instance.envoyerDemandeAvis()
        # Création des avis suivant formulaire
        if 'villes_concernees' in form.cleaned_data and form.cleaned_data['villes_concernees']:
            for ville in form.cleaned_data['villes_concernees']:
                if ville not in [avis.destination_object for avis in Avis.objects.filter(service_concerne='mairie').filter(instruction=form.instance)]:
                    avis = Avis.objects.create(service_concerne='mairie', instruction=form.instance, destination_object=ville)
                    avis.notifier_creation_avis(origine=self.request.user, agents=avis.get_agents(), non_agent_recipient=avis.get_service())
        if form.cleaned_data['services_concernes']:
            for service in form.cleaned_data['services_concernes']:
                if service not in [avis.destination_object for avis in Avis.objects.filter(service_concerne='service').filter(instruction=form.instance)]:
                    avis = Avis.objects.create(service_concerne='service', instruction=form.instance, destination_object=service)
                    avis.notifier_creation_avis(origine=self.request.user, agents=avis.get_agents(), non_agent_recipient=avis.get_service())
        if form.instance.cg_concerne:
            cg = getattr(form.instance.manif.ville_depart.get_departement(), 'cg')
            if not Avis.objects.filter(service_concerne='cg').filter(instruction=form.instance).exists():
                avis = Avis.objects.create(service_concerne='cg', instruction=form.instance, destination_object=cg)
                avis.notifier_creation_avis(origine=self.request.user, agents=avis.get_agents(), non_agent_recipient=avis.get_service())
        if form.instance.ddsp_concerne:
            ddsp = getattr(form.instance.manif.ville_depart.get_departement(), 'ddsp')
            if not Avis.objects.filter(service_concerne='ddsp').filter(instruction=form.instance).exists():
                avis = Avis.objects.create(service_concerne='ddsp', instruction=form.instance, destination_object=ddsp)
                avis.notifier_creation_avis(origine=self.request.user, agents=avis.get_agents(), non_agent_recipient=avis.get_service())
        if form.instance.ggd_concerne:
            ggd = getattr(form.instance.manif.ville_depart.get_departement(), 'ggd')
            if not Avis.objects.filter(service_concerne='ggd').filter(instruction=form.instance).exists():
                avis = Avis.objects.create(service_concerne='ggd', instruction=form.instance, destination_object=ggd)
                avis.notifier_creation_avis(origine=self.request.user, agents=avis.get_agents(), non_agent_recipient=avis.get_service())
        if form.instance.edsr_concerne:
            edsr = getattr(form.instance.manif.ville_depart.get_departement(), 'edsr')
            # inutile car déjà testé dans le get_form
            # if form.instance.get_instance().get_workflow_ggd() == Instance.WF_EDSR:
            if not Avis.objects.filter(service_concerne='edsr').filter(instruction=form.instance).exists():
                avis = Avis.objects.create(service_concerne='edsr', instruction=form.instance, destination_object=edsr)
                avis.notifier_creation_avis(origine=self.request.user, agents=avis.get_agents(), non_agent_recipient=avis.get_service())
        if form.instance.sdis_concerne:
            sdis = getattr(form.instance.manif.ville_depart.get_departement(), 'sdis')
            if form.instance.get_instance().get_workflow_sdis() == Instance.WF_SDIS:
                if not Avis.objects.filter(service_concerne='sdis').filter(instruction=form.instance).exists():
                    avis = Avis.objects.create(service_concerne='sdis', instruction=form.instance, destination_object=sdis)
                    avis.notifier_creation_avis(origine=self.request.user, agents=avis.get_agents(), non_agent_recipient=avis.get_service())
        return super().form_valid(form)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        # Modifier l'état des champs selon la configuration actuelle
        if not self.request.user.has_role('instructeur'):
            # pas de sélection d'avis de commune pour pour les instructions par les mairies
            del form.fields['villes_concernees']
        if self.object.get_instance().get_workflow_ggd() in [Instance.WF_GGD_EDSR, Instance.WF_GGD_SUBEDSR]:
            form.fields['ggd_concerne'].initial = True
            form.fields['edsr_concerne'].initial = False
            form.fields['edsr_concerne'].widget = forms.HiddenInput()
        elif self.object.get_instance().get_workflow_ggd() == Instance.WF_EDSR:
            form.fields['edsr_concerne'].initial = True
            form.fields['ggd_concerne'].initial = False
            form.fields['ggd_concerne'].widget = forms.HiddenInput()
        if not self.object.get_instance().active_ggd:
            form.fields['edsr_concerne'].initial = False
            form.fields['edsr_concerne'].widget = forms.HiddenInput()
            form.fields['ggd_concerne'].initial = False
            form.fields['ggd_concerne'].widget = forms.HiddenInput()
        if not self.object.get_instance().active_ddsp:
            form.fields['ddsp_concerne'].initial = False
            form.fields['ddsp_concerne'].widget = forms.HiddenInput()
        if not self.object.get_instance().active_sdis:
            form.fields['sdis_concerne'].initial = False
            form.fields['sdis_concerne'].widget = forms.HiddenInput()
        if not self.object.get_instance().active_cg:
            form.fields['cg_concerne'].initial = False
            form.fields['cg_concerne'].widget = forms.HiddenInput()
        return form

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['initial'].update({'departements_concernes': Departement.objects.by_cities(self.object, as_initial=True)})
        return kwargs

    def get_success_url(self):
        """ Retourne l'URL de destination lorsque le formulaire est validé """
        pk = self.object.id
        return reverse('instructions:instruction_detail', kwargs={'pk': pk})


@method_decorator(verifier_secteur_instruction(arr_wrt=True), name='dispatch')
class InstructionPublishBylawView(UpdateView):
    # Configuration
    model = Instruction
    form_class = InstructionPublishForm

    # Overrides
    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['user'] = forms.IntegerField(initial=self.request.user.id, required=False)
        return form

    def form_valid(self, form):
        if form.cleaned_data['fichier']:
            nature = int(form.cleaned_data['nature'])
            user = User.objects.get(id=form.fields['user'].initial)
            if nature == 1:
                if form.instance.manif.get_cerfa().consultServices == AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation obligatoire']:
                    if form.instance.get_nb_avis() < 2:
                        messages.error(self.request, "Aucune action effectuée : Consultation obligatoire, "
                                                     "le nombre d'avis émis est insuffisant.")
                        return HttpResponseRedirect(self.get_success_url())
            DocumentOfficiel.objects.create(fichier=form.cleaned_data['fichier'], nature=nature,
                                            instruction=form.instance, utilisateur=user)
            form.instance.referent = self.request.user
            if nature == 0:
                form.instance.ajouter_interdiction()
            elif nature == 4:
                form.instance.ajouter_annulation()
            elif nature == 2:
                form.instance.ajouter_document_officiel()
            else:
                form.instance.ajouter_autorisation()
        return super().form_valid(form)

    def get_success_url(self):
        """ Retourne l'URL de destination lorsque le formulaire est validé """
        return self.object.get_absolute_url()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['publier'] = True
        return context


class InstructionTransfertPjVersDocOfficiel(UpdateView):
    # Configuration
    model = Instruction
    form_class = InstructionPublierForm
    template_name = 'instructions/instruction_form_transf_pj_vers_doc_officiel_ajax.html'

    # Overrides
    @method_decorator(verifier_secteur_instruction(arr_wrt=True))
    def dispatch(self, *args, **kwargs):
        self.kwargs['doc'] = doc = get_object_or_404(PieceJointeAvis, pk=self.request.GET.get('pj'))
        # Si un document officiel existe déjà pour la pièce jointe, erreur
        if DocumentOfficiel.objects.filter(fichier=doc.fichier).exists():
            return render(self.request, 'core/access_restricted.html', status=403)
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['doc'] = self.kwargs['doc']
        return context

    def form_valid(self, form):
        nature = int(form.cleaned_data['nature'])
        user = self.request.user
        doc = self.kwargs['doc']
        if nature == 1:
            if form.instance.manif.get_cerfa().consultServices == AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation obligatoire']:
                if form.instance.get_nb_avis() < 2:
                    messages.error(self.request, "Aucune action effectuée : Consultation obligatoire, "
                                                 "le nombre d'avis émis est insuffisant.")
                    return HttpResponseRedirect(self.get_success_url())
        DocumentOfficiel.objects.create(fichier=doc.fichier, nature=nature, instruction=form.instance, utilisateur=user)
        form.instance.referent = user
        if nature == 0:
            form.instance.ajouter_interdiction()
        elif nature == 4:
            form.instance.ajouter_annulation()
        elif nature == 2:
            form.instance.ajouter_document_officiel()
        else:
            form.instance.ajouter_autorisation()
        return super().form_valid(form)


@method_decorator(verifier_secteur_instruction(arr_wrt=True), name='dispatch')
class InstructionValiderPJView(View):

    # Overrides
    def get(self, request, *args, **kwargs):
        instruction = Instruction.objects.get(pk=self.kwargs['pk'])
        instruction.doc_verif = True
        Message.objects.creer_et_envoyer('info_suivi', self.request.user, [instruction.manif.structure.organisateur],
                                         'Pièces jointes validées', 'Vos pièces jointes ont été validées par l\'instructeur',
                                         manifestation_liee=instruction.manif,objet_lie_nature="piece_jointe")
        instruction.referent = self.request.user
        instruction.save()
        return redirect(reverse('instructions:instruction_detail', kwargs={'pk': int(self.kwargs['pk'])}))


@method_decorator(verifier_secteur_instruction(arr_wrt=True), name='dispatch')
class InstructionInValidDocView(View):

    # Overrides
    def get(self, request, *args, **kwargs):
        instruction = Instruction.objects.get(pk=self.kwargs['pk'])
        instruction.doc_verif = False
        Message.objects.creer_et_envoyer('info_suivi', self.request.user, [instruction.manif.structure.organisateur],
                                         'Pièces jointes invalidées', 'Vos pièces jointes ont été invalidées par l\'instructeur',
                                         manifestation_liee=instruction.manif,objet_lie_nature="piece_jointe")
        instruction.referent = self.request.user
        instruction.save()
        return redirect(reverse('instructions:instruction_detail', kwargs={'pk': int(self.kwargs['pk'])}))


@method_decorator(verifier_secteur_instruction(arr_wrt=True), name='dispatch')
class RelanceAvis(View):
    """Vue pour relancer les avis non envoyé"""
    def get(self, request, pk):
        liste_avis = Avis.objects.filter(instruction__pk=pk).filter(~Q(etat="rendu")).order_by('-pk')
        for avis in liste_avis:
            avis.notifier_creation_avis(origine=self.request.user.get_service(), agents=avis.get_agents(), non_agent_recipient=avis.get_service())
            avis.log_resend(self.request.user, avis.get_service().__str__())
        return redirect(reverse('instructions:instruction_detail', kwargs={'pk': pk}))
