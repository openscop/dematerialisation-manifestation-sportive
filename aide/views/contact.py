from django.shortcuts import render
from smtplib import SMTPException
from django.contrib.sites.models import Site
from bs4 import BeautifulSoup as Bs
from django.core.mail import get_connection, send_mail
from django.core.mail.message import EmailMultiAlternatives
from django.conf import settings
import logging

from ..forms import DemandeForm
from core.models import User

mail_logger = logging.getLogger('smtp')


def demande(request):
    """
    Formulaire de contact pour une demande
    :param request:
    :return:
    """
    if request.method == "POST":
        form = DemandeForm(request.POST)
        if form.is_valid():
            demande = form.save(commit=False)
            if not request.user.is_anonymous:
                demande.user = request.user
                exception_sender_email = request.user.get_instance().get_email()
            else:
                exception_sender_email = settings.DEFAULT_FROM_EMAIL
            demande.save()

            if demande.type == 1 or demande.type == 2:  # renseignements ou assistance
                destinataires = User.objects.filter(groups__name__contains="Administrateurs d'instance").filter(default_instance__departement=demande.departement)
                if not destinataires:
                    destinataires = User.objects.filter(groups__name__contains="Administrateurs techniques")
            else:  # autres demandes
                destinataires = User.objects.filter(groups__name__contains="Administrateurs techniques")

            content = Bs(demande.contenu, "html.parser")
            url = 'https://' + Site.objects.get_current().domain + '/admin/aide/demande/' + str(demande.pk) + '/change/'
            if demande.nom or demande.prenom:
                body_text = "Ceci est une demande émise avec le formulaire de contact de la plateforme Manifestation Sportive." + chr(10) + \
                            "Envoyée par " + demande.nom+" " + demande.prenom + "." + chr(10) + \
                            "-------------------------------------------------------------------------------------------------" + chr(10) + \
                            demande.contenu
                body_html = "Ceci est une demande émise avec le formulaire de contact de la plateforme Manifestation Sportive.<hr>" \
                            "<br> Envoyée par " + demande.nom+" " + demande.prenom + "." + chr(
                    10) + demande.contenu
            else:
                body_text = "Ceci est une demande émise avec le formulaire de contact de la plateforme Manifestation Sportive." + chr(10) + \
                       "-------------------------------------------------------------------------------------------------" + chr(10) + \
                       demande.contenu
                body_html = "Ceci est une demande émise avec le formulaire de contact de la plateforme Manifestation Sportive.<hr>" + chr(
                    10) + demande.contenu
            body_text = Bs(body_text, "html.parser").get_text()



            mail = EmailMultiAlternatives(subject='[Plateforme Manifestation Sportive ' + demande.departement.name + '] ' + demande.get_type_display(),
                                          body=body_text,
                                          from_email=demande.email,
                                          reply_to=[demande.email],
                                          to=[d.email for d in destinataires],
                                          cc=['formulaire-contact@manifestationsportive.fr'],
                                          connection=get_connection(),
                                          )
            mail.attach_alternative(body_html, 'text/html')
            try:
                mail.send()
            except SMTPException as e:
                if hasattr(e, 'smtp_error'):
                    if type(e.smtp_error) is bytes:
                        msg = e.smtp_error.decode('utf8')
                    else:
                        msg = e.smtp_error
                else:
                    msg = '-'
                mail_logger.exception("aide.contact.demande: erreur smtp , " + str(type(e)) + ", message : " + msg)
                if settings.ADMINS:
                    try:
                        send_mail(subject="[Manifestation Sportive] SMTPException sur aide/demande",
                                  message="message d'erreur : " + msg,
                                  from_email=exception_sender_email,
                                  recipient_list=[admin[1] for admin in settings.ADMINS],
                                  connection=get_connection())
                    except SMTPException:
                        pass

            return render(request, "aide/contact-reponse.html")
    else:
        if not request.user.is_anonymous and hasattr(request.user, 'email'):
            form = DemandeForm(initial={'email': request.user.email})
        else:
            form = DemandeForm()
    return render(request, "aide/contact-form.html", {'form': form})
