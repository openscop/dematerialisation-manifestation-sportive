# coding: utf-8
import datetime

from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.db import models
from django.utils import timezone
from django.apps import apps

from core.util.types import make_iterable
from notifications.models.base import LogQuerySet


class ActionQuerySet(LogQuerySet):
    """ Queryset des actions """

    # Setter
    def log(self, users, action, manifestation):
        """ Consigner la trace d'une action """
        users = make_iterable(users)
        for user in users:
            user = getattr(user, 'user', user)
            if type(manifestation)._meta.object_name != 'Manif':
                self.create(user=user, action=action, manifestation=manifestation)
            else:
                self.create(user=user, action=action, manif=manifestation)


class Action(models.Model):
    """ Enregistrement d'une action sur une manifestation """

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        if self.manifestation:
            return ' - '.join([self.user.username, self.manifestation.name, self.action])
        return ' - '.join([self.user.username, self.manif.nom if self.manif.nom != None else 'nom indéfini !', self.action])

    # Champs
    creation_date = models.DateTimeField('date', default=timezone.now)
    action = models.CharField('action', max_length=255)
    manifestation = models.ForeignKey('events.manifestation', verbose_name="manifestation", on_delete=models.CASCADE, null=True)
    manif = models.ForeignKey('evenements.manif', verbose_name="manif", on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="utilisateur", on_delete=models.CASCADE)
    objects = ActionQuerySet.as_manager()

    # Meta
    class Meta:
        verbose_name = 'action'
        default_related_name = "actions"
        app_label = "notifications"
