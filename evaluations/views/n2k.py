# coding: utf-8
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView

from core.util.permissions import require_role
from organisateurs.decorators import verifier_proprietaire
from events.models import Manifestation
from evenements.models import Manif
from evaluations.forms import Natura2000EvaluationForm
from evaluations.models import N2KEvaluation
from protected_areas.models.n2k import SiteN2K


class Natura2000EvaluationCreate(CreateView):
    """ Création d'évaluation N2K """

    # Configuration
    model = N2KEvaluation
    form_class = Natura2000EvaluationForm

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        manifestation = get_object_or_404(Manifestation, pk=self.kwargs['manifestation_pk'])
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if N2KEvaluation.objects.filter(manifestation=manifestation).exists():
            return redirect(manifestation.get_absolute_url())
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        manifestation = get_object_or_404(Manifestation, pk=self.kwargs['manifestation_pk'])
        self.object = form.save(commit=False)
        self.object.manifestation = manifestation
        return super().form_valid(form)


class Natura2000EvalCreate(CreateView):
    """ Création d'évaluation N2K pour un modèle Manif"""

    # Configuration
    model = N2KEvaluation
    form_class = Natura2000EvaluationForm

    # Overrides
    @method_decorator(verifier_proprietaire)
    def dispatch(self, *args, **kwargs):
        self.manif = get_object_or_404(Manif, pk=self.kwargs['pk'])
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if N2KEvaluation.objects.filter(manif=self.manif).exists():
            return redirect(self.manif.get_absolute_url())
        return super().dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        form = super().get_form()
        dept = self.manif.ville_depart.get_departement()
        form.fields['sites'].queryset = SiteN2K.objects.filter(departement=dept)
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dept'] = self.manif.ville_depart.get_departement().name
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.manif = self.manif
        return super().form_valid(form)


@method_decorator(verifier_proprietaire(), name='dispatch')
class Natura2000EvaluationUpdate(UpdateView):
    """ Modification de l'évaluation N2K """

    # Configuration
    model = N2KEvaluation
    form_class = Natura2000EvaluationForm

    # Overrides
    def get_form(self, form_class=None):
        form = super().get_form()
        if hasattr(self.get_object(), 'manif'):
            dept = self.get_object().manif.ville_depart.get_departement()
            form.fields['sites'].queryset = SiteN2K.objects.filter(departement=dept)
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dept'] = self.get_object().manif.ville_depart.get_departement().name
        return context
