master : [![Build Status](https://travis-ci.com/openscop/dematerialisation-manifestation-sportive.svg?branch=master)](https://travis-ci.com/openscop/dematerialisation-manifestation-sportive) | preproduction : [![Build Status](https://travis-ci.com/openscop/dematerialisation-manifestation-sportive.svg?branch=preproduction)](https://travis-ci.com/openscop/dematerialisation-manifestation-sportive) | production : [![Build Status](https://travis-ci.com/openscop/dematerialisation-manifestation-sportive.svg?branch=production)](https://travis-ci.com/openscop/dematerialisation-manifestation-sportive)


manifestationsportive.fr
========================

Mettre en place l'environnement de développement
-

### Cloner le dépôt git

```
git clone git@bitbucket.org:openscop/d-mat.-manif.-sport.-v3.git
git pull
```

### Installer les paquets python de base
```
sudo aptitude install python python-dev build-essential libpq-dev python-pip
```

### Installer virtualenv
```
sudo pip install virtualenv
```

### Se placer à la racine du projet
```
cd d-mat.-manif.-sport.-v2/
```


### Créer un virtualenv spécialement pour le projet
Cet environnement virtuel aura pour utilité d'isoler les pacquest python de votre distribution linux des pacquets python utiles au projet
```
virtualenv env
source env/bin/activate
```

Cette dernière commande sera utile (et donc vous devrez l'executer) à chaque fois que l'on voudra travailler sur le projet

### Installer les dépendances requises par le projet
dans le fichier requirements.txt se trouvent les dépendances nécessaires à la plateforme de production
et dans requirements-dev.txt sont ajoutées celles nécessaires au développement ainsi qu'aux tests unitaires

```
pip install -r configuration/pip/requirements.txt
pip install -r configuration/pip/requirements-dev.txt
```


### Création des fichiers de configuration à l'aide des patrons fournis
```
cp portail/settings/samples/openrunner.sample.py portail/settings/thirdparty/openrunner.py
cp portail/settings/samples/base.sample.py portail/settings/base.py
```

Dans le nouveau fichier `portail/settings/base.py`, modifier les valeurs de :
- `STATIC_ROOT = '/home/<login>/...'` dossier des fichiers statiques
- `MEDIA_ROOT = '...'` dossier où sont enregistrés les fichiers envoyés


### Effectuer la migration initiale de la base de données 
La première commande va créer la base de données du projet ainsi que toutes les tables nécessaires au projet.

La seconde commande lance le serveur de développement.

```
#!
python manage.py migrate
python manage.py runserver_plus
```


### Télécharger les données de production

Cette étape nécessite d'avoir les bons identifiants (login et mot de passe) de la plateforme de production

```
#!
cp fabfile.sample.py fabfile.py
```

Dans le fichier fabfile.py, modifier la variable OWNER avec le login du serveur de production ainsi que les variables env.hostname, env.db_name, env.db_user, env.db_password de la fonction "def production()"

Lancer le script de la manière suivante :

```
#!

fab production dump_and_restore
```


### Se créer un compte administrateur

```
#!
python manage.py createsuperuser
```

L'application devrait être accessible à l'adresse suivante : `http://localhost:8000` et `http://localhost:8000/admin` pour l'interface d'administration.

### Style de code / Git

- [f] Fix
- [t] Test
- [m] Misc
- [+] Ajout
- [-] Suppression
- [d] Documentation
