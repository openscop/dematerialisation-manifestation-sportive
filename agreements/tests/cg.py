# coding: utf-8
from django.urls import reverse

from agreements.factories.factories import CGAvisFactory
from agreements.tests.base import AvisTestsBase
from authorizations.models.autorisation import ManifestationAutorisation


class CGAvisTests(AvisTestsBase):
    """ Test d'un avis CG """

    # Configuration
    def setUp(self):
        super().setUp()

    # Tests
    def test_access_url(self):
        """ Tester l'URL d'accès """
        avis = CGAvisFactory.create(authorization=self.authorization)
        self.assertEqual(avis.get_absolute_url(), reverse('agreements:cg_agreement_detail', kwargs={'pk': avis.pk}))

    def test_preavis(self):
        """ Vérifier l'état des préavis """
        avis = CGAvisFactory.create(authorization=self.authorization)
        self.assertTrue(avis.are_preavis_validated())  # Aucun préavis

    def test_workflow(self):
        """ Tester la création de l'avis """
        if isinstance(self.authorization, ManifestationAutorisation):
            self.authorization.cg_concerned = True  # Demander la création d'un avis CG
            self.authorization.save()
            self.assertEqual(self.authorization.get_avis_count(), 2)  # Avis fédération (tjs) + avis CG
            self.assertEqual(self.authorization.get_validated_avis_count(), 0)  # Aucun avis n'est rendu en toute logique
