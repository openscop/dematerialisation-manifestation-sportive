# Generated by Django 2.2.1 on 2019-07-05 07:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('messagerie', '0006_auto_20190704_1521'),
    ]

    operations = [
        migrations.AlterField(
            model_name='enveloppe',
            name='action_traitee',
            field=models.BooleanField(blank=True, null=True),
        ),
    ]
