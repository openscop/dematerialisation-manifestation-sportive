# coding: utf-8
"""
Effectuer un changement de commune sur tous les objets en relation avec cette commune
Commande à faire après la création de la commune de remplacement
Ne concerne pas l'ancienne réglementation
Utilisable uniquement en mode DEBUG.
"""
from django.core.management.base import BaseCommand
from django.contrib.contenttypes.models import ContentType
from django.db import transaction

from core.util.security import protect_code
from administrative_division.models import Commune
from administration.models import CGD, Brigade, CIS, Commissariat, MairieAgent
from organisateurs.models import Structure
from contacts.models import Adresse
from evaluation_incidence.models import N2kOperateurSite, RnrAdministrateur
from evenements.models import Manif
from instructions.models import Instruction, Avis

from agreements.models import MairieAvis
from events.models import Manifestation
from authorizations.models import ManifestationAutorisation


class Command(BaseCommand):
    """ Changement de commune dans la BD """
    args = ''
    help = 'Réaffecter tout ce qui concerne une commune à une autre'

    def handle(self, *args, **options):
        """ Exécuter la commande """
        protect_code()
        print('Inclure les anciennes apps dans le remplacement (y/n).')
        inclure = input()
        liste_modeles = ((MairieAgent, 'Agent mairie', 'commune'),
                         (CGD, 'Service CGD', 'commune'),
                         (Brigade, 'Service Brigade', 'commune'),
                         (CIS, 'Service CIS', 'commune'),
                         (Commissariat, 'Service Commissariat', 'commune'),
                         (Structure, 'Structure', 'commune'),
                         (Adresse, 'Adresse', 'commune'),
                         (N2kOperateurSite, 'Opérateur N2k', 'commune'),
                         (RnrAdministrateur, 'Administrateur Rnr', 'commune'),
                         (Manif, 'Ville de départ de manifestation', 'ville_depart'),
                         (Manif, 'Ville traversée de manifestation', 'villes_traversees'),
                         (Instruction, 'Ville concernée d\'instruction', 'villes_concernees'),
                         (Avis, 'Champ générique d\'avis', 'filtre_avis'))
        liste_modeles_old = ((MairieAvis, 'Avis de mairie (old)', 'commune'),
                             (Manifestation, 'Ville de départ de manifestation (old)', 'departure_city'),
                             (Manifestation, 'Ville traversée de manifestation (old)', 'crossed_cities'),
                             (ManifestationAutorisation, 'Ville concernée d\'instruction (old)', 'concerned_cities'))
        liste_travail = liste_modeles
        if inclure in ['Y', 'y', 'O', 'o']:
            liste_travail = liste_modeles + liste_modeles_old
        print('Entrer le code INSEE de la commune source :')
        code_old = input()
        if code_old.isdecimal():
            try:
                com_old = Commune.objects.get(code=code_old)
                print("Commune trouvée : " + com_old.get_zipcoded_name())
                print('Entrer le code INSEE de la commune de destination (si nouvelle, la crééer au préalable) : ')
                code_new = input()
                if code_new.isdecimal():
                    try:
                        com_new = Commune.objects.get(code=code_new)
                        print("Commune trouvée : " + com_new.get_zipcoded_name())
                        print()
                        print('Occurences à réaffecter :')
                        for travail in liste_travail:
                            if travail[2] == 'filtre_avis':
                                liste = travail[0].objects.filter(
                                    content_type=ContentType.objects.get(model='commune').id,
                                    object_id=com_old.pk)
                            else:
                                liste = travail[0].objects.filter(**{travail[2]: com_old})
                            if liste:
                                print(str(len(liste)) + ' ' + travail[1] + ' trouvé')
                                for obj in liste:
                                    print('\t' + str(obj))

                        print('Entrez Y pour changer la commune.')
                        if input() in ['Y', 'y', 'O', 'o']:
                            with transaction.atomic():
                                for travail in liste_travail:
                                    if travail[2] == 'filtre_avis':
                                        liste = travail[0].objects.filter(
                                            content_type=ContentType.objects.get(model='commune').id,
                                            object_id=com_old.pk)
                                    else:
                                        liste = travail[0].objects.filter(**{travail[2]: com_old})
                                    if liste:
                                        for obj in liste:
                                            if travail[2] == 'filtre_avis':
                                                setattr(obj, 'destination_object', com_new)
                                                obj.save()
                                            elif travail[2] == 'villes_traversees':
                                                obj.villes_traversees.remove(com_old)
                                                obj.villes_traversees.add(com_new)
                                            elif travail[2] == 'villes_concernees':
                                                obj.villes_concernees.remove(com_old)
                                                obj.villes_concernees.add(com_new)
                                            elif travail[2] == 'crossed_cities':
                                                obj.crossed_cities.remove(com_old)
                                                obj.crossed_cities.add(com_new)
                                            elif travail[2] == 'concerned_cities':
                                                obj.concerned_cities.remove(com_old)
                                                obj.concerned_cities.add(com_new)
                                            else:
                                                setattr(obj, travail[2], com_new)
                                                obj.save()
                                print('La commune source peut à présent être supprimée depuis l\'interface d\'administration.')
                        else:
                            print('Abandon')
                    except:
                        print('Pas de commune avec ce code !')
                else:
                    print('Le code doit être décimal !')
            except:
                print('Pas de commune avec ce code !')
        else:
            print('Le code doit être décimal !')
