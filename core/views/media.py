import urllib

from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.conf import settings

from oauth2_provider.models import AccessToken

from evenements.models import Manif


def media_access(request, path):
    """
    Fonction pour verifier le droit d'acces au fichier media
    - Fichier ckeditor ou tmp tout le monde
    - admin techniques : tout les droits
    - admin instance & instructeur & mairie agent : tous ceux de son instance
    - organisateur ceux de ses manif
    - autres selon le droit d'acces à la manif en question

    Nessecite de configurer Nginx avec le code suivant

    location /protected/
        {
            internal;
            alias /var/www/html/;
        }

    """
    # cas de fichiers publics d'aide
    if path.split('/')[0] == "uploads" or path.split('/')[0] == "exportation":
        response = HttpResponse()
        path = urllib.parse.quote(path)
        # Content-type will be detected by nginx
        del response['Content-Type']
        response['X-Accel-Redirect'] = '/protected/media/' + path
        return response

    # cas utilisateur non auth
    if not request.user.is_authenticated:
        refus = True
        # avons-nous un token d'accès d'API ?
        if 'Authorization' in request.headers:
            auth = request.headers.get('Authorization').split(' ')
            if auth[0] == 'Bearer':
                if AccessToken.objects.filter(token=auth[1]).exists():
                    request.user = AccessToken.objects.get(token=auth[1]).user
                    refus = False
        if refus:
            return HttpResponseForbidden("Vous n'avez pas les droits pour voir ce fichier")

    # cas de fichiers de manifestation
    if len(path.split("/")) < 2:
        return HttpResponseForbidden("url incorrecte")
    manif_id = path.split("/")[1]
    if not manif_id.isdigit():
        return HttpResponseForbidden("Vous n'avez pas les droits pour voir ce fichier")

    manif = get_object_or_404(Manif, pk=manif_id)
    user = request.user
    ok = False
    if user.has_group('Administrateurs techniques'):
        ok = True
    elif user.has_group("Administrateurs d'instance"):
        if manif.get_instance() == user.get_instance():
            ok = True
    elif hasattr(user, 'organisateur') and user.organisateur == manif.get_organisateur():
        ok = True
    elif hasattr(manif, "instruction"):
        if hasattr(user, 'instructeur') and user in manif.instruction.get_instructeurs_prefecture(arr_wrt=False):
            ok = True
        if user.has_role('agent') and manif.instruction.get_avis_user(user):
            ok = True
        if user.has_role('mairieagent') and user in manif.instruction.get_instructeurs_mairie():
                ok = True
        if user.has_role('agentlocal') and manif.instruction.get_preavis_user(user):
            ok = True

    if ok:
        response = HttpResponse()
        # Content-type will be detected by nginx
        del response['Content-Type']
        # gunicorn et nginx ne semble pas vouloir se comprendre cette ligne permet de mettre le bon encodage
        path = urllib.parse.quote(path)
        response['X-Accel-Redirect'] = '/protected/media/' + path
        if settings.TESTS_IN_PROGRESS:
            response.content = 'ok pour voir le fichier'
        return response
    else:
        return HttpResponseForbidden("Vous n'avez pas les droits pour voir ce fichier")
