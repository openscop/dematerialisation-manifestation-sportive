# coding: utf-8
"""
Préprocesseurs de requête
"""
from django.conf import settings

from core.models.instance import Instance


def core(request):
    """
    Renvoie toutes les informations d'instance d'application

    Par défaut, les informations sont récupérées depuis le sous-domaine.
    Si l'utilisateur est authentifié, alors récupérer ses options
    au lieu de celles du sous-domaine.

    Ce que fait ce middleware :
    - Ajoute un attribut `subdomain` à l'objet request
    """

    # Contexte de sortie
    context = {}
    context.update({'INSTANCE': Instance.objects.get_for_request(request)})  # INSTANCE
    context.update(Instance.WORKFLOW_NAMES)  # WORKFLOW_GGD_EDSR etc.
    context.update({'TEST': settings.TESTS_IN_PROGRESS})
    context.update({'MESSAGERIE_CPT_TIME_REFRESH': settings.MESSAGERIE_CPT_TIME_REFRESH})
    return context
