# coding: utf-8
from datetime import timedelta

from django.views.generic.base import TemplateView
from django.db.models import Count, Q, F
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from collections import OrderedDict

from core.models import User
from events.models import Manifestation as EventsManifestation
from evenements.models import Manif as EvenementManif, Avtm, Avtmcir, Dvtm, Dnm, Dnmc, Dcnm, Dcnmc
from agreements.models import Avis as AgreementsAvis
from sub_agreements.models import PreAvis as SubAgreementsPreAvis
from instructions.models import Avis as InstructionAvis
from instructions.models import PreAvis as InstructionPreAvis
from organisateurs.models import Organisateur
from administration.models import Agent, AgentLocal, MairieAgent, Instructeur
from notifications.models import Action, Notification
from messagerie.models import Enveloppe
from administrative_division.models.departement import Departement
from evaluations.models import N2KEvaluation, RNREvaluation
from evaluation_incidence.models import EvaluationN2K, EvaluationRnr
from configuration import settings

@method_decorator(login_required, name='dispatch')
class Stat(TemplateView):
    """ Page de statistique """

    # Configuration
    template_name = 'stat.html'

    def count_by_year(self, **kwargs):
        data = None
        if "liste" in kwargs and "critere" in kwargs:
            liste = kwargs.get('liste')
            critere = kwargs.get('critere')
            data = {}
            for y in range(2014, timezone.now().year + 2):
                filter = critere + "__year"
                data[y] = liste.filter(**{filter: y}).count()
        return OrderedDict(sorted(data.items(), key=lambda t: t[0]))

    def merge_dicts(self, d1, d2):
        array = [d1, d2]
        result = {}
        for k in d1.keys():
            result[k] = tuple(result[k] for result in array)
        return result

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)

        stat = {}
        stat['annees'] = range(2014, timezone.now().year + 2)

        liste_dept = []
        # récupérer la liste des départements
        for d in Departement.objects.configured():
            liste_dept.append((d.name, d))
        context['liste_dept'] = liste_dept

        # filtrage par département
        dept = ""
        if self.request.user.get_departement():             # département de l'utilisateur par défaut (s'il en a un)
            dept = self.request.user.get_departement().name
        if self.request.GET.get('dept'):                    # département demandé en paramètre d'url ?
            if self.request.user.is_superuser or self.request.GET.get('dept') == dept:   # vérifie que l'utilisateur peut consulter le département demandé
                dept = self.request.GET.get('dept')
            if self.request.GET.get('dept')=="nationale":   # ne pas définir de département si "nationale" passé en paramètre d'url
                dept=""
        if dept:                                            # chargé un département dans le filtre si nécessaire
            context['dept'] = Departement.objects.get(name__startswith=dept)
        filtre_old_dept = Q(departure_city__arrondissement__departement=context['dept']) if dept else Q()
        filtre_new_dept = Q(ville_depart__arrondissement__departement=context['dept']) if dept else Q()

        # Nombre de sessions en cours
        nb_session_filter = Q(default_instance__departement=context['dept']) if dept else Q()
        last_time = timezone.now() - timedelta(minutes=settings.INACTIVE_TIME)
        stat['connected_user'] = User.objects.filter(nb_session_filter, last_visit__gte=last_time).count()

        # Nombre de nouveaux utilisateurs
        nb_utilisateur_filter = Q(default_instance__departement=context['dept']) if dept else Q()
        stat['nb_utilisateur'] = User.objects.filter(nb_utilisateur_filter).count()
        stat['nb_utilisateur_par_annee'] = self.count_by_year(
            liste=User.objects.filter(nb_utilisateur_filter), critere="date_joined")

        # Nombre de nouveaux instructeurs
        nb_instructeur_filter = Q(user__default_instance__departement=context['dept']) if dept else Q()
        stat['nb_instructeur'] = Instructeur.objects.filter(nb_instructeur_filter).count()
        stat['nb_instructeur_par_annee'] = self.count_by_year(
            liste=Instructeur.objects.filter(nb_instructeur_filter), critere="user__date_joined")

        # Nombre de nouveaux organisateurs
        nb_utilisateur_subclass_filter = Q(user__default_instance__departement=context['dept']) if dept else Q()
        stat['nb_organisateur'] = Organisateur.objects.filter(nb_utilisateur_subclass_filter).count()
        stat['nb_organisateur_par_annee'] = self.count_by_year(
            liste=Organisateur.objects.filter(nb_utilisateur_subclass_filter), critere="user__date_joined")

        # Nombre d'agents
        nb_agent_subclass_filter = Q(user__default_instance__departement=context['dept']) if dept else Q()
        stat['nb_agent'] = Agent.objects.filter(nb_agent_subclass_filter).count()
        stat['nb_agent_par_annee'] = self.count_by_year(
            liste=Agent.objects.filter(nb_agent_subclass_filter), critere="user__date_joined")

        # Nombre d'agents locaux
        nb_agentlocaux_subclass_filter = Q(user__default_instance__departement=context['dept']) if dept else Q()
        stat['nb_agentlocaux'] = AgentLocal.objects.filter(nb_agentlocaux_subclass_filter).count()
        stat['nb_agentlocaux_par_annee'] = self.count_by_year(
            liste=AgentLocal.objects.filter(nb_agentlocaux_subclass_filter), critere="user__date_joined")

        # Nombre d'agents mairie
        nb_agentmairie_subclass_filter = Q(user__default_instance__departement=context['dept']) if dept else Q()
        stat['nb_agentmairie'] = MairieAgent.objects.filter(nb_agentmairie_subclass_filter).count()
        stat['nb_agentmairie_par_annee'] = self.count_by_year(
            liste=MairieAgent.objects.filter(nb_agentmairie_subclass_filter), critere="user__date_joined")

        # Nombre de dossiers prévus
        stat['nb_dossier'] = EventsManifestation.objects.filter(filtre_old_dept).count() + EvenementManif.objects.filter(filtre_new_dept).count()
        if stat['nb_dossier'] == 0: # éviter une division par zéro
            stat['nb_dossier'] = 0.1
        ancien = self.count_by_year(liste=EventsManifestation.objects.filter(filtre_old_dept), critere="begin_date")
        nouveau = self.count_by_year(liste=EvenementManif.objects.filter(filtre_new_dept), critere="date_debut")
        stat['nb_dossier_par_annee'] = OrderedDict([(key, ancien[key] + nouveau[key]) for key in ancien.keys()])

        # Nombre de dossiers ancien prévus
        stat['nb_dossier_anciens'] = EventsManifestation.objects.filter(
            filtre_old_dept).count()
        stat['pourcentage_ancien'] = round((stat['nb_dossier_anciens'] * 100) / stat['nb_dossier'])
        stat['ancien_par_annee'] = self.count_by_year(liste=EventsManifestation.objects.filter(filtre_old_dept), critere="begin_date")

        # Nombre de dossiers prévus par type de cerfa
        # DNM
        filtre_cerfa_dept = Q(manifestation_ptr__ville_depart__arrondissement__departement=context['dept']) if dept else Q()
        stat['nb_cerfa_dnm'] = Dnm.objects.filter(filtre_cerfa_dept).count()
        stat['pourcentage_cerfa_dnm'] = round((stat['nb_cerfa_dnm'] * 100) / stat['nb_dossier'])
        nb_cerfa_dnm_par_annee = self.count_by_year(liste=Dnm.objects.filter(filtre_cerfa_dept), critere="date_debut")
        pourcentage_cerfa_dnm_par_annee = OrderedDict()
        for key, value in nb_cerfa_dnm_par_annee.items():
            pourcentage_cerfa_dnm_par_annee[key] = round(
                (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
        stat['dnm_par_annee'] = self.merge_dicts(nb_cerfa_dnm_par_annee, pourcentage_cerfa_dnm_par_annee)

        # DNMC
        stat['nb_cerfa_dnmc'] = Dnmc.objects.filter(filtre_cerfa_dept).count()
        stat['pourcentage_cerfa_dnmc'] = round((stat['nb_cerfa_dnmc'] * 100) / stat['nb_dossier'])
        nb_cerfa_dnmc_par_annee = self.count_by_year(liste=Dnmc.objects.filter(filtre_cerfa_dept), critere="date_debut")
        pourcentage_cerfa_dnmc_par_annee = OrderedDict()
        for key, value in nb_cerfa_dnmc_par_annee.items():
            pourcentage_cerfa_dnmc_par_annee[key] = round(
                (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
        stat['dnmc_par_annee'] = self.merge_dicts(nb_cerfa_dnmc_par_annee, pourcentage_cerfa_dnmc_par_annee)

        # DCNM
        stat['nb_cerfa_dcnm'] = Dcnm.objects.filter(filtre_cerfa_dept).count()
        stat['pourcentage_cerfa_dcnm'] = round((stat['nb_cerfa_dcnm'] * 100) / stat['nb_dossier'])
        nb_cerfa_dcnm_par_annee = self.count_by_year(liste=Dcnm.objects.filter(filtre_cerfa_dept), critere="date_debut")
        pourcentage_cerfa_dcnm_par_annee = OrderedDict()
        for key, value in nb_cerfa_dcnm_par_annee.items():
            pourcentage_cerfa_dcnm_par_annee[key] = round(
                (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
        stat['dcnm_par_annee'] = self.merge_dicts(nb_cerfa_dcnm_par_annee, pourcentage_cerfa_dcnm_par_annee)

        # DCNMC
        stat['nb_cerfa_dcnmc'] = Dcnmc.objects.filter(filtre_cerfa_dept).count()
        stat['pourcentage_cerfa_dcnmc'] = round((stat['nb_cerfa_dcnmc'] * 100) / stat['nb_dossier'])
        nb_cerfa_dcnmc_par_annee = self.count_by_year(liste=Dcnmc.objects.filter(filtre_cerfa_dept), critere="date_debut")
        pourcentage_cerfa_dcnmc_par_annee = OrderedDict()
        for key, value in nb_cerfa_dcnmc_par_annee.items():
            pourcentage_cerfa_dcnmc_par_annee[key] = round(
                (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
        stat['dcnmc_par_annee'] = self.merge_dicts(nb_cerfa_dcnmc_par_annee, pourcentage_cerfa_dcnmc_par_annee)

        # DVTM
        stat['nb_cerfa_dvtm'] = Dvtm.objects.filter(filtre_cerfa_dept).count()
        stat['pourcentage_cerfa_dvtm'] = round((stat['nb_cerfa_dvtm'] * 100) / stat['nb_dossier'])
        nb_cerfa_dvtm_par_annee = self.count_by_year(liste=Dvtm.objects.filter(filtre_cerfa_dept), critere="date_debut")
        pourcentage_cerfa_dvtm_par_annee = OrderedDict()
        for key, value in nb_cerfa_dvtm_par_annee.items():
            pourcentage_cerfa_dvtm_par_annee[key] = round(
                (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
        stat['dvtm_par_annee'] = self.merge_dicts(nb_cerfa_dvtm_par_annee, pourcentage_cerfa_dvtm_par_annee)

        # AVTM
        stat['nb_cerfa_avtm'] = Avtm.objects.filter(filtre_cerfa_dept).count()
        stat['pourcentage_cerfa_avtm'] = round((stat['nb_cerfa_avtm'] * 100) / stat['nb_dossier'])
        nb_cerfa_avtm_par_annee = self.count_by_year(liste=Avtm.objects.filter(filtre_cerfa_dept), critere="date_debut")
        pourcentage_cerfa_avtm_par_annee = OrderedDict()
        for key, value in nb_cerfa_avtm_par_annee.items():
            pourcentage_cerfa_avtm_par_annee[key] = round(
                (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
        stat['avtm_par_annee'] = self.merge_dicts(nb_cerfa_avtm_par_annee, pourcentage_cerfa_avtm_par_annee)

        # AVTMCIR
        stat['nb_cerfa_avtmcir'] = Avtmcir.objects.filter(filtre_cerfa_dept).count()
        stat['pourcentage_cerfa_avtmcir'] = round((stat['nb_cerfa_avtmcir'] * 100) / stat['nb_dossier'])
        nb_cerfa_avtmcir_par_annee = self.count_by_year(liste=Avtmcir.objects.filter(filtre_cerfa_dept), critere="date_debut")
        pourcentage_cerfa_avtmcir_par_annee = OrderedDict()
        for key, value in nb_cerfa_avtmcir_par_annee.items():
            pourcentage_cerfa_avtmcir_par_annee[key] = round(
                (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
        stat['avtmcir_par_annee'] = self.merge_dicts(nb_cerfa_avtmcir_par_annee, pourcentage_cerfa_avtmcir_par_annee)

        # Nombre de manifestations créées
        # Calcul du pourcentage incohérent du fait du décalage entre la date de dépot de
        # dossier et la date réelle de la manifestation
        stat['nb_manif_crees'] = EventsManifestation.objects.filter(filtre_old_dept).count() + \
                                  EvenementManif.objects.filter(filtre_new_dept).count()
        # stat['pourcentage_manif_prevue'] = round(
        #     (stat['nb_manif_crees'] * 100) / stat['nb_dossier'])
        ancien = self.count_by_year(liste=EventsManifestation.objects.filter(filtre_old_dept), critere="creation_date")
        nouveau = self.count_by_year(liste=EvenementManif.objects.filter(filtre_new_dept), critere="date_creation")
        stat['nb_manif_crees_par_annee'] = OrderedDict([(key, ancien[key] + nouveau[key]) for key in ancien.keys()])

        # Nombre de manifestations publiées dans le calendrier
        nb_manif_dans_calendrier_ancien_filter = Q(
            registered_calendar=True, departure_city__arrondissement__departement=context['dept']) if dept else Q(registered_calendar=True)
        nb_manif_dans_calendrier_nouveau_filter = Q(
            dans_calendrier=True, ville_depart__arrondissement__departement=context['dept']) if dept else Q(dans_calendrier=True)
        stat['nb_manif_dans_calendrier'] = EventsManifestation.objects.filter(nb_manif_dans_calendrier_ancien_filter).count() + \
                                           EvenementManif.objects.filter(nb_manif_dans_calendrier_nouveau_filter).count()
        stat['pourcentage_manif_dans_calendrier'] = round((stat['nb_manif_dans_calendrier'] * 100) / stat['nb_dossier'])
        nouveau = self.count_by_year(
            liste=EvenementManif.objects.filter(nb_manif_dans_calendrier_nouveau_filter), critere="date_debut")
        ancien = self.count_by_year(
            liste=EventsManifestation.objects.filter(nb_manif_dans_calendrier_ancien_filter), critere="begin_date")
        nb_manif_dans_calendrier_par_annee = OrderedDict([(key, nouveau[key] + ancien[key]) for key in ancien.keys()])
        pourcentage_manif_dans_calendrier_par_annee = OrderedDict()
        for key, value in nb_manif_dans_calendrier_par_annee.items():
            pourcentage_manif_dans_calendrier_par_annee[key] = round(
                (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
        stat['manif_dans_calendrier_par_annee'] = self.merge_dicts(nb_manif_dans_calendrier_par_annee, pourcentage_manif_dans_calendrier_par_annee)

        # Nombre de manifestations motorisées
        manif_old_moto = EventsManifestation.objects.filter(activite__discipline__motorise=True).filter(filtre_old_dept)
        manif_new_moto = EvenementManif.objects.filter(activite__discipline__motorise=True).filter(filtre_new_dept)
        stat['nb_manif_motorisees'] = manif_new_moto.count() + manif_old_moto.count()
        stat['pourcentage_manif_motorisees'] = round((stat['nb_manif_motorisees'] * 100) / stat['nb_dossier'])
        old_moto = self.count_by_year(liste=manif_old_moto, critere="begin_date")
        new_moto = self.count_by_year(liste=manif_new_moto, critere="date_debut")
        nb_manif_motorisees_par_annee = OrderedDict([(key, new_moto[key] + old_moto[key]) for key in new_moto.keys()])
        pourcentage_manif_motorisees_par_annee = OrderedDict()
        for key, value in nb_manif_motorisees_par_annee.items():
            pourcentage_manif_motorisees_par_annee[key] = round(
                (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
        stat['manif_motorisees_par_annee'] = self.merge_dicts(nb_manif_motorisees_par_annee, pourcentage_manif_motorisees_par_annee)

        # Nombre de manifestations non motorisées
        manif_old_not_moto = EventsManifestation.objects.filter(activite__discipline__motorise=False).filter(filtre_old_dept)
        manif_new_not_moto = EvenementManif.objects.filter(activite__discipline__motorise=False).filter(filtre_new_dept)
        stat['nb_manif_non_motorisees'] = manif_new_not_moto.count() + manif_old_not_moto.count()
        stat['pourcentage_manif_non_motorisees'] = round((stat['nb_manif_non_motorisees'] * 100) / stat['nb_dossier'])
        old_not_moto = self.count_by_year(liste=manif_old_not_moto, critere="begin_date")
        new_not_moto = self.count_by_year(liste=manif_new_not_moto, critere="date_debut")
        nb_manif_non_motorisees_par_annee = OrderedDict([(key, new_not_moto[key] + old_not_moto[key]) for key in new_not_moto.keys()])
        pourcentage_manif_non_motorisees_par_annee = OrderedDict()
        for key, value in nb_manif_non_motorisees_par_annee.items():
            pourcentage_manif_non_motorisees_par_annee[key] = round(
                (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
        stat['manif_non_motorisees_par_annee'] = self.merge_dicts(nb_manif_non_motorisees_par_annee, pourcentage_manif_non_motorisees_par_annee)

        # Nombre de manifestations sur une seule commune
        filtre_old_1_comm = (Q(departure_city__in=F('crossed_cities')) & Q(nbr_cities=1) | Q(crossed_cities__isnull=True))
        stat['nb_manif_une_commune'] = EventsManifestation.objects.annotate(nbr_cities=Count('crossed_cities')).filter(
            filtre_old_1_comm).filter(filtre_old_dept).count() + EvenementManif.objects.filter(villes_traversees__isnull=True).filter(filtre_new_dept).count()
        stat['pourcentage_manif_une_commune'] = round((stat['nb_manif_une_commune'] * 100) / stat['nb_dossier'])
        ancien = self.count_by_year(liste=EventsManifestation.objects.annotate(nbr_cities=Count('crossed_cities')).filter(
            filtre_old_1_comm).filter(filtre_old_dept), critere="begin_date")
        nouveau = self.count_by_year(liste=EvenementManif.objects.filter(villes_traversees__isnull=True).filter(
            filtre_new_dept), critere="date_debut")
        nb_manif_une_commune_par_annee = OrderedDict([(key, ancien[key] + nouveau[key]) for key in ancien.keys()])
        pourcentage_manif_une_commune_par_annee = OrderedDict()
        for key, value in nb_manif_une_commune_par_annee.items():
            pourcentage_manif_une_commune_par_annee[key] = round(
                (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
        stat['manif_une_commune_par_annee'] = self.merge_dicts(nb_manif_une_commune_par_annee, pourcentage_manif_une_commune_par_annee)

        # Nombre d'évaluations N2k
        nb_eval_n2k_old_old_filter = Q(manifestation__departure_city__arrondissement__departement=context['dept']) if dept else Q()
        nb_eval_n2k_old_new_filter = Q(manif__ville_depart__arrondissement__departement=context['dept']) if dept else Q()
        nb_eval_n2k_new_filter = Q(manif__ville_depart__arrondissement__departement=context['dept']) if dept else Q()
        eval_n2k_old = N2KEvaluation.objects.filter(nb_eval_n2k_old_old_filter | nb_eval_n2k_old_new_filter)
        eval_n2k_new = EvaluationN2K.objects.filter(nb_eval_n2k_new_filter)
        stat['nb_eval_n2k'] = eval_n2k_old.count() + eval_n2k_new.count()
        stat['pourcentage_eval_n2k'] = round((stat['nb_eval_n2k'] * 100) / stat['nb_dossier'])
        ancien1 = self.count_by_year(liste=eval_n2k_old, critere="manifestation__begin_date")
        ancien2 = self.count_by_year(liste=eval_n2k_old, critere="manif__date_debut")
        nouveau = self.count_by_year(liste=eval_n2k_new, critere="manif__date_debut")
        nb_eval_n2k_par_annee = OrderedDict([(key, ancien1[key] + ancien2[key] + nouveau[key]) for key in ancien1.keys()])
        pourcentage_eval_n2k_par_annee = OrderedDict()
        for key, value in nb_eval_n2k_par_annee.items():
            pourcentage_eval_n2k_par_annee[key] = round(
                (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
        stat['nb_eval_n2k_par_annee'] = self.merge_dicts(nb_eval_n2k_par_annee, pourcentage_eval_n2k_par_annee)

        # Nombre d'évaluations Rnr
        nb_eval_rnr_old_old_filter = Q(manifestation__departure_city__arrondissement__departement=context['dept']) if dept else Q()
        nb_eval_rnr_old_new_filter = Q(manif__ville_depart__arrondissement__departement=context['dept']) if dept else Q()
        nb_eval_rnr_new_filter = Q(manif__ville_depart__arrondissement__departement=context['dept']) if dept else Q()
        eval_rnr_old = RNREvaluation.objects.filter(nb_eval_rnr_old_old_filter | nb_eval_rnr_old_new_filter)
        eval_rnr_new = EvaluationRnr.objects.filter(nb_eval_rnr_new_filter)
        stat['nb_eval_rnr'] = eval_rnr_old.count() + eval_rnr_new.count()
        stat['pourcentage_eval_rnr'] = round((stat['nb_eval_rnr'] * 100) / stat['nb_dossier'])
        ancien1 = self.count_by_year(liste=eval_rnr_old, critere="manifestation__begin_date")
        ancien2 = self.count_by_year(liste=eval_rnr_old, critere="manif__date_debut")
        nouveau = self.count_by_year(liste=eval_rnr_new, critere="manif__date_debut")
        nb_eval_rnr_par_annee = OrderedDict([(key, ancien1[key] + ancien2[key] + nouveau[key]) for key in ancien1.keys()])
        pourcentage_eval_rnr_par_annee = OrderedDict()
        for key, value in nb_eval_rnr_par_annee.items():
            pourcentage_eval_rnr_par_annee[key] = round(
                (value * 100) / stat['nb_dossier_par_annee'][key]) if stat['nb_dossier_par_annee'][key] else 0
        stat['nb_eval_rnr_par_annee'] = self.merge_dicts(nb_eval_rnr_par_annee, pourcentage_eval_rnr_par_annee)

        # Nombre d'avis émis
        nb_avis_ancien_filter = Q(authorization__manifestation__departure_city__arrondissement__departement=context['dept']) if dept else Q()
        nb_avis_nouveau_filter = Q(instruction__manif__ville_depart__arrondissement__departement=context['dept']) if dept else Q()
        stat['nb_avis'] = AgreementsAvis.objects.filter(nb_avis_ancien_filter).count() + InstructionAvis.objects.filter(nb_avis_nouveau_filter).count()
        ancien = self.count_by_year(liste=AgreementsAvis.objects.filter(nb_avis_ancien_filter), critere="request_date")
        nouveau = self.count_by_year(liste=InstructionAvis.objects.filter(nb_avis_nouveau_filter), critere="date_demande")
        stat['nb_avis_par_annee'] = OrderedDict([(key, ancien[key] + nouveau[key]) for key in ancien.keys()])

        # Nombre de préavis émis
        nb_preavis_ancien_filter = Q(avis__authorization__manifestation__departure_city__arrondissement__departement=context['dept']) if dept else Q()
        nb_preavis_nouveau_filter = Q(avis__instruction__manif__ville_depart__arrondissement__departement=context['dept']) if dept else Q()
        stat['nb_preavis'] = SubAgreementsPreAvis.objects.filter(nb_preavis_ancien_filter).count() + \
                             InstructionPreAvis.objects.filter(nb_preavis_nouveau_filter).count()
        ancien = self.count_by_year(liste=SubAgreementsPreAvis.objects.filter(nb_preavis_ancien_filter), critere="request_date")
        nouveau = self.count_by_year(liste=InstructionPreAvis.objects.filter(nb_preavis_nouveau_filter), critere="date_demande")
        stat['nb_preavis_par_annee'] = OrderedDict([(key, ancien[key] + nouveau[key]) for key in ancien.keys()])

        # Nombre d'actions majeures réalisées
        nb_action_ancien_filter = Q(manifestation__departure_city__arrondissement__departement=context['dept']) if dept else Q()
        nb_action_nouveau_filter = Q(manif__ville_depart__arrondissement__departement=context['dept']) if dept else Q()
        stat['nb_action'] = Action.objects.filter(nb_action_nouveau_filter).count() + Action.objects.filter(nb_action_ancien_filter).count()
        ancien = self.count_by_year(liste=Action.objects.filter(nb_action_ancien_filter), critere="creation_date")
        nouveau = self.count_by_year(liste=Action.objects.filter(nb_action_nouveau_filter), critere="creation_date")
        stat['nb_action_par_annee'] = OrderedDict([(key, ancien[key] + nouveau[key]) for key in ancien.keys()])

        # Nombre de notifications envoyées
        stat['nb_notification'] = Notification.objects.filter(nb_utilisateur_subclass_filter).count()
        stat['nb_notification_par_annee'] = self.count_by_year(liste=Notification.objects.filter(nb_utilisateur_subclass_filter), critere="creation_date")

        # Nombre de conversation envoyées
        stat['nb_msg_conversation'] = Enveloppe.objects.filter(type="conversation").distinct('corps').count()
        stat['nb_msg_conversation_par_annee'] = self.count_by_year(liste=Enveloppe.objects.filter(type="conversation").distinct('corps'), critere="date")

        # Nombre d'action envoyées
        stat['nb_msg_action'] = Enveloppe.objects.filter(type="action").distinct('corps').count()
        stat['nb_msg_action_par_annee'] = self.count_by_year(liste=Enveloppe.objects.filter(type="action").distinct('corps'),
                                                               critere="date")

        # Nombre d'info_suivi envoyées
        stat['nb_msg_info_suivi'] = Enveloppe.objects.filter(type="info_suivi").distinct('corps').count()
        stat['nb_msg_info_suivi_par_annee'] = self.count_by_year(liste=Enveloppe.objects.filter(type="info_suivi").distinct('corps'),
                                                         critere="date")

        # Nombre de news envoyées
        stat['nb_msg_news'] = Enveloppe.objects.filter(type="news").distinct('corps').count()
        stat['nb_msg_news_par_annee'] = self.count_by_year(liste=Enveloppe.objects.filter(type="news").distinct('corps'),
                                                             critere="date")

        # Nombre de tracabillite envoyées
        stat['nb_msg_tracabilite'] = Enveloppe.objects.filter(type="tracabilite").distinct('corps').count()
        stat['nb_msg_tracabilite_par_annee'] = self.count_by_year(liste=Enveloppe.objects.filter(type="tracabilite").distinct('corps'),
                                                       critere="date")
        
        # Code commenté car execution très lente
        # stat['nb_manif_validee'] = [m.processing() for m in Manifestation.objects.all()].count(True)
        # data = {}
        # for y in range(2014, timezone.now().year + 2):
        #     data[y] = [m.processing() for m in Manifestation.objects.filter(begin_date__contains=y).all()].count(True)
        # stat['nb_manif_validee_par_annee'] = data

        context['stat'] = stat
        return context
