# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView

from ..decorators import *
from ..forms import *
from ..models import *


class StructureCreate(CreateView):
    """ Créer une structure """

    # Configuration
    model = Structure
    form_class = StructureForm

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        return super(StructureCreate, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        """ Le formulaire est valide """
        form.instance.organisateur = self.request.user.organisateur
        return super(StructureCreate, self).form_valid(form)


class StructureUpdateView(UpdateView):
    """ Modifier une structure """

    # Configuration
    model = Structure
    form_class = StructureForm
    success_url = '/accounts/profile/'

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        """ Protéger la vue pour l'accès aux organisateurs uniquement """
        return super(StructureUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        """ Renvoyer l'instance à modifier """
        return self.request.user.organisateur.structure

    def get_form_kwargs(self):
        """ Renvoyer le dictionnaire kwargs utilisé pour initialiser le formulaire """
        self.request.session['extradata'] = ''
        kwargs = super().get_form_kwargs()
        kwargs['initial'].update({'departement': self.object.commune.get_departement()})
        return kwargs

    def get_form(self, form_class=None):
        self.request.session['extradata'] = ''
        form = super().get_form(form_class)
        if self.object and self.object.commune:
            form.fields['commune'].queryset = Commune.objects.by_departement(self.object.commune.get_departement())
        return form
