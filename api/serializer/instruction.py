from rest_framework import serializers

from instructions.models import Instruction, Avis, PreAvis


class PreAvisRendreSerializer(serializers.ModelSerializer):

    class Meta:
        ref_name = None
        model = PreAvis
        fields = (
            'favorable',
            'prescriptions',
        )


class PreAvisCourtSerializer(serializers.ModelSerializer):

    service = serializers.CharField(source='destination_object')

    class Meta:
        ref_name = None
        model = PreAvis
        fields = (
            'id',
            'service',
            'etat',
        )


class PreAvisSerializer(serializers.ModelSerializer):

    service = serializers.CharField(source='destination_object')
    manif_id = serializers.IntegerField(source='avis.instruction.manif.pk')

    class Meta:
        model = PreAvis
        fields = (
            'id',
            'etat',
            'service_concerne',
            'service',
            'agentlocal',
            'date_demande',
            'date_reponse',
            'favorable',
            'prescriptions',
            'manif_id',
        )


class AvisRendreSerializer(serializers.ModelSerializer):

    class Meta:
        ref_name = None
        model = PreAvis
        fields = (
            'favorable',
            'prescriptions',
        )


class AvisCourtSerializer(serializers.ModelSerializer):

    service = serializers.CharField(source='destination_object')

    class Meta:
        ref_name = None
        model = PreAvis
        fields = (
            'id',
            'service',
            'etat',
        )


class AvisDetailSerializer(serializers.ModelSerializer):

    service = serializers.CharField(source='destination_object')
    preavis = PreAvisSerializer(many=True, read_only=True)
    manif_id = serializers.IntegerField(source='instruction.manif.pk')

    class Meta:
        ref_name = 'Avis'
        model = Avis
        fields = (
            'id',
            'etat',
            'service_concerne',
            'service',
            'agent',
            'date_demande',
            'date_reponse',
            'favorable',
            'prescriptions',
            'manif_id',
            'preavis',
        )


class AvisSerializer(serializers.ModelSerializer):

    service = serializers.CharField(source='destination_object')
    preavis = PreAvisCourtSerializer(many=True, read_only=True)
    manif_id = serializers.IntegerField(source='instruction.manif.pk')

    class Meta:
        ref_name = None
        model = Avis
        fields = (
            'id',
            'etat',
            'service_concerne',
            'service',
            'date_demande',
            'date_reponse',
            'favorable',
            'manif_id',
            'preavis',
        )


class InstructionDetailSerializer(serializers.ModelSerializer):

    manif_id = serializers.IntegerField(source='manif.id')
    referent = serializers.StringRelatedField()
    avis = AvisSerializer(many=True)

    class Meta:
        ref_name = 'Instruction'
        model = Instruction
        fields = (
            'id',
            'etat',
            'date_creation',
            'date_consult',
            'date_derniere_action',
            'referent',
            'doc_verif',
            'manif_id',
            'avis'
        )


class InstructionSerializer(serializers.ModelSerializer):

    manif_id = serializers.IntegerField(source='manif.id')
    avis = AvisCourtSerializer(many=True)

    class Meta:
        ref_name = None
        model = Instruction
        fields = (
            'id',
            'etat',
            'date_creation',
            'manif_id',
            'avis'
        )
