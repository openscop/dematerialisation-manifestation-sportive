Bonjour,

L'utilisateur {{ username }} a vu son mot de passe expiré.
Un lien de changement de mot de passe lui a été envoyé à l'adresse {{ email }}

=== VEUILLEZ NE PAS RÉPONDRE À CET E-MAIL ===