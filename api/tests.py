from django.test import TestCase
from django.utils import timezone
from django.contrib.auth import hashers
from django.contrib.auth.models import Group
from core.models import User
from organisateurs.factories import OrganisateurFactory, StructureFactory
from administration.factories import InstructeurFactory, DDSPAgentFactory, CommissariatAgentFactory, CommissariatFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from core.factories import UserFactory
from sports.factories.sport import ActiviteFactory
from evenements.factories.manif import DcnmFactory, AvtmFactory, DocComplementFactory
from instructions.factories import InstructionFactory, AvisInstructionFactory, PreAvisInstructionFactory, DocOfficielFactory
from evaluation_incidence.factories import N2kEvalFactory, RNREvalFactory


class ApiTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        print()
        print('========= Test Api (Clt) ==========')

        # Création des lieux
        dep = DepartementFactory.create(name='42')
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        prefecture = arrondissement.prefecture
        commune1 = CommuneFactory(name='Bard', arrondissement=arrondissement)
        commune2 = CommuneFactory(name='Bully', arrondissement=arrondissement)
        commiss = CommissariatFactory.create(commune=commune1)

        # Création des utilisateurs
        UserFactory.create(username='simple_user', password=hashers.make_password(123),
                           first_name='django', default_instance=dep.instance)
        user = UserFactory.create(username='staff_user', password=hashers.make_password(123),
                                  default_instance=dep.instance)
        cls.instructeur = UserFactory.create(username='instructeur', password=hashers.make_password(123),
                                             default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur, prefecture=prefecture)
        cls.agent = agent = UserFactory.create(username='agent', password=hashers.make_password(123),
                                               default_instance=dep.instance)
        DDSPAgentFactory.create(user=agent, ddsp=dep.ddsp)
        cls.agentlocal = agentlocal = UserFactory.create(username='agentlocal', password=hashers.make_password(123),
                                                         default_instance=dep.instance)
        CommissariatAgentFactory.create(user=agentlocal, commissariat=commiss)
        groupe = Group.objects.create(name='API')
        user.groups.add(groupe)
        cls.instructeur.groups.add(groupe)
        agent.groups.add(groupe)
        agentlocal.groups.add(groupe)

        # Création d'une structure
        organ = OrganisateurFactory.create(user=user)
        struc = StructureFactory.create(name="structure_test", commune=commune1, organisateur=organ)
        # Création d'une activite
        sport = ActiviteFactory.create(name='sport_test')
        # Création des manifestations
        # Manif NM sur commmune 1
        cls.manifestation1 = DcnmFactory.create(ville_depart=commune1, structure=struc, activite=sport,
                                                nom='Manifestation_Test_1')
        N2kEvalFactory.create(manif=cls.manifestation1)
        RNREvalFactory.create(manif=cls.manifestation1)
        DocComplementFactory.create(manif=cls.manifestation1, information_requise="demande",
                                    document_attache="document")
        cls.manifestation1.reglement_manifestation = "url"
        cls.manifestation1.save()
        # Manif NM sur commmune 2
        cls.manifestation2 = DcnmFactory.create(ville_depart=commune2, structure=struc, activite=sport,
                                                nom='Manifestation_Test_2')
        # Manif NM sur 2 commmunes
        cls.manifestation3 = DcnmFactory.create(ville_depart=commune1, structure=struc, activite=sport,
                                                nom='Manifestation_Test_3', villes_traversees=(commune2,))
        # Manif NM sur 2 commmunes
        cls.manifestation4 = DcnmFactory.create(ville_depart=commune2, structure=struc, activite=sport,
                                                nom='Manifestation_Test_4', villes_traversees=(commune1,))
        # Manif VTM sur 1 commmune
        cls.manifestation5 = AvtmFactory.create(ville_depart=commune2, structure=struc, activite=sport,
                                                nom='Manifestation_Test_7')

        # Création des instructions
        cls.instruction1 = InstructionFactory.create(manif=cls.manifestation1)
        cls.instruction2 = InstructionFactory.create(manif=cls.manifestation2)
        cls.instruction3 = InstructionFactory.create(manif=cls.manifestation3)
        cls.instruction4 = InstructionFactory.create(manif=cls.manifestation4)
        cls.instruction5 = InstructionFactory.create(manif=cls.manifestation5)

        # Créations complémentaires sur instruction 1
        cls.avis1 = AvisInstructionFactory.create(instruction=cls.instruction1, service_concerne='ddsp',
                                                  destination_object=dep.ddsp)
        cls.preavis1 = PreAvisInstructionFactory.create(avis=cls.avis1, service_concerne='commissariats',
                                                        destination_object=commiss)
        cls.docOff = DocOfficielFactory.create(instruction=cls.instruction1, nature=2, utilisateur=cls.instructeur,
                                               fichier="arrete_circulation")

    def test1_user(self):
        """
        Tester si l'utilisateur est bien créé
        """
        user = User.objects.get(username='simple_user')
        name = user.get_short_name()
        self.assertEqual(name, 'django')

    def test2_access_anonymous(self):
        """
        Verifie si l'accès est refusé sans login
        """
        print('\t >>> Accès Api')
        reponse = self.client.get('/api/')
        self.assertEqual(reponse.status_code, 401)
        self.assertContains(reponse, "authentification", status_code=401)

    def test3_access_simple_user(self):
        """
        Verifie si l'accès est ok avec login
        """
        self.assertTrue(self.client.login(username='simple_user', password='123'))
        reponse = self.client.get('/api/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("Calendrier", reponse.json())
        self.assertIn("Evenement", reponse.json())
        self.assertIn("Instruction", reponse.json())
        self.assertIn("Avis", reponse.json())
        self.assertIn("Preavis", reponse.json())
        self.assertIn("Organisateur", reponse.json())

    def test4_Route_Calendrier(self):
        """
        Verifie la route calendrier sans login
        """
        print('\t >>> Route Calendrier')
        reponse = self.client.get('/api/Calendrier/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(reponse.json()['count'], 5)
        self.assertIn("Manifestation_Test", reponse.json()['results'][0]['nom'])
        self.assertIn("Sport_test", reponse.json()['results'][0]['activite'])
        self.assertNotIn("nb_participants", reponse.json()['results'][0])

        reponse = self.client.get('/api/Calendrier/' + str(self.manifestation3.pk) + '/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("Manifestation_Test_3", reponse.json()['nom'])
        self.assertIn("Bully", reponse.json()['villes_traversees'][0])
        self.assertIn("structure_test", reponse.json()['structure']['name'])

    def test5_Route_Evenement(self):
        """
        Verifie la route Evenement"
        """
        print('\t >>> Route Evenement')
        self.assertTrue(self.client.login(username='staff_user', password='123'))
        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/')
        self.assertContains(reponse, 'pas concerné par ce dossier', status_code=403)
        self.client.logout()

        self.assertTrue(self.client.login(username='instructeur', password='123'))
        reponse = self.client.get('/api/Evenement/')
        self.assertContains(reponse, 'fournir un numéro de dossier', status_code=403)
        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("Manifestation_Test_1", reponse.json()['nom'])
        self.assertIn("Sport_test", reponse.json()['activite'])
        self.assertIn("structure_test", reponse.json()['structure']['name'])
        self.assertIn("Bard", reponse.json()['ville_depart'])

        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/pieceJointe/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("itineraire_horaire", reponse.json())
        self.assertIn("https://example.com/media/url", reponse.json()['reglement_manifestation'])

        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/evaluationN2k/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("impact_estime", reponse.json())
        self.assertIn("conclusion_natura_2000", reponse.json())

        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/evaluationRnr/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("impact_estime", reponse.json())
        self.assertIn("conclusion_rnr", reponse.json())

        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/docOfficiel/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("Arrêté de circulation", reponse.json()[0]['nature'])
        self.assertEqual(self.instructeur.pk, reponse.json()[0]['utilisateur'])
        self.assertIn(str(timezone.now().year), reponse.json()[0]['date_depot'])
        self.assertIn("/media/arrete_circulation", reponse.json()[0]['fichier'])

        reponse = self.client.get('/api/Evenement/' + str(self.manifestation1.pk) + '/docComp/')
        # print(reponse.json())
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("demande", reponse.json()[0]['information_requise'])
        self.assertIn(str(timezone.now().year), reponse.json()[0]['date_demande'])
        self.assertIn("/media/document", reponse.json()[0]['document_attache'])

    def test6_Route_Instruction(self):
        """
        Verifie la route Instruction
        """
        print('\t >>> Route Instruction')
        self.assertTrue(self.client.login(username='staff_user', password='123'))
        reponse = self.client.get('/api/Instruction/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()

        self.assertTrue(self.client.login(username='instructeur', password='123'))
        reponse = self.client.get('/api/Instruction/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(reponse.json()['count'], 5)

        reponse = self.client.get('/api/Instruction/' + str(self.instruction1.pk) + '/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(self.manifestation1.pk, reponse.json()['manif_id'])
        self.assertIn("demandée", reponse.json()['etat'])
        self.assertEqual(self.avis1.pk, reponse.json()['avis'][0]['id'])
        self.assertIn("demandé", reponse.json()['avis'][0]['etat'])
        self.assertIn("ddsp", reponse.json()['avis'][0]['service_concerne'])

    def test7_Route_Avis(self):
        """
        Verifie la route Avis
        """
        print('\t >>> Route Avis')
        self.assertTrue(self.client.login(username='staff_user', password='123'))
        reponse = self.client.get('/api/Avis/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()

        self.assertTrue(self.client.login(username='agent', password='123'))
        reponse = self.client.get('/api/Avis/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(reponse.json()['count'], 1)

        reponse = self.client.get('/api/Avis/' + str(self.avis1.pk) + '/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(self.avis1.pk, reponse.json()['id'])
        self.assertIn("demandé", reponse.json()['etat'])
        self.assertIn("ddsp", reponse.json()['service_concerne'])
        self.assertIn("DDSP 42", reponse.json()['service'])

        data = {"favorable": True, "prescriptions": "ma prescription"}
        reponse = self.client.post('/api/Avis/' + str(self.avis1.pk) + '/rendre/', data)
        self.assertContains(reponse, 'Avis rendu', status_code=200)

        reponse = self.client.get('/api/Avis/' + str(self.avis1.pk) + '/')
        self.assertEqual(reponse.status_code, 200)
        # print(reponse)
        # print(reponse.json())
        self.assertIn("rendu", reponse.json()['etat'])
        self.assertEqual(self.agent.pk, reponse.json()['agent'])
        self.assertIn(str(timezone.now().year), reponse.json()['date_reponse'])
        self.assertTrue(reponse.json()['favorable'])
        self.assertIn("ma prescription", reponse.json()['prescriptions'])

        data = {"favorable": True, "prescriptions": "ma prescription"}
        reponse = self.client.post('/api/Avis/' + str(self.avis1.pk) + '/rendre/', data)
        self.assertContains(reponse, 'Avis déjà rendu', status_code=406)

    def test8_Route_Preavis(self):
        """
        Verifie la route Preavis
        """
        print('\t >>> Route Preavis')
        self.assertTrue(self.client.login(username='staff_user', password='123'))
        reponse = self.client.get('/api/Preavis/')
        self.assertContains(reponse, 'pas accès à ces informations', status_code=403)
        self.client.logout()

        self.assertTrue(self.client.login(username='agentlocal', password='123'))
        reponse = self.client.get('/api/Preavis/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(reponse.json()['count'], 1)

        reponse = self.client.get('/api/Preavis/' + str(self.preavis1.pk) + '/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(self.preavis1.pk, reponse.json()['id'])
        self.assertIn("demandé", reponse.json()['etat'])
        self.assertIn("commissariats", reponse.json()['service_concerne'])
        self.assertIn("Commissariat Bard", reponse.json()['service'])

        data = {"favorable": True, "prescriptions": "ma prescription"}
        reponse = self.client.post('/api/Preavis/' + str(self.preavis1.pk) + '/rendre/', data)
        self.assertContains(reponse, 'Préavis rendu', status_code=200)

        reponse = self.client.get('/api/Preavis/' + str(self.preavis1.pk) + '/')
        self.assertEqual(reponse.status_code, 200)
        # print(reponse)
        # print(reponse.json())
        self.assertIn("rendu", reponse.json()['etat'])
        self.assertEqual(self.agentlocal.pk, reponse.json()['agentlocal'])
        self.assertIn(str(timezone.now().year), reponse.json()['date_reponse'])
        self.assertTrue(reponse.json()['favorable'])
        self.assertIn("ma prescription", reponse.json()['prescriptions'])

        data = {"favorable": True, "prescriptions": "ma prescription"}
        reponse = self.client.post('/api/Preavis/' + str(self.preavis1.pk) + '/rendre/', data)
        self.assertContains(reponse, 'Préavis déjà rendu', status_code=406)

    def test9_Route_Organisateur(self):
        """
        Verifie la route Organisateur
        """
        print('\t >>> Route Organisateur')
        # L'utilisateur n'a pas accès à cette route
        self.assertTrue(self.client.login(username='simple_user', password='123'))
        reponse = self.client.get('/api/Organisateur/')
        self.assertEqual(reponse.status_code, 403)
        self.assertIn("pas accès à ces informations", reponse.json()['detail'])
        self.client.logout()

        # L'utilisateur a accès à cette route
        self.assertTrue(self.client.login(username='staff_user', password='123'))
        reponse = self.client.get('/api/Organisateur/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(1, reponse.json()['count'])
        self.assertIn("Bard", reponse.json()['results'][0]['commune'])
        self.assertIn("structure_test", reponse.json()['results'][0]['structure'])
