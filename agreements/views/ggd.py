# coding: utf-8
from django.contrib import messages
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView
from django.views.generic.edit import UpdateView

from administration.models.service import CGD, EDSR
from agreements.views.base import BaseAcknowledgeView, BaseResendView, BaseDispatchView, BaseFormatView
from core.util.permissions import require_role
from core.util.user import UserHelper
from ..forms import *
from ..models import *


class GGDAvisDetail(DetailView):
    """ Vue de détail d'un avis GGD """

    # Configuration
    model = GGDAvis

    # Overrides
    @method_decorator(require_role(['edsragent', 'ggdagent', 'brigadeagent']))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue uniquement pour les agents gendarmerie """
        return super(GGDAvisDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.authorization.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class GGDAvisPass(UpdateView):
    """ Vue de passage de l'avis à l'EDSR (juste renseigner l'EDSR en fait) """

    # Configuration
    model = GGDAvis
    form_class = GGDAvisPassForm
    template_name_suffix = '_pass_form'

    # Overrides
    @method_decorator(require_role('ggdagent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour les agents GGD uniquement """
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        """
        Notifier les agents de l'EDSR sélectionné

        Par défaut, aucun signal n'existe dans le cas où un avis GGD existe déjà et
        qu'un champ vient d'être renseigné. Ici, on souhaite que des agents EDSR
        reçoivent une notification quand le champ edsr_concerned d'un avis GGD est
        renseigné. Le plus simple, c'est de la faire dans le formulaire.
        """
        edsr = form.instance.concerned_edsr
        if edsr is not None:
            avis = form.instance
            avis.notify_creation(edsr.get_edsr_agents())
        return super().form_valid(form)


class GGDAvisDispatch(BaseDispatchView):
    """ Vue d'envoi des demandes d'avis """

    # Configuration
    model = GGDAvis
    form_class = GGDAvisDispatchForm

    # Overrides
    @method_decorator(require_role(['edsragent', 'ggdagent']))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour les agents GGD uniquement """
        return super(GGDAvisDispatch, self).dispatch(*args, **kwargs)

    # Overrides
    def form_valid(self, form):
        """ Le formulaire est valide, gérer la réponse HTTP """
        if form.instance.state == 'dispatched':
            messages.error(self.request, "Aucune action effectuée : les demandes de pré-avis ont déjà été envoyées.")
            return HttpResponseRedirect(self.get_success_url())
        else:
            if Instance.objects.get_for_request(self.request).get_workflow_ggd() == Instance.WF_GGD_EDSR:
                # Dans le workflow GGD avec agent EDSR, ne pas considérer comme dispatché tant que l'EDSR n'est pas intervenu
                if UserHelper.get_role_name(self.request.user) == 'edsragent':
                    form.instance.dispatch()
            elif Instance.objects.get_for_request(self.request).get_workflow_ggd() == Instance.WF_GGD_SUBEDSR:
                # Dispatcher dans tous les cas
                form.instance.dispatch()
            return super(BaseDispatchView, self).form_valid(form)

    def get_form(self, form_class=None):
        """ Renvoyer le formulaire """
        form = super().get_form(form_class=form_class)
        # Filtrer les choix à ceux possibles selon la manifestation de l'avis
        form.fields['concerned_cgd'].queryset = CGD.objects.filter(
            commune__arrondissement__departement__in=self.object.get_manifestation().get_crossed_departements())
        form.fields['concerned_edsr'].queryset = EDSR.objects.filter(
            departement__in=self.object.get_manifestation().get_crossed_departements())
        if Instance.objects.get_for_request(self.request).get_workflow_ggd() == Instance.WF_GGD_EDSR:
            # Dans le workflow GGD avec agent EDSR, cacher le choix d'EDSR à l'agent EDSR
            if UserHelper.get_role_name(self.request.user) == 'edsragent':
                form.fields.pop('concerned_edsr')
            # Dans le workflow GGD avec agent EDSR, cacher le choix de CGD à l'agent GGD
            elif UserHelper.get_role_name(self.request.user) == 'ggdagent':
                form.fields.pop('concerned_cgd')
        return form


class GGDAvisFormat(BaseFormatView):
    """ Vue de mise en forme de l'avis GGD """

    # Configuration
    model = GGDAvis

    # Overrides
    @method_decorator(require_role('edsragent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue aux agents EDSR unqiuement """
        return super(GGDAvisFormat, self).dispatch(*args, **kwargs)


class GGDAvisAcknowledge(BaseAcknowledgeView):
    """ Vue de rendu d'avis GGD """

    # Configuration
    model = GGDAvis

    # Overrides
    @method_decorator(require_role('ggdagent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue aux agents GGD uniquement """
        return super(GGDAvisAcknowledge, self).dispatch(*args, **kwargs)


class GGDAvisResend(BaseResendView):
    """ Vue de renvoi des demandes d'avis """

    # Configuration
    model = GGDAvis

    # Overrides
    def get(self, request, *args, **kwargs):
        """ Vue pour la méthode GET """
        instance = self.get_object()
        instance.notify_creation(agents=instance.authorization.manifestation.departure_city.get_departement().ggd.ggdagents.all())
        instance.log_resend(recipient=instance.get_ggd().__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect(reverse('authorizations:authorization_detail', kwargs={'pk': instance.authorization.id}))
