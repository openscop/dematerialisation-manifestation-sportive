# coding: utf-8
import csv

from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand

from administration.models import CGD, Brigade, CIS, Commissariat, MairieAgent
from administrative_division.models import Commune, Arrondissement, Departement
from agreements.models import MairieAvis
from authorizations.models import ManifestationAutorisation
from contacts.models import Adresse
from evaluation_incidence.models import N2kOperateurSite, RnrAdministrateur
from evenements.models import Manif
from events.models import Manifestation
from instructions.models import Instruction, Avis
from organisateurs.models import Structure


class Command(BaseCommand):
    """ Mise à jour des commune dans la BD """
    args = ''
    help = 'Mise à jour des communes. Tous les problème seront répertorié dans les fichiers erreur_arron.csv et erreur_commune.csv'

    def handle(self, *args, **options):
        dep_sans_instance = Departement.objects.filter(instance__isnull=True)
        name = ['14', '16', '17', '48', '56', '70', '80', '88']
        dep_instance = Departement.objects.filter(name__in=name)
        deps = dep_sans_instance | dep_instance
        deps_liste = [departement.name for departement in deps]
        liste_modeles = ((MairieAgent, 'Agent mairie', 'commune'),
                         (CGD, 'Service CGD', 'commune'),
                         (Brigade, 'Service Brigade', 'commune'),
                         (CIS, 'Service CIS', 'commune'),
                         (Commissariat, 'Service Commissariat', 'commune'),
                         (Structure, 'Structure', 'commune'),
                         (Adresse, 'Adresse', 'commune'),
                         (N2kOperateurSite, 'Opérateur N2k', 'commune'),
                         (RnrAdministrateur, 'Administrateur Rnr', 'commune'),
                         (Manif, 'Ville de départ de manifestation', 'ville_depart'),
                         (Manif, 'Ville traversée de manifestation', 'villes_traversees'),
                         (Instruction, 'Ville concernée d\'instruction', 'villes_concernees'),
                         (Avis, 'Champ générique d\'avis', 'filtre_avis'))
        liste_modeles_old = ((MairieAvis, 'Avis de mairie (old)', 'commune'),
                             (Manifestation, 'Ville de départ de manifestation (old)', 'departure_city'),
                             (Manifestation, 'Ville traversée de manifestation (old)', 'crossed_cities'),
                             (ManifestationAutorisation, 'Ville concernée d\'instruction (old)', 'concerned_cities'))

        liste_travail = liste_modeles + liste_modeles_old
        print('suppression')
        count = 0
        total_dep = len(deps)
        for dep in deps:
            count +=1
            print(f"{count}/{total_dep}")
            for arron in dep.arrondissements.all():
                for commune in arron.communes.all():
                    lien = False
                    for travail in liste_travail:
                        liste = None
                        if travail[2] == 'filtre_avis':
                            liste = travail[0].objects.filter(
                                content_type=ContentType.objects.get(model='commune').id,
                                object_id=commune.pk)
                        else:
                            liste = travail[0].objects.filter(**{travail[2]: commune})
                        if liste:
                            lien = True
                    if not lien:
                        commune.delete()

                arrondissement = Arrondissement.objects.get(pk=arron.pk)
                if not arrondissement.communes.all():
                    arrondissement.delete()

        arron_csv = open("arrondissement2021.csv", newline='')
        commune_csv = open("commune2021.csv", newline='')
        arron_reader = csv.DictReader(arron_csv, delimiter=',')
        arron_tab = [row for row in arron_reader if row['DEP'] in deps_liste]
        commune_reader = csv.DictReader(commune_csv, delimiter=',')
        commune_tab = [row for row in commune_reader if row['DEP'] in deps_liste and row['TYPECOM']=="COM"]
        postal_csv = open("laposte_hexasmal.csv", newline='')
        postal_reader = csv.DictReader(postal_csv, delimiter=';')
        postal_tab = [row for row in postal_reader]
        erreur_arron = []
        erreur_commune = []
        count = 0
        total_arron = len(arron_tab)
        for arron in arron_tab:
            count += 1
            print(f"{count}/{total_arron}")
            if not Arrondissement.objects.filter(code=arron['ARR']):
                dep = Departement.objects.get(name=arron['DEP'])
                new_arron = Arrondissement(name=arron['LIBELLE'], code=arron['ARR'], departement=dep)
                new_arron.save()
            else:
                old_arron = Arrondissement.objects.get(code=arron['ARR'])
                if not old_arron.name == arron['LIBELLE']:
                    erreur_arron.append([arron['ARR'], 'arrondissement existant'])

        count = 0
        total_comm = len(commune_tab)
        for commune in commune_tab:
            count += 1
            print(f"{count}/{total_comm}")
            if not Commune.objects.filter(code=commune['COM']):
                recherche = list(filter(lambda sch: sch['Code_commune_INSEE'] == commune['COM'], postal_tab))
                if len(recherche) == 0:
                    erreur_commune.append([commune['COM'], 'erreur insee'])
                else:
                    arron = Arrondissement.objects.filter(code=commune['ARR'])
                    if not arron:
                        erreur_commune.append([commune['COM'], 'erreur arron'])
                    else:
                        arron = arron.first()
                        comm = Commune(arrondissement=arron, code=commune['COM'], name=commune['LIBELLE'],
                                       zip_code=recherche[0]['Code_postal'])
                        comm.save()
            else:
                old_comm = Commune.objects.get(code=commune['COM'])
                if not old_comm.name == commune['LIBELLE']:
                    erreur_commune.append([commune['COM'], 'commune existante'])


        if erreur_arron:
            nom = "erreur_arron.csv"
            with open(nom, 'w') as csvfile:
                writer = csv.writer(csvfile, delimiter=";", quotechar='"')
                row = ["code", "raison"]
                writer.writerow(row)
                for erreur in erreur_arron:
                    writer.writerow(erreur)

        if erreur_commune:
            nom = "erreur_commune.csv"
            with open(nom, 'w') as csvfile:
                writer = csv.writer(csvfile, delimiter=";", quotechar='"')
                row = ["code", "raison"]
                writer.writerow(row)
                for erreur in erreur_commune:
                    writer.writerow(erreur)


        print("finit,")

