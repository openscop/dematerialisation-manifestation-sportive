# coding: utf-8
from django.urls import path, re_path

from .views.calendar import Calendar, CalendarAjaxView
from .views.index import HomePage
from .views.stat import Stat
from .views.stat_instructeur import StatInstructeur


app_name = 'portail'
urlpatterns = [

    # Accueil
    path('', HomePage.as_view(), name='home_page'),

    # Calendrier
    re_path('calendar/(?P<activite>\d+)/', Calendar.as_view(), name="filtered_calendar"),
    path('calendar/', Calendar.as_view(), name="calendar"),
    path('calendar/ajax/', CalendarAjaxView.as_view(), name="calendar_ajax"),
    path('stat/', Stat.as_view(), name="stat"),
    path('stat_instructeur/', StatInstructeur.as_view(), name="stat_instructeur"),
]
