import re, os

from django.test import TestCase, override_settings
from django.contrib.auth.hashers import make_password as make

from post_office.models import EmailTemplate, Email

from core.models import Instance, User
from core.factories import UserFactory
from evenements.factories import DcnmFactory
from instructions.models import Instruction, PreAvis, Avis
from organisateurs.factories import OrganisateurFactory, StructureFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import (InstructeurFactory, FederationAgentFactory, GGDAgentFactory, EDSRAgentFactory,
                                      CGDAgentFactory, DDSPAgentFactory, CommissariatAgentFactory, MairieAgentFactory,
                                      CGAgentFactory, CGServiceAgentFactory, CGSuperieurFactory, SDISAgentFactory,
                                      GroupementAgentFactory, CODISAgentFactory, CISAgentFactory, CGDFactory, BrigadeAgentFactory)
from administration.factories import BrigadeFactory, CommissariatFactory, CGServiceFactory, CompagnieFactory, CISFactory
from sports.factories import ActiviteFactory, FederationFactory
from messagerie.models import Message


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class Circuit_EDSRTests(TestCase):
    """
    Test du circuit d'instance EDSR pour une Dnm
        workflow_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('============ WF_EDSR (Clt) =============')
        # Création des objets sur le 42
        cls.dep = dep = DepartementFactory.create(name='42',
                                                  instance__name="instance de test",
                                                  instance__workflow_ggd=Instance.WF_EDSR,
                                                  instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        cls.prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        # Création des utilisateurs
        cls.organisateur = UserFactory.create(username='organisateur', password=make(123), default_instance=dep.instance, email='orga@test.fr')
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        cls.structure = StructureFactory(commune=cls.commune, organisateur=organisateur)

        cls.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance, email='inst@test.fr')
        InstructeurFactory.create(user=cls.instructeur, prefecture=cls.prefecture)
        cls.agent_fede = UserFactory.create(username='agent_fede', password=make(123), default_instance=dep.instance, email='fede@test.fr')
        activ = ActiviteFactory.create()
        cls.fede = FederationFactory.create()
        FederationAgentFactory.create(user=cls.agent_fede, federation=cls.fede)
        cls.agent_ggd = UserFactory.create(username='agent_ggd', password=make(123), default_instance=dep.instance, email='ggd@test.fr')
        GGDAgentFactory.create(user=cls.agent_ggd, ggd=dep.ggd)
        cls.edsr = dep.edsr
        cls.agent_edsr = UserFactory.create(username='agent_edsr', password=make(123), default_instance=dep.instance, email='edsr@test.fr')
        EDSRAgentFactory.create(user=cls.agent_edsr, edsr=dep.edsr)
        cls.cgd1 = CGDFactory.create(commune=cls.commune)
        cls.agent_cgd1 = UserFactory.create(username='agent_cgd1', password=make(123), default_instance=dep.instance, email='cgd1@test.fr')
        CGDAgentFactory.create(user=cls.agent_cgd1, cgd=cls.cgd1)
        cls.cgd2 = CGDFactory.create(commune=cls.autrecommune)
        cls.agent_cgd2 = UserFactory.create(username='agent_cgd2', password=make(123), default_instance=dep.instance, email='cgd2@test.fr')
        CGDAgentFactory.create(user=cls.agent_cgd2, cgd=cls.cgd2)
        cls.brigade1 = BrigadeFactory.create(kind='bta', cgd=cls.cgd1, commune=cls.commune)
        cls.agent_brg1 = UserFactory.create(username='agent_brg1', password=make(123), default_instance=dep.instance, email='brg1@test.fr')
        BrigadeAgentFactory.create(user=cls.agent_brg1, brigade=cls.brigade1)
        cls.brigade2 = BrigadeFactory.create(kind='bta', cgd=cls.cgd2, commune=cls.commune)
        cls.agent_brg2 = UserFactory.create(username='agent_brg2', password=make(123), default_instance=dep.instance, email='brg2@test.fr')
        BrigadeAgentFactory.create(user=cls.agent_brg2, brigade=cls.brigade2)
        cls.agent_ddsp = UserFactory.create(username='agent_ddsp', password=make(123), default_instance=dep.instance, email='ddsp@test.fr')
        DDSPAgentFactory.create(user=cls.agent_ddsp, ddsp=dep.ddsp)
        cls.commiss = CommissariatFactory.create(commune=cls.commune)
        cls.agent_commiss = UserFactory.create(username='agent_commiss', password=make(123), default_instance=dep.instance, email='comm@test.fr')
        CommissariatAgentFactory.create(user=cls.agent_commiss, commissariat=cls.commiss)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance, email='mair@test.fr')
        MairieAgentFactory.create(user=cls.agent_mairie, commune=cls.commune)
        cls.agent_cg = UserFactory.create(username='agent_cg', password=make(123), default_instance=dep.instance, email='cg@test.fr')
        CGAgentFactory.create(user=cls.agent_cg, cg=dep.cg)
        cls.cgserv = CGServiceFactory.create(name='STD_test', cg=dep.cg, service_type='STD')
        cls.agent_cgserv = UserFactory.create(username='agent_cgserv', password=make(123), default_instance=dep.instance, email='cgserv@test.fr')
        CGServiceAgentFactory.create(user=cls.agent_cgserv, cg_service=cls.cgserv)
        cls.agent_cgsup = UserFactory.create(username='agent_cgsup', password=make(123), default_instance=dep.instance, email='cgsup@test.fr')
        CGSuperieurFactory.create(user=cls.agent_cgsup, cg=dep.cg)
        cls.agent_sdis = UserFactory.create(username='agent_sdis', password=make(123), default_instance=dep.instance, email='sdis@test.fr')
        SDISAgentFactory.create(user=cls.agent_sdis, sdis=dep.sdis)
        cls.group = CompagnieFactory.create(sdis=dep.sdis, number=98)
        cls.agent_group = UserFactory.create(username='agent_group', password=make(123), default_instance=dep.instance, email='group@test.fr')
        GroupementAgentFactory.create(user=cls.agent_group, compagnie=cls.group)
        cls.agent_codis = UserFactory.create(username='agent_codis', password=make(123), default_instance=dep.instance, email='codis@test.fr')
        CODISAgentFactory.create(user=cls.agent_codis, codis=dep.codis)
        cls.cis = CISFactory.create(name='CIS_test', compagnie=cls.group, commune=cls.commune)
        cls.agent_cis = UserFactory.create(username='agent_cis', password=make(123), default_instance=dep.instance, email='cis@test.fr')
        CISAgentFactory.create(user=cls.agent_cis, cis=cls.cis)
        for user in User.objects.all():
            user.optionuser.notification_mail = True
            user.optionuser.action_mail = True
            user.optionuser.save()

        EmailTemplate.objects.create(name='new_msg')

        # Création de l'événement
        cls.manifestation = DcnmFactory.create(ville_depart=cls.commune, structure=cls.structure, activite=activ,
                                               nom='Manifestation_Test', villes_traversees=(cls.autrecommune,))
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.descriptions_parcours = 'parcours de 10Km'
        cls.manifestation.nb_participants = 1
        cls.manifestation.nb_organisateurs = 10
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.save()
        cls.manifestation.notifier_creation()

        cls.avis_nb = 6
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'itineraire_horaire', 'recepisse_declaration'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Suppression des fichiers créés dans /tmp
        """
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'itineraire_horaire', 'recepisse_declaration'):
            os.remove(file+".txt")

        os.system('rm -R /tmp/maniftest/media/42/')

        super(Circuit_EDSRTests, cls).tearDownClass()

    def test_Circuit_EDSR(self):
        """
        Test des différentes étapes du circuit EDSR pour une autorisationNM
        """
        """
        def print(string="", end=None):
            # Supprimer les prints hors debug
            pass
        """
        def presence_avis(username, state, log=True):
            """
            Appel de la dashboard de l'utilisateur pour tester la présence et l'état de l'événement
            :param username: agent considéré
            :param state: couleur de l'événement
            :param log: booléen pour connecter l'agent
            :return: retour: la réponse http
            """
            # Connexion avec l'utilisateur
            if log:
                self.assertTrue(self.client.login(username=username, password='123'))
            # Appel de la page
            retour = self.client.get('/instructions/tableaudebord/', HTTP_HOST='127.0.0.1:8000')
            # print(retour.content.decode('utf-8'))
            # f = open('/var/log/manifsport/test_output.html', 'w', encoding='utf-8')
            # f.write(str(retour.content.decode('utf-8')).replace('\\n',""))
            # f.close()
            # Test du contenu
            if state == 'none':
                if username in ["agent_cis", "agent_codis", "agent_brg1", "agent_brg2"]:
                    nb_bloc = re.search('test_nb_rendu">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                else:
                    # Recherche sur "avis demandés", on sous-entend que c'est un TdB agent
                    nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('0', nb_bloc.group('nb'))
            # Test du chiffre dans la bonne case
            elif state == 'nouveau':
                nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            elif state == 'encours':
                nb_bloc = re.search('test_nb_encours">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            elif state == 'rendu':
                nb_bloc = re.search('test_nb_rendu">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            else:
                self.assertEqual('0', '1', 'paramètre inconnu')
            return retour

        def vue_detail(page):
            """
            Appel de la vue de détail de la manifestation
            :param page: réponse précédente
            :return: reponse suivante
            """
            detail = re.search('data-href=\'(?P<url>(/[^"]+))', page.content.decode('utf-8'))
            if hasattr(detail, 'group'):
                page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
            self.assertContains(page, 'Manifestation_Test')
            return page

        def aucune_action(page):
            """
            Test auncune action affichée dans la zone action de la dashboard
            :param page: réponse précédente
            """
            action = re.search(', aucune action nécessaire', page.content.decode('utf-8'))
            attente = re.search(' devez attendre ', page.content.decode('utf-8'))
            if not action and not attente:
                self.assertIsNotNone(action, msg="message non trouvé")

        def affichage_avis():
            """
            Affichage des avis émis pour l'événement avec leur status
            """
            for avis in self.instruction.get_tous_avis():
                if avis.etat != 'rendu':
                    print(avis, end=" ; ")
                    print(avis.etat)

        print('**** test 1 creation manif ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('1', nb_bloc.group('nb'))
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get('/tableau-de-bord-organisateur/liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file4:
            self.client.post(url_script.group('url'), {'cartographie': file4}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/itineraire_horaire.txt') as file5:
            reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file5}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(declar, 'group'):
            self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        self.instruction = Instruction.objects.get(manif=self.manifestation)
        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().get_instance())
        print(self.manifestation.ville_depart.get_departement().ggd, end=" ; ")
        print(self.manifestation.ville_depart.get_departement().edsr, end=" ; ")
        print(self.manifestation.ville_depart.get_departement().ddsp)
        print(self.manifestation.structure, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline, end=" ; ")
        print(self.manifestation.activite.discipline.get_federations().first().name)
        affichage_avis()
        self.assertEqual(str(self.instruction), str(self.manifestation))
        self.id_avis_fede = Avis.objects.last().id
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        index = len(outbox)
        index_mess = len(messages)
        self.assertEqual(len(outbox), 6)
        self.assertEqual(outbox[0].to, [self.agent_fede.email])
        self.assertEqual(outbox[1].to, [self.fede.email])
        self.assertEqual(outbox[1].subject, 'Demande d\'avis à traiter')
        # création + connexion avant notification => message #3
        self.assertEqual(messages[2].object_id, self.id_avis_fede)          # Avis fédération
        self.assertEqual(messages[2].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(outbox[2].to, [self.instructeur.email])
        self.assertEqual(outbox[3].to, [self.prefecture.email])
        self.assertEqual(outbox[3].subject, 'Nouveau dossier à instruire')
        self.assertEqual(outbox[4].to, [self.organisateur.email])
        self.assertEqual(outbox[4].subject, 'Accusé de réception : Manifestation_Test')
        self.client.logout()

        print('**** test 2 vérification avis; 0 pour tous sauf la fédération ****')
        # Vérification des avis des divers agents
        # GGD
        presence_avis('agent_ggd', 'none')
        self.client.logout()
        # EDSR
        presence_avis('agent_edsr', 'none')
        self.client.logout()
        # Mairie
        presence_avis('agent_mairie', 'none')
        self.client.logout()
        # CG
        presence_avis('agent_cg', 'none')
        self.client.logout()
        # CGSuperieur
        presence_avis('agent_cgsup', 'none')
        self.client.logout()
        # SDIS
        presence_avis('agent_sdis', 'none')
        self.client.logout()

        print('**** test 3 instructeur - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        presence_avis('instructeur', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes d\'avis', count=2)
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        edit_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(edit_form, 'group'):
            reponse = self.client.post(edit_form.group('url'),
                                       {'edsr_concerne': True,
                                        'ddsp_concerne': True,
                                        'sdis_concerne': True,
                                        'cg_concerne': True,
                                        'villes_concernees': [self.commune.pk]}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction)
        affichage_avis()
        # Vérifier le passage en encours et le nombre d'avis manquants
        presence_avis('instructeur', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        self.id_avis_edsr = Avis.objects.filter(service_concerne='edsr').last().id
        self.id_avis_ddsp = Avis.objects.filter(service_concerne='ddsp').last().id
        self.id_avis_cg = Avis.objects.filter(service_concerne='cg').last().id
        self.id_avis_sdis = Avis.objects.filter(service_concerne='sdis').last().id
        self.id_avis_mairie = Avis.objects.filter(service_concerne='mairie').last().id

        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 11)
        mailist = [self.agent_edsr.email, self.edsr.email, self.agent_sdis.email, self.dep.sdis.email,
                   self.agent_ddsp.email, self.dep.ddsp.email, self.agent_cg.email, self.dep.cg.email,
                   self.agent_mairie.email, self.commune.email, self.structure.organisateur.user.email]
        for email in outbox:
            self.assertIn(email.to[0], mailist)
        self.assertEqual(outbox[0].subject, 'Démarrage de l\'instruction')
        self.assertEqual(outbox[1].subject, 'Demande d\'avis à traiter')
        self.assertEqual(messages[1].object_id, self.id_avis_mairie)          # Avis Mairie
        self.assertEqual(messages[1].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(messages[2].object_id, self.id_avis_cg)          # Avis CG
        self.assertEqual(messages[2].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(messages[3].object_id, self.id_avis_ddsp)          # Avis DDSP
        self.assertEqual(messages[3].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(messages[4].object_id, self.id_avis_edsr)          # Avis EDSR
        self.assertEqual(messages[4].message_enveloppe.first().doc_objet, "avis")
        self.assertEqual(messages[5].object_id, self.id_avis_sdis)          # Avis SDIS
        self.assertEqual(messages[5].message_enveloppe.first().doc_objet, "avis")

        print('**** test 4 avis fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        presence_avis('agent_fede', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 28 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Valider l'événement avec l'url fournie et tester la redirection
        ackno = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ackno, 'group'):
            reponse = self.client.post(ackno.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # Vérifier le passage en rendu
        presence_avis('agent_fede', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[1].to, [self.prefecture.email])
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture')
        self.assertEqual(messages[0].object_id, self.id_avis_fede)          # Avis fédération
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")

        print('**** test 5 vérification avis; 0 pour GGD, CGD et Brigade ****')
        # Vérification des avis des divers agents
        # EDSR
        presence_avis('agent_ggd', 'none')
        self.client.logout()
        # CGD
        presence_avis('agent_cgd1', 'none')
        self.client.logout()
        # BRG
        presence_avis('agent_brg1', 'none')
        self.client.logout()

        print('**** test 6 avis edsr - distribution ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en nouveau
        presence_avis('agent_edsr', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        self.assertNotContains(reponse, 'Mettre l\'avis directement en forme')
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'cgd_concerne': [self.cgd1.pk, self.cgd2.pk]}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.filter(service_concerne='cgd')
        for pr in preavis:
            print(pr, end=" ; ")
            print(pr.etat)
        affichage_avis()
        # Vérification du passage en encours
        presence_avis('agent_edsr', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '2 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 4)
        self.assertIn(outbox[0].to[0], [self.agent_cgd2.email, self.agent_cgd1.email])
        self.assertIn(outbox[1].to[0], [self.cgd2.email, self.cgd1.email])
        self.assertIn(outbox[2].to[0], [self.agent_cgd2.email, self.agent_cgd1.email])
        self.assertIn(outbox[3].to[0], [self.cgd2.email, self.cgd1.email])
        self.assertEqual(outbox[0].subject, 'Demande de préavis à traiter')
        self.assertIn(messages[0].object_id, preavis.values_list('id', flat=True))          # Préavis Cgd
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "preavis")

        print('**** test 7 preavis cgd1 ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en nouveau
        presence_avis('agent_cgd1', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        print('\t >>> Informer brigade')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Informer les brigades', count=1)
        # informer les brigades de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Informer les brigades', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'brigades_concernees': [self.brigade1.pk]}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation', count=1)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_brg1.email])
        self.assertEqual(outbox[1].to, [self.brigade1.email])
        self.assertEqual(outbox[0].subject, 'Nouveau dossier')
        self.assertIn(messages[0].object_id, preavis.values_list('id', flat=True))          # Préavis Cgd
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "preavis")
        presence_avis('agent_brg1', 'encours')
        self.client.logout()
        presence_avis('agent_brg2', 'none')
        self.client.logout()
        print('\t >>> Rendre préavis')
        # Vérification du passage en encours
        presence_avis('agent_cgd1', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.filter(service_concerne='cgd')
        for pr in preavis:
            print(pr, end=" ; ")
            print(pr.etat)
        affichage_avis()
        # Vérification du passage en rendu
        presence_avis('agent_cgd1', 'rendu', log=False)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_edsr.email])
        self.assertEqual(outbox[1].to, [self.edsr.email])
        self.assertEqual(outbox[0].subject, 'Préavis rendu (1 en attente)')
        # trois connexions avant notification => message #5
        self.assertIn(messages[5].object_id, preavis.values_list('id', flat=True))          # Préavis Cgd
        self.assertEqual(messages[5].message_enveloppe.first().doc_objet, "preavis")

        print('**** test 7 preavis cgd2 ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en nouveau
        presence_avis('agent_cgd2', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        print('\t >>> Informer brigade')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Informer les brigades', count=1)
        # informer les brigades de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Informer les brigades', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'brigades_concernees': [self.brigade2.pk]}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation', count=1)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_brg2.email])
        self.assertEqual(outbox[1].to, [self.brigade2.email])
        self.assertEqual(outbox[0].subject, 'Nouveau dossier')
        self.assertIn(messages[0].object_id, preavis.values_list('id', flat=True))          # Préavis Cgd
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "preavis")
        # Vérification de la présence de la manif chez la brigade2
        presence_avis('agent_brg2', 'encours')
        self.client.logout()
        print('\t >>> Rendre préavis')
        # Vérification du passage en encours
        presence_avis('agent_cgd2', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.filter(service_concerne='cgd')
        for pr in preavis:
            print(pr, end=" ; ")
            print(pr.etat)
        affichage_avis()
        # Vérification du passage en rendu
        presence_avis('agent_cgd2', 'rendu', log=False)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_edsr.email])
        self.assertEqual(outbox[1].to, [self.edsr.email])
        self.assertEqual(outbox[0].subject, 'Tous les préavis sont rendus')
        # deux connexions avant notification => message #4
        self.assertIn(messages[3].object_id, preavis.values_list('id', flat=True))          # Préavis Cgd
        self.assertEqual(messages[3].message_enveloppe.first().doc_objet, "preavis")

        print('**** test 8 avis edsr - formattage ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en encours
        presence_avis('agent_edsr', 'encours')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Mettre en forme l\'avis', count=1)
        # Formater l'avis de l'événement avec l'url fournie et tester la redirection
        form_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Mettre en forme l\'avis', reponse.content.decode('utf-8'))
        if hasattr(form_form, 'group'):
            reponse = self.client.post(form_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        affichage_avis()
        # vérification de la présence de l'événement en rendu
        presence_avis('agent_edsr', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_ggd.email])
        self.assertEqual(outbox[1].to, [self.dep.ggd.email])
        self.assertEqual(outbox[0].subject, 'Avis à valider et à envoyer')
        self.assertEqual(messages[0].object_id, self.instruction.avis.get(service_concerne='edsr').id)          # Avis Edsr
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")

        print('**** test 9 avis ggd ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en nouveau
        presence_avis('agent_ggd', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # vérification de la présence de l'événement en rendu
        presence_avis('agent_ggd', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 8)
        mailist = [self.instructeur.email, self.prefecture.email, self.agent_edsr.email, self.edsr.email,
                   self.agent_cgd1.email, self.cgd1.email, self.agent_cgd2.email, self.cgd2.email]
        for email in outbox:
            self.assertIn(email.to[0], mailist)
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture')
        self.assertEqual(messages[0].object_id, self.instruction.avis.get(service_concerne='edsr').id)          # Avis Edsr
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")

        print('**** test 10 vérification avis; 0 pour Commissariat ****')
        # Vérification des avis des divers agents
        # CGD
        presence_avis('agent_commiss', 'none')
        self.client.logout()

        print('**** test 11 avis ddsp - distribution ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        presence_avis('agent_ddsp', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        self.assertNotContains(reponse, 'Rendre l\'avis directement')
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'commissariats_concernes': self.commiss.pk}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='commissariats')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérifier le passage en encours
        presence_avis('agent_ddsp', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_commiss.email])
        self.assertEqual(outbox[1].to, [self.commiss.email])
        self.assertEqual(outbox[0].subject, 'Demande de préavis à traiter')
        self.assertEqual(messages[0].object_id, preavis.id)          # Pré-avis Commissariat
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "preavis")

        print('**** test 12 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        presence_avis('agent_commiss', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='commissariats')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérification du passage en rendu
        presence_avis('agent_commiss', 'rendu',log=False)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_ddsp.email])
        self.assertEqual(outbox[1].to, [self.dep.ddsp.email])
        self.assertEqual(outbox[0].subject, 'Tous les préavis sont rendus')
        self.assertEqual(messages[0].object_id, preavis.id)          # Pré-avis Commissariat
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "preavis")

        print('**** test 13 avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en encours
        presence_avis('agent_ddsp', 'encours')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # Vérifier le passage en rendu
        presence_avis('agent_ddsp', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 4)
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[1].to, [self.prefecture.email])
        self.assertEqual(outbox[2].to, [self.agent_commiss.email])
        self.assertEqual(outbox[3].to, [self.commiss.email])
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture')
        self.assertEqual(messages[0].object_id, self.instruction.avis.get(service_concerne='ddsp').id)          # Avis Edsr
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")

        print('**** test 14 avis mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en nouveau
        presence_avis('agent_mairie', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Valider l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # Vérifier le passage en rendu
        presence_avis('agent_mairie', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[1].to, [self.prefecture.email])
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture')
        self.assertEqual(messages[0].object_id, self.id_avis_mairie)          # Avis Mairie
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")

        print('**** test 15 vérification avis; 0 pour CGService ****')
        # Vérification des avis des divers agents
        # CGService
        presence_avis('agent_cgserv', 'none')
        self.client.logout()

        print('**** test 16 avis cg - distribution ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en nouveau
        presence_avis('agent_cg', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        self.assertNotContains(reponse, 'Mettre l\'avis directement en forme')
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'services_concernes': self.cgserv.pk}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='services')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérifier le passage en encours
        presence_avis('agent_cg', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_cgserv.email])
        self.assertEqual(outbox[1].to, [self.cgserv.email])
        self.assertEqual(outbox[0].subject, 'Demande de préavis à traiter')
        self.assertEqual(messages[0].object_id, preavis.id)          # Pré-avis CG
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "preavis")

        print('**** test 17 preavis cgservice ****')
        # Instruction du préavis par le cgservice, vérification de la présence de l'événement en nouveau
        presence_avis('agent_cgserv', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='services')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérification du passage en rendu
        presence_avis('agent_cgserv', 'rendu', log=False)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_cg.email])
        self.assertEqual(outbox[1].to, [self.dep.cg.email])
        self.assertEqual(outbox[0].subject, 'Tous les préavis sont rendus')
        self.assertEqual(messages[0].object_id, preavis.id)          # Pré-avis CG
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "preavis")

        print('**** test 18 vérification avis; 0 pour CGSupérieur ****')
        # Vérification des avis des divers agents
        # CGSuperieur
        presence_avis('agent_cgsup', 'none')
        self.client.logout()

        print('**** test 19 avis cg - formattage ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en encours
        presence_avis('agent_cg', 'encours')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Mettre en forme l\'avis', count=1)
        # Mettre en forme l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Mettre en forme l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        affichage_avis()
        # vérification de la présence de l'événement en rendu
        presence_avis('agent_cg', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].to, [self.agent_cgsup.email])
        self.assertEqual(outbox[0].subject, 'Avis à valider et à envoyer')
        self.assertEqual(messages[0].object_id, self.instruction.avis.get(service_concerne='cg').id)          # Avis CG
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")

        print('**** test 20 agent cgsup ****')
        # Instruction de l'avis par le cgsup, vérification de la présence de l'événement en nouveau
        presence_avis('agent_cgsup', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # Vérifier le passage en rendu
        presence_avis('agent_cgsup', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 6)
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[1].to, [self.prefecture.email])
        self.assertEqual(outbox[2].to, [self.agent_cg.email])
        self.assertEqual(outbox[3].to, [self.dep.cg.email])
        self.assertEqual(outbox[4].to, [self.agent_cgserv.email])
        self.assertEqual(outbox[5].to, [self.cgserv.email])
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture')
        self.assertEqual(messages[0].object_id, self.instruction.avis.get(service_concerne='cg').id)          # Avis CG
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")

        print('**** test 21 vérification avis; 0 pour Groupement, codis et cis ****')
        # Vérification des avis des divers agents
        # CIS
        presence_avis('agent_cis', 'none')
        self.client.logout()
        # CODIS
        presence_avis('agent_codis', 'none')
        self.client.logout()
        # SDIS groupement
        presence_avis('agent_group', 'none')
        self.client.logout()

        print('**** test 22 avis sdis - distribution ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en nouveau
        presence_avis('agent_sdis', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        self.assertNotContains(reponse, 'Rendre l\'avis directement')
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'compagnies_concernees': self.group.pk}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='compagnies')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérification du passage en encours
        presence_avis('agent_sdis', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_group.email])
        self.assertEqual(outbox[1].to, [self.group.email])
        self.assertEqual(outbox[0].subject, 'Demande de préavis à traiter')
        self.assertEqual(messages[0].object_id, preavis.id)          # Pré-avis Groupement
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "preavis")

        print('**** test 23 preavis groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en nouveau
        presence_avis('agent_group', 'nouveau')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='compagnies')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérification du passage en rendu
        presence_avis('agent_group', 'rendu', log=False)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_sdis.email])
        self.assertEqual(outbox[1].to, [self.dep.sdis.email])
        self.assertEqual(outbox[0].subject, 'Tous les préavis sont rendus')
        self.assertEqual(messages[0].object_id, preavis.id)          # Pré-avis Groupement
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "preavis")

        print('**** test 24 avis sdis ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en encours
        presence_avis('agent_sdis', 'encours')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # Vérifier le passage en rendu
        presence_avis('agent_sdis', 'rendu', log=False)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'avis manquants')
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 6)
        self.assertEqual(outbox[0].to, [self.agent_group.email])
        self.assertEqual(outbox[1].to, [self.group.email])
        self.assertEqual(outbox[0].subject, 'Avis rendu à la préfecture - CIS à notifier')
        self.assertEqual(outbox[2].to, [self.instructeur.email])
        self.assertEqual(outbox[3].to, [self.prefecture.email])
        self.assertEqual(outbox[4].to, [self.agent_codis.email])
        self.assertEqual(outbox[5].to, [self.dep.codis.email])
        self.assertEqual(outbox[2].subject, 'Avis rendu à la préfecture')
        self.assertEqual(messages[0].object_id, self.instruction.avis.get(service_concerne='sdis').id)          # Avis Sdis
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")

        print('**** test 25 avis codis ****')
        # Vérification de la présence de l'événement
        presence_avis('agent_codis', 'rendu')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.client.logout()

        print('**** test 26 notification cis du groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en encours
        presence_avis('agent_group', 'encours')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Informer les CIS', count=1)
        # Informer les CIS de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Informer les CIS', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'cis_concernes': self.cis.pk}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        # Vérification du passage en rendu
        presence_avis('agent_group', 'rendu', log=False)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_cis.email])
        self.assertEqual(outbox[1].to, [self.cis.email])
        self.assertEqual(outbox[0].subject, 'Nouveau dossier')
        self.assertEqual(messages[0].object_id, self.instruction.avis.get(service_concerne='sdis').id)          # Avis Sdis
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")

        print('**** test 27 avis cis ****')
        # Vérification de la présence de l'événement
        presence_avis('agent_cis', 'rendu')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.client.logout()

        print('**** test 28 instructeur - autorisation ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en warning
        presence_avis('instructeur', 'encours')
        messages = Message.objects.all()[index_mess:]
        index_mess += len(messages)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'avis manquants')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Ajouter un document officiel', count=1)
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        publish_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Ajouter un document officiel', reponse.content.decode('utf-8'))
        if hasattr(publish_form, 'group'):
            with open('/tmp/recepisse_declaration.txt') as file1:
                self.client.post(publish_form.group('url'), {'nature': '3', 'fichier': file1}, follow=True, HTTP_HOST='127.0.0.1:8000')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=autorise', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction)
        # Vérifier le passage en info et le nombre d'avis manquants
        self.assertContains(reponse, 'table_termine', count=1)
        self.client.logout()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')[index:]
        messages = Message.objects.all()[index_mess:]
        index += len(outbox)
        index_mess += len(messages)
        self.assertEqual(len(outbox), 35)
        # for out in outbox:
        #     print(out.to)
        # for mess in messages:
        #     print(mess.corps)
        mailist = [self.agent_edsr.email, self.edsr.email, self.agent_ggd.email, self.dep.ggd.email, self.agent_cgd1.email,
                   self.cgd1.email, self.agent_cgd2.email, self.cgd2.email, self.agent_brg1.email, self.brigade1.email,
                   self.agent_brg2.email, self.brigade2.email, self.agent_ddsp.email, self.dep.ddsp.email,
                   self.agent_commiss.email, self.commiss.email, self.agent_fede.email, self.fede.email,
                   self.agent_cg.email, self.dep.cg.email, self.agent_cgserv.email, self.cgserv.email, self.agent_cgsup.email,
                   self.agent_sdis.email, self.dep.sdis.email, self.agent_codis.email, self.dep.codis.email,
                   self.agent_group.email, self.group.email, self.agent_cis.email, self.cis.email,
                   self.organisateur.email, self.agent_mairie.email, self.commune.email, self.prefecture.email]
        self.assertEqual(messages[0].corps, 'Récépissé de déclaration publié pour la manifestation Manifestation_Test')
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "recepisse")
        for email in outbox:
            self.assertIn(email.to[0], mailist)
