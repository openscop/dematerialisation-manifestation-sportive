# coding: utf-8
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout, Fieldset
from django import forms

from core.forms.base import GenericForm
from .models import ManifestationDeclaration


class DeclarationForm(GenericForm):
    """ Formulaire de déclaration """

    def __init__(self, *args, **kwargs):
        """ Créer le layout du formulaire via crispy_forms """
        super(DeclarationForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            FormActions(Submit('save', "Soumettre la déclaration"))
        )

    # Meta
    class Meta:
        model = ManifestationDeclaration
        fields = ()


class DeclarationPublishReceiptForm(GenericForm):
    """ Formulaire de publication du récépissé de déclaration """

    def __init__(self, *args, **kwargs):
        """ Créer le layout du formulaire via crispy_forms """
        super(DeclarationPublishReceiptForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset("Sélection du récépissé de déclaration", 'receipt'),
            FormActions(Submit('save', "Publier le récépissé de déclaration"))
        )

    # Validation
    def clean_receipt(self):
        data = self.cleaned_data['receipt']
        if not data:
            raise forms.ValidationError("Vous devez sélectionner un récépissé de déclaration")
        return data

    # Meta
    class Meta:
        model = ManifestationDeclaration
        fields = ['receipt']
