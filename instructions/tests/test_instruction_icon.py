import re, os

from django.test import TestCase, override_settings
from django.contrib.auth.hashers import make_password as make

from post_office.models import EmailTemplate

from core.models import Instance
from core.factories import UserFactory
from evenements.factories import DcnmFactory
from instructions.models import Instruction, PreAvis
from organisateurs.factories import OrganisateurFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import InstructeurFactory,\
    FederationAgentFactory, DDSPAgentFactory, CommissariatAgentFactory, MairieAgentFactory
from administration.factories import CommissariatFactory
from organisateurs.factories import StructureFactory
from sports.factories import ActiviteFactory, FederationFactory


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class Circuit_Instruction_IconTests(TestCase):
    """
    Test du déroulement de l'instruction pour une Dcnm
        workflow_GGD : Avis GGD
        instruction par département
        openrunner false par défaut
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========== Instruction Icônes (Clt) ===========')
        # Création des objets sur le 78
        cls.dep = dep = DepartementFactory.create(name='78',
                                                  instance__name="instance de test",
                                                  instance__workflow_ggd=Instance.WF_GGD_EDSR,
                                                  instance__instruction_mode=Instance.IM_DEPARTEMENT)
        arrondissement = ArrondissementFactory.create(name='Versailles', code='99', departement=dep)
        cls.prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Plaisir', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        # Création des utilisateurs
        cls.organisateur = UserFactory.create(username='organisateur', password=make(123), default_instance=dep.instance)
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        cls.structure = StructureFactory(commune=cls.commune, organisateur=organisateur)

        cls.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur, prefecture=cls.prefecture)
        cls.agent_fede = UserFactory.create(username='agent_fede', password=make(123), default_instance=dep.instance)
        activ = ActiviteFactory.create()
        fede = FederationFactory.create()
        FederationAgentFactory.create(user=cls.agent_fede, federation=fede)
        cls.agent_ddsp = UserFactory.create(username='agent_ddsp', password=make(123), default_instance=dep.instance)
        DDSPAgentFactory.create(user=cls.agent_ddsp, ddsp=dep.ddsp)
        cls.commiss = CommissariatFactory.create(commune=cls.commune)
        cls.agent_commiss = UserFactory.create(username='agent_commiss', password=make(123), default_instance=dep.instance)
        CommissariatAgentFactory.create(user=cls.agent_commiss, commissariat=cls.commiss)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance)
        MairieAgentFactory.create(user=cls.agent_mairie, commune=cls.commune)

        EmailTemplate.objects.create(name='new_msg')

        # Création de l'événement
        cls.manifestation = DcnmFactory.create(ville_depart=cls.commune, structure=cls.structure, activite=activ,
                                               nom='Manifestation_Test', villes_traversees=(cls.autrecommune,))
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.descriptions_parcours = 'parcours de 10Km'
        cls.manifestation.nb_participants = 1
        cls.manifestation.nb_organisateurs = 10
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.save()

        cls.avis_nb = 3
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'itineraire_horaire'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Suppression des fichiers créés dans /tmp
        """
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'itineraire_horaire'):
            os.remove(file+".txt")

        os.system('rm -R /tmp/maniftest/media/78/')

        super(Circuit_Instruction_IconTests, cls).tearDownClass()

    def test_Circuit_Instruction(self):
        """
        Test du déroulement d'une instruction
        """
        """
        def print(string="", end=None):
            # Supprimer les prints hors debug
            pass
        """
        def presence_avis(username, state, log=True):
            """
            Appel de la dashboard de l'utilisateur pour tester la présence et l'état de l'événement
            :param username: agent considéré
            :param state: couleur de l'événement
            :param log: booléen pour connecter l'agent
            :return: retour: la réponse http
            """
            # Connexion avec l'utilisateur
            if log:
                self.assertTrue(self.client.login(username=username, password='123'))
            # Appel de la page
            retour = self.client.get('/instructions/tableaudebord/', HTTP_HOST='127.0.0.1:8000')
            # f = open('/var/log/manifsport/test_output.html', 'w', encoding='utf-8')
            # f.write(str(retour.content.decode('utf-8')).replace('\\n',""))
            # f.close()
            # Test du contenu
            if state == 'none':
                if username in ["agent_cis", "agent_codis"]:
                    nb_bloc = re.search('test_nb_rendu">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                else:
                    # Recherche sur "avis demandés", on sous-entend que c'est un TdB agent
                    nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('0', nb_bloc.group('nb'))
            # Test du chiffre dans la bonne case
            elif state == 'nouveau':
                nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            elif state == 'encours':
                nb_bloc = re.search('test_nb_encours">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            elif state == 'rendu':
                nb_bloc = re.search('test_nb_rendu">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            elif state == 'autorise':
                nb_bloc = re.search('test_nb_auto">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            elif state == 'interdite':
                nb_bloc = re.search('test_nb_inter">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            else:
                self.assertEqual('0', '1', 'paramètre inconnu')
            return retour

        def vue_detail(page):
            """
            Appel de la vue de détail de la manifestation
            :param page: réponse précédente
            :return: reponse suivante
            """
            detail = re.search('data-href=\'(?P<url>(/[^"]+))', page.content.decode('utf-8'))
            if hasattr(detail, 'group'):
                page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
            self.assertContains(page, 'Manifestation_Test')
            return page

        def aucune_action(page):
            """
            Test auncune action affichée dans la zone action de la dashboard
            :param page: réponse précédente
            """
            action = re.search(', aucune action nécessaire', page.content.decode('utf-8'))
            attente = re.search(' devez attendre ', page.content.decode('utf-8'))
            if not action and not attente:
                self.assertIsNotNone(action, msg="message non trouvé")

        def affichage_avis():
            """
            Affichage des avis émis pour l'événement avec leur status
             """
            for avis in self.instruction.get_tous_avis():
                if avis.etat != 'rendu':
                    print(avis, end=" ; ")
                    print(avis.etat)

        print('**** test 1 creation manif ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/tableau-de-bord-organisateur/liste?filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file4:
            self.client.post(url_script.group('url'), {'cartographie': file4}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/itineraire_horaire.txt') as file5:
            reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file5}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(declar, 'group'):
            self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.client.logout()

        self.instruction = Instruction.objects.get(manif=self.manifestation)
        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().get_instance())
        print(self.manifestation.ville_depart.get_departement().ggd, end=" ; ")
        print(self.manifestation.ville_depart.get_departement().edsr, end=" ; ")
        print(self.manifestation.ville_depart.get_departement().ddsp)
        print(self.manifestation.structure, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline, end=" ; ")
        print(self.manifestation.activite.discipline.get_federations().first().name)
        affichage_avis()
        self.assertEqual(str(self.instruction), str(self.manifestation))

        print('**** test 2 instructeur ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        presence_avis('instructeur', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Test des informations affichées
        self.assertContains(reponse, '<i class="declaration', count=1)
        self.assertContains(reponse, '<i class="sans-vtm', count=1)
        self.assertContains(reponse, '<i class="voie-publique', count=1)
        self.assertContains(reponse, '<i class="competition', count=1)
        self.assertContains(reponse, '<i class="delai-60j', count=1)
        self.assertContains(reponse, '<i class="demandee', count=1)
        self.assertContains(reponse, '<i class="dossier-complet-non-verifie', count=1)

        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Test des informations affichées
        self.assertContains(reponse, '<i class="declaration', count=1)
        self.assertContains(reponse, '<i class="delai-60j', count=1)
        self.assertContains(reponse, '<i class="dossier-complet-non-verifie', count=1)
        self.assertContains(reponse, '<i class="demandee', count=1)

        # Vérifier l'action disponible
        self.assertContains(reponse, 'Demander un document complémentaire', count=1)
        self.assertContains(reponse, 'Envoyer les demandes d\'avis', count=2)
        self.assertContains(reponse, 'Valider la complétude des pièces jointes', count=1)
        self.assertContains(reponse, 'Ajouter un document officiel', count=1)
        self.assertContains(reponse, 'Modifier l\'affichage dans le calendrier public', count=1)

        # Valider les documents
        valid_url = re.search('id="valider_doc_div">\\n.+href="(?P<url>(/[^"]+)).+test_valider_doc', reponse.content.decode('utf-8'))
        if not hasattr(valid_url, 'group'):
            self.assertIsNotNone(valid_url)
        reponse = self.client.get(valid_url.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '<i class="dossier-complet-verifie', count=1)

        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        edit_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(edit_form, 'group'):
            reponse = self.client.post(edit_form.group('url'),
                                       {'ddsp_concerne': True,
                                        'villes_concernees': [self.commune.pk]}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction)
        self.assertContains(reponse, '<i class="distribue', count=1)
        affichage_avis()
        # Vérifier le passage en encours et le nombre d'avis manquants
        presence_avis('instructeur', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        print('**** test 3 agent fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en nouveau
        presence_avis('agent_fede', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 28 jours')
        self.assertContains(reponse, '<i class="demandee" data-titre="Avis demandé par l\'instructeur"', count=1)
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Valider l'événement avec l'url fournie et tester la redirection
        ackno = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ackno, 'group'):
            reponse = self.client.post(ackno.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # Vérifier le passage en rendu
        presence_avis('agent_fede', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '<i class="autorisee" data-titre="Avis favorable rendu"', count=1)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        print('**** test 4 agent ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        presence_avis('agent_ddsp', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.assertContains(reponse, '<i class="demandee" data-titre="Avis demandé par l\'instructeur"', count=1)
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'commissariats_concernes': self.commiss.pk}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='commissariats')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérifier le passage en encours
        presence_avis('agent_ddsp', 'encours', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.assertContains(reponse, '<i class="distribuee" data-titre="Demandes de préavis envoyées"', count=1)
        self.client.logout()

        print('**** test 5 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        presence_avis('agent_commiss', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '<i class="demandee" data-titre="Préavis demandé"', count=1)
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='commissariats')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérification du passage en rendu
        presence_avis('agent_commiss', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '<i class="autorisee" data-titre="Préavis favorable rendu"', count=1)
        self.client.logout()

        print('**** test 6 agent ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en encours
        presence_avis('agent_ddsp', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.assertContains(reponse, '<i class="distribuee" data-titre="Demandes de préavis envoyées"', count=1)
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # Vérifier le passage en rendu
        presence_avis('agent_ddsp', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '<i class="autorisee" data-titre="Avis favorable rendu"', count=1)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        self.client.logout()

        print('**** test 7 agent mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en nouveau
        presence_avis('agent_mairie', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        self.assertContains(reponse, '<i class="demandee" data-titre="Avis demandé par l\'instructeur"', count=1)
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Valider l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # Vérifier le passage en rendu
        presence_avis('agent_mairie', 'rendu', log=False)
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '<i class="autorisee" data-titre="Avis favorable rendu"', count=1)
        self.client.logout()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = presence_avis('instructeur', 'encours')
        self.assertNotContains(reponse, 'avis manquants')
        # print(reponse.content.decode('utf-8'))
