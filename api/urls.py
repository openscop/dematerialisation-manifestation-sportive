from django.urls import include, path, re_path
from api.views.organisateur import OrganisateurViewSet
from api.views.manif import ManifestationViewSet, ManifPublicViewSet
from api.views.instruction import InstructionViewSet
from api.views.preavis import PreavisViewSet
from api.views.avis import AvisView
from rest_framework import routers, permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="API de manifestationSportive.fr",
        default_version='v1',
        description="Permet l'utilisation de la plateforme à partir d'une autre application\n"
                    "Les urls affichées sont celles que vous pouvez accèder",
        contact=openapi.Contact(name="Openscop", url="https://www.openscop.fr/", email="contact@openscop.fr"),
        license=openapi.License(name="Creative Commons BY-NC-ND 4.0", url="https://creativecommons.org/licenses/by-nc-nd/4.0/")
    ),
    public=False,
    permission_classes=(permissions.AllowAny,),
)

app_name = 'api'
router = routers.DefaultRouter()
router.register(r'Calendrier', ManifPublicViewSet, basename='Calendrier')
router.register(r'Instruction', InstructionViewSet, basename='Instruction')
router.register(r'Evenement', ManifestationViewSet, basename='Evenement')
router.register(r'Preavis', PreavisViewSet, basename='Preavis')
router.register(r'Avis', AvisView, basename='Avis')
router.register(r'Organisateur', OrganisateurViewSet)

urlpatterns = [
    path('', include(router.urls)),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

]
