import logging

from django.contrib.contenttypes.models import ContentType
from django.utils import timezone

from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from django_filters.rest_framework import DjangoFilterBackend, DateTimeFilter, FilterSet
from drf_yasg.utils import swagger_auto_schema
from bootstrap_datepicker_plus import DateTimePickerInput

from instructions.models import Avis
from api.serializer.instruction import AvisSerializer, AvisRendreSerializer, AvisDetailSerializer
from api.permissions import IsAgent, IsInGroupApi

api_logger = logging.getLogger('api')


class AvisFilter(FilterSet):

    date_debut = DateTimeFilter(field_name="instruction__manif__date_debut",
                                label='date de début de la manifestation',
                                widget=DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'locale': 'fr'}),
                                lookup_expr='gte')

    date_demande = DateTimeFilter(field_name="date_demande",
                                  label='date de demande',
                                  widget=DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'locale': 'fr'}),
                                  lookup_expr='gte')

    class Meta:
        model = Avis
        fields = ('etat', 'date_demande', 'instruction', 'date_debut', 'id')


class AvisView(viewsets.ModelViewSet):
    """
    Liste des demandes d'avis envoyées à l'agent\n
    Cette route est réservée aux agents authentifiés.
    """
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter,)
    queryset = Avis.objects.all()
    permission_classes = (IsAuthenticated, IsAgent, IsInGroupApi)
    serializer_class = AvisSerializer
    detail_serializer_class = AvisDetailSerializer
    rendre_serializer_class = AvisRendreSerializer
    filter_class = AvisFilter
    search_fields = ('prescriptions',)
    ordering_fields = ('date_demande', 'date_reponse')

    """
    Utilisation de la fonction finalize_response pour bénéficier des variables request et response
    dans lesquelles se trouvent les infos à logger
    """
    def finalize_response(self, request, response, *args, **kwargs):
        if 'count' in response.data:
            count = response.data['count']
        else:
            count = 'none'
        api_logger.info("Utilisateur %s | %s de %s | %s | réponse %s, %s | items : %s" %
                        (request.user, request.method, self.request.get_host(), self.request.get_full_path(),
                         response.status_code, response.status_text, count
                         ))
        return super().finalize_response(request, response, *args, **kwargs)

    def get_serializer_class(self):
        if self.action == 'retrieve':
            if hasattr(self, 'detail_serializer_class'):
                return self.detail_serializer_class
        if self.action == 'rendre':
            if hasattr(self, 'rendre_serializer_class'):
                return self.rendre_serializer_class
        return super().get_serializer_class()

    def get_queryset(self):
        service = self.request.user.get_service()
        ct_service = ContentType.objects.get_for_model(service)
        return Avis.objects.filter(content_type=ct_service, object_id=service.id)

    @swagger_auto_schema(auto_schema=None)
    def partial_update(self, request, *args, **kwargs):
        return Response({"detail": "Opération interdite."}, status=403)

    @swagger_auto_schema(auto_schema=None)
    def update(self, request, *args, **kwargs):
        return Response({"detail": "Opération interdite."}, status=403)

    @swagger_auto_schema(auto_schema=None)
    def create(self, request, *args, **kwargs):
        return Response({"detail": "Opération interdite."}, status=403)

    @swagger_auto_schema(auto_schema=None)
    def destroy(self, request, *args, **kwargs):
        return Response({"detail": "Opération interdite."}, status=403)

    def retrieve(self, request, *args, **kwargs):
        """
        Détail d'un avis\n
        {id} est le numéro de l'avis.
        Cette route est réservée aux agents, l'avis demandé doit appartenir au service de l'agent.
        """
        avis = self.get_object()
        serializer = AvisDetailSerializer(avis)
        return Response(serializer.data)

    @swagger_auto_schema(responses={'200': 'Avis rendu'})
    @action(methods=['post'], detail=True)
    def rendre(self, request, *args, **kwargs):
        """
        Rendre un avis\n
        {id} est le numéro de l'avis.
        Cette route est réservée aux agents.
        """
        avis = self.get_object()
        if avis.etat == 'rendu':
            return Response({"detail": "Avis déjà rendu"}, status=406)
        try:
            serializer = AvisRendreSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            avis.date_reponse = timezone.now()
            avis.agent = self.request.user
            avis.favorable = serializer.validated_data['favorable']
            avis.prescriptions = serializer.validated_data['prescriptions']
            avis.rendreAvis(self.request.user)
            avis.save()
            return Response("Avis rendu.")
        except:
            return Response({"detail": "Problème de données."}, status=406)
