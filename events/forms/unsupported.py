# coding: utf-8

from django.forms.models import ModelMultipleChoiceField, ModelChoiceField

from administrative_division.models.departement import Departement
from events.forms.manifestation import ManifestationForm, FORM_WIDGETS
from events.models.unsupported import UnsupportedManifestation


class UnsupportedManifestationForm(ManifestationForm):
    """ Formulaire de base pour les manifestations non prises en charge """

    # Champs
    other_departments_crossed = ModelMultipleChoiceField(required=False, queryset=Departement.objects.all(), label="Autres départements traversés")
    departure_departement = ModelChoiceField(required=False, queryset=Departement.objects.unconfigured(), label="Département de départ")

    # Meta
    class Meta:
        model = UnsupportedManifestation
        widgets = FORM_WIDGETS
        fields = ['id', 'name', 'begin_date', 'end_date', 'description', 'extra_activite', 'discipline', 'activite', 'departure_departement', 'departure_city',
                  'other_departments_crossed', 'crossed_cities', 'number_of_entries', 'max_audience']
