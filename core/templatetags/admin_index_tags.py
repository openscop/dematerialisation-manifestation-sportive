from django import template

register = template.Library()


@register.filter
def movetocore(app_liste):
    """ Transférer les modeles de admin et auth dans core """
    if isinstance(app_liste, list):
        if len(app_liste) > 1:
            models_admin, models_auth = [], []
            app_admin_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'admin'), None)
            app_auth_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'auth'), None)
            app_core_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'core'), None)
            if app_admin_index is not None:
                models_admin = app_liste[app_admin_index]['models']
            if app_auth_index is not None:
                models_auth = app_liste[app_auth_index]['models']
            if app_core_index is not None:
                app_liste[app_core_index]['models'] = sorted(app_liste[app_core_index]['models'] + models_admin + models_auth, key=lambda c: c['name'])
            if app_admin_index is not None:
                del app_liste[app_admin_index]
            app_auth_index = next((index for (index, item) in enumerate(app_liste) if item['app_label'] == 'auth'), None)
            if app_auth_index is not None:
                del app_liste[app_auth_index]
        return app_liste
    else:
        return app_liste