# Generated by Django 2.2.12 on 2020-07-08 13:14

import django.core.validators
from django.db import migrations, models
import re


class Migration(migrations.Migration):

    dependencies = [
        ('evenements', '0027_auto_20200615_1308'),
    ]

    operations = [
        migrations.AddField(
            model_name='documentcomplementaire',
            name='date_demande',
            field=models.DateTimeField(blank=True, editable=False, null=True, verbose_name='date de demande'),
        ),
        migrations.AddField(
            model_name='documentcomplementaire',
            name='date_depot',
            field=models.DateTimeField(blank=True, editable=False, null=True, verbose_name='date de dépot'),
        ),
    ]
