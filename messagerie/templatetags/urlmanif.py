from django import template

register = template.Library()


@register.filter()
def urlmanif(msg, user):
    """
    Ce tag nous permet de recuperer dans les templates l'url d'une manifestation selon l'utilisateur
    """
    url = msg.get_url_manif_associe(user)
    return url
