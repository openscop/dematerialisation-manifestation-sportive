from django.core.management.base import BaseCommand
from django.db import transaction
from django.db.models import Q

from evenements.models import Manif
from notifications.models import Notification, Action
from messagerie.models import Message, Enveloppe


class Command(BaseCommand):
    """
    commande pour transformer les notification en message
    les notification ne sont PAS effacées
    """
    def handle(self, *args, **options):
        with transaction.atomic():
            manifs = Manif.objects.all().order_by("pk")
            count = 1
            for manif in manifs:
                print(str(count)+"/"+str(len(manifs)))
                count += 1
                actions = Action.objects.filter(manif=manif, action="description de la manifestation")
                for action in actions:
                    message = Message.objects.creer_et_envoyer('tracabilite', None, [action.user], 'Création de la manifestation',
                                                     '<p></p>', manifestation_liee=action.manif)
                    for envel in message.message_enveloppe.all():
                        envel.date = action.creation_date
                        envel.save()
                notifs = Notification.objects.filter(manif=manif)
                last_notif = None
                last_env = None
                for notif in notifs:
                    if last_notif:
                        delta = notif.creation_date - last_notif.creation_date
                        if delta.seconds < 1:
                            message = last_env.corps
                        else:
                            message = Message(corps=notif.subject)
                    else:
                        message = Message(corps=notif.subject)
                    user = notif.content_object.organisateur.user if notif.content_object.__class__.__name__=="Structure" else None
                    user_txt = str(notif.content_object.organisateur.user.get_service())+" → "+str(notif.content_object.organisateur.user.last_name)+' '+str(notif.content_object.organisateur.user.first_name) if notif.content_object.__class__.__name__=="Structure" else str(notif.content_object)

                    # ici on va detecter grâce au sujet de quoi parle la notification
                    if "avis requis" in notif.subject:
                        msg_obj = "Avis requis"
                        doc_objet = "avis"
                        type = "action"
                    elif "pré-avis rendu" in notif.subject:
                        msg_obj = "Pré-avis rendu"
                        doc_objet = "preavis"
                        type = "action"
                    elif "avis rendu" in notif.subject:
                        msg_obj = "Avis rendu"
                        doc_objet = "avis"
                        type = "info_suivi"
                    elif "Pièce jointe ajoutée" in notif.subject:
                        msg_obj = "Pièce jointe ajoutée"
                        doc_objet = "piece_jointe"
                        type = "info_suivi"
                    elif "document ajouté au d" in notif.subject:
                        msg_obj = "Pièce jointe ajoutée"
                        doc_objet = "dossier"
                        type = "info_suivi"
                    elif "Documents complémentaires envoyés" in notif.subject:
                        msg_obj = "Document complémentaire envoyé"
                        doc_objet = "piece_jointe"
                        type = "info_suivi"
                    elif "avis mis en forme" in notif.subject:
                        msg_obj = "Avis mis en forme"
                        doc_objet = 'avis'
                        type = "action"
                    elif "pré-avis demandés" in notif.subject:
                        msg_obj = "Pré-avis demandé"
                        doc_objet = "preavis"
                        type = "action"
                    elif "Prenez connaissance des informations" in notif.subject:
                        msg_obj = "Prenez connaissance des informations"
                        doc_objet = "avis"
                        type = "info_suivi"
                    elif "début de" in notif.subject:
                        msg_obj = "Début de l'instruction"
                        doc_objet = "dossier"
                        type = "info_suivi"
                    elif "Documents complémentaires requis" in notif.subject:
                        msg_obj = "Documents complémentaires requis"
                        doc_objet = "piece_jointe"
                        type = "action"
                    elif "demande d" in notif.subject:
                        msg_obj = "Demande d'instruction"
                        doc_objet = "dossier"
                        type = "action"
                    elif "autorisation publié" in notif.subject:
                        msg_obj = "Arrêté d'autorisation"
                        doc_objet = "arrete"
                        type = "info_suivi"
                    elif "récépissé de déclaration" in notif.subject:
                        msg_obj = "Récépissé de déclaration"
                        doc_objet = "recepisse"
                        type = "info_suivi"
                    elif "arrêté de circulation" in notif.subject:
                        msg_obj = "Arrêté de circulation"
                        doc_objet = "arrete"
                        type = "info_suivi"
                    elif "interdiction" in notif.subject:
                        msg_obj = "Arrêté d'interdiction"
                        doc_objet = "arrete"
                        type = "info_suivi"
                    elif "email pour la" in notif.subject:
                        msg_obj = "Problème d'email"
                        doc_objet = "dossier"
                        type = "info_suivi"
                    else:
                        msg_obj = notif.subject
                        doc_objet = "dossier"
                        type = "info_suivi"
                    message.save()
                    enveloppe = Enveloppe(corps=message, expediteur_id=user, expediteur_txt=user_txt, destinataire_id=
                                          notif.user, destinataire_txt=str(notif.user.get_service())+' → '+str(notif.user.last_name)+' '+str(notif.user.first_name), nombre_destinataire=1, objet=msg_obj,
                                          type=type, date=notif.creation_date, manifestation=manif, lu_requis=True, lu_datetime=notif.creation_date,
                                          affichage_messagerie=True, notif_bureau=False, doc_objet=doc_objet)
                    enveloppe.save()
                    last_env = enveloppe
                    last_notif = notif









