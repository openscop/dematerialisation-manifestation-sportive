# coding: utf-8

from django import template

from instructions.models.instruction import Instruction
from instructions.models.acces import AutorisationAcces
from instructions.models.avis import Avis


register = template.Library()


@register.filter
def avis(value, user):
    """ Renvoyer l'avis correspondant à l'instruction et à l'agent """
    if isinstance(value, Instruction):
        return value.get_avis_user(user)
    return None


@register.filter
def preavis(value, user):
    """ Renvoyer le préavis correspondant à l'instruction et à l'agent local """
    if isinstance(value, Instruction):
        return value.get_preavis_user(user)
    return None


@register.filter
def cis(value):
    """ Renvoyer si un CIS a une autorisation d'accès pour l'avis """
    if isinstance(value, Avis):
        if AutorisationAcces.objects.filter(avis_id=value.id).count() != 0:
            return True
    return False
