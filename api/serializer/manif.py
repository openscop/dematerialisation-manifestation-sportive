from django.conf import settings
from django.contrib.sites.models import Site

from rest_framework import serializers

from evenements.models import Manif, DocumentComplementaire, Dnm, Dnmc, Dcnm, Dcnmc, Dvtm,  Avtm, Avtmcir
from evaluation_incidence.models import EvaluationRnr, EvaluationN2K
from instructions.models import DocumentOfficiel
from administrative_division.models import Commune
from api.serializer.structure import StructureSerializer

class CommuneField(serializers.RelatedField):
    """
    Modification de la représentation de l'objet
    """
    queryset = Commune.objects.all()
    label = 'Code INSEE et nom de la commune'

    def to_representation(self, value):
        return '%s, %s' % (value.code, value.name)


class ParcoursField(serializers.URLField):
    def to_representation(self, value):
        ids = [route for route in value.strip(',').split(',') if route]
        url = settings.OPENRUNNER_HOST + "kml/exportImportGPX.php?rttype=0&id="
        return [url + idx for idx in ids]


class FileUrlField(serializers.URLField):
    def to_representation(self, value):
        return 'https://' + (Site.objects.get_current().domain + settings.MEDIA_URL + str(value)) if value else None


class RnrevaluationSerilizer(serializers.ModelSerializer):

    class Meta:
        ref_name = 'Evaluation Rnr'
        model = EvaluationRnr
        fields = ('__all__')


class N2kevaluationSerilizer(serializers.ModelSerializer):

    class Meta:
        ref_name = 'Evaluation Natura2000'
        model = EvaluationN2K
        fields = ('__all__')


class ManifPublicSerializer(serializers.ModelSerializer):

    # Utilisation de la classe StringRelatedField pour afficher
    # la représentation de l'objet défini par __str__
    # departure_city = serializers.StringRelatedField()
    #
    # Utilisation de la classe définie plus haut pour afficher
    # une représentation personnalisée de l'objet
    ville_depart = CommuneField()
    villes_traversees = CommuneField(many=True)
    activite = serializers.StringRelatedField()
    structure = StructureSerializer()

    class Meta:
        ref_name = None
        model = Manif
        fields = (
            'nom',
            'description',
            'activite',
            'date_debut',
            'date_fin',
            'ville_depart',
            'villes_traversees',
            'structure',
        )
        # depth = 2 pour voir les attributs de l'objet valeur d'un champ


class ManifestationSerializer(serializers.ModelSerializer):

    instance = serializers.StringRelatedField()
    instruction = serializers.IntegerField(source='instruction.pk')
    ville_depart = CommuneField()
    villes_traversees = CommuneField(many=True)
    departements_traverses = serializers.StringRelatedField(many=True)
    structure = StructureSerializer()
    activite = serializers.StringRelatedField()
    parcours_openrunner = ParcoursField()
    emprise = serializers.CharField(source='get_emprise_display')
    sites_natura2000 = serializers.StringRelatedField(many=True)
    zones_rnr = serializers.StringRelatedField(many=True)
    lieux_pdesi = serializers.StringRelatedField(many=True)
    rnrevaluation = serializers.IntegerField(source='rnrevaluation.id')
    n2kevaluation = serializers.IntegerField(source='n2kevaluation.id')

    class Meta:
        ref_name = 'Manifestation'
        model = Manif
        exclude = (
            'delai_cours',
            'cartographie',
            'reglement_manifestation',
            'engagement_organisateur',
            'disposition_securite',
            'topo_securite',
            'presence_docteur',
            'certificat_assurance',
            'docs_additionels',
            'charte_dispense_site_n2k',
            'convention_police'
        )


class DnmSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Dnm'
        model = Dnm
        exclude = ManifestationSerializer.Meta.exclude + (
            'itineraire_horaire',
        )


class DnmcSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Dnmc'
        model = Dnmc
        exclude = ManifestationSerializer.Meta.exclude + (
            'itineraire_horaire',
            'dossier_tech_cycl'
        )


class DcnmSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Dcnm'
        model = Dcnm
        exclude = ManifestationSerializer.Meta.exclude + (
            'itineraire_horaire',
            'liste_signaleurs'
        )


class DcnmcSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Dcnmc'
        model = Dcnmc
        exclude = ManifestationSerializer.Meta.exclude + (
            'itineraire_horaire',
            'liste_signaleurs',
            'dossier_tech_cycl'
        )


class DvtmSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Dvtm'
        model = Dvtm
        exclude = ManifestationSerializer.Meta.exclude + (
            'itineraire_horaire',
        )


class AvtmSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Avtm'
        model = Avtm
        exclude = ManifestationSerializer.Meta.exclude + (
            'avis_federation_delegataire',
            'carte_zone_public',
            'commissaires',
            'certificat_organisateur_tech',
            'itineraire_horaire',
            'participants',
            'plan_masse'
        )


class AvtmcirSerializer(ManifestationSerializer):
    """
    Sérialiseur des cerfas
    """
    type = serializers.CharField(source='get_type_manif')

    class Meta(ManifestationSerializer.Meta):
        ref_name = 'Avtmcir'
        model = Avtmcir
        exclude = ManifestationSerializer.Meta.exclude + (
            'plan_masse',
            'certificat_organisateur_tech',
            'participants'
        )


class ManifPJSerializer(serializers.Serializer):
    """
    Sérialiseur pour afficher les fichiers de la manifestation
    """
    reglement_manifestation = FileUrlField()
    engagement_organisateur = FileUrlField()
    disposition_securite = FileUrlField()
    topo_securite = FileUrlField()
    presence_docteur = FileUrlField()
    certificat_assurance = FileUrlField()
    docs_additionels = FileUrlField()
    charte_dispense_site_n2k = FileUrlField()
    convention_police = FileUrlField()
    itineraire_horaire = FileUrlField(required=False)
    dossier_tech_cycl = FileUrlField(required=False)
    liste_signaleurs = FileUrlField(required=False)
    avis_federation_delegataire = FileUrlField(required=False)
    carte_zone_public = FileUrlField(required=False)
    commissaires = FileUrlField(required=False)
    certificat_organisateur_tech = FileUrlField(required=False)
    participants = FileUrlField(required=False)
    plan_masse = FileUrlField(required=False)
    cartographie = FileUrlField()

    class Meta:
        ref_name = 'Pièces jointes'


class DocCompSerializer(serializers.ModelSerializer):
    """
    Sérialiseur des documents complémentaires d'une manifestation
    """
    document_attache = FileUrlField()

    class Meta:
        ref_name = 'Document complémentaire'
        model = DocumentComplementaire
        fields = (
            'id',
            'information_requise',
            'date_demande',
            'date_depot',
            'document_attache'
        )


class DocOfficielSerializer(serializers.ModelSerializer):
    """
    Sérialiseur des documents officiels d'une manifestation
    """
    fichier = FileUrlField()
    nature = serializers.CharField(source='get_nature_display')

    class Meta:
        ref_name = 'Document officiel'
        model = DocumentOfficiel
        fields = (
            'id',
            'date_depot',
            'nature',
            'utilisateur',
            'fichier'
        )
