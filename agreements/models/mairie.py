# coding: utf-8
import datetime

from django.urls import reverse
from django.db import models
from django_fsm import transition

from agreements.models.avis import Avis, AvisQuerySet


class MairieAvis(Avis):
    """ Avis mairie """

    # Champs
    avis_ptr = models.OneToOneField("agreements.avis", parent_link=True, related_name='mairieavis', on_delete=models.CASCADE)
    commune = models.ForeignKey('administrative_division.commune', verbose_name='commune', on_delete=models.CASCADE)
    objects = AvisQuerySet.as_manager()

    # Override
    def __str__(self):
        return ' - '.join([str(self.get_manifestation()), str(self.commune)])

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'avis """
        return reverse('agreements:townhall_agreement_detail', kwargs={'pk': self.pk})

    def get_agents(self):
        """ Renvoyer les agents concernés par l'avis """
        return self.commune.get_mairieagents()

    # Action
    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        """ Rendre l'avis """
        self.reply_date = datetime.date.today()
        self.save()
        self.notify_ack(content_object=self.commune)
        self.log_ack(agents=self.get_agents())

    # Meta
    class Meta:
        verbose_name = 'avis mairie'
        verbose_name_plural = 'avis mairies'
        app_label = 'agreements'
        default_related_name = 'mairieaviss'
