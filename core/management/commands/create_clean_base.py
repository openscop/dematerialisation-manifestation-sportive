# coding: utf-8
import os
from time import time

from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.db import connections


class Command(BaseCommand):
    """
    Créer une base de données par défaut

    C'est normalement impossible à cause des migrations :
    une migration créée par django_custom_user_migration nécessite
    la présence de auth_user. Or, le modèle n'existe pas dans le graphe.
    On va donc créer la table avant de procéder à la migration.
    """
    args = ''
    help = "Créer une base par défaut vide"

    def add_arguments(self, parser):
        parser.add_argument('--no-input', action='store_false', dest='interactive', default=True,
                            help="Ne pas demander de validation de l'utilisateur")

    def handle(self, *args, **options):
        """ Exécuter la commande """
        interactive = options.get('interactive')
        reply = ''

        if interactive is True:
            reply = input("Cette commande va vider la base de données 'default'. Continuer ? [oui/*]\n")

        if reply.lower() in ["oui", "o"] or not interactive:
            start = time()
            db_name = settings.DATABASES['default']['NAME']
            db_user = settings.DATABASES['default']['USER']
            os.system("dropdb --if-exists -U{u} {name}".format(u=db_user, name=db_name))
            os.system("createdb -U{u} {name}".format(u=db_user, name=db_name))

            # Créer la table auth_user (PostgreSQL uniquement)
            auth_user_script = """
            CREATE TABLE public.auth_user (
                id integer NOT NULL DEFAULT 0,
                password character varying(128) NOT NULL,
                last_login timestamp with time zone,
                is_superuser boolean NOT NULL,
                username character varying(30) NOT NULL,
                first_name character varying(30) NOT NULL,
                last_name character varying(30) NOT NULL,
                email character varying(254) NOT NULL,
                is_staff boolean NOT NULL,
                is_active boolean NOT NULL,
                date_joined timestamp with time zone NOT NULL,
                CONSTRAINT auth_user_pkey PRIMARY KEY (id),
                CONSTRAINT auth_user_username_key UNIQUE (username)
            )
            WITH (
                OIDS=FALSE
            );

            CREATE TABLE public.auth_user_groups (
                id integer NOT NULL DEFAULT 0,
                user_id integer NOT NULL,
                group_id integer NOT NULL,
                CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id)
            );

            CREATE TABLE public.auth_user_user_permissions (
                id integer NOT NULL DEFAULT 0,
                user_id integer NOT NULL,
                permission_id integer NOT NULL,
                CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id),
                CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id)
            )
            WITH (
                OIDS=FALSE
            );
            """
            cursor = connections['default'].cursor()
            cursor.execute(auth_user_script)
            call_command('migrate', database='default', interactive=True)  # s'il y a un problème, il va poser des questions
            call_command('loaddata', 'base_administrative_division', database='default', interactive=True)

            # Afficher le récapitulatif
            elapsed = time() - start
            print("Opérations effectuées avec succès en {elapsed:.01f} secondes.".format(elapsed=elapsed))
