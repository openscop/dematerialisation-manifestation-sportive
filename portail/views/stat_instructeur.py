# coding: utf-8
from django.views.generic.base import TemplateView
from django.contrib.sessions.models import Session
from django.db.models import Count, Variance, Q, Avg, F
from django.db.models.functions import Extract
from django.utils import timezone
from django.utils.decorators import method_decorator

from collections import OrderedDict

from core.models import User
from core.util.permissions import require_role
from instructions.models import Instruction
from instructions.models import Avis as InstructionAvis
from instructions.models import PreAvis as InstructionPreAvis
from administrative_division.models.departement import Departement


@method_decorator(require_role('instructeur'), name='dispatch')
class StatInstructeur(TemplateView):
    """ Page de statistiques pour les instructeurs """

    # Configuration
    template_name = 'stat_instructeur.html'

    def get_current_users(self, **kwargs):
        active_sessions = Session.objects.filter(expire_date__gte=timezone.now())
        user_id_list = []
        for session in active_sessions:
            data = session.get_decoded()
            user_id_list.append(data.get('_auth_user_id', None))
        if 'dept' in kwargs and kwargs.get('dept'):
            dept = kwargs.get('dept')
            return User.objects.filter(id__in=user_id_list, default_instance__departement=dept)
        else:
            return User.objects.filter(id__in=user_id_list)

    def moyenne_par_annee(self, **kwargs):
        data = None
        if "liste" in kwargs and "critere" in kwargs:
            liste = kwargs.get('liste')
            critere = kwargs.get('critere')
            data = {}
            for y in range(2018, timezone.now().year + 2):
                filtre = critere + "__year"
                delta = liste.filter(**{filtre: y}).aggregate(delai=Avg(F('date_reponse') - F('date_demande')))
                data[y] = delta['delai'].days if delta['delai'] else ''
        return OrderedDict(sorted(data.items(), key=lambda t: t[0]))

    """ 
        Résultats donnés par l'écart-type sont peu parlants donc
        commentés dans le template pour l'instant
        envisager un calcul d'intervalle de confiance
    """
    def ecart_type_par_annee(self, **kwargs):
        data = None
        if "liste" in kwargs and "critere" in kwargs:
            liste = kwargs.get('liste')
            critere = kwargs.get('critere')
            data = {}
            for y in range(2018, timezone.now().year + 2):
                filtre = critere + "__year"
                variance = liste.filter(**{filtre: y}).aggregate(
                    variance=Variance(Extract(F('date_reponse') - F('date_demande'), 'day')))['variance']
                data[y] = round(variance**0.5) if variance else ''
        return OrderedDict(sorted(data.items(), key=lambda t: t[0]))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        stat = {}
        stat['annees'] = range(2018, timezone.now().year + 2)

        liste_dept = []
        # récupérer la liste des départements
        for d in Departement.objects.configured():
            liste_dept.append((d.name, d))
        context['liste_dept'] = liste_dept

        # filtrage par département
        dept = self.request.user.get_departement().name  # département de l'utilisateur par défaut
        if self.request.GET.get('dept'):  # département demandé en paramètre d'url ?
            if self.request.user.is_superuser or self.request.GET.get(
                    'dept') == self.request.user.get_departement().name:  # vérifie que l'utilisateur peut consulter le département demandé
                dept = self.request.GET.get('dept')
            if self.request.GET.get(
                    'dept') == "nationale":  # ne pas définir de département si "nationale" passé en paramètre d'url
                dept = ""
        if dept:
            context['dept'] = Departement.objects.get(name__startswith=dept)


        # Délais de restitution des avis
        avis_dept_filter = Q(
            instruction__manif__ville_depart__arrondissement__departement=context['dept']) if dept else Q()
        liste_avis = InstructionAvis.objects.exclude(date_reponse__isnull=True).filter(avis_dept_filter)
        stat['delai_avis_moyen'] = liste_avis.aggregate(
            delai_moyen=Avg(F('date_reponse') - F('date_demande')))['delai_moyen'].days if liste_avis else ''
        stat['delai_avis_ecart_type'] = round(liste_avis.aggregate(
            variance=Variance(Extract(F('date_reponse') - F('date_demande'), 'day')))['variance']**0.5) if liste_avis else ''
        stat['delai_avis_moyen_par_annee'] = self.moyenne_par_annee(liste=liste_avis, critere="date_demande")
        stat['delai_avis_ecart_type_par_annee'] = self.ecart_type_par_annee(liste=liste_avis, critere="date_demande")

        liste_services_avis = dict(InstructionAvis.LIST_SERVICES)
        stat['delai_avis_par_service'] = list()
        for service in liste_services_avis:
            delta = liste_avis.filter(service_concerne=service).aggregate(delai=Avg(F('date_reponse') - F('date_demande')))
            delai_avis_moyen = delta['delai'].days if delta['delai'] else ''
            variance = liste_avis.filter(service_concerne=service).aggregate(
                variance=Variance(Extract(F('date_reponse') - F('date_demande'), 'day')))['variance']
            delai_avis_ecart_type = round(variance ** 0.5) if variance else ''
            delai_avis_moyen_par_annee = self.moyenne_par_annee(
                liste=liste_avis.filter(service_concerne=service), critere="date_demande")
            delai_avis_ecart_type_par_annee = self.ecart_type_par_annee(
                liste=liste_avis.filter(service_concerne=service), critere="date_demande")
            data = {
                'nom': service.upper() if service != 'service' else 'Autres services',
                'delai_avis_moyen': delai_avis_moyen,
                'delai_avis_ecart_type': delai_avis_ecart_type,
                'delai_avis_moyen_par_annee': delai_avis_moyen_par_annee,
                'delai_avis_ecart_type_par_annee': delai_avis_ecart_type_par_annee
            }
            stat['delai_avis_par_service'].append(data)

        # Délais de restitution des préavis
        preavis_dept_filter = Q(
            avis__instruction__manif__ville_depart__arrondissement__departement=context['dept']) if dept else Q()
        liste_preavis = InstructionPreAvis.objects.filter(preavis_dept_filter).exclude(date_reponse__isnull=True)
        stat['delai_preavis_moyen'] = liste_preavis.aggregate(
            delai=Avg(F('date_reponse') - F('date_demande')))['delai'].days if liste_preavis else ''
        stat['delai_preavis_ecart_type'] = round(liste_preavis.aggregate(
            variance=Variance(Extract(F('date_reponse') - F('date_demande'), 'day')))['variance']**0.5) if liste_preavis else ''
        stat['delai_preavis_moyen_par_annee'] = self.moyenne_par_annee(liste=liste_preavis, critere="date_demande")
        stat['delai_preavis_ecart_type_par_annee'] = self.ecart_type_par_annee(liste=liste_preavis,critere="date_demande")

        liste_services_preavis = dict(InstructionPreAvis.LIST_SERVICES)
        stat['delai_preavis_par_service'] = list()
        for service in liste_services_preavis:
            delta = liste_preavis.filter(service_concerne=service).aggregate(
                delai=Avg(F('date_reponse') - F('date_demande')))['delai']
            delai_preavis_moyen = delta.days if delta else ''
            variance = liste_preavis.filter(service_concerne=service).aggregate(
                variance=Variance(Extract(F('date_reponse') - F('date_demande'), 'day')))['variance']
            delai_preavis_ecart_type = round(variance ** 0.5) if variance else ''
            delai_preavis_moyen_par_annee = self.moyenne_par_annee(
                liste=liste_preavis.filter(service_concerne=service), critere="date_demande")
            delai_preavis_ecart_type_par_annee = self.ecart_type_par_annee(
                liste=liste_preavis.filter(service_concerne=service), critere="date_demande")
            data = {'nom': service.upper() if service != 'services' else 'Autres services',
                    'delai_preavis_moyen': delai_preavis_moyen,
                    'delai_preavis_ecart_type': delai_preavis_ecart_type,
                    'delai_preavis_moyen_par_annee': delai_preavis_moyen_par_annee,
                    'delai_preavis_ecart_type_par_annee': delai_preavis_ecart_type_par_annee}
            stat['delai_preavis_par_service'].append(data)

        # Compte des instructions en cours selon leur statut
        instruction_dept_filter = Q(
            manif__ville_depart__arrondissement__departement=context['dept']) if dept else Q()
        manif_pas_passe = Q(manif__date_debut__gte=timezone.now())
        context['instruction_demande'] = Instruction.objects.filter(instruction_dept_filter, etat='demandée').filter(manif_pas_passe).count()
        context['instruction_commence'] = Instruction.objects.filter(instruction_dept_filter, etat='distribuée').filter(manif_pas_passe).count()
        context['instruction_attente_decision'] = Instruction.objects.annotate(
            nb_avis=Count('avis')).annotate(nb_avis_rendu=Count('avis', filter=Q(avis__etat='rendu'))).filter(
            instruction_dept_filter, etat='distribuée').filter(manif_pas_passe).filter(nb_avis=F('nb_avis_rendu')).count()
        context['instruction_en_attente_avis'] = Instruction.objects.annotate(
            nb_avis=Count('avis'), nb_preavis=Count('avis__preavis')).annotate(
            nb_avis_rendu=Count('avis', filter=Q(avis__etat='rendu')),
            nb_preavis_rendu=Count('avis__preavis', filter=Q(avis__preavis__etat='rendu'))).filter(
            instruction_dept_filter, etat='distribuée').filter(manif_pas_passe).filter(nb_preavis=F('nb_preavis_rendu'),
                                                               nb_avis__gt=F('nb_avis_rendu')).count()
        context['instruction_en_attente_preavis'] = Instruction.objects.annotate(
            nb_preavis=Count('avis__preavis')).annotate(
            nb_preavis_rendu=Count('avis__preavis', filter=Q(avis__preavis__etat='rendu'))).filter(
            instruction_dept_filter, etat='distribuée').filter(manif_pas_passe).filter(nb_preavis__gt=F('nb_preavis_rendu')).count()

        context['instruction_en_cours'] = Instruction.objects.exclude(etat__in=['interdite', 'autorisée', 'annulée']).filter(manif_pas_passe).filter(instruction_dept_filter).count()
        context['instruction_terminée'] = Instruction.objects.exclude(etat__in=['distribuée', 'demandée']).filter(manif_pas_passe).filter(instruction_dept_filter).count()

        context['stat'] = stat
        return context
