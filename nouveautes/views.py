# coding: utf-8
from django.http.response import Http404
from django.shortcuts import render
from django.template.context import RequestContext
from django.utils import timezone

from nouveautes.models.lecture import LectureNouveaute
from nouveautes.models.nouveaute import Nouveaute


def view_nouveautes(request, slug):
    """ Affichage d'une nouveauté """
    try:
        nouveaute = Nouveaute.objects.get(slug=slug)
        LectureNouveaute.objects.update_lecture(request.user, nouveaute.creation)
        return render(request, "nouveautes/nouveaute_detail.html", {'nouveaute': nouveaute})
    except Nouveaute.DoesNotExist:
        raise Http404()


def list_nouveautes(request):
    """ Affichage de la liste des nouveautés """
    if not request.user.is_staff:
        if not request.user.is_anonymous:
            nouveautes = Nouveaute.objects.for_user(request.user)
            #LectureNouveaute.objects.update_lecture(request.user, timezone.now())
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Vous devez être connecté pour accèder à cette page"}, status=403)
    else:
        nouveautes = Nouveaute.objects.all().order_by('-id')
        #LectureNouveaute.objects.update_lecture(request.user, timezone.now())
    return render(request, "nouveautes/nouveaute_list.html", {'object_list': nouveautes})
