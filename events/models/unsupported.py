# coding: utf-8
from django.db import models

from events.models.manifestation import Manifestation


class UnsupportedManifestationManager(models.Manager):
    """ Queryset des manifestations non prises en charge """

    def get_queryset(self):
        """ Qeryset par défaut """
        return super().get_queryset().filter(models.Q(instance__departement__isnull=True) | models.Q(instance__isnull=True))


class UnsupportedManifestation(Manifestation):
    """
    Manifestation non prise en charge

    Les manifestations non prises en charge sont les manifestations
    qui ont lieu dans des départements qui ne sont pas abonnés
    à la plateforme

    Utilise exactement la même table que Manifestation,
    mais utilise un queryset différent par défaut, et possède
    un nom verbeux (verbose_name) différent
    """

    # Manager
    objects = UnsupportedManifestationManager()

    # Méta
    class Meta:
        verbose_name = "Manifestation non prise en charge"
        verbose_name_plural = "Manifestations non prises en charge"
        proxy = True
        app_label = 'events'
