import time

from django.test import tag
from django.utils import timezone
from django.contrib.auth.hashers import make_password as make
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException

from allauth.account.models import EmailAddress

from instructions.tests.test_base_selenium import SeleniumCommunClass
from instructions.factories import InstructionFactory
from core.factories import UserFactory
from administration.factories import DDSPAgentFactory, SDISAgentFactory
from messagerie.models import Enveloppe
from administrative_division.factories import ArrondissementFactory
from administration.factories import InstructeurFactory

class SeleniumMessagerieTests(SeleniumCommunClass):
    """
    Test des fonctions de base de la messagerie
    """
    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ Test Messagerie (Sel) =============')
        SeleniumCommunClass.init_setup(cls)

        cls.instructeur.is_superuser = True
        cls.instructeur.save()
        cls.organisateur.last_login = timezone.now() - timezone.timedelta(days=370)
        cls.organisateur.save()

        arrondissement2 = ArrondissementFactory.create(name='Roanne', code='99', departement=cls.dep)
        cls.prefecture2 = arrondissement2.prefecture
        cls.instructeur2 = UserFactory.create(username='instructeur2', password=make(123), default_instance=cls.dep.instance)
        EmailAddress.objects.create(user=cls.instructeur2, email='instructeur2@example.com', primary=True, verified=True)
        InstructeurFactory.create(user=cls.instructeur2, prefecture=cls.prefecture2)

        cls.agent_ddsp2 = UserFactory.create(username='agent_ddsp2', password=make(123), default_instance=cls.dep.instance)
        EmailAddress.objects.create(user=cls.agent_ddsp2, email='agent_ddsp2@example.com', primary=True, verified=True)
        DDSPAgentFactory.create(user=cls.agent_ddsp2, ddsp=cls.dep.ddsp)
        cls.agent_sdis2 = UserFactory.create(username='agent_sdis2', password=make(123), default_instance=cls.dep.instance)
        EmailAddress.objects.create(user=cls.agent_sdis2, email='agent_sdis2@example.com', primary=True, verified=True)
        SDISAgentFactory.create(user=cls.agent_sdis2, sdis=cls.dep.sdis)

        super().setUpClass()

    @tag('selenium')
    def test_messagerie(self):
        """
        Test de la messagerie
        """
        qs_env = Enveloppe.objects.order_by('pk')

        print('*** messagerie instructeur liste organisateur ***')
        self.connexion('instructeur')
        # Affichage de la messagerie
        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//a[@title="Messagerie"]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY)
        # Envoi d'un msg
        self.selenium.find_element_by_id("show_write").click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 200)")
        time.sleep(self.DELAY)
        self.chosen_select('type_desti_chosen', 'Organisateurs actifs depuis un an')
        time.sleep(self.DELAY)
        chosen_select = self.selenium.find_element_by_id('carnet_chosen')
        chosen_select.click()
        time.sleep(self.DELAY)
        menu = chosen_select.find_element_by_class_name('chosen-drop')
        self.assertEqual(len(menu.find_elements_by_tag_name('li')), 0, msg="la liste doit être vide")
        time.sleep(self.DELAY)
        self.chosen_select('type_desti_chosen', 'Organisateurs inactifs depuis un an')
        time.sleep(self.DELAY)
        chosen_select = self.selenium.find_element_by_id('carnet_chosen')
        chosen_select.click()
        time.sleep(self.DELAY)
        menu = chosen_select.find_element_by_class_name('chosen-drop')
        self.assertEqual(len(menu.find_elements_by_tag_name('li')), 1, msg="la liste doit contenir un nom")
        self.deconnexion()

        print('*** test de la messagerie organisateur avant envoi manif ***')
        self.connexion('organisateur')
        # Affichage Tdb
        bloc = self.selenium.find_element_by_xpath("//*[starts-with(@id, 'btn')][contains(@id, '_etat_attente')]")
        bloc.click()
        self.assertEqual('1', bloc.find_element_by_class_name('test_nb_atraiter').text)
        declar = self.selenium.find_element_by_tag_name('tbody')
        self.assertIn('Manifestation_Test', declar.text)
        try:
            declar.find_element_by_class_name('table_afaire')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas de nouveau dossier")
        # Appel de la vue de détail et test présence manifestation
        self.selenium.execute_script("window.scroll(0, 700)")
        url = self.selenium.current_url
        self.selenium.find_element_by_xpath('//td[text()="Manifestation_Test"]').click()
        wait = WebDriverWait(self.selenium, 15)
        wait.until(lambda driver: self.selenium.current_url != url)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        time.sleep(self.DELAY)
        # Appel messagerie
        self.selenium.find_element_by_partial_link_text('Messagerie').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id("show_write").click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 500)")
        chosen_select = self.selenium.find_element_by_id('carnet_chosen')
        chosen_select.click()
        time.sleep(self.DELAY)
        menu = chosen_select.find_element_by_class_name('chosen-drop')
        self.assertEqual(len(menu.find_elements_by_tag_name('li')), 1, msg="la liste contient 1 nom")
        self.deconnexion()

        print('*** test de la messagerie organisateur après envoi manif ***')
        self.instruction = InstructionFactory.create(manif=self.manifestation)
        self.connexion('organisateur')
        # Affichage Tdb
        bloc = self.selenium.find_element_by_xpath("//*[starts-with(@id, 'btn')][contains(@id, '_etat_demande')]")
        bloc.click()
        self.assertEqual('1', bloc.find_element_by_class_name('test_nb_encours').text)
        declar = self.selenium.find_element_by_tag_name('tbody')
        self.assertIn('Manifestation_Test', declar.text)
        try:
            declar.find_element_by_class_name('table_encours')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas de nouveau dossier")
        # Appel de la vue de détail et test présence manifestation
        self.selenium.execute_script("window.scroll(0, 700)")
        url = self.selenium.current_url
        self.selenium.find_element_by_xpath('//td[text()="Manifestation_Test"]').click()
        wait = WebDriverWait(self.selenium, 15)
        wait.until(lambda driver: self.selenium.current_url != url)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        time.sleep(self.DELAY)
        # Appel messagerie
        self.selenium.find_element_by_partial_link_text('Messagerie').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY)
        # Envoi d'un msg à l'instructeur
        self.selenium.find_element_by_id("show_write").click()
        time.sleep(self.DELAY * 2)
        self.selenium.execute_script("window.scroll(0, 500)")
        self.chosen_select('carnet_chosen', 'Sous-préfecture de Montbrison - Bob ROBERT')
        time.sleep(self.DELAY)
        self.chosen_select('doc_objet_chosen', 'Dossier')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id("msg_objet").send_keys('Test orga vers instr')
        # Ici on change pour ecrire dans l'iframe
        frame = self.selenium.find_element_by_class_name('cke_wysiwyg_frame')
        self.selenium.switch_to.frame(frame)
        body = self.selenium.find_element_by_tag_name('body')
        body.send_keys('tester organisateur vers instructeur')
        self.selenium.switch_to.default_content()
        self.selenium.find_element_by_id("envoi").click()
        time.sleep(self.DELAY)

        # Vérifier presence dans la liste des mails
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY * 2)
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY * 2)
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('show_mail').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY)
        # Quatrième message créé
        env_1 = qs_env.last()
        env_1_msg = env_1.corps
        print("id enveloppe : " + str(env_1.pk))
        self.selenium.find_element_by_id(env_1.pk).click()
        time.sleep(self.DELAY)
        texte = self.selenium.find_element_by_id("corps_msg_" + str(env_1_msg.pk))
        self.assertIn('tester organisateur vers instructeur', texte.text)
        self.deconnexion()

        print('*** test de la messagerie instructeur ***')
        self.connexion('instructeur')
        # Affichage de la messagerie
        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//a[@title="Messagerie"]').click()
        time.sleep(self.DELAY)
        # Vérifier presence dans la liste des mails
        mess = self.selenium.find_element_by_id(env_1.pk)
        self.assertIn('bold', mess.get_attribute("style"))
        mess.click()
        texte = self.selenium.find_element_by_id("corps_msg_" + str(env_1_msg.pk))
        self.assertIn('tester organisateur vers instructeur', texte.text)
        mess.click()
        # Vérifier passage en "lu"
        self.assertNotIn('bold', mess.get_attribute("style"))

        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY)

        # Envoi d'un msg
        self.selenium.find_element_by_id("show_write").click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 200)")
        time.sleep(self.DELAY)
        self.chosen_select('type_desti_chosen', 'Organisateurs actifs depuis un an')
        time.sleep(self.DELAY)
        self.chosen_select('carnet_chosen', 'Grs0 - Bob Robert')
        time.sleep(self.DELAY)
        self.chosen_select('doc_objet_chosen', 'Dossier')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id("msg_objet").send_keys('test instr vers orga')
        # Ici on change pour ecrire dans l'iframe
        frame = self.selenium.find_element_by_class_name('cke_wysiwyg_frame')
        self.selenium.switch_to.frame(frame)
        body = self.selenium.find_element_by_tag_name('body')
        body.send_keys('tester instructeur vers organisateur')
        self.selenium.switch_to.default_content()
        self.selenium.find_element_by_id("envoi").click()
        time.sleep(self.DELAY)

        # Vérifier presence dans la liste des mails
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY * 2)
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('show_mail').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY)
        env_2 = qs_env.last()
        env_2_msg = env_2.corps
        print("id enveloppe : " + str(env_2.pk))
        self.assertNotIn('bold', self.selenium.find_element_by_id(env_2.pk).get_attribute("style"))
        self.selenium.find_element_by_id(env_2.pk).click()
        time.sleep(self.DELAY)
        texte = self.selenium.find_element_by_id("corps_msg_" + str(env_2_msg.pk))
        self.assertIn('tester instructeur vers organisateur', texte.text)
        time.sleep(self.DELAY)

        # Vérifier les filtres
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY)
        test = self.selenium.find_element_by_id('filtre_mail').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_class_name('avis').click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_id('filtre_save').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('nom_filtre').send_keys('aze')
        self.selenium.find_element_by_id("form_filtre_submit").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_class_name('filtre_perso_list').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_class_name("supprimer").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id("supp_conf_1").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id("supp_1").click()
        time.sleep(self.DELAY)
        test = self.selenium.find_element_by_id('filtre_mail').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY * 2)
        # self.selenium.find_element_by_xpath('//a[@title="Tableau de bord"]').click()
        self.selenium.find_element_by_partial_link_text('Tableau de bord').click()

        print('*** Instructeur : distribution des avis ***')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes d\'avis', self.selenium.page_source)
        # Distribuer les demandes d'avis de l'événement
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes d\'avis').click()
        time.sleep(self.DELAY * 2)
        self.assertIn('Choix des avis', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis DDSP requis')]/..").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('submit-id-save').click()
        time.sleep(self.DELAY * 8)
        env_3 = qs_env.filter(objet__icontains='Demande').first()
        env_3_msg = env_3.corps
        env_3_org = qs_env.filter(objet__icontains='Démarrage').first()
        print("id enveloppe service : " + str(env_3.pk))
        print("id enveloppe orga : " + str(env_3_org.pk))
        self.deconnexion()

        print('**** Message avis ddsp1 ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'nouveau')
        # Affichage de la messagerie
        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//a[@title="Messagerie"]').click()
        time.sleep(self.DELAY)
        # Vérifier presence dans la liste des mails
        mess = self.selenium.find_element_by_id(env_3.pk)
        self.assertIn('bold', mess.get_attribute("style"))
        mess.click()
        texte = self.selenium.find_element_by_id("corps_msg_" + str(env_3_msg.pk))
        self.assertIn('Demande d\'avis à traiter', texte.text)
        mess.click()
        self.deconnexion()

        print('**** Message avis ddsp2 ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ddsp2')
        self.presence_avis('agent_ddsp2', 'nouveau')
        # Affichage de la messagerie
        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//a[@title="Messagerie"]').click()
        time.sleep(self.DELAY)
        # Vérifier presence dans la liste des mails
        mess = self.selenium.find_element_by_id(env_3.pk + 1)
        self.assertIn('bold', mess.get_attribute("style"))
        mess.click()
        texte = self.selenium.find_element_by_id("corps_msg_" + str(env_3_msg.pk))
        self.assertIn('Demande d\'avis à traiter', texte.text)
        mess.click()
        self.deconnexion()

        print('*** Instructeur : envoi à un service ***')
        # Envoi message à un service
        self.connexion('instructeur')
        # Affichage de la messagerie
        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//a[@title="Messagerie"]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY)
        # Envoi d'un msg
        self.selenium.find_element_by_id("show_write").click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 200)")
        time.sleep(self.DELAY)
        self.chosen_select('type_desti_chosen', 'Services consultés (hors Mairies)')
        time.sleep(self.DELAY)
        self.chosen_select('service_chosen', 'SDIS 42')
        time.sleep(self.DELAY)
        self.chosen_select('doc_objet_chosen', 'Dossier')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id("msg_objet").send_keys('message sdis')
        # Ici on change pour ecrire dans l'iframe
        frame = self.selenium.find_element_by_class_name('cke_wysiwyg_frame')
        self.selenium.switch_to.frame(frame)
        body = self.selenium.find_element_by_tag_name('body')
        body.send_keys('tester instruc vers service')
        self.selenium.switch_to.default_content()
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_id("envoi").click()
        time.sleep(self.DELAY)
        # Vérifier presence dans la liste des mails
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY * 2)
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('show_mail').click()
        env_4 = qs_env.last()
        env_4_msg = env_4.corps
        print("id enveloppe : " + str(env_4.pk))
        self.deconnexion()

        print('**** Message sdis1 ****')
        # Vérification de la présence du message
        self.connexion('agent_sdis')
        # Affichage de la messagerie
        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//a[@title="Messagerie"]').click()
        time.sleep(self.DELAY)
        # Vérifier presence dans la liste des mails
        mess = self.selenium.find_element_by_id(env_4.pk - 1)
        self.assertIn('bold', mess.get_attribute("style"))
        mess.click()
        texte = self.selenium.find_element_by_id("corps_msg_" + str(env_4_msg.pk))
        self.assertIn('tester instruc vers service', texte.text)
        mess.click()
        self.deconnexion()

        print('**** Message sdis2 ****')
        # Vérification de la présence du message
        self.connexion('agent_sdis2')
        # Affichage de la messagerie
        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//a[@title="Messagerie"]').click()
        time.sleep(self.DELAY)
        # Vérifier presence dans la liste des mails
        mess = self.selenium.find_element_by_id(env_4.pk)
        self.assertIn('bold', mess.get_attribute("style"))
        mess.click()
        texte = self.selenium.find_element_by_id("corps_msg_" + str(env_4_msg.pk))
        self.assertIn('tester instruc vers service', texte.text)
        mess.click()
        self.deconnexion()

        print('*** messagerie organisateur ***')
        # Vérification de la présence du message
        self.connexion('organisateur')
        # Affichage de la messagerie
        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//a[@title="Messagerie"]').click()
        time.sleep(self.DELAY)
        # Vérifier presence dans la liste des mails
        mess = self.selenium.find_element_by_id(env_3_org.pk)
        self.assertIn('bold', mess.get_attribute("style"))
        mess.click()
        texte = self.selenium.find_element_by_id("corps_msg_" + str(env_3_org.corps.pk))
        self.assertIn('traitement de votre dossier', texte.text)
        mess.click()
        mess = self.selenium.find_element_by_id(env_2.pk)
        self.assertIn('bold', mess.get_attribute("style"))
        mess.click()
        texte = self.selenium.find_element_by_id("corps_msg_" + str(env_2_msg.pk))
        self.assertIn('tester instructeur vers organisateur', texte.text)
        mess.click()
        self.deconnexion()
