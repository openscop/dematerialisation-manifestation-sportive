# coding: utf-8
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from contacts.admin import ContactInline, AddressInline
from core.util.admin import RelationOnlyFieldListFilter
from ..models import SiteN2K, OperateurSiteN2K
from administrative_division.models import Departement


@admin.register(OperateurSiteN2K)
class OperateurSiteN2KAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    list_display = ['pk', 'name', 'email', 'site', 'get_departement']
    list_filter = ['site', ('departement', RelationOnlyFieldListFilter)]
    search_fields = ['name__unaccent', 'site__name__unaccent']
    inlines = [AddressInline, ContactInline]
    list_per_page = 25

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(site__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['site'].queryset = SiteN2K.objects.filter(departement=request.user.get_departement())
        return form


@admin.register(SiteN2K)
class N2KAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration Natura 2000 """

    list_display = ['pk', 'name', 'site_type', 'index', 'get_departement']
    list_filter = [('departement', RelationOnlyFieldListFilter)]
    search_fields = ['name__unaccent', 'index']
    list_per_page = 25

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['departement'].queryset = Departement.objects.filter(id=request.user.get_departement().id)
            form.base_fields['departement'].initial = request.user.get_departement()
        return form
