from django.conf import settings
from annoying.functions import get_object_or_None

from rest_framework import permissions

from evenements.models import Manif
from instructions.models import Avis


class IsInGroupApi(permissions.BasePermission):
    message = 'Vous n\'avez pas accès à ces informations. Contactez Openscop au 04 77 36 91 35.'

    def has_permission(self, request, view):

        grouplist = request.user.groups.values_list('name', flat=True)
        if 'API' in grouplist:
            return True
        return False


class IsAgent(permissions.BasePermission):
    message = 'Vous n\'avez pas accès à ces informations. Contactez Openscop au 04 77 36 91 35.'

    def has_permission(self, request, view):

        if hasattr(request.user, 'agent'):
            return True
        return False


class IsAgentLocal(permissions.BasePermission):
    message = 'Vous n\'avez pas accès à ces informations. Contactez Openscop au 04 77 36 91 35.'

    def has_permission(self, request, view):

        if hasattr(request.user, 'agentlocal'):
            return True
        return False


class IsInstructeur(permissions.BasePermission):
    message = 'Vous n\'avez pas accès à ces informations. Contactez Openscop au 04 77 36 91 35.'

    def has_permission(self, request, view):

        if hasattr(request.user, 'instructeur'):
            return True
        return False


class IsInvolvedInInstruction(permissions.BasePermission):
    message = 'Vous n\'êtes pas concerné par ce dossier'

    def has_permission(self, request, view):

        if view.action == 'list':
            # Laisser passer pour avoir le message correct de la vue
            return True
        manif = get_object_or_None(Manif, pk=view.kwargs.get('pk'))
        if not manif:
            # Laisser passer pour avoir le message correct avec get_object
            return True
        if not hasattr(manif, 'instruction'):
            return False
        if hasattr(request.user, 'instructeur'):
            dep = request.user.instructeur.prefecture.get_departement()
            if manif.get_instance().get_departement() == dep:
                return True
        if hasattr(request.user, 'agent'):
            avis_list = Avis.objects.filter(instruction=manif.instruction)
            for avis in avis_list:
                if avis.destination_object == request.user.get_service():
                    return True
        return False
