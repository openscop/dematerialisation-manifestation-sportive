from django import forms
from django.forms.models import ChoiceField

from core.models import Instance

class AvisInlineForm(forms.ModelForm):
    choix = (('demandé', 'demandé'),
             ('rendu', 'rendu'),
             ('distribué', 'distribué'),
             ('formaté', 'formaté'),
             ('transmis', 'transmis'))
    etat = ChoiceField(choices=choix)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            if self.instance.service_concerne == 'cg' or \
                    (self.instance.service_concerne == 'ggd' and
                     self.instance.instruction.get_instance().get_workflow_ggd() != Instance.WF_GGD_SUBEDSR):
                self.fields['etat'].choices = self.choix
            elif self.instance.service_concerne in ['sdis', 'ddsp']:
                self.fields['etat'].choices = self.choix[:3]
            else:
                self.fields['etat'].choices = self.choix[:2]

    class Meta:
        exclude = []


class PreavisInlineForm(forms.ModelForm):
    choix = (('demandé', 'demandé'),
             ('rendu', 'rendu'),
             ('notifié', 'notifié'))
    etat = ChoiceField(choices=choix)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            if self.instance.service_concerne != 'cgd':
                self.fields['etat'].choices = self.choix[:2]

    class Meta:
        exclude = []
