Style du code
=====
- Nombre maximum de colonnes : 160
- Indentation de 4 espaces (ASCII 32)
- Commentaires d'une ligne au format "# text"
- Commentaires de fin de ligne précédés de deux espaces
- Docstrings au format triple guillemets anglais (")
- Éléments de docstrings au format ":param name: desc"

Commits
=======
Les commentaires des commits doivent suivre les règles suivantes :

Typographie et orthographe correcte !

__Le sujet__

Le sujet du commit doit être composé de : 
- le nom de l'application concernée, 
- puis éventuellement des entités concernées
- puis un résumé du quoi/pourquoi qui commence par un verbe à l'infinitif

(Le titre du commit, devrait si possible faire suite à cette introduction : « Appliquer ce commit va... »)

Exemple : 
- *Portail / Statistiques* > Ajouter le nombre d'évaluations d'incidence
- *Instruction / Services complexes* > Permettre de ne pas consulter les services internes
- *Manifestations / Interface d'admin* > Ajouter un filtre par date à la liste des manifestations

Ne pas terminer la ligne du sujet par un point.

__Le corps__

Séparer le sujet du corps du commit par une ligne vide.

Le corps du commit peut être composé de plusieurs lignes qui détaille les sujets traités par le commit.

Chaque ligne commence par un marquage rapide sur la nature du travail réalisé :
- __[m]__ opérations diverses
- __[t]__ tests (correction ou ajout de tests)
- __[f]__ fix (correction d'un bug)
- __[+]__ ajout de code ou fonctionnalité ou amélioration d'une fonction
- __[-]__ suppression de code ou fonctionnalité
- __[d]__ modification de la documentation

Utiliser le corps du commit pour détailler quoi/pourquoi et le contexte (notamment en cas de bug) plutôt que comment.