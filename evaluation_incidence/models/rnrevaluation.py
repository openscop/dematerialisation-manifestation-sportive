# coding: utf-8
from django.db import models
from django.utils.encoding import smart_text

from evaluation_incidence.models import EvaluationManif


class EvaluationRnr(EvaluationManif):
    """ Évalutation réserve naturelle régionale """

    LISTE_CHAMPS_REQUIS = ['cout', 'Longueur_totale_parcours', 'duree', 'participants', 'spectateurs',
                           'date_contact_administrateur', 'sensibilisation', 'canalisation_public', 'impact_estime']

    # Constantes
    DURATION_CHOICES = (
        ('1', "demande d'autorisation pour 1 année"),
        ('3', "demande d'autorisation pour 3 années avec engagement de non modification des parcours et des conditions d'organisation"),
    )
    # Frequency
    duree = models.CharField("durée de la demande d'autorisation", max_length=1, choices=DURATION_CHOICES, blank=True)
    # Attendance
    participants = models.PositiveIntegerField("participants concernés par le passage dans la RNR", blank=True, null=True)
    spectateurs = models.PositiveIntegerField("spectateurs concernés par le passage dans la RNR", blank=True, null=True)
    # Provisions taken
    date_contact_administrateur = models.DateField("date de contact avec les gestionnaires de la réserve", blank=True, null=True)
    sensibilisation = models.CharField("dispositif de sensibilisation à l'environnement et à la réserve envisagé", max_length=255, blank=True)
    canalisation_public = models.CharField("dispositif de canalisation du public", max_length=255, blank=True)
    presence_administrateur = models.BooleanField(
        "l’organisateur souhaite la présence du gestionnaire de la RNR lors de la "
        "manifestation",
        default=False
    )
    # Éuipements et installations liés
    eclairage_nocturne = models.BooleanField("éclairage nocturne", default=False)
    description_eclairage_nocturne = models.TextField("description", blank=True)
    balisage = models.BooleanField(
        "balisage des sentiers", default=False,
        help_text="Seuls les balisages temporaires retirés sous 24h sont autorisés dans le "
                  "cadre de manifestations sportives. Par conséquent ne seront pas autorisés "
                  "l'usage de peinture, confettis ou tout autre type de balisage laissant une "
                  "empreinte sur les milieux",
    )
    description_balisage = models.TextField('description', blank=True)
    autre_amenagement = models.BooleanField("autres types d'aménagements", default=False)
    description_autre_amenagement = models.TextField('description', blank=True)

    # Potential implications
    emissions_sonores = models.BooleanField(
        "les émissions sonores sont susceptibles de déranger les espèces présentes "
        "sur le site (puissance des émissions et proximité du site)",
        default=False,
        help_text="Le bruit peut entraîner le dérangement de certaines espèces animales, "
                  "notamment en période de nidification ou d’hivernage pour les oiseaux.",
    )
    gestion_dechets = models.BooleanField(
        "vous avez prévu des dispositifs particuliers pour la gestion des déchets "
        "pendant et après la manifestation [À décrire dans le champ réservé]",
        default=False,
        help_text="Les déchets polluent le milieu naturel et représentent un danger pour les "
                  "espèces animales (risque d’ingestion notamment).",
    )
    description_gestion_dechets = models.TextField('description', blank=True)
    prise_conscience = models.BooleanField(
        "vous avez prévu la mise en place d'actions de communication ou de "
        "sensibilisation concernant les enjeux environnementaux",
        default=False,
        help_text="La sensibilisation et la communication permettent de réduire les impacts sur "
                  "l’environnement. Contacter l’animateur du site pour la réalisation ou la "
                  "mise à disposition de documents de communication.",
    )
    reduction_impact = models.BooleanField(
        "vous avez pris des mesures pour réduire l'impact de la manifestation sur le "
        "milieu naturel (contacts structures, choix du lieu en fonction des enjeux, "
        "délimitation de zones interdites au public...) [À décrire dans le champ "
        "réservé]",
        default=False,
        help_text="réduction des impacts",
    )
    description_reduction_impact = models.TextField('description', blank=True)
    engins_aeriens = models.BooleanField("il y aura présence d'engins aériens [À décrire dans le champ réservé]", default=False)
    description_engins = models.TextField("dispositif pour la gestion des engins aériens", blank=True)
    pietinement = models.BooleanField("la présence des participants et du public entraînera un piétinement des sols "
                                    "[À décrire dans le champ réservé]", default=False)
    description_pietinement = models.TextField("dispositif pour la gestion du piétinement", blank=True)
    gestion_balisage = models.BooleanField("vous avez prévu un balisage, une signalisation spécifique à la manifestation "
                                 "[À décrire dans le champ réservé]", default=False)
    description_gestion_balisage = models.TextField("dispositif pour la gestion du balisage et de la signalisation", blank=True)
    # Conclusion
    respect_reglement = models.BooleanField(
        "le projet, la manifestation respecte bien la réglementation propre à la réserve",
        default=False,
        help_text="voir encadré ci-dessus"
    )
    conclusion_rnr = models.BooleanField("Je déclare conclure à l'absence d'incidence significative sur "
                                         "le ou les sites RNR concernés par la manifestation", default=False)

    # Overrides
    def __str__(self):
        return smart_text(self.manif.__str__())

    def formulaire_rnr_complet(self):
        """ Indiquer si le formulaire est complet """
        for champ in self.get_liste_champs_requis():
            if getattr(self, champ) == None:
                return False
            elif isinstance(getattr(self, champ), str) and getattr(self, champ) == '':
                return  False
        return True

    def get_liste_champs_requis(self):
        """ Retourne la liste des champs obligatoires pour la finalisation de l'incidence """
        return self.LISTE_CHAMPS_REQUIS

    # Méta
    class Meta(EvaluationManif.Meta):
        verbose_name = "évaluation RNR"
        verbose_name_plural = "évaluations RNR"
        default_related_name = "rnrevaluation"
        app_label = "evaluation_incidence"
