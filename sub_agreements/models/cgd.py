# coding: utf-8
import datetime

from django.urls import reverse
from django.db import models
from django_fsm import transition

from notifications.models.action import Action
from notifications.models.notification import Notification
from sub_agreements.models.abstract import PreAvis, PreAvisQuerySet


class PreAvisCGD(PreAvis):
    """ Préavis CGD """

    # Champs
    preavis_ptr = models.OneToOneField("sub_agreements.preavis", parent_link=True, related_name='preaviscgd', on_delete=models.CASCADE)
    cgd = models.ForeignKey("administration.cgd", verbose_name="CGD", on_delete=models.CASCADE)
    concerned_brigades = models.ManyToManyField("administration.brigade", verbose_name="Brigades concernées")
    objects = PreAvisQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        manifestation = self.avis.get_manifestation()
        cgd = self.cgd
        return ' - '.join([str(manifestation), str(cgd)])

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'objet """
        url = 'sub_agreements:cgd_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_edsr(self):
        """ Renvoyer l'EDSR pour le préavis """
        return self.cgd.commune.arrondissement.departement.edsr

    def get_agents(self):
        """ Renvoyer les agents concernés par le préavis """
        return self.cgd.ggdagentslocaux.all()

    # Action
    @transition(field='state', source=['created', 'notified'], target='notified')
    def notify_brigades(self):
        """ Notifier tous les agents des brigades concernées par le préavis """
        recipients = [agent for brigade in self.concerned_brigades.all() for agent in brigade.brigadeagents.all()]
        Notification.objects.notify_and_mail(recipients, "Prenez connaissance des informations", self.cgd, self.avis.get_manifestation())
        Action.objects.log(self.get_agents(), "Brigades informées", self.avis.get_manifestation())

    @transition(field='state', source='notified', target='acknowledged')
    def acknowledge(self):
        """ Rendre le préavis """
        self.reply_date = datetime.date.today()
        self.save()
        self.notify_ack(agents=self.get_edsr().edsragents.all(), content_object=self.cgd)
        self.log_ack(agents=self.get_agents())

    # Meta
    class Meta:
        verbose_name = "pré-avis CGD"
        verbose_name_plural = "pré-avis CGD"
        default_related_name = "preaviscgd_set"
        app_label = "sub_agreements"
