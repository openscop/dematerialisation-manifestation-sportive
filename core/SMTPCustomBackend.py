from django.core.mail.backends.smtp import EmailBackend
from core.models import Instance


class SMTPCustomBackend(EmailBackend):
    """
    Surchage du backend smpt de django pour pouvoir y mettre les paramètres des configs d'instance
    """

    def send_messages(self, email_messages):
        # Si l'adresse correspond à une instance et si des parametres sont renseignés on change les paramètres
        # chargement config par default
        from configuration import settings
        self.host = settings.EMAIL_HOST
        self.port = settings.EMAIL_PORT
        self.username = settings.EMAIL_HOST_USER
        self.password = settings.EMAIL_HOST_PASSWORD

        # chargement d'une eventuelle config départementale
        adresse_envoi = email_messages[0].from_email
        instance = Instance.objects.filter(sender_email=adresse_envoi)
        if instance:
            instance = instance.first()
            if instance.email_settings:
                settings = instance.email_settings.split(';')
                self.host = settings[0]
                self.port = settings[1]
                self.username = settings[2]
                self.password = settings[3]

        self.connection = None
        self.close()
        return super(SMTPCustomBackend, self).send_messages(email_messages)
