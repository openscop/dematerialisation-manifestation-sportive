import logging

from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend

from api.permissions import IsInstructeur, IsInGroupApi
from api.serializer.instruction import InstructionSerializer, InstructionDetailSerializer
from api.views.manif import ManifFilter
from instructions.models import Instruction

api_logger = logging.getLogger('api')


class InstructionFilter(ManifFilter):

    class Meta:
        model = Instruction
        fields = ('arrondissement', 'commune', 'activite', 'etat',
                  'date_debut', 'date_fin', 'id')


class InstructionViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Liste des instructions de dossiers\n
    Réservé aux instructeurs.
    Ne retourne que les dossiers du département de l'instructeur authentifié.
    Recherche textuelle possible sur les champs 'nom' et 'description'.
    """
    permission_classes = (IsAuthenticated, IsInstructeur, IsInGroupApi)
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('manif__nom', 'manif__description',)
    ordering_fields = ('manif__date_debut', 'manif__activite', 'manif__ville_depart')
    queryset = Instruction.objects.all()
    filter_class = InstructionFilter
    serializer_class = InstructionSerializer
    detail_serializer_class = InstructionDetailSerializer

    """
    Utilisation de la fonction finalize_response pour bénéficier des variables request et response
    dans lesquelles se trouvent les infos à logger
    """
    def finalize_response(self, request, response, *args, **kwargs):
        if 'count' in response.data:
            count = response.data['count']
        else:
            count = 'none'
        api_logger.info("Utilisateur %s | %s de %s | %s | réponse %s, %s | items : %s" %
                        (request.user, request.method, self.request.get_host(), self.request.get_full_path(),
                         response.status_code, response.status_text, count
                         ))
        return super().finalize_response(request, response, *args, **kwargs)

    def get_serializer_class(self):
        if self.action == 'retrieve':
            if hasattr(self, 'detail_serializer_class'):
                return self.detail_serializer_class

        return super().get_serializer_class()

    def get_queryset(self):
        # Les instructueurs ont accès aux dossiers de tout le département
        dep = self.request.user.get_departement()
        return Instruction.objects.filter(manif__ville_depart__arrondissement__departement=dep)

    def retrieve(self, request, *args, **kwargs):
        """
        Détail d'une instruction de dossier.\n
        Réservé aux instructeurs.
        Ne retourne que les dossiers du département de l'instructeur authentifié.
        """
        return super().retrieve(request, *args, **kwargs)
