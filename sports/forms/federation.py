# coding: utf-8
from django import forms

from administrative_division.models import Departement
from sports.models import Federation


class FederationForm(forms.ModelForm):
    """ Formulaire fédération """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.fields and 'departement' in self.fields:
            self.fields["departement"].queryset = Departement.objects.configured()

    # Meta
    class Meta:
        model = Federation
        exclude = []
