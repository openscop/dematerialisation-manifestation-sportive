/*
 * jQuery Live validation
 * Makes a form auto-validated : every form field shows an error if left in a wrong state.
 * The state is fetched from an URL which returns a JSON dictionary (dict)
 * The 'valid' key is a boolean which should tell if the whole form is valid.
 * If not, the dict should contain other keys, amongst which '_all_' represents the list of global form errors
 * the remaining keys are the field ids, and the associated errors.
 * The plugin populates tags with the [field_id]_errors id and [errors_id] class.
 * The refresh occurs when an input field loses the focus, and only updates the tag for the blurred input.
 *
 */
(function ($) {
    $.fn.liveValidate = function (url, settings) {
        settings = $.extend({fields: false, event: 'blur keyup', errors_id: 'errors', errors_class: 'is-invalid', submitHandler: null, delay: 250}, settings);
        var container;
        // Lors de la perte de focus des enfants input de l'élément
        this.find('input, textarea, select').on(settings.event, function (event) {
            // Envoyer une requête AJAX-POST vers l'URL contenant les données du formulaire
            var data = $(this).parents('form').serialize();
            if (settings.fields) {
                data += '&' + $.param(settings.fields);
            }
            setTimeout(function () { // Retarder la vérification (en cas de submit ne pas vérifier)
                $.ajax({
                           url: url, type: 'POST', data: data,
                           async: false, datatype: 'json',
                           error: function (XHR, status, err) {
                           },
                           success: function (data, status) {
                               // Ici, on lit le contenu JSON dans une variable
                               var info = $.parseJSON(data);
                               var valid = info.valid;
                               var input_id = $(event.target).attr('id');
                               container = $(event.target).parent();
                               if (valid === false) {
                                   // Parcourir les clés du dictionnaire JSON à la recherche de l'ID du contrôle à l'origine de l'événement
                                   for (var key in info) {
                                       if (key == input_id || (input_id && input_id.indexOf(key + "_") != -1)) {
                                           if (info[key].length > 0) {
                                               // Créer une liste des erreurs
                                               $.each(info[key], function (err, text) {
                                                    if (container.children('span').text().search(text) == -1) {
                                                        container.append('<span class="invalid-feedback"><strong>' + text + '</strong></span>');
                                                    }
                                               });
                                               $(event.target).addClass(settings.errors_class);
                                           } else {
                                               container.children('span.invalid-feedback').remove();
                                               $(event.target).removeClass(settings.errors_class);
                                           }
                                       }
                                   }
                               } else {
                                   container.children('span.invalid-feedback').remove();
                                   $(event.target).removeClass(settings.errors_class);
                               }
                           }
                       });
            }, settings.delay);
        });
    };
})(jQuery);
