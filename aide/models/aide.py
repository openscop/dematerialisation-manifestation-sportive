# coding: utf-8
import textwrap
from django.urls import reverse
from django.db import models
from django.http.response import Http404
from django.utils.html import mark_safe, strip_tags, format_html
from django_extensions.db.fields import AutoSlugField

from core.util.admin import set_admin_info


class PageAideManager(models.Manager):
    """ Manager des pages d'aide """

    # Getter
    def get_for_url(self, sub_url):
        """ Renvoyer la page d'aide disponible à un chemin/slug"""
        try:
            return self.get(path=sub_url)
        except PageAide.DoesNotExist:
            raise Http404("Pas de page d'aide pour cette URL : "+sub_url)

    def visible(self, request):
        """ Renvoyer les pages d'aide visibles pour l'utilisateur connecté """
        if request.user.is_anonymous:
            return self.filter(active=True, authenticated_only=False)
        return self


class PageAide(models.Model):
    """ Page d'aide """

    HELP_PATH = "Caractères autorisés : lettres majuscules et minuscules, chiffres, le tiret et le slash."

    # Champs
    title = models.CharField(max_length=192, blank=False, verbose_name="Titre")
    slug = AutoSlugField(max_length=192, populate_from='title', unique_for_date=True, verbose_name="Slug")
    path = models.CharField(max_length=80, blank=True, verbose_name="Chemin URL", help_text=HELP_PATH)
    active = models.BooleanField(default=True, verbose_name="Active")
    contentbefore = models.TextField(blank=True, verbose_name="Contenu avant")
    contentafter = models.TextField(blank=True, verbose_name="Contenu après")
    role = models.TextField(blank=True, help_text="Rôles d'utilisateurs autorisés à voir la page", verbose_name="Rôles")
    groupe = models.TextField(blank=True, help_text="Groupes d'utilisateurs autorisés à voir la page", verbose_name="Groupes")
    departements = models.ManyToManyField('administrative_division.departement', blank=True, verbose_name="Départements")
    authenticated_only = models.BooleanField(default=False, verbose_name="Accès authentifié uniquement")
    updated = models.DateTimeField(auto_now=True, verbose_name="Mise à jour")
    nbr_vues = models.IntegerField(default=0, verbose_name="Nombre de fois où cette aide a été vue")
    objects = PageAideManager()

    def __str__(self):
        return self.title

    # Getter
    def get_contentbefore_html(self):
        """ Effectuer le rendu HTML du contenu avant accordion """
        return mark_safe(self.contentbefore)

    def get_contentafter_html(self):
        """ Effectuer le rendu HTML du contenu après accordion """
        return mark_safe(self.contentafter)

    @set_admin_info(short_description="panneaux")
    def get_panels_count(self):
        """ Renvoyer le nombre total de titres pour la page """
        panels = self.get_panels()
        return len(panels)

    @set_admin_info(short_description="onglets")
    def get_tabs_count(self):
        """ Renvoyer le nombre total d'onglets pour la page """
        return self.onglet.count()

    @set_admin_info(short_description="Notes")
    def get_note_count(self):
        """ Renvoyer le nombre total de notes pour la page """
        return self.notes.count()

    def get_tabs(self):
        """ Renvoyer les onglets pour la page d'aide """
        tabs = self.onglet.all().order_by('my_order')
        return tabs

    def get_panels(self):
        """ Renvoyer les panneaux pour la page d'aide """
        tabs = self.get_tabs()
        panels = PanneauPageAide.objects.none()
        for tab in tabs:
            tab_panel = tab.panels.all().order_by('paneltabs__panel_order')
            panels = panels | tab_panel
        return panels

    # Overrides
    def get_absolute_url(self):
        """ Renvoyer l'URL de la page d'aide """
        # Corriger l'URL si démarre par un '/'
        if str(self.path).startswith('/'):
            self.path = str(self.path).replace('/', '', 1)  # Remplacer le premier slash
            self.save()
        return reverse('aide:page-aide', kwargs={'path': '{0}'.format(self.path)})

    # Meta
    class Meta:
        verbose_name = "Page d'aide"
        verbose_name_plural = "Pages d'aide"
        app_label = 'aide'


class OngletPageAide(models.Model):
    """ Onglet d'une page d'aide """

    title = models.CharField(max_length=120, blank=True, verbose_name="Titre",
                             help_text="Si la page ne comporte qu'un onglet, celui-ci ne sera pas affiché, donc le titre ne sera pas utilisé")
    page = models.ForeignKey('aide.PageAide', related_name='onglet', verbose_name="Page", on_delete=models.CASCADE)
    my_order = models.PositiveIntegerField(blank=False, null=False)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['my_order']
        verbose_name = "Onglet de page d'aide"
        verbose_name_plural = "Onglets de pages d'aide"
        app_label = 'aide'


class PanneauPageAide(models.Model):
    """ Panneau d'une page d'aide """

    # Champs
    tabs = models.ManyToManyField('aide.OngletPageAide', related_name='panels', through='PanelTabs')
    title = models.CharField(max_length=255, blank=False, verbose_name="Titre")
    active = models.BooleanField(default=True, verbose_name="Active")
    content = models.TextField(blank=True, verbose_name="Contenu")
    role = models.TextField(blank=True, help_text="Rôles autorisés à voir le panel", verbose_name="Rôles")
    groupe = models.TextField(blank=True, help_text="Groupes d'utilisateurs autorisés à voir la page", verbose_name="Groupes")
    departements = models.ManyToManyField('administrative_division.departement', blank=True,
                                          help_text="Départements autorisés à voir le panel", verbose_name="Départements")
    updated = models.DateTimeField(auto_now=True, verbose_name="Mise à jour")
    my_order = models.PositiveIntegerField(blank=False, null=False, default=0)
    nbr_vues = models.IntegerField(default=0, verbose_name="Nombre de fois où ce panneau a été vu")

    def __str__(self):
        return self.title.title()

    def apercu(self):
        """ Effectuer le rendu HTML du début du texte """
        # Enlever les balises a qui perturbent le rendu html et le fonctionnement de admin-sortable2
        # contenu = re.sub('</?a[^>]*>', '', self.content)
        # Finalement, suppression de toutes les balises
        return mark_safe(textwrap.shorten(strip_tags(self.content), width=250, placeholder="..."))

    def contenu(self):
        """ Effectuer le rendu HTML du contenu du panneau """
        return mark_safe(self.content)

    @set_admin_info(short_description="Notes")
    def get_note_count(self):
        """ Renvoyer le nombre total de notes pour la page """
        return self.notes.count()

    def get_tabs_with_page(self):
        list_tabs = ''
        for tab in self.tabs.all():
            list_tabs += str(tab.page) + ' -> ' + tab.title + '<br>'
        return format_html(list_tabs[:-4])
    get_tabs_with_page.short_description = "Utilisation"

    # Meta
    class Meta:
        ordering = ['my_order']
        verbose_name = "Panneau de page d'aide"
        verbose_name_plural = "Panneaux de pages d'aide"
        app_label = 'aide'


class PanelTabs(models.Model):
    """ Table de jonction entre les panneaux et les onglets """
    # Nécessaire pour admin-sortable2
    # Du coup, permet d'avoir deux ordres, un pour la liste et un pour les inlines

    panel = models.ForeignKey('aide.PanneauPageAide', verbose_name="panneaux", on_delete=models.CASCADE)
    tab = models.ForeignKey('aide.OngletPageAide', verbose_name="Onglets", on_delete=models.CASCADE)
    panel_order = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.panel.title

    class Meta:
        ordering = ['panel_order']
        verbose_name = "Panneau de page d'aide"
        verbose_name_plural = "Panneaux de pages d'aide"
        app_label = 'aide'


class NotePageAide(models.Model):
    """ Note jointe à une page d'aide """

    # Champs
    page_complexe = models.ForeignKey('aide.PageAide', related_name='notes', null=True, blank=True, verbose_name="Page complexe", on_delete=models.SET_NULL)
    panneaux = models.ForeignKey('aide.PanneauPageAide', related_name='notes', null=True, blank=True, verbose_name='Panneaux', on_delete=models.SET_NULL)
    title = models.CharField(max_length=192, blank=True, verbose_name="Titre")
    active = models.BooleanField(default=True, verbose_name="Active")
    content = models.TextField(blank=False, verbose_name="Contenu")
    role = models.TextField(blank=True, help_text="Rôles autorisés à voir la note", verbose_name="Rôles")
    groupe = models.TextField(blank=True, help_text="Groupes d'utilisateurs autorisés à voir la page", verbose_name="Groupes")
    departements = models.ManyToManyField('administrative_division.departement', blank=True, help_text="Départements autorisés à voir la note", verbose_name="Départements")
    updated = models.DateTimeField(auto_now=True, verbose_name="Mise à jour")

    # Getter
    def get_content_html(self):
        """ Effectuer le rendu HTML de la réponse """
        return mark_safe(self.content)

    # Meta
    class Meta:
        verbose_name = "Note de bas de page"
        verbose_name_plural = "Notes de bas de pages"
        app_label = 'aide'
