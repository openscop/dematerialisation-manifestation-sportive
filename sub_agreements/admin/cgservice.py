# coding: utf-8
from django.contrib import admin

from ..models import PreAvisServiceCG


class PreAvisServiceCGInline(admin.StackedInline):
    """ Inline Préavis """

    # Configuration
    model = PreAvisServiceCG
    extra = 0
