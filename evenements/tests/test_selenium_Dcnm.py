import os, time

from django.test import tag, override_settings
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.hashers import make_password as make
from django.utils import timezone

from celery.contrib.testing.worker import start_worker
from configuration.celery import app
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException
from allauth.account.models import EmailAddress
from post_office.models import EmailTemplate

from core.models import Instance
from core.factories import UserFactory
from administration.factories import InstructeurFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from organisateurs.factories import StructureFactory, OrganisateurFactory
from sports.factories import ActiviteFactory
from evenements.models import Manif


@override_settings(MEDIA_ROOT='/tmp/maniftest/media/')
class TestAiguillageDcnm(StaticLiveServerTestCase):
    """
    Test du circuit organisateur avec sélénium
        workflow_GGD : Avis GGD + préavis EDSR
        instruction par arrondissement
        openrunner false par défaut
    Test de l'aiguillage organisateur sur un cerfa DCNM
    Vérification des champs rentrés, des fichiers rentrés
    Vérification du TdB instructeur
    Vérification et ajout des pièces jointes à J-6 et J-21
    Vérification des alertes retards sur les fichiers
    """

    DELAY = 0.4

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets nécessaires au formulaire de création de manifestation
            Création des fichiers nécessaires à joindre à la création de la manifestation
        """
        print()
        print('========== Manifestation DCNM ==========')
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(800, 1200)

        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__workflow_ggd=Instance.WF_GGD_SUBEDSR,
                                            instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        cls.organisateur = UserFactory.create(username='organisateur',
                                              email='organisateur@example.com',
                                              password=make(123),
                                              default_instance=cls.dep.instance,)
        EmailAddress.objects.create(user=cls.organisateur, email='organisateur@example.com', primary=True, verified=True)
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        cls.instructeur = UserFactory.create(username='instructeur',
                                             email='instructeur@example.com',
                                             password=make(123),
                                             default_instance=cls.dep.instance)
        EmailAddress.objects.create(user=cls.instructeur, email='instructeur@example.com', primary=True, verified=True)
        InstructeurFactory.create(user=cls.instructeur, prefecture=prefecture)

        EmailTemplate.objects.create(name='new_msg')

        cls.structure = StructureFactory(commune=cls.commune, organisateur=organisateur)
        cls.activ = ActiviteFactory.create()

        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'certificat_organisateur_tech', 'commissaires', 'itineraire_horaire', 'topo_securite',
                     'liste_signaleurs', 'certificat_assurance'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()
        super(TestAiguillageDcnm, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Suppression des fichiers créés dans /tmp
            Arrêt du driver sélénium
        """
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'certificat_organisateur_tech', 'commissaires', 'itineraire_horaire', 'topo_securite',
                     'liste_signaleurs', 'certificat_assurance'):
            os.remove(file+".txt")

        os.system('rm -R /tmp/maniftest/media/42/')

        cls.selenium.quit()
        super(TestAiguillageDcnm, cls).tearDownClass()


    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.find_element_by_css_selector("a").click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    def chosen_select_multi(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.find_element_by_css_selector("input").click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    @tag('selenium')
    def test_Dcnm(self):
        """
        Circuit organisateur pour une déclaration sans véhicules
        :return:
        """
        print('**** test aiguillage ****')
        # Connexion
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element_by_name("login")
        pseudo_input.send_keys('organisateur')
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('123')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY*4)
        self.assertIn('Connexion avec organisateur réussie', self.selenium.page_source)

        self.selenium.find_element_by_partial_link_text('Déclarer ou demander').click()
        time.sleep(self.DELAY)
        # remplir formulaire d'aiguillage
        self.chosen_select('id_discipline_chosen', 'Discipline')
        time.sleep(self.DELAY)
        self.chosen_select('id_activite_chosen', 'Activite')
        time.sleep(self.DELAY)
        self.chosen_select('id_departement_chosen', '42')
        time.sleep(self.DELAY)
        self.chosen_select('id_ville_depart_chosen', 'Bard')
        time.sleep(self.DELAY)
        self.chosen_select('id_emprise_chosen', 'Totalement sur voies publiques ou ouvertes à la circulation')
        time.sleep(self.DELAY)
        entries = self.selenium.find_element_by_id('id_nb_participants')
        entries.send_keys('150')

        self.selenium.execute_script("window.scroll(0, 1000)")
        self.selenium.find_element_by_xpath("//button[contains(text(),'Rechercher')]").click()
        time.sleep(self.DELAY)
        # Tester bon aiguillage
        self.assertIn('Dcnm', self.selenium.page_source)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()

        self.assertIn('Organisation d\'une manifestation sportive', self.selenium.page_source)

        print('**** remplissage formulaire ****')
        # Remplir formulaire manifestation

        time.sleep(self.DELAY*4)
        name = self.selenium.find_element_by_id('id_nom')
        name.send_keys('La grosse course')

        description = self.selenium.find_element_by_id('id_description')
        description.send_keys('une grosse course qui déchire')
        time.sleep(self.DELAY)

        self.selenium.execute_script("window.scroll(0, 800)")
        self.chosen_select_multi('id_villes_traversees_chosen', 'Roche')
        time.sleep(self.DELAY)

        audience = self.selenium.find_element_by_id('id_nb_spectateurs')
        audience.send_keys('100')
        time.sleep(self.DELAY*2)

        organisateurs = self.selenium.find_element_by_id('id_nb_organisateurs')
        organisateurs.send_keys('20')
        time.sleep(self.DELAY)

        vehicles = self.selenium.find_element_by_id('id_nb_vehicules_accompagnement')
        vehicles.send_keys('1')
        time.sleep(self.DELAY)

        self.selenium.execute_script("window.scroll(0, 1000)")
        signaleurs = self.selenium.find_element_by_id('id_nb_signaleurs')
        signaleurs.send_keys('1')
        time.sleep(self.DELAY)

        nom_contact = self.selenium.find_element_by_id('id_nom_contact')
        nom_contact.send_keys('durand')
        time.sleep(self.DELAY)

        prenom_contact = self.selenium.find_element_by_id('id_prenom_contact')
        prenom_contact.send_keys('joseph')
        time.sleep(self.DELAY)

        tel_contact = self.selenium.find_element_by_id('id_tel_contact')
        tel_contact.send_keys('0605555555')
        time.sleep(self.DELAY)

        self.selenium.execute_script("window.scroll(0, 3000)")
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        self.selenium.find_element_by_partial_link_text('Cartographie').click()
        time.sleep(self.DELAY)

        self.selenium.find_element_by_class_name('carto_update').click()
        time.sleep(self.DELAY*30)

        parcours = self.selenium.find_element_by_id('id_descriptions_parcours')
        parcours.send_keys('parcours de 10Km')

        self.selenium.execute_script("window.scroll(0, 1000)")
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        print('**** test dossier complet ****')
        # Tester la présence de la page détail
        self.assertIn('une grosse course qui déchire', self.selenium.page_source)
        # Tester le status de la manifestation
        boutons = self.selenium.find_elements_by_css_selector('.page-header li')

        try:
            boutons[0].find_element_by_class_name('fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="L'étape 'Préparation du dossier' n\'est pas vert")
        try:
            boutons[2].find_element_by_class_name('action-requise-blanc')
        except NoSuchElementException:
            self.assertTrue(False, msg="L'étape 'Envoi de la demande' n\'est pas rouge")
        # Tester les fichiers manquants
        card = self.selenium.find_element_by_xpath("//div[@class='card-header bg-danger text-white']//parent::*")
        self.assertIn('Liste des pièces jointes manquantes', card.text)
        files = card.find_elements_by_css_selector('li')
        self.assertEqual(5, len(files))
        for file in files:
            if file.text not in ('réglement de la manifestation', 'déclaration et engagement de l\'organisateur',
                                 'dispositions prises pour la sécurité', 'itinéraire horaire',
                                 'cartographie de parcours'):
                self.assertTrue(False, msg="le fichier "+file.text+" n\'est pas dans la liste")

        print('**** test ajout fichiers ****')
        self.selenium.find_element_by_partial_link_text('Pièces jointes').click()
        self.assertIn('Pièces jointes', self.selenium.page_source)

        manif = Manif.objects.get()
        manif.openrunner_route = (456132,)
        manif.save()

        file1 = self.selenium.find_element_by_id('id_reglement_manifestation')
        file1.send_keys('/tmp/reglement_manifestation.txt')
        file2 = self.selenium.find_element_by_id('id_engagement_organisateur')
        file2.send_keys('/tmp/engagement_organisateur.txt')
        file3 = self.selenium.find_element_by_id('id_disposition_securite')
        file3.send_keys('/tmp/disposition_securite.txt')
        file8 = self.selenium.find_element_by_id('id_cartographie')
        file8.send_keys('/tmp/cartographie.txt')
        file4 = self.selenium.find_element_by_id('id_itineraire_horaire')
        file4.send_keys('/tmp/itineraire_horaire.txt')
        self.selenium.execute_script("window.scroll(0, 2000)")
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()

        print('**** test envoi ****')
        time.sleep(self.DELAY*4)
        self.selenium.find_element_by_partial_link_text('Envoyer la demande').click()
        time.sleep(self.DELAY*2)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        # Retour en page détail
        self.assertIn('une grosse course qui déchire', self.selenium.page_source)
        # Tester le status
        boutons = self.selenium.find_elements_by_css_selector('.page-header li')
        try:
            boutons[0].find_element_by_class_name('fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="L'étape 'Préparation du dossier' n\'est pas vert")
        try:
            boutons[2].find_element_by_class_name('fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="L'étape 'Envoi de la demande' n\'est pas vert")

        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_class_name('deconnecter').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()

        print('**** test instructeur ****')
        # Connexion
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element_by_name("login")
        pseudo_input.send_keys('instructeur')
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('123')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY*4)
        self.assertIn('Connexion avec instructeur réussie', self.selenium.page_source)
        # Tester la présence de la manifestation
        self.selenium.find_element_by_id('btn_etat_demande').click()
        time.sleep(self.DELAY)
        declar = self.selenium.find_element_by_id('instruction-table')
        try:
            declar.find_element_by_xpath("//td/span[contains(text(),'La grosse course')]")
            declar.find_element_by_class_name('table_afaire')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'instruction de manifestation en attente")
        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_class_name('deconnecter').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()

        print('**** test Pjs manquantes delai limite ****')
        # Connexion
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element_by_name("login")
        pseudo_input.send_keys('organisateur')
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('123')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Connexion avec organisateur réussie', self.selenium.page_source)
        # Tester la présence de la manifestation
        self.selenium.find_element_by_id('btn_etat_demande').click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1000)")
        declar = self.selenium.find_element_by_id('manif-table')
        try:
            declar.find_element_by_xpath("//td[contains(text(),'La grosse course')]")
            declar.find_element_by_class_name('table_encours')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas de manifestation en cours d'instruction")

        self.selenium.find_element_by_xpath('//td[text()="La grosse course"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        action = self.selenium.find_element_by_xpath("//div[contains(@class, 'card')][contains(string(),'Actions requises')]")
        self.assertIn('Pas d\'actions requises', action.text)
        self.selenium.execute_script("window.scroll(0, 700)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)

        print('**** test Pjs manquantes delai etape 1-2 ****')
        manif.date_debut = timezone.now() + timezone.timedelta(days=25)
        manif.date_fin = timezone.now() + timezone.timedelta(days=25)
        manif.save()
        self.selenium.find_element_by_id('btn_etat_demande').click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1000)")
        self.selenium.find_element_by_xpath('//td[text()="La grosse course"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        action = self.selenium.find_element_by_xpath("//div[contains(@class, 'card')][contains(string(),'Actions requises')]")
        self.assertIn('pièces jointes manquantes', action.text)
        self.assertIn('liste des signaleurs', action.text)
        self.selenium.execute_script("window.scroll(0, 700)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        manif.date_debut = timezone.now() + timezone.timedelta(days=19)
        manif.date_fin = timezone.now() + timezone.timedelta(days=19)
        manif.save()
        self.selenium.find_element_by_id('btn_etat_demande').click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1000)")
        self.selenium.find_element_by_xpath('//td[text()="La grosse course"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        action = self.selenium.find_element_by_xpath("//div[contains(@class, 'card')][contains(string(),'Actions requises')]")
        self.assertIn('pièces jointes manquantes', action.text)
        self.assertIn('Vous êtes en retard', action.text)
        self.assertIn('liste des signaleurs', action.text)
        self.selenium.find_element_by_partial_link_text('Pièces jointes').click()
        # Test du message d'alerte au niveau du champ
        formulaire1 = self.selenium.find_element_by_id('form_liste_signaleurs')
        self.assertIn('Vous êtes en retard', formulaire1.text)
        formulaire4 = self.selenium.find_element_by_id('form_certificat_assurance')
        self.assertNotIn('Vous êtes en retard', formulaire4.text)
        file4 = self.selenium.find_element_by_id('id_liste_signaleurs')
        file4.send_keys('/tmp/liste_signaleurs.txt')
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        action = self.selenium.find_element_by_xpath("//div[contains(@class, 'card')][contains(string(),'Actions requises')]")
        self.assertIn('pièces jointes manquantes', action.text)
        self.assertIn('assurance', action.text)
        self.selenium.execute_script("window.scroll(0, 700)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)

        print('**** test Pjs manquantes delai etape 2-3 ****')
        manif.date_debut = timezone.now() + timezone.timedelta(days=5)
        manif.date_fin = timezone.now() + timezone.timedelta(days=5)
        manif.save()
        self.selenium.find_elements_by_id('btn_etat_demande')[0].click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1000)")
        self.selenium.find_element_by_xpath('//td[text()="La grosse course"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        action = self.selenium.find_element_by_xpath("//div[contains(@class, 'card')][contains(string(),'Actions requises')]")
        self.assertIn('pièces jointes manquantes', action.text)
        self.assertIn('Vous êtes en retard', action.text)
        self.selenium.find_element_by_partial_link_text('Pièces jointes').click()
        time.sleep(self.DELAY)
        # Test du message d'alerte au niveau du champ
        self.selenium.execute_script("window.scroll(0, 700)")
        formulaire2 = self.selenium.find_element_by_id('form_certificat_assurance')
        self.assertIn('Vous êtes en retard', formulaire2.text)
        file4 = self.selenium.find_element_by_id('id_certificat_assurance')
        file4.send_keys('/tmp/certificat_assurance.txt')
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY*2)
        action = self.selenium.find_element_by_xpath("//div[contains(@class, 'card')][contains(string(),'Actions requises')]")
        self.assertIn('Pas d\'actions requises', action.text)
