import json, time

from django.test import TestCase
from django.utils import timezone
from django.contrib.auth.hashers import make_password as make

from post_office.models import EmailTemplate

from core.models import Instance
from core.factories import UserFactory
from organisateurs.factories import OrganisateurFactory, StructureFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import InstructeurFactory, EDSRAgentFactory, MairieAgentFactory
from sports.factories import ActiviteFactory
from evenements.factories import DcnmFactory
from instructions.factories import InstructionFactory
from messagerie.models import Enveloppe


class MessagerieTests(TestCase):

    @classmethod
    def setUpTestData(cls):

        print()
        print('========== Message ===========')

        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_EDSR,
                                        instance__instruction_mode=Instance.IM_ARRONDISSEMENT_COMPLEXE_5)
        arrond_1 = ArrondissementFactory.create(name='Montbrison', code='421', departement=dep, prefecture__sous_prefecture=False)
        arrond_2 = ArrondissementFactory.create(name='Roanne', code='422', departement=dep)
        arrond_3 = ArrondissementFactory.create(name='ST Etienne', code='420', departement=dep)
        cls.dep = dep
        cls.instance = dep.instance
        cls.prefecture1 = arrond_1.prefecture
        cls.prefecture2 = arrond_2.prefecture
        cls.prefecture3 = arrond_3.prefecture
        cls.commune1 = CommuneFactory(name='Bard', arrondissement=arrond_1)
        cls.autrecommune1 = CommuneFactory(name='Roche', arrondissement=arrond_1)
        cls.commune2 = CommuneFactory(name='Bully', arrondissement=arrond_2)
        cls.autrecommune2 = CommuneFactory(name='Régny', arrondissement=arrond_2)
        cls.commune3 = CommuneFactory(name='Villars', arrondissement=arrond_3)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        # Organisateur actif
        organisateur1 = OrganisateurFactory.create(user__username='organisateur1', user__password=make(123),
                                                   user__last_name='Orga1', user__default_instance=dep.instance)
        structure1 = StructureFactory(commune=cls.commune1, organisateur=organisateur1)
        cls.orga1 = organisateur1.user
        # Organisateur inactif
        organisateur2 = OrganisateurFactory.create(user__username='organisateur2', user__password=make(123),
                                                   user__last_name='Orga2', user__default_instance=dep.instance)
        organisateur2.user.last_login = timezone.now() - timezone.timedelta(days=370)
        organisateur2.user.save()
        StructureFactory(commune=cls.commune2, organisateur=organisateur2)
        cls.orga2 = organisateur2.user
        # Instructeur de préfecture
        cls.instructeur1 = UserFactory.create(username='instructeur1', password=make(123),
                                              last_name='Instru1', default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur1, prefecture=cls.prefecture1)
        cls.instructeur2 = UserFactory.create(username='instructeur2', password=make(123),
                                              last_name='Instru2', default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur2, prefecture=cls.prefecture2)
        cls.instructeur3 = UserFactory.create(username='instructeur3', password=make(123),
                                              last_name='Instru3', default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur3, prefecture=cls.prefecture3)
        # Agent mairie de la commune 1
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123),
                                              last_name='Mairie1', default_instance=dep.instance, email='mair@test.fr')
        MairieAgentFactory.create(user=cls.agent_mairie, commune=cls.commune1)
        # Agent EDSR
        cls.agent_edsr = UserFactory.create(username='agent_edsr', password=make(123),
                                            last_name='Edsr', default_instance=dep.instance, email='edsr@test.fr')
        EDSRAgentFactory.create(user=cls.agent_edsr, edsr=dep.edsr)
        # Création d'une manif NM sur 2 commmune, arrondissement de préfecture1
        cls.manifestation1 = DcnmFactory.create(ville_depart=cls.commune1, structure=structure1, activite=activ,
                                                nom='Manifestation_Test', villes_traversees=(cls.autrecommune1,))
        InstructionFactory.create(manif=cls.manifestation1)

        EmailTemplate.objects.create(name='new_msg')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def test_affichagemessagerie(self):

        print('\t>>> Test affichage')
        # Affichage sans login 403
        retour = self.client.get("/messagerie/", HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 401)

        self.assertTrue(self.client.login(username="organisateur1", password='123'))

        # Affichage messagerie principale
        retour = self.client.get("/messagerie/", HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, 'Messagerie')
        self.assertTemplateUsed(retour, 'messagerie/messagerie_globale.html')

        # Affichage du body de la messagerie global
        retour = self.client.get("/messagerie/messagerie/", HTTP_HOST='127.0.0.1:8000')
        self.assertTemplateUsed(retour, 'messagerie/messagerie.html')
        self.assertContains(retour, "Vous êtes dans la messagerie générale")

        # Affichage du compteur
        retour = self.client.get('/messagerie/cpt/', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        self.assertTemplateUsed(retour, 'messagerie/compteurnonlu.html')
        self.assertContains(retour, '</i>0', 4)

        # Affichage de la liste des msg
        retour = self.client.get('/messagerie/listemessage/?limit=10&manif=&suivi=1&action=1&conv=1&nouv=1&dossier=1&docjoin=1&avis=1&preavis=1&arrete=1&recepisse=1&envoi=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertTemplateUsed(retour, 'messagerie/liste_message.html')
        self.assertContains(retour, "Destinataire/Expediteur")

        print('\t>>> Test filtre')
        # Création d'un filtre
        data = {
            "titre": "filtre_test",
            'option': "mes-options",
        }
        retour = self.client.post('/messagerie/filtre/', data=data, HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)

        # Affichage du filtre
        retour = self.client.get('/messagerie/filtre/', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Test du contenu retourné
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"model": "messagerie.filtre",
              "pk": 1,
              "fields": {
                  "titre": "filtre_test",
                  "utilisateur": self.orga1.pk,
                  "option": "mes-options"
              }}]
        )

        # Suppression du filtre
        retour = self.client.get('/messagerie/filtre/del/?pk=1', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)

        print('\t>>> Test envoi')
        # Envoi d'un message
        retour = self.client.get('/messagerie/envoimessage/?manif=' + str(self.manifestation1.pk), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "<div id=\"formmail")
        self.assertIn(str(self.prefecture1) + " - " + self.instructeur1.get_full_name(), retour.content.decode('utf-8'))

        # Envoi du msg conversation dossier
        data = {
            "service": "orga",
            'destinataires': self.instructeur1.pk,
            "doc_objet": "dossier",
            "objet": "test envoi sur dossier message instructeur1",
            "corps": "<p>test dossier</p>"
        }
        retour = self.client.post('/messagerie/envoimessage/?manif=' + str(self.manifestation1.pk), data=data, HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        self.msg1 = Enveloppe.objects.last()
        time.sleep(1)
        # Envoi du msg conversation avis
        data = {
            "service": "orga",
            'destinataires': self.instructeur1.pk,
            "doc_objet": "avis",
            "objet": "test envoi sur avis message instructeur1",
            "corps": "<p>test avis</p>"
        }
        retour = self.client.post('/messagerie/envoimessage/?manif=' + str(self.manifestation1.pk), data=data, HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        self.msg2 = Enveloppe.objects.last()
        time.sleep(1)

        print('\t>>> Test affichage messages organisateur')
        # Affichage de la liste des msg conversation concernant un dossier, envoyés et lus de la manif 1
        retour = self.client.get('/messagerie/listemessage/?manif=' + str(self.manifestation1.pk) + '&conv=1&dossier=1&envoi=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertContains(retour, "Pour Préfecture de Montbrison")
        self.assertContains(retour, "test envoi sur dossier message instructeur1")
        # Affichage de la liste générale des msg conversation concernant un dossier, envoyés et lus
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&envoi=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        self.assertContains(retour, "Pour Préfecture de Montbrison")
        self.assertContains(retour, "test envoi sur dossier message instructeur1")
        # Affichage de la liste générale des msg conversation concernant un dossier, envoyés et non lus
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&envoi=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertNotIn('class="msg_header', retour.content.decode('utf-8'))
        # Affichage de la liste générale des conversation concernant un dossier, msg reçus et lus
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertNotIn('class="msg_header', retour.content.decode('utf-8'))
        # Affichage de la liste générale des msg conversation concernant un avis, envoyés et non lus
        time.sleep(1)
        retour = self.client.get('/messagerie/listemessage/?manif=&suivi=1&action=1&conv=1&trac=1&nouv=1&dossier=1&docjoin=1&avis=1&preavis=1&arrete=1&recepisse=1&envoi=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        self.assertContains(retour, "Pour Préfecture de Montbrison")
        self.assertContains(retour, "test envoi sur avis message instructeur1")
        # Affichage de la liste générale des msg action concernant un dossier, envoyés et non lus
        retour = self.client.get('/messagerie/listemessage/?action=1&dossier=1&envoi=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertNotIn('class="msg_header', retour.content.decode('utf-8'))
        # Affichage de la liste générale des msg traçabilité, envoyés et non lus
        retour = self.client.get('/messagerie/listemessage/?trac=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        self.assertContains(retour, "Manifestation Sportive")
        self.assertContains(retour, "Connexion à la plateforme")

        print('\t>>> Test carnet instructeur')
        # Delog pour log en instructeur
        self.client.logout()
        self.assertTrue(self.client.login(username="instructeur1", password='123'))

        # Récupération du carnet sans query : 403 normalement
        retour = self.client.get('/messagerie/carnetinstructeur/', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 403)

        # Récupération du carnet instructeur "organisateurs actifs"
        retour = self.client.get('/messagerie/carnetinstructeur/?idservice=orga|new', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Test du contenu retourné
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"pk": self.orga1.pk,
              "service": str(self.orga1.organisateur.structure),
              "first_name": self.orga1.first_name,
              "last_name": self.orga1.last_name}]
        )
        # Récupération du carnet instructeur "organisateurs inactifs"
        retour = self.client.get('/messagerie/carnetinstructeur/?idservice=orga|old', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Test du contenu retourné
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"pk": self.orga2.pk,
              "service": str(self.orga2.organisateur.structure),
              "first_name": self.orga2.first_name,
              "last_name": self.orga2.last_name}]
        )
        # Récupération du carnet instructeur "organisateur de la manifestation"
        retour = self.client.get('/messagerie/carnetinstructeur/?idservice=orga|' + str(self.manifestation1.pk), HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Test du contenu retourné
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"pk": self.orga1.pk,
              "service": str(self.orga1.organisateur.structure),
              "first_name": self.orga1.first_name,
              "last_name": self.orga1.last_name}]
        )
        # Récupération de la liste des services adressables
        retour = self.client.get('/messagerie/listeservice/?pk=' + str(self.dep.pk) + '&type=service', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Test du contenu retourné
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"pk": self.dep.sdis.pk,
              "classname": self.dep.sdis.__class__.__name__,
              "nom": str(self.dep.sdis)},
             {"pk": self.dep.codis.pk,
              "classname": self.dep.codis.__class__.__name__,
              "nom": str(self.dep.codis)},
             {"pk": self.dep.ggd.pk,
              "classname": self.dep.ggd.__class__.__name__,
              "nom": str(self.dep.ggd)},
             {"pk": self.dep.edsr.pk,
              "classname": self.dep.edsr.__class__.__name__,
              "nom": str(self.dep.edsr)},
             {"pk": self.dep.ddsp.pk,
              "classname": self.dep.ddsp.__class__.__name__,
              "nom": str(self.dep.ddsp)},
             {"pk": self.dep.cg.pk,
              "classname": self.dep.cg.__class__.__name__,
              "nom": str(self.dep.cg)},
             {"pk": self.prefecture1.pk,
              "classname": self.prefecture1.__class__.__name__,
              "nom": str(self.prefecture1)},
             {"pk": self.prefecture2.pk,
              "classname": self.prefecture2.__class__.__name__,
              "nom": str(self.prefecture2)},
             {"pk": self.prefecture3.pk,
              "classname": self.prefecture3.__class__.__name__,
              "nom": str(self.prefecture3)}]
        )
        # Récupération du carnet instructeur "service EDSR"
        retour = self.client.get('/messagerie/carnetinstructeur/?idservice=' + str(self.dep.edsr.pk) + '|EDSR&dep=' + str(self.dep.pk), HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Test du contenu retourné
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"pk": self.agent_edsr.pk,
              "service": str(self.dep.edsr),
              "first_name": self.agent_edsr.first_name,
              "last_name": self.agent_edsr.last_name}]
        )
        # Récupération de la liste des mairies adressables
        retour = self.client.get('/messagerie/listeservice/?pk=' + str(self.dep.pk) + '&type=mairie', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Test du contenu retourné
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"pk": self.commune1.pk,
              "classname": "Mairie",
              "nom": str(self.commune1)},
             {"pk": self.commune2.pk,
              "classname": "Mairie",
              "nom": str(self.commune2)},
             {"pk": self.autrecommune2.pk,
              "classname": "Mairie",
              "nom": str(self.autrecommune2)},
             {"pk": self.autrecommune1.pk,
              "classname": "Mairie",
              "nom": str(self.autrecommune1)},
             {"pk": self.commune3.pk,
              "classname": "Mairie",
              "nom": str(self.commune3)}]
        )
        # Récupération du carnet instructeur "service Mairie"
        retour = self.client.get('/messagerie/carnetinstructeur/?idservice=' + str(self.commune1.pk) + '|Mairie&dep=' + str(self.dep.pk), HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Test du contenu retourné
        self.assertJSONEqual(
            str(retour.content, encoding='utf8'),
            [{"pk": self.agent_mairie.pk,
              "service": str(self.commune1),
              "first_name": self.agent_mairie.first_name,
              "last_name": self.agent_mairie.last_name}]
        )
        # Récupération de la liste des familles d'utilisateurs adressables sans département en paramètre
        retour = self.client.get('/messagerie/listeservice/?type=groupe', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Test du contenu retourné, c'est une liste figée : tester un ou deux éléments
        self.assertIn("Instructeur", str(retour.content, encoding='utf8'))
        self.assertIn("Agent mairie", str(retour.content, encoding='utf8'))
        # Récupération du carnet instructeur "famille instructeur"
        retour = self.client.get('/messagerie/carnetinstructeur/?idservice=instructeur|role&dep=' + str(self.dep.pk), HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # print(json.loads(retour.content))
        # Test du contenu retourné
        self.assertEqual(3, len(json.loads(retour.content)))
        self.assertIn("Instru1", str(retour.content, encoding='utf8'))
        self.assertIn("Instru2", str(retour.content, encoding='utf8'))
        self.assertIn("Instru3", str(retour.content, encoding='utf8'))
        # Récupération de la liste avec paramètre érroné
        retour = self.client.get('/messagerie/listeservice/?type=00000', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Test du contenu retourné, vide
        self.assertEqual(0, len(retour.content))

        print('\t>>> Test affichage message instructeur')
        # Le paramètre nonlu agit en inverse : nonlu=1 => tous les messages, sans paramètre => uniquement les non lus
        # Affichage de la liste des msg conversation concernant un dossier, reçus et non lus de la manif 1
        retour = self.client.get('/messagerie/listemessage/?manif=' + str(self.manifestation1.pk) + '&conv=1&dossier=1&recu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertContains(retour, self.orga1.organisateur.structure)
        self.assertContains(retour, "test envoi sur dossier message instructeur1")
        # Affichage de la liste générale des msg conversation concernant un dossier, reçus et non lus
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&recu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        self.assertContains(retour, self.orga1.organisateur.structure)
        self.assertContains(retour, "test envoi sur dossier message instructeur1")
        # Affichage de la liste générale des msg conversation concernant un avis, reçus et non lus
        retour = self.client.get('/messagerie/listemessage/?conv=1&avis=1&recu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        self.assertContains(retour, self.orga1.organisateur.structure)
        self.assertContains(retour, "test envoi sur avis message instructeur1")

        # Nombre de message non lus : 2
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&avis=1&recu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertContains(retour, 'class="msg_header', count=2)

        # Affichage du contenu du message
        retour = self.client.get('/messagerie/message/' + str(self.msg2.pk), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Préfecture de Montbrison")
        self.assertContains(retour, "test avis")
        # Todo a faire dans les selnium à cause de la non précense de l'ajax ici
        # self.assertContains(retour, "Repondre à ce message")

        # Marquer ce message comme lu
        retour = self.client.post('/messagerie/lu/?msg=' + str(self.msg2.pk), HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        # Marquer ce message comme lu
        retour = self.client.post('/messagerie/lu/?msg=' + str(self.msg1.pk), HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)

        # Nombre de message non lus : 0
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&avis=1&recu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertNotContains(retour, 'class="msg_header')

        # Nombre de message lus : 2
        retour = self.client.get('/messagerie/listemessage/?conv=1&dossier=1&avis=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertContains(retour, 'class="msg_header', count=2)

        # Affichage de la liste générale des msg conversation concernant un avis, reçus et lus
        retour = self.client.get('/messagerie/listemessage/?conv=1&avis=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        self.assertContains(retour, self.orga1.organisateur.structure)
        self.assertContains(retour, "test envoi sur avis message instructeur1")

        print('\t>>> Test envoi réponse')
        # Envoi d'une réponse au message
        retour = self.client.post('/messagerie/envoimessagereponse/?pk=' + str(self.msg2.pk),
                                  {'objet': "Réponse au message #3", 'corps': "Réponse au message #3"},
                                  HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        time.sleep(1)
        retour = self.client.post('/messagerie/envoimessagereponse/?pk=' + str(self.msg2.pk),
                                  {'objet': "Réponse au message #3", 'corps': "Réponse au message #3"},
                                  HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)
        time.sleep(1)

        # Affichage de la liste générale des msg conversation concernant un avis, reçus, envoyés et lus
        retour = self.client.get('/messagerie/listemessage/?manif=&suivi=1&action=1&conv=1&trac=1&nouv=1&dossier=1&docjoin=1&avis=1&preavis=1&arrete=1&recepisse=1&envoi=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")
        self.assertIn('class="msg_header title_msg', retour.content.decode('utf-8'))
        self.assertContains(retour, self.orga1.organisateur.structure)
        self.assertIn('Réponse au message #3', retour.content.decode('utf-8'))
        self.assertContains(retour, "Réponse au message #3")
        self.assertIn('class="msg_header', retour.content.decode('utf-8'))
        self.assertContains(retour, self.orga1.organisateur.structure)
        self.assertContains(retour, "test envoi sur avis message instructeur1")

        # print(retour.content.decode('utf-8'))
        # for user in User.objects.all():
        #     print(user.username + " - " + user.get_full_name())
