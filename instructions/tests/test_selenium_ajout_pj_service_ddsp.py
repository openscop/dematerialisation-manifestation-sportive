import re, time

from django.test import tag
from post_office.models import Email
from django.test.utils import override_settings
from django.conf import settings

from selenium.common.exceptions import NoSuchElementException

from .test_base_selenium import SeleniumCommunClass
from instructions.models import PreAvis, Avis
from messagerie.models import Message


class InstructionAjoutPjTests(SeleniumCommunClass):
    """
    Test d'ajout de PJ du circuit d'instance EDSR avec sélénium pour une Dcnm
        workflow_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    Test sur les services complexes avec transfert des PJ du préavis dans l'avis et
    transfert des PJ d'avis en document officiel
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ Pièces Jointes Service DDSP (Sel) =============')
        SeleniumCommunClass.init_setup(cls)
        cls.avis_nb = 1
        super().setUpClass()

    @tag('selenium')
    @override_settings(DEBUG=True)
    def test_Ajout_PJ(self):
        """
        Test d'ajout de PJ pour les différentes étapes du circuit EDSR pour une autorisationNM
        """
        assert settings.DEBUG

        print('**** test 1 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        reponse = self.client.get('/tableau-de-bord-organisateur/liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file4:
            self.client.post(url_script.group('url'), {'cartographie': file4}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/itineraire_horaire.txt') as file5:
            reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file5}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 2 distribution avis ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes d\'avis', self.selenium.page_source)
        # Distribuer les demandes d'avis de l'événement
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes d\'avis').click()
        time.sleep(self.DELAY*4)
        self.assertIn('Choix des avis', self.selenium.page_source)
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis DDSP requis')]/..").click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('submit-id-save').click()
        time.sleep(self.DELAY * 6)

        # Annuler la demande d'avis fédération
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']")
        avis.click()
        self.selenium.execute_script("window.scroll(0, 400)")
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']/following::*")
        self.assertIn('Annuler cette demande', carte.text)
        carte.find_element_by_partial_link_text('Annuler cette demande').click()
        carte.find_element_by_partial_link_text('Confirmer').click()

        # Vérifier le passage en encours et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1200)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 3 avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_commissariats_concernes_chosen', 'Commissariat Bard')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Test d ela partie pièces jointes
        self.assertIn('Aucune', self.selenium.page_source)
        self.assertIn('Ajouter une pièce jointe', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ddsp', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 4 preavis commissariat ****')
        # Instruction du préavis par le commissariat, vérification de la présence de l'événement en nouveau
        self.connexion('agent_commiss')
        self.presence_avis('agent_commiss', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre le pré-avis', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # Rendre le préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre le pré-avis').click()
        time.sleep(self.DELAY)
        self.assertIn('pouvez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Aucune', self.selenium.page_source)  # Pièces jointes
        self.selenium.execute_script("window.scroll(0, 500)")
        # Ajouter une pièce jointe
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_1.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence des pièces jointes
        pan = self.selenium.find_element_by_xpath("//div[@id='subagreement']")
        self.assertIn('pj_1', pan.text)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_commiss', 'rendu')
        self.deconnexion()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_ddsp.email])
        self.assertEqual(outbox[1].to, [self.ddsp.email])
        self.assertEqual(messages[0].corps, 'Pièce jointe ajoutée à un préavis au sujet de la manifestation Manifestation_Test')
        self.assertEqual(messages[0].object_id, PreAvis.objects.get(service_concerne='commissariats').id)          # Pré-avis DDSP
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "preavis")
        Email.objects.all().delete()
        Message.objects.all().delete()


        print('**** test 5 avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en encours
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Vérifier affichage
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertIn('Aucune', pan.text)  # Pièces jointes
        self.assertIn('Ajouter une pièce jointe', pan.text)
        # Vérifier les icônes du préavis
        preavis = self.selenium.find_element_by_xpath("//div[contains(string(),'Commissariats')][@data-toggle='collapse']")
        icones = preavis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            preavis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 800)")
        # Déplier le préavis
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        # Tester la présence des PJs dans le préavis et des boutons de transfert
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Commissariats')][@data-toggle='collapse']/following::*")
        self.assertIn('pj_1', carte.text)
        lien1 = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/following-sibling::a")
        self.assertIn('Transférer le document sur', lien1.text)
        # Transférer la PJ sur l'avis
        lien1.click()
        # Tester aucune notifications
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 0)
        # Tester la présence de la PJ sur l'avis
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertNotIn('Aucune', pan.text)
        self.assertIn('pj_1', pan.text)
        self.selenium.execute_script("window.scroll(0, 800)")
        # Déplier le préavis
        preavis = self.selenium.find_element_by_xpath("//div[contains(string(),'Commissariats')][@data-toggle='collapse']")
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        # Tester l'absence du bouton de transfert pour PJ_1
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Commissariats')][@data-toggle='collapse']/following::*")
        item = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/parent::*")
        self.assertNotIn('Transférer le document sur', item.text)
        self.selenium.execute_script("window.scroll(0, 200)")
        # Rendre l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)

        Email.objects.all().delete()
        Message.objects.all().delete()

        # Ajouter une pièce jointe
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_2.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence ds PJs
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_2', pan.text)
        # Tester les notifications

        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[1].to, [self.prefecture.email])
        self.assertEqual(messages[0].corps, 'Pièce jointe ajoutée à un avis au sujet de la manifestation Manifestation_Test')
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_concerne='ddsp').id)          # Avis DDSP
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # vérification de la présence de l'événement en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ggd', 'rendu')
        self.deconnexion()

        print('**** test 6 instructeur transfert pj ****')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.vue_detail()
        # Avis DDSP
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'DDSP')][@data-toggle='collapse']")
        icones = avis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            avis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        # Déplier l'avis
        avis.click()
        self.selenium.execute_script("window.scroll(0, 800)")
        # Tester la présence des PJs dans le préavis et des boutons de transfert
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'DDSP')][@data-toggle='collapse']/following::*")
        time.sleep(self.DELAY*2)
        self.assertIn('pj_1', carte.text)
        lien1 = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien1.text)
        self.assertIn('pj_2', carte.text)
        lien2 = carte.find_element_by_xpath("//a[contains(text(), 'pj_2')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien2.text)
        pj = self.selenium.find_element_by_partial_link_text('pj_1')
        # Transférer la PJ en document officiel
        lien1.click()
        nom_pj = pj.get_attribute('text')
        self.selenium.execute_script("window.scroll(0, 1500)")
        self.selenium.find_element_by_xpath('//select[@name="nature"]/option[text()="Arrêté d\'interdiction"]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY)
        # Reprendre l'avis et tester plus de bouton de transfert
        self.selenium.find_element_by_xpath("//div[contains(string(),'DDSP')][@data-toggle='collapse']").click()
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'DDSP')][@data-toggle='collapse']/following::*")
        time.sleep(self.DELAY*2)
        self.assertIn('pj_1', carte.text)
        item = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/parent::*")
        self.assertNotIn('Transférer le document sur', item.text)
        self.assertIn('pj_2', carte.text)
        lien2 = carte.find_element_by_xpath("//a[contains(text(), 'pj_2')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien2.text)
        self.selenium.execute_script("window.scroll(0, 0)")
        self.assertIn('Documents officiels (1)', self.selenium.page_source)
        self.selenium.find_element_by_partial_link_text('Documents officiels (1)').click()
        pan = self.selenium.find_element_by_xpath("//div[@id='officialdata']")
        self.assertIn('Arrêté d\'interdiction', pan.text)
        self.assertIn('Déposé le', pan.text)
        self.assertIn(nom_pj, pan.text)
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 6)
        self.assertEqual(outbox[0].to, [self.organisateur.email])
        self.assertEqual(outbox[1].to, [self.prefecture.email])
        self.assertEqual(outbox[2].to, [self.agent_commiss.email])
        self.assertEqual(outbox[3].to, [self.commiss.email])
        self.assertEqual(outbox[4].to, [self.agent_ddsp.email])
        self.assertEqual(outbox[5].to, [self.ddsp.email])
        self.assertEqual(messages[0].corps, 'Arrêté d\'interdiction publié pour la manifestation Manifestation_Test')
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "arrete")
        # Vérifier le passage en autorise
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'interdite')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.deconnexion()

