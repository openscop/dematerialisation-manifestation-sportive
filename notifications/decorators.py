# coding: utf-8
from django.shortcuts import get_object_or_404, render
from functools import wraps

from core.util.permissions import require_role
from instructions.models.instruction import Instruction
from instructions.models.avis import Avis
from instructions.models.preavis import PreAvis
from administration.models.service import GGD, EDSR, CODIS, Brigade


def verifier_acces(function=None):

    def decorator(view_func):
        @require_role(['instructeur', 'mairieagent', 'agent', 'agentlocal'])
        @wraps(view_func)
        def wrapper(request, *args, **kwargs):
            user = request.user
            from core.models import Instance
            if kwargs['path'] == 'instruction':
                has_role = False
                for name in ['instructeur', 'mairieagent']:
                    has_role |= user.has_role(name)
                if has_role:
                    instruction = get_object_or_404(Instruction, pk=kwargs['pko'])
                    if str(instruction.manif.pk) == kwargs['pk']:
                        if user.has_role('instructeur'):
                            if request.user in instruction.get_instructeurs_prefecture(arr_wrt=False):
                                return view_func(request, *args, **kwargs)
                        elif user.has_role('mairieagent'):
                            if user in instruction.get_instructeurs_mairie():
                                return view_func(request, *args, **kwargs)
            elif kwargs['path'] == 'avis':
                if user.has_role('agent'):
                    avis = get_object_or_404(Avis, pk=kwargs['pko'])
                    if str(avis.instruction.manif.pk) == kwargs['pk']:
                        # L'agent ne peut accéder qu'aux avis concernant son service sauf ...
                        instruction = avis.instruction
                        if instruction.get_avis_user(user).id == avis.id:
                            return view_func(request, *args, **kwargs)
            elif kwargs['path'] == 'preavis':
                if user.has_role('agentlocal'):
                    preavis = get_object_or_404(PreAvis, pk=kwargs['pko'])
                    if str(preavis.avis.instruction.manif.pk) == kwargs['pk']:
                        # L'agent ne peut accéder qu'aux preavis concernant son service
                        user_service = user.get_service()
                        liste_service = [preavis.destination_object]
                        if user_service in liste_service:
                            return view_func(request, *args, **kwargs)
            return render(request, "core/access_restricted.html",{'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)

        return wrapper
    if function:
        return decorator(function)
    return decorator
