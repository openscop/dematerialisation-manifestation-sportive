# coding: utf-8
from django.contrib import admin, messages
from django.utils.html import format_html
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from core.util.admin import RelationOnlyFieldListFilter
from organisateurs.forms.structure import StructureForm
from organisateurs.models import Structure, StructureType, Organisateur


@admin.register(StructureType)
class StructureTypeAdmin(ExportActionModelAdmin):
    pass


@admin.register(Structure)
class StructureAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des structures organisatrices """

    list_display = ['pk', 'name', 'type_of_structure', 'lien_user', 'commune__arrondissement__departement']
    list_filter = ['type_of_structure__type_of_structure', ('commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['name__unaccent', 'organisateur__user__username', 'organisateur__user__last_name']
    form = StructureForm
    list_display_links = ['pk', 'name']

    def lien_user(self, obj):
        return format_html('<a href="/admin/core/user/' + str(obj.organisateur.user.pk) + '/change/">' + obj.organisateur.user.get_full_name() + '</a>')
    lien_user.short_description = "Organisateur"

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(commune__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['organisateur'].queryset = Organisateur.objects.filter(user__default_instance=request.user.get_instance())
        return form

    def delete_model(self, request, obj):
        # Si il y a cascade, c'est à dire que cette suppression va entrainer la suppression d'une ou plusieurs manifs,
        # et si l'utilisateur n'a pas les droits pour les suppressions en cascade, la méthode ne sera pas atteinte
        # Si il a les droits, alors il faudra en plus qu'il soit superuser ou du groupe "Administrateurs techniques
        if obj.manifs.filter(instruction__isnull=False).exists():
            if not request.user.is_superuser and not "Administrateurs techniques" in request.user.groups.values_list('name', flat=True):
                self.message_user(request, "Cette opération n'est pas possible, car elle entrainerait des suppressions de dossiers de manifestation !", level=messages.WARNING)
                return
        super().delete_model(request, obj)
