# coding: utf-8
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout

from core.forms.base import GenericForm
from ..models import PreAvis


class PreAvisForm(GenericForm):
    """ Formulaire des préavis """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(PreAvisForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'favorable',
            'prescriptions',
            FormActions(
                Submit('save', "Rendre le pré-avis")
            )
        )

    class Meta:
        model = PreAvis
        fields = ('favorable', 'prescriptions')
