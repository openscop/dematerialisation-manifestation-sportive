# coding: utf-8
from django.apps import AppConfig


class AideConfig(AppConfig):
    """ Configuration de l'application """
    name = 'aide'
    verbose_name = "Aide"

    def ready(self):
        """ Installer les récepteurs de signaux (listeners) """
        pass


default_app_config = 'aide.AideConfig'
