# coding: utf-8
"""
Utilitaires pour les utilisateurs

UserHelper : cette classe permet plusieurs choses :
    - récupérer le type d'agent correspondant au rôle d'un utilisateur
    - récupérer le service attaché à un utilisateur. ex.: s'il est agent GGD, renvoyer son GGD
"""
from collections import OrderedDict
from operator import itemgetter


class UserHelper:
    """ Utilitaires liés aux utilisateurs """

    # Constantes
    ROLE_TYPES = ['ggdagent', 'edsragent', 'cgdagentlocal', 'brigadeagent', 'codisagent', 'sdisagent', 'compagnieagentlocal', 'cisagent',
                  'ddspagent', 'commissariatagentlocal', 'serviceagent', 'federationagent', 'mairieagent', 'cgagent',
                  'cgsuperieuragent', 'cgserviceagentlocal', 'organisateur', 'instructeur', 'edsragentlocal', 'secouriste', 'observateur']
    AGENT_TYPES = ['ggdagent', 'edsragent', 'brigadeagent', 'codisagent', 'sdisagent', 'cisagent',
                   'ddspagent', 'serviceagent', 'federationagent', 'mairieagent', 'cgagent', 'cgsuperieuragent']
    AGENTLOCAL_TYPES = ['cgdagentlocal', 'commissariatagentlocal', 'cgserviceagentlocal', 'edsragentlocal', 'compagnieagentlocal']
    OBSERVATEUR_TYPES = ['secouriste', ]
    DIRECT_TYPES = ['organisateur', 'instructeur', 'observateur']
    ROLE_CHOICES = [
                    ['organisateur', "Organisateur"],
                    ['instructeur', "Instructeur"],
                    ['mairieagent', "Agent mairie"],
                    ['serviceagent', "Agent service simple"],
                    ['ggdagent', "Gendarmerie : agent GGD"],
                    ['edsragent', "Gendarmerie : agent EDSR (avis)"],
                    ['edsragentlocal', "Gendarmerie : agent EDSR (préavis)"],
                    ['cgdagentlocal', "Gendarmerie : agent CGD"],
                    ['brigadeagent', "Gendarmerie : agent brigade"],
                    ['ddspagent', "Police : agent DDSP"],
                    ['commissariatagentlocal', "Police : agentl commissariat"],
                    ['sdisagent', "Pompier : agent SDIS"],
                    ['compagnieagentlocal', "Pompier : agent compagnie"],
                    ['cisagent', "Pompier : agent CIS"],
                    ['codisagent', "Pompier : agent CODIS"],
                    ['cgagent', "CD : agent départemental"],
                    ['cgsuperieuragent', "CD : agent départemental N+1"],
                    ['cgserviceagentlocal', "CD : agent service"],
                    ['federationagent', "Agent fédération"],
                    ]

    SERVICES = {'ggdagent': 'ggd', 'edsragent': 'edsr', 'edsragentlocal': 'edsr', 'cgdagentlocal': 'cgd', 'brigadeagent': 'brigade', 'codisagent': 'codis',
                'sdisagent': 'sdis', 'compagnieagentlocal': 'compagnie', 'cisagent': 'cis', 'ddspagent': 'ddsp', 'commissariatagentlocal': 'commissariat',
                'serviceagent': 'service', 'federationagent': 'federation', 'mairieagent': 'commune', 'cgagent': 'cg', 'cgsuperieuragent': 'cg',
                'cgserviceagentlocal': 'cg_service', 'instructeur': 'prefecture', 'organisateur': 'structure'}
    ROLE_CHOICES_DICT = OrderedDict(ROLE_CHOICES)

    # Fonctions
    @staticmethod
    def get_role_names(user):
        """ Renvoie les chaînes correspondant aux rôles de l'utilisateur """
        if user.tableau_role is None:
            user.remplir_tableau_role()
        return user.tableau_role.split(',')

    @staticmethod
    def get_role_name(user):
        """ Renvoyer un nom de rôle pour l'utilisateur """
        roles = UserHelper.get_role_names(user)
        return roles[0] if roles else None

    @staticmethod
    def get_role_instance(user, role=None):
        """ Renvoyer l'objet de rôle pour l'utilisateur """
        if not role:
            if user.tableau_role is None:
                user.remplir_tableau_role()
            roles = user.tableau_role.split(',')
            for role in roles:
                # Tester les rôles dont le modèle est lié directement à User.agent
                try:
                    if getattr(user.agent, role) is not None:
                        return getattr(user.agent, role)
                except AttributeError:
                    pass
                # Tester les rôles dont le modèle est lié directement à User.agentlocal
                try:
                    if getattr(user.agentlocal, role) is not None:
                        return getattr(user.agentlocal, role)
                except AttributeError:
                    pass
                # Tester les rôles dont le modèle est lié directement à User
                try:
                    if getattr(user, role) is not None:
                        return getattr(user, role)
                except AttributeError:
                    pass
        else:
            try:
                if getattr(user.agent, role) is not None:
                    return getattr(user.agent, role)
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User.agentlocal
            try:
                if getattr(user.agentlocal, role) is not None:
                    return getattr(user.agentlocal, role)
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User
            try:
                if getattr(user, role) is not None:
                    return getattr(user, role)
            except AttributeError:
                pass
        # TODO : lignes commentées à cause d'un bug suite à un tableau de rôle faussé, le user avait un rôle agent et un rôle agentlocal
        #  et seul le rôle agentlocal était existant, du coup la fonction renvoyait l'instance agentlocal à la place de l'instance agent
        #  => Bug dans l'admin utilisateurs à la page du user concerné : à supprimer après validation
        # Si les autres méthodes ne marchent pas, on suppose que parent_link a été utilisé
        # try:
        #     return user.agentlocal
        # except AttributeError:
        #     pass
        # try:
        #     return user.agent
        # except AttributeError:
        #     pass
        return None

    @staticmethod
    def has_role(user, role):
        """ Renvoyer si l'utilisateur possède un rôle, donné par son nom par UserHelper.ROLE_TYPES """
        # Tester les rôles dont le modèle est lié directement à User
        if user.is_anonymous:
            return False
        try:
            if getattr(user, role) is not None:
                return True
        except AttributeError:
            pass
        # Tester les rôles dont le modèle est lié directement à User.agent
        try:
            if getattr(user.agent, role) is not None:
                return True
        except AttributeError:
            pass
        # Tester les rôles dont le modèle est lié directement à User.agentlocal
        try:
            if getattr(user.agentlocal, role) is not None:
                return True
        except AttributeError:
            pass
        return False

    @staticmethod
    def get_service_instance(user):
        """ Renvoyer l'instance de service correspondant à l'utilisateur """
        role_instance = UserHelper.get_role_instance(user)
        role_field = UserHelper.get_role_name(user)
        service_field = UserHelper.SERVICES.get(role_field, None)
        try:
            if service_field is not None and role_instance is not None:
                return getattr(role_instance, service_field)
            else:
                return None
        except AttributeError:
            return None
