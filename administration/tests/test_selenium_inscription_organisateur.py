import time
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.core import mail
from django.test import tag

from selenium.webdriver.firefox.webdriver import WebDriver

from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from organisateurs.factories import StructureTypeFactory
from core.models.instance import Instance


class InscriptionOrganisateur(StaticLiveServerTestCase):
    """
    Test de l'inscription d'un agent de préfecture
    """
    DELAY = 0.4

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
        """
        print()
        print('============ Inscription organisateur (Sel) =============')
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(600, 800)

        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__workflow_ggd=Instance.WF_GGD_SUBEDSR,
                                            instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        cls.prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.structure_type = StructureTypeFactory()

        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """
        cls.selenium.quit()
        super().tearDownClass()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.find_element_by_css_selector("a").click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    @tag('selenium')
    def test_inscription_organisateur(self):
        """
        Test complet de l'inscription d'un agent de préfecture
        """

        print('**** test 1 remplissage formulaire d\'inscription organisateur ****')

        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/organisateur'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Structure organisatrice', self.selenium.page_source)

        struc = self.selenium.find_element_by_id('id_structure_name')
        struc.send_keys('structure_test')
        time.sleep(self.DELAY)

        self.chosen_select('id_type_of_structure_chosen', self.structure_type.type_of_structure)
        time.sleep(self.DELAY*2)

        address = self.selenium.find_element_by_id('id_address')
        address.send_keys('6 rue vauban')
        time.sleep(self.DELAY)

        self.chosen_select('id_departement_chosen', '42')
        time.sleep(self.DELAY)

        self.chosen_select('id_commune_chosen', 'Bard')
        time.sleep(self.DELAY)

        tel = self.selenium.find_element_by_id('id_phone')
        tel.send_keys('0655555555')
        time.sleep(self.DELAY)

        prenom = self.selenium.find_element_by_id('id_first_name')
        prenom.send_keys('Jean')
        time.sleep(self.DELAY)

        nom = self.selenium.find_element_by_id('id_last_name')
        nom.send_keys('Dupont')
        time.sleep(self.DELAY)

        id_user = self.selenium.find_element_by_id('id_username')
        id_user.send_keys('jandup')
        time.sleep(self.DELAY)

        email = self.selenium.find_element_by_id('id_email')
        email.send_keys('jandup@manifestationsportive.fr')
        time.sleep(self.DELAY)

        pass1 = self.selenium.find_element_by_id('id_password1')
        pass1.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)

        pass2 = self.selenium.find_element_by_id('id_password2')
        pass2.send_keys('azeaz5646dfzdsf')

        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_xpath("//span[contains(text(),'En cochant cette case')]").click()
        time.sleep(self.DELAY)

        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        print('**** test 2 vérification ****')

        self.assertIn('Vérifiez votre adresse email', self.selenium.page_source)
        self.assertIn('E-mail de confirmation envoyé à jandup@manifestationsportive.fr', self.selenium.page_source)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, '[example.com] Confirmer l\'adresse email')
        self.assertEqual(mail.outbox[0].to, ['jandup@manifestationsportive.fr'])
        url = mail.outbox[0].body.split()[-1]
        self.selenium.get(url)

        print('**** test 3 confirmation ****')

        self.assertIn('Confirmer une adresse email', self.selenium.page_source)
        self.assertIn('>jandup@manifestationsportive.fr</a> est bien une adresse email de jandup.', self.selenium.page_source)

        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY)

        self.assertIn('Adresse email confirmée', self.selenium.page_source)
        self.assertIn('Vous avez confirmé jandup@manifestationsportive.fr.', self.selenium.page_source)

        print('**** test 4 echec inscription ****')

        print('>>> email déjà utilisé')
        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/organisateur'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Structure organisatrice', self.selenium.page_source)

        id_user = self.selenium.find_element_by_id('id_username')
        id_user.send_keys('jannotdup')
        time.sleep(self.DELAY)

        email = self.selenium.find_element_by_id('id_email')
        email.send_keys('jandup@manifestationsportive.fr')
        time.sleep(self.DELAY)

        pass1 = self.selenium.find_element_by_id('id_password1')
        pass1.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)
        self.assertIn('Un autre utilisateur utilise déjà cette adresse e-mail', self.selenium.page_source)

        print('>>> username déjà utilisé')
        self.selenium.get('%s%s' % (self.live_server_url, '/inscription/organisateur'))
        self.assertIn('Inscription', self.selenium.page_source)
        self.assertIn('Structure organisatrice', self.selenium.page_source)

        id_user = self.selenium.find_element_by_id('id_username')
        id_user.send_keys('jandup')
        time.sleep(self.DELAY)

        prenom = self.selenium.find_element_by_id('id_first_name')
        prenom.send_keys('Jeannot')
        time.sleep(self.DELAY)
        self.assertIn('Un utilisateur avec ce nom existe déjà', self.selenium.page_source)

        print('**** test 5 connexion réussie ****')

        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element_by_name("login")
        pseudo_input.send_keys('jandup')
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('azeaz5646dfzdsf')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY * 4)
        self.assertIn('Tableau de bord organisateur', self.selenium.page_source)
        self.assertIn('Connexion avec jandup réussie.', self.selenium.page_source)
