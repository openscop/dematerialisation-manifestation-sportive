from django.core.management.base import BaseCommand
from django.db import transaction

from evenements.models import Manif

class Command(BaseCommand):
    """
    commande pour créer les delais des manifestations.
    """
    def handle(self, *args, **options):
        Manif.objects.last().ecrire_delai(all=True)










