# coding: utf-8
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.forms import models
from django.forms.fields import TypedMultipleChoiceField
from django.utils import timezone
from django.contrib.auth.models import Group
from ajax_select.fields import AutoCompleteSelectMultipleField

from aide.models.aide import PageAide, PanneauPageAide, OngletPageAide, NotePageAide
from core.util.user import UserHelper


class PageAideForm(models.ModelForm):
    """ Formulaire admin des pages d'aide """
    model = PageAide

    # Champs
    role = TypedMultipleChoiceField(choices=UserHelper.ROLE_CHOICES, required=False, label="Rôle")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groupe'] = TypedMultipleChoiceField(choices=list(Group.objects.all().values_list('id', 'name')),
                                                         required=False, label="Groupes")
        if 'role' in self.initial:
            self.initial['role'] = self.initial['role'].split(',')
        if 'groupe' in self.initial and self.initial['groupe']:
            self.initial['groupe'] = self.initial['groupe'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        self.instance.groupe = ','.join(sorted(self.cleaned_data['groupe']))
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'contentbefore': CKEditorUploadingWidget(), 'contentafter': CKEditorUploadingWidget()}
        labels = {
            'get_contentbefore_html': 'Contenu avant',
            'get_contentafter_html': 'Contenu après',
        }


class PanneauPageAideForm(models.ModelForm):
    """ Formulaire admin des panneaux de page d'aide """
    model = PanneauPageAide

    # Champs
    role = TypedMultipleChoiceField(choices=UserHelper.ROLE_CHOICES, required=False, label="Rôle")
    tabs = AutoCompleteSelectMultipleField('tabs', required=False, help_text=None)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groupe'] = TypedMultipleChoiceField(choices=list(Group.objects.all().values_list('id', 'name')),
                                                         required=False, label="Groupes")
        if 'role' in self.initial and self.initial['role']:
            self.initial['role'] = self.initial['role'].split(',')
        if 'groupe' in self.initial and self.initial['groupe']:
            self.initial['groupe'] = self.initial['groupe'].split(',')

    def clean(self):
        cleaned_data = super().clean()
        # Le champ de django-ajax-selects utilisé pour 'tabs' ne renvoie que des ids
        # alors que les données initiales sont des objets 'tab'
        # du coup, une nouvelle entrée dans la table de jonction est créée et on perd la position du panneau
        # convertir les données pour que le formulaire ne les considère pas changées
        if 'tabs' in self.initial:
            if cleaned_data['tabs'] == [str(tab.id) for tab in self.initial['tabs']]:
                self.changed_data.remove('tabs')
        cleaned_data['tabs'] = list(OngletPageAide.objects.filter(id__in=cleaned_data['tabs']))
        return cleaned_data

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.updated = timezone.now()
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        self.instance.groupe = ','.join(sorted(self.cleaned_data['groupe']))
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'content': CKEditorUploadingWidget()}


class NotePageAideForm(models.ModelForm):
    """ Formulaire admin des nouveautés """
    model = NotePageAide

    # Champs
    role = TypedMultipleChoiceField(choices=UserHelper.ROLE_CHOICES, required=False, label="Rôle")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groupe'] = TypedMultipleChoiceField(choices=list(Group.objects.all().values_list('id', 'name')),
                                                         required=False, label="Groupes")
        self.fields['departements'].help_text = None
        if 'role' in self.initial and self.initial['role']:
            self.initial['role'] = self.initial['role'].split(',')
        if 'groupe' in self.initial and self.initial['groupe']:
            self.initial['groupe'] = self.initial['groupe'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        self.instance.groupe = ','.join(sorted(self.cleaned_data['groupe']))
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'content': CKEditorUploadingWidget()}
