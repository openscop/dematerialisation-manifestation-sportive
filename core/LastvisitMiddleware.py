import re
from django.utils import timezone
from django.shortcuts import redirect, reverse
from django.contrib import messages
from django.http import HttpResponse
from django.contrib.auth import logout

from evenements.models import Manif
from instructions.models import Instruction, Avis, PreAvis
from configuration import settings


class LastvisitMiddleware:
    """
    Middleware servant à enregistrer l'heure de connexion et le dossier consulté
    pour calculer le nombre d'utilisateurs connectés et le nombre d'utilisateurs sur un dossier
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user = request.user
        if user.is_authenticated:
            # Verification des ip superuser et admin d'instance
            if user.is_superuser or user.has_group("Administrateurs technique"):
                x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
                if x_forwarded_for:
                    ip = x_forwarded_for.split(',')[0]
                else:
                    ip = request.META.get('REMOTE_ADDR')
                tableau_universel = settings.ADMIN_INSTANCE_IP
                if ip not in tableau_universel:
                    logout(request)
                    messages.error(request, 'Vous avez été déconnecté automatiquement ! Cette adresse ip n\'est pas autorisée.')
                    return redirect(reverse('account_login'))
            # Enregistrement de l'heure de connexion
            user.last_visit = timezone.now()
            # Enregistrement du dossier consulté
            url = request.path.split('/')
            pk_manif = 0
            # Urls de l'appli "instructions"
            if url[1] == 'instructions':
                # pk en seconde position => instruction
                if url[2].isdecimal():
                    # Vérifier l'objet
                    if Instruction.objects.filter(pk=url[2]).exists():
                        pk_manif = Instruction.objects.get(pk=url[2]).manif.pk
                # Préavis demandé et pk en troisième position
                elif url[2] == 'preavis' and url[3].isdecimal():
                    # Vérifier l'objet
                    if PreAvis.objects.filter(pk=url[3]).exists():
                        pk_manif = PreAvis.objects.get(pk=url[3]).avis.instruction.manif.pk
                #  Avis demandé et pk en troisième position
                elif url[2] == 'avis'and url[3].isdecimal():
                    # Cas particulier : transfert de pièce jointe de préavis sur un avis
                    if url[4] and request.GET == 'addfile':
                        #  Vérifier l'objet
                        if PreAvis.objects.filter(pk=url[3]).exists():
                            pk_manif = PreAvis.objects.get(pk=url[3]).avis.instruction.manif.pk
                    else:
                        #  Vérifier l'objet
                        if Avis.objects.filter(pk=url[3]).exists():
                            pk_manif = Avis.objects.get(pk=url[3]).instruction.manif.pk
                elif url[2] == 'add' and url[3].isdecimal():
                    if Manif.objects.filter(pk=url[3]).exists():
                        pk_manif = url[3]
            # Urls de l'appli "evenements" limité aux accès cerfas (Dnm, Dcnm, Avtm ...)
            elif re.match(r'^[A,D]\w+', url[1]):
                if len(url) > 2 and url[2].isdecimal():
                    manif = Manif.objects.filter(pk=url[2])
                    pk_manif = manif.first().pk if manif else None
            if pk_manif:
                user.last_manif = pk_manif
            user.save()
        else:
            # Cas d'accès aux urls quand un utilisateur a été déconnecté par la plateforme
            url = request.path.split('/')
            if url[1] == 'accounts':
                # Pour l'url de changement de password
                if not (len(url) > 2 and url[2] in ['login', 'confirm-email', 'email_confirmed', 'inactive']) and not (len(url) > 2 and url[2] == 'password' and len(url) > 3 and url[3] == "reset"):
                    messages.error(request, 'Vous avez été déconnecté automatiquement ! Veuillez vous reconnecter.')
                    return redirect(reverse('account_login'))
            if 'HTTP_X_REQUESTED_WITH' in request.META and request.META['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest':
                # Pour les appels Ajax
                if url[1] in ['messagerie', 'instructions']:
                    messages.error(request, 'Vous avez été déconnecté automatiquement ! Veuillez vous reconnecter.')
                    return HttpResponse(status=401)

        return self.get_response(request)
