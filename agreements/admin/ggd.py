# coding: utf-8
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from agreements.models.ggd import GGDAvis
from core.util.admin import RelationOnlyFieldListFilter
from sub_agreements.admin import PreAvisCGDInline


@admin.register(GGDAvis)
class GGDAvisAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin de l'avis GGD """
    list_display = ['pk', 'state', 'request_date', 'reply_date', 'favorable', 'authorization',
                    'authorization__manifestation__departure_city__arrondissement__departement']
    list_filter = ['request_date', 'reply_date', 'favorable', 'state',
                   ('authorization__manifestation__departure_city__arrondissement__departement', RelationOnlyFieldListFilter)]
    inlines = [PreAvisCGDInline]
    search_fields = ['authorization__manifestation__name__unaccent', 'state', 'authorization__manifestation__description__unaccent']
    list_per_page = 25

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(authorization__manifestation__departure_city__arrondissement__departement=request.user.get_departement())
        return queryset
