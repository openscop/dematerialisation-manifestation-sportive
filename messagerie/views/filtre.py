from django.views.generic import View
from django.http import HttpResponse, HttpResponseForbidden
from django.core import serializers
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404

from messagerie.models.filtre import Filtre
from core.util.permissions import login_requis


@method_decorator(login_requis(), name='dispatch')
class FiltrePerso(View):
    """
    affichage et enregistrement des filtres
    on rentre le lien ajax dans la base
    on va cherche l'ensemble des filtres de l'utilisateurs l'ajax s'occupera du reste
    """
    def get(self, request):
        filtres = Filtre.objects.filter(utilisateur=request.user).all()
        data = serializers.serialize("json", filtres)
        return HttpResponse(data)

    def post(self, request):
        count = Filtre.objects.filter(utilisateur=request.user).count()
        if count < 6:
            filtre = Filtre(utilisateur=request.user, titre=request.POST['titre'], option=request.POST['option'])
            filtre.save()
            return HttpResponse(200)
        else:
            return HttpResponse('Vous avez trop de filtres !')


@method_decorator(login_requis(), name='dispatch')
class FiltrePersoSupprime(View):
    """
    suppression d'un filtre custom
    """
    def get(self, request):
        pk = request.GET.get('pk')
        filtre = get_object_or_404(Filtre, pk=pk)
        if filtre.utilisateur == request.user:
            filtre.delete()
            return HttpResponse(200)
        else:
            return HttpResponseForbidden(403)
