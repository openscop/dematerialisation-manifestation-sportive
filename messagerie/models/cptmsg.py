from django.db import models
from configuration import settings


class CptMsg(models.Model):
    """
    filtre d'affichage custom des utilisateurs
    """

    utilisateur = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='cpt_msg')
    conversation = models.IntegerField(default=0)
    action = models.IntegerField(default=0)
    info_suivi = models.IntegerField(default=0)
    news = models.IntegerField(default=0)

    def __str__(self):
        return self.utilisateur.username

    def update_cpt(self):
        user = self.utilisateur
        from messagerie.models import Enveloppe
        self.conversation = Enveloppe.objects.filter(
            destinataire_id=user, lu_datetime__isnull=True, lu_requis=True, type='conversation').count()
        self.action = Enveloppe.objects.filter(
            destinataire_id=user, lu_datetime__isnull=True, lu_requis=True, type='action').count()
        self.info_suivi = Enveloppe.objects.filter(
            destinataire_id=user, lu_datetime__isnull=True, lu_requis=True, type='info_suivi').count()
        self.news = Enveloppe.objects.filter(
            destinataire_id=user, lu_datetime__isnull=True, lu_requis=True, type='news').count()
        self.save()
