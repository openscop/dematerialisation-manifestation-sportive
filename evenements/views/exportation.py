import os
import zipfile
from io import BytesIO
import xhtml2pdf.pisa as pisa
from PyPDF3 import PdfFileMerger
from preview_generator.manager import PreviewManager
from PIL import Image
from func_timeout import func_timeout

from django.shortcuts import render
from django.http import HttpResponse
from django.template.loader import get_template
from django.utils.decorators import method_decorator
from django.core import serializers
from django.views.generic import View
from django.shortcuts import get_object_or_404

from evenements.models import Manif
from instructions.models import *
from organisateurs.decorators import verifier_proprietaire
from instructions.decorators import verifier_secteur_instruction, verifier_service_avis, verifier_service_preavis
from messagerie.models import Message
from configuration import settings

"""
Détail des librairies :
- Zipfile : permet de gérer les fichiers zip en python
- Pisa : permet de créer un PDF à partir d'un template django
- ByteIO : permet à Pisa d'écrire le fichier PDF
- PreviewManager : permet ici de transformer des fichiers en PDF
- PILLOW : permet ici de transformer les images en PDF ce que ne fait pas PreviewManager

Fonctionnement global :
L'utilisateur selon son rôle va arriver sur une des classes "export_pour". 
On vérifie s’il souhaite les avis ou préavis correspondant à ses droits d'accès et s’il veut le zip ou le pdf.
Puis on passe par la classe Exportdossier. L'initialisation nous permet de récupérer la manif et de créer ou nettoyer
les dossiers utilisés.
Ensuite, on va créer le résumé pdf notamment avec la classe RenderSave; On y intégrera s'il y a les avis ou préavis.
Après nous regroupons tous les fichiers concernant une manifestation : fichiers joints, doc officiels, doc complémentaires.
Pièces jointes de l'avis ou préavis sélectionné, dans un tableau pour les mettre dans le zip ou le pdf.

Si le format de l'export est zip, on va d'abord chercher le fichier GPS des parcours associés à cette manifestation puis on l'intègre au tableau des fichiers.
On met le résumé puis tous les fichiers présents dans le tableau dans le ZIP.
On finit par répondre en donnant le zip.

Si le format de l'export est PDF, on n'intègre pas de carto, car il est impossible d'obtenir quelque chose convertissable en pdf.
On va parcourir le tableau de fichier, pour transformer toutes les pièces jointes en pdf, soit pour les images en utilisant Pillow
soit en utilisant PreviewManager pour les fichiers text et documents docx odt etc...
On met tous ces fichiers transformés dans un tableau.
Puis on fusionne le résumé avec tous les fichiers pdf.
On finit par répondre en donnant ce pdf.
"""


def link_callback(uri, rel):
    """
    afficher les images dans le pdf, Il transforme l'uri dans le template dans le bon format
    :param uri:
    :param rel:
    :return:
    """
    s_url = settings.STATIC_URL  # Typically /static/
    s_root = settings.STATIC_ROOT  # Typically /home/userX/project_static/
    m_url = settings.MEDIA_URL  # Typically /static/media/
    m_root = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

    if uri.startswith(m_url):
        path = os.path.join(m_root, uri.replace(m_url, ""))
    elif uri.startswith(s_url):
        path = os.path.join(s_root, uri.replace(s_url, ""))
    else:
        return uri
    if not os.path.isfile(path):
        raise Exception(
            'media URI must start with %s or %s' % (s_url, m_url)
        )
    return path


def file_to_pdf(file, path):
    """
    Transformer des fichier (sauf image) en pdf
    """
    manager = PreviewManager(path, create_folder=True)
    temp = manager.get_pdf_preview(file)
    return temp


def convert_avec_libreoffice(file, path):
    """
    Transformer des fichiers en pdf avec libreoffice
    :return:
    """
    os.system('soffice --convert-to pdf ' + file + ' --outdir ' + path)
    # verification que le fichier a bien été généré (au cas où office plante)
    if os.path.exists(path + file.split('/')[-1].replace(file.split('.')[-1], '') + 'pdf'):
        return path + file.split('/')[-1].replace(file.split('.')[-1], '') + 'pdf'
    else:
        return False

class Rendersave:
    """
    créer un pdf et le sauvegarder
    """
    @staticmethod
    def render(path: str, params: dict, name: str):
        template = get_template(path)
        html = template.render(params)
        file = open(settings.MEDIA_ROOT+name+'.pdf', "w+b")
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), dest=file, link_callback=link_callback)
        file.close()
        if not pdf.err:
            return file
        else:
            return pdf.err


def creer_pdf_resume(template, data, nom):
    """
    Fonction pour contourner le problème d'enregistrement du pdf en forçant plusieurs tentative
    TODO à voir après le passage dans un environnement séparé
    """
    tentative = 0
    pdf = None
    while tentative < 5 and not pdf:
        tentative += 1
        try:
            pdf = Rendersave.render(template, data, nom)
        except:
            pass
    return pdf


class Exportdossier:
    """
    envoi du dossier pdf ou zip
    """

    @staticmethod
    def export(request, pk, avis=None, preavis=None, preferelezip=False):
        """
        Export de la manif. Nous allons
        - préparer le pdf
        - Créer une liste de fichier à mettre dans le zip ou le pdf
        - Créer soi le zip soi le pdf
        """
        # initialisation
        manif = get_object_or_404(Manif, pk=pk)
        cerfa = manif.cerfa

        # création d'un msg traçabilité

        if avis or preavis:
            titre = "Export avec avis ou préavis sous zip" if preferelezip else "Export avec avis ou préavis sous pdf"
        else:
            titre = "Export du dossier simple sous zip" if preferelezip else "Export du dossier simple sous pdf"

        Message.objects.creer_et_envoyer('tracabilite', None, [request.user], titre, titre, manifestation_liee=manif)

        # si pas de dossier tmp on le créé
        if os.path.exists(settings.MEDIA_ROOT + "exportation"):
            pass
        else:
            os.makedirs(settings.MEDIA_ROOT + "exportation")

        pathdelamanif = settings.MEDIA_ROOT + "exportation/" + str(manif.pk) + "/"

        # on nettoie ou crée le dossier temporaire exclusif à la manif
        if os.path.exists(pathdelamanif):
            for file in os.listdir(pathdelamanif):
                file_path = os.path.join(pathdelamanif, file)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                except Exception as e:
                    print(e)
        else:
            os.makedirs(pathdelamanif)

        # Préparation du pdf du résumé de la manifestation
        data = serializers.serialize('python', [manif, ])
        parametres = {
            'manif': manif,
            'datas': data,
            'export': True
        }
        nom_zip = "exportation/" + str(manif.pk) + "/detail_manifestation"
        pdf = creer_pdf_resume('evenements/resume_pdf.html', parametres, nom_zip)

        if avis:
            parametres = {
                'manif': manif,
                'liste_avis': avis,
                'export': True
            }
            nom_zip = "exportation/" + str(manif.pk) + "/avis"
            pdf_avis = creer_pdf_resume('evenements/avis_pdf.html', parametres, nom_zip)

        if preavis:
            parametres = {
                'manif': manif,
                'liste_avis': preavis,
                'export': True
            }
            nom_zip = "exportation/" + str(manif.pk) + "/preavis"
            pdf_preavis = creer_pdf_resume('evenements/avis_pdf.html', parametres, nom_zip)

        # on prend tout les fichier en rapport avec la manifestationd dans un tableau pour ensuite le mettre dans le zip ou pdf
        fichier = []
        fichier.append(cerfa.cartographie.path) if cerfa.cartographie else ''
        fichier.append(cerfa.reglement_manifestation.path) if cerfa.reglement_manifestation else ''
        fichier.append(cerfa.engagement_organisateur.path) if cerfa.engagement_organisateur else ''
        fichier.append(cerfa.disposition_securite.path) if cerfa.disposition_securite else ''
        fichier.append(cerfa.topo_securite.path) if cerfa.topo_securite else ''
        fichier.append(cerfa.presence_docteur.path) if cerfa.presence_docteur else ''
        fichier.append(cerfa.certificat_assurance.path) if cerfa.certificat_assurance else ''
        fichier.append(cerfa.docs_additionels.path) if cerfa.docs_additionels else ''
        fichier.append(cerfa.charte_dispense_site_n2k.path) if cerfa.charte_dispense_site_n2k else ''
        fichier.append(cerfa.convention_police.path) if cerfa.convention_police else ''
        fichier.append(cerfa.carte_zone_public.path) if hasattr(cerfa, "carte_zone_public") and cerfa.carte_zone_public else ""
        fichier.append(cerfa.commissaires.path) if hasattr(cerfa, "commissaires") and cerfa.commissaires else ""
        fichier.append(cerfa.certificat_organisateur_tech.path) if hasattr(cerfa, "certificat_organisateur_tech") and cerfa.certificat_organisateur_tech else ""
        fichier.append(cerfa.itineraire_horaire.path) if hasattr(cerfa,  "itineraire_horaire") and cerfa.itineraire_horaire else ''
        fichier.append(cerfa.participants.path) if hasattr(cerfa, "participants") and cerfa.participants else ""
        fichier.append(cerfa.avis_federation_delegataire.path) if hasattr(cerfa, 'avis_federation_delegataire') and cerfa.avis_federation_delegataire else ""
        fichier.append(cerfa.plan_masse.path) if hasattr(cerfa, 'plan_masse') and cerfa.plan_masse else ""
        fichier.append(cerfa.liste_signaleurs.path) if hasattr(cerfa, "liste_signaleurs") and cerfa.liste_signaleurs else ''
        fichier.append(cerfa.dossier_tech_cycl.path) if hasattr(cerfa, 'dossier_tech_cycl') and cerfa.dossier_tech_cycl else ''

        pj = fichier
        # documents complementaires
        doccomple = []
        for doc in manif.documentscomplementaires.all():
            fichier.append(doc.document_attache.path) if doc.document_attache else ''
            doccomple.append(doc.document_attache.path) if doc.document_attache else ''
        # documents officiels
        docoffi = []
        if hasattr(manif, "instruction"):
            for doc in manif.instruction.documents.all():
                fichier.append(doc.fichier.path) if doc.fichier else ''
                docoffi.append(doc.fichier.path) if doc.fichier else ''
        # avis
        avispj = []
        if avis:
            for avi in avis:
                for doc in PieceJointeAvis.objects.filter(avis=avi):
                    fichier.append(doc.fichier.path) if doc.fichier else ""
                    avispj.append(doc.fichier.path) if doc.fichier else ""
        # preavis
        preavispj = []
        if preavis:
            for preavi in preavis:
                for doc in PieceJointeAvis.objects.filter(preavis=preavi):
                    fichier.append(doc.fichier.path) if doc.fichier else ""
                    preavispj.append(doc.fichier.path) if doc.fichier else ""
        cache_path = pathdelamanif
        # si l'option zip est faite
        if preferelezip:
            resume = []
            if pdf:
                resume.append(pdf.name)
            if avis and pdf_avis:
                resume.append(pdf_avis.name)
            if preavis and pdf_preavis:
                resume.append(pdf_preavis.name)

            # cartographie
            cartos = manif.get_parcours_manif()
            for carto in cartos:
                os.system('cd ' + pathdelamanif + " && wget  --no-check-certificate -O " + str(carto)+".gpx \"" +
                          settings.OPENRUNNER_HOST + "kml/exportImportGPX.php?rttype=0&id=" + str(carto)+"\"")
                fichier.append(pathdelamanif+str(carto)+".gpx")

            # creation du zip
            nom_zip = "exportation/" + str(manif.pk) + "/" + manif.nom.replace(' ', '_').replace('/', '_')
            zip_filename = settings.MEDIA_ROOT + nom_zip + ".zip"
            zipf = zipfile.ZipFile(zip_filename, "w", zipfile.ZIP_DEFLATED)
            nomgrosdossier = str(manif.date_debut.date()) + "_dossier_" + manif.nom.replace(' ', '_').replace('/', '_')[:20] + '/'
            # remplissage du zip avec tout les fichiers
            for file in resume:
                if os.path.exists(file):
                    dir, base_filename = os.path.split(file)
                    os.chdir(dir)
                    zipf.write(base_filename, nomgrosdossier+base_filename)
            for file in pj:
                if os.path.exists(file):
                    dir, base_filename = os.path.split(file)
                    os.chdir(dir)
                    zipf.write(base_filename, nomgrosdossier+"piece_jointe/"+base_filename)
            for file in doccomple:
                if os.path.exists(file):
                    dir, base_filename = os.path.split(file)
                    os.chdir(dir)
                    zipf.write(base_filename, nomgrosdossier+"doc_complementaire/"+base_filename)
            for file in docoffi:
                if os.path.exists(file):
                    dir, base_filename = os.path.split(file)
                    os.chdir(dir)
                    zipf.write(base_filename,  nomgrosdossier+"doc_officiel/"+base_filename)
            for file in avispj:
                if os.path.exists(file):
                    dir, base_filename = os.path.split(file)
                    os.chdir(dir)
                    zipf.write(base_filename,  nomgrosdossier+"doc_avis/"+base_filename)
            for file in preavispj:
                if os.path.exists(file):
                    dir, base_filename = os.path.split(file)
                    os.chdir(dir)
                    zipf.write(base_filename,  nomgrosdossier+"doc_preavis/"+base_filename)
            zipf.close()

            # envoi du zip
            f = open(zip_filename, 'rb')
            response = HttpResponse(f, content_type='application/x-zip')
            response['Content-Disposition'] = 'attachment; filename="' + str(manif.date_debut.date()) + "_dossier_" +\
                                              manif.nom.replace(' ', '_').replace('/', '_') + '.zip"'
            return response

        # si option pdf

        else:
            tableau_pdf = []
            if pdf:
                tableau_pdf.append(pdf.name)
            if avis and pdf_avis:
                tableau_pdf.append(pdf_avis.name)
            if preavis and pdf_preavis:
                tableau_pdf.append(pdf_preavis.name)



            # ici on transforme tout en pdf
            for file in fichier:
                # transformation des images
                if file.split('.')[-1] == "jpg" or file.split('.')[-1] == "jpeg" or file.split('.')[-1] == "png":
                    if os.path.exists(file):
                        image = Image.open(file)
                        im1 = image.convert('RGB')
                        im1.save(cache_path+file.split('/')[-1]+'.pdf')
                        tableau_pdf.append(cache_path+file.split('/')[-1]+'.pdf')
                else:
                    if os.path.exists(file):
                        # convertion via libre office
                        try:
                            result = func_timeout(10, convert_avec_libreoffice, kwargs={'file': file, 'path': pathdelamanif})
                            if result:
                                tableau_pdf.append(result)
                            # conversion via preview manager
                        except:
                            try:
                                result = func_timeout(10, file_to_pdf, kwargs={'file': file, 'path': cache_path})
                                if result:
                                    tableau_pdf.append(result)
                            except:
                                # rien ne fonctionne
                                pass

            # fusion dans un pdf final
            # false pour eviter les crashs de pdf
            merger = PdfFileMerger(strict=False)
            for file in tableau_pdf:
                merger.append(file)
            try:
                merger.write(cache_path+"export"+manif.nom.replace(' ', '_').replace('/', '_')+".pdf")

                # envoi du pdf
                f = open(cache_path+"export"+manif.nom.replace(' ', '_').replace('/', '_')+".pdf", 'rb')
                response = HttpResponse(f, content_type='application/pdf')
                response['Content-Disposition'] = 'attachment; filename="' + str(manif.date_debut.date()) + "_dossier_" +\
                                                  manif.nom.replace(' ', '_').replace('/', '_') + '.pdf"'
                return response
            except:
                return render(request, "evenements/erreur_exportation.html",
                              {'message': "Il n'est pas possible d'exporter ce dossier en PDF (sans doute à cause de la présence de pièce(s) jointe(s) incompatible(s). Vous pouvez néanmoins utiliser l'exportation en fichier ZIP."}, status=500)


@method_decorator(verifier_secteur_instruction(), name='dispatch')
class ExportPourInstructeur(View):

    def get(self, request, pk):
        manif = Manif.objects.get(pk=pk)
        if request.GET.get("avis", None) == "1":
            instru = manif.get_instruction()
            avis = Avis.objects.filter(instruction=instru)
        else:
            avis = None
        if request.GET.get("zip", None) == "1":
            preferelezip = True
        else:
            preferelezip = False
        return Exportdossier.export(request, manif.pk, preferelezip=preferelezip, avis=avis)


@method_decorator(verifier_service_avis(), name='dispatch')
class ExportPourAgent(View):

    def get(self, request, pk):
        manif = Manif.objects.get(instruction__avis__pk=pk)
        if request.GET.get('avis', None) == "1":
            avis = Avis.objects.filter(pk=pk)
            preavis = PreAvis.objects.filter(avis=avis.first())
        else:
            avis = None
            preavis = None
        if request.GET.get("zip", None) == "1":
            preferelezip = True
        else:
            preferelezip = False

        return Exportdossier.export(request, manif.pk, avis=avis, preavis=preavis, preferelezip=preferelezip)


@method_decorator(verifier_service_preavis(), name='dispatch')
class ExportPourAgentlocal(View):

    def get(self, request, pk):
        manif = Manif.objects.get(instruction__avis__preavis__pk=pk)
        if request.GET.get('avis', None) == "1":
            preavis = PreAvis.objects.filter(pk=pk)
        else:
            preavis = None
        if request.GET.get("zip", None) == "1":
            preferelezip = True
        else:
            preferelezip = False
        return Exportdossier.export(request, manif.pk, preavis=preavis, preferelezip=preferelezip)


@method_decorator(verifier_proprietaire, name='dispatch')
class ExportPourOrganisateur(View):

    def get(self, request, pk):
        if request.GET.get("zip", None) == "1":
            return Exportdossier.export(request, pk, preferelezip=True)
        else:
            return Exportdossier.export(request, pk)
