# coding: utf-8
from ajax_select.helpers import make_ajax_form
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin

from ..models import Activite
from ..models import Discipline


@admin.register(Discipline)
class DisciplineAdmin(ExportActionModelAdmin):
    """ Configuration admin """

    list_display = ['pk', 'name', 'milieu', 'motorise', 'get_aide_page', 'activités_liées']
    list_filter = ['motorise', 'milieu']
    list_editable = ['milieu']
    fields = ['name', 'milieu', 'motorise', 'activités_liées', 'aide']
    readonly_fields = ['activités_liées']
    search_fields = ['name__unaccent', 'federations__name__unaccent', 'activites__name__unaccent']
    list_per_page = 25
    list_display_links = ['pk', 'name']
    form = make_ajax_form(Discipline, {'aide': 'PageAide'})

    def activités_liées(self, obj):
        return " | ".join([a.name for a in obj.activites.all()])
    activités_liées.allow_tags = True


@admin.register(Activite)
class ActiviteAdmin(ExportActionModelAdmin):
    """ Configuration admin """

    list_display = ['pk', 'name', 'discipline']
    list_filter = ['discipline', 'discipline__motorise']
    search_fields = ['name__unaccent', 'discipline__name__unaccent']
    list_per_page = 25
    list_display_links = ['pk', 'name']
