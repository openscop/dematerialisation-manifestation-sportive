# coding: utf-8
from django.test import TestCase

from evaluation_incidence.factories import RNREvalFactory, N2kEvalFactory


class Natura2000EvaluationMethodTests(TestCase):
    def test_str_(self):
        '''
        __str__() should return the name of the evaluation
        '''
        n2000_evaluation = N2kEvalFactory.build()
        self.assertEqual(n2000_evaluation.__str__(), n2000_evaluation.manif.__str__())


class RNREvaluationMethodTests(TestCase):
    def test_str_(self):
        '''
        __str__() should return the name of the evaluation
        '''
        rnr_evaluation = RNREvalFactory.build()
        self.assertEqual(rnr_evaluation.__str__(), rnr_evaluation.manif.__str__())
