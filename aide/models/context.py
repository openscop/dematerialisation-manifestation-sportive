# coding: utf-8
from django.db import models
from django.template.loader import render_to_string

from core.util.user import UserHelper

class ContextHelpQuerySet(models.QuerySet):
    """ Manager des aides contextuelles """

    # Getter
    def for_identifier(self, identifier):
        """ Renvoie l'aide contextuelle pour un nom de contexte """
        names = [name.lower().strip()[len(ContextHelp.HELP_MARKER):] for name in identifier.split() if name.startswith(ContextHelp.HELP_MARKER)]
        return self.filter(name__in=names)

    def active(self):
        """ Renvoie les aides contextuelles actives """
        return self.filter(active=True)


class ContextHelp(models.Model):
    """
    Contenu d'aide contextuelle

    Lorsqu'un utilisateur survole des éléments du site, des messages d'aide peuvent
    être déclenchés. Par exemple, si l'on souhaite afficher un message d'aide lorsqu'on
    survole un bouton, il suffit d'exécuter les actions suivantes :
    - Ajouter une classe commençant par "ctx-help-" (ex. <button class="ctx-help-button1"...>)
    - Créer dans l'admin une aide contextuelle dont le nom correspond au nom de la classe du bouton, amputé de "ctx-help-" (ex. "button1")

    Il suffit alors de charger le fichier aide/contextHelp.jquery.js dans votre page pour faire fonctionner les tooltips d'aide.
    Il est possible de restreindre l'accès à l'aide contextuelle, afin qu'elle ne soit accessible qu'à certains rôles; ainsi
    l'on peut faire en sorte que, par exemple, seuls les agents EDSR soient capables de voir une information d'aide sur un élément.
    """

    # Constantes
    ROLE_HELP = "Rôles pouvant voir l'aide. Laisser vide pour tous."
    GROUPE_HELP ="Groupes d'utilisateurs pouvant voir l'aide. Laisser vide pour tous."
    PAGE_NAMES_HELP = "Un nom de page par ligne. Le nom technique des page est visible dans l'attribut 'class' de la balise 'body'. Par exemple : '<strong>home_page</strong>'"
    POSITIONS_HELP = "Une position par ligne. Chaque position est défini sous la forme '<strong>type d'insertion | sélecteur CSS</strong>'. Les types sont '<strong>after</strong>' et '<strong>in</strong>'. Par exemple : '<strong>after | #footer-info a</strong>' ou bien '<strong>in | h1</strong>'"
    HELP_MARKER = 'ctx-help-'

    # Champs
    active = models.BooleanField(default=True, verbose_name="Active")
    name = models.CharField(max_length=64, blank=False, unique=True, verbose_name="Nom de la classe dans le code HTML")
    text = models.TextField(blank=False, verbose_name="Texte")
    role = models.TextField(blank=True, help_text=ROLE_HELP, verbose_name="Rôles")
    groupe = models.TextField(blank=True, help_text=GROUPE_HELP, verbose_name="Groupes")
    departements = models.ManyToManyField('administrative_division.departement', blank=True,
                                          verbose_name="Départements")
    page_names = models.CharField(max_length=256, blank=True, verbose_name="Nom technique des pages où insérer dynamiquement l'icône d'aide", help_text=PAGE_NAMES_HELP)
    positions = models.CharField(max_length=512, blank=True, verbose_name="Position de l'aide dans la page", help_text=POSITIONS_HELP)

    nbr_vues = models.IntegerField(default=0, verbose_name="Nombre de fois où cette aide a été vue")
    objects = ContextHelpQuerySet.as_manager()

    # Getter
    def is_visible(self, request_or_user):
        """ Renvoyer si l'aide est visible pour l'utilisateur """
        if self.active is False:
            return False  # Cacher si inactif évidemment
        if request_or_user is not None:
            request_or_user = getattr(request_or_user, 'user', request_or_user)
            user_role = ""
            if hasattr(request_or_user, 'is_anonymous') and not request_or_user.is_anonymous:
                user_role = UserHelper.get_role_name(request_or_user)
            # Si le rôle de l'utilisateur est sélectionné (ou si aucun rôle n'est choisi, renvoyer True
            help_roles = [name for name in self.role.split(',') if name.strip()]
            if help_roles:
                return user_role in help_roles
            return True  # Visible pour tous les utilisateurs connectés si aucun rôle sélectionné
        help_roles = [name for name in self.role.split(',') if name.strip()]
        return not help_roles  # Invisible pour les non-inscrits si un rôle est sélectionné

    # Rendu
    def render(self, request=None):
        """ Effectuer le rendu du markup pour l'aide contextuelle """
        if self.is_visible(request):
            output = render_to_string('aide/context-help.html', {'help': self}, request=request)
            return output
        return None

    def position_as_list(self):
        positions = self.positions.split('\r\n')
        return [ dict(zip(('typeinsert', 'selecteur'),position.split(' | '))) for position in positions ]

    def __str__(self):
        return self.name

    # Meta
    class Meta:
        verbose_name = "Aide contextuelle"
        verbose_name_plural = "Aide contextuelle"
        app_label = 'aide'

