import re, time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import tag
from post_office.models import Email
from messagerie.models import Message
from django.test.utils import override_settings
from django.conf import settings

from .test_base_selenium import SeleniumCommunClass


class InstructionAjoutPjTests(SeleniumCommunClass):
    """
    Test des boutons Annulation des avis et préavis dans une instruction du circuit d'instance EDSR avec sélénium pour une Dcnm
        workflow_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    Test sur un service simple et un service complexe.
    Vérifie que le bouton Annulation est présent et envoie bien un email quand utilisé,
    et que le bouton n'est plus présent quand l'avis est distribué ou rendu et le préavis est rendu.
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ Annulation avis / préavis (Sel) =============')
        SeleniumCommunClass.init_setup(cls)
        cls.avis_nb = 2
        super().setUpClass()

    @tag('selenium')
    @override_settings(DEBUG=True)
    def test_Annulation(self):
        """
        Test d'u bouton Annulation pour avis et préavis pendant l'instruction
        """
        assert settings.DEBUG

        print('**** test 1 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        reponse = self.client.get('/tableau-de-bord-organisateur/liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file4:
            self.client.post(url_script.group('url'), {'cartographie': file4}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/itineraire_horaire.txt') as file5:
            reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file5}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 2 distribution avis ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes d\'avis', self.selenium.page_source)
        # Distribuer les demandes d'avis de l'événement
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes d\'avis').click()
        time.sleep(self.DELAY*4)
        self.assertIn('Choix des avis', self.selenium.page_source)
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis DDSP requis')]/..").click()
        self.selenium.execute_script("window.scroll(0, 1400)")
        time.sleep(self.DELAY)
        self.chosen_select('id_villes_concernees_chosen', 'Bard')
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('submit-id-save').click()
        time.sleep(self.DELAY * 6)

        Email.objects.all().delete()
        Message.objects.all().delete()

        # Vérifier les boutons d'annulation et annuler
        # Annuler la demande d'avis ddsp
        self.selenium.execute_script("window.scroll(0, 200)")
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'DDSP')][@data-toggle='collapse']")
        avis.click()
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY)
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'DDSP')][@data-toggle='collapse']/following::*")
        self.assertIn('Annuler cette demande', carte.text)
        carte.find_element_by_partial_link_text('Annuler cette demande').click()
        time.sleep(self.DELAY)
        carte.find_element_by_partial_link_text('Confirmer').click()
        time.sleep(self.DELAY)
        self.assertNotIn('DDSP', self.selenium.page_source)
        # Annuler la demande d'avis mairie
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'Mairie')][@data-toggle='collapse']")
        avis.click()
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY)
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Mairie')][@data-toggle='collapse']/following::*")
        self.assertIn('Annuler cette demande', carte.text)
        carte.find_element_by_partial_link_text('Annuler cette demande').click()
        time.sleep(self.DELAY)
        carte.find_element_by_partial_link_text('Confirmer').click()
        time.sleep(self.DELAY)
        self.assertNotIn('Mairie', self.selenium.page_source)
        # Annuler la demande d'avis fédération
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']")
        avis.click()
        self.selenium.execute_script("window.scroll(0, 400)")
        time.sleep(self.DELAY)
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']/following::*")
        self.assertIn('Annuler cette demande', carte.text)
        carte.find_element_by_partial_link_text('Annuler cette demande').click()
        time.sleep(self.DELAY)
        carte.find_element_by_partial_link_text('Confirmer').click()
        time.sleep(self.DELAY)
        self.assertNotIn('Fédération', self.selenium.page_source)
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 6)
        self.assertEqual(outbox[0].to, [self.agent_ddsp.email])
        self.assertEqual(outbox[1].to, [self.dep.ddsp.email])
        self.assertEqual(outbox[2].to, [self.agent_mairie.email])
        self.assertEqual(outbox[3].to, [self.commune.email])
        self.assertEqual(outbox[4].to, [self.agent_fede.email])
        self.assertEqual(outbox[5].to, [self.fede.email])
        self.assertEqual(messages[0].corps, 'Suppression de la demande d\'avis pour la manifestation Manifestation_Test')
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")

        Email.objects.all().delete()
        Message.objects.all().delete()
        # Vérification du status de l'instruction
        self.selenium.execute_script("window.scroll(0, 1200)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()

        # Redistribuer les demandes d'avis de l'événement
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes d\'avis').click()
        time.sleep(self.DELAY*4)
        self.assertIn('Choix des avis', self.selenium.page_source)
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis DDSP requis')]/..").click()
        self.selenium.execute_script("window.scroll(0, 1400)")
        time.sleep(self.DELAY)
        self.chosen_select('id_villes_concernees_chosen', 'Bard')
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('submit-id-save').click()
        time.sleep(self.DELAY * 6)
        self.assertIn('DDSP', self.selenium.page_source)
        self.assertIn('Mairie', self.selenium.page_source)


        # Vérifier le passage en encours et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1200)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 3 agent mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en nouveau
        self.connexion('agent_mairie')
        self.presence_avis('agent_mairie', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Valider l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_mairie', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 4 avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_commissariats_concernes_chosen', 'Commissariat Bard')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Commissariats', self.selenium.page_source)
        Email.objects.all().delete()
        Message.objects.all().delete()
        # Tester bouton annulation et annuler le préavis
        self.selenium.execute_script("window.scroll(0, 500)")
        preavis = self.selenium.find_element_by_xpath("//div[contains(string(),'Commissariats')][@data-toggle='collapse']")
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 400)")
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Commissariats')][@data-toggle='collapse']/following::*")
        self.assertIn('Annuler cette demande', carte.text)
        carte.find_element_by_partial_link_text('Annuler cette demande').click()
        carte.find_element_by_partial_link_text('Confirmer').click()
        self.assertNotIn('Commissariats', self.selenium.page_source)
        # Vérifier les emails envoyés

        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_commiss.email])
        self.assertEqual(outbox[1].to, [self.commiss.email])
        self.assertEqual(messages[0].corps, 'Suppression de la demande de préavis au sujet de la manifestation Manifestation_Test')
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "preavis")
        Email.objects.all().delete()
        Message.objects.all().delete()
        # Vérification du passage en nouveau
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ddsp', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Redistribuer le préavis
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_commissariats_concernes_chosen', 'Commissariat Bard')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Commissariats', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ddsp', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 5 instructeur boutons annulation ****')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.vue_detail()
        # Vérifier les boutons d'annulation
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'DDSP')][@data-toggle='collapse']")
        avis.click()
        self.selenium.execute_script("window.scroll(0, 400)")
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'DDSP')][@data-toggle='collapse']/following::*")
        self.assertNotIn('Annuler cette demande', carte.text)
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'Mairie')][@data-toggle='collapse']")
        avis.click()
        self.selenium.execute_script("window.scroll(0, 400)")
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Mairie')][@data-toggle='collapse']/following::*")
        self.assertNotIn('Annuler cette demande', carte.text)
        self.deconnexion()

        print('**** test 6 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en nouveau
        self.connexion('agent_commiss')
        self.presence_avis('agent_commiss', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre le pré-avis', self.selenium.page_source)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        self.selenium.find_element_by_partial_link_text('Rendre le pré-avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_commiss', 'rendu')
        self.deconnexion()

        print('**** test 7 agent ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en encours
        self.connexion('agent_ddsp')
        self.presence_avis('agent_ddsp', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Tester bouton annulation
        self.selenium.execute_script("window.scroll(0, 500)")
        preavis = self.selenium.find_element_by_xpath("//div[contains(string(),'Commissariats')][@data-toggle='collapse']")
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 400)")
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Commissariats')][@data-toggle='collapse']/following::*")
        self.assertNotIn('Annuler cette demande', carte.text)
        # Rendre l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ddsp', 'rendu')
        self.deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.deconnexion()
