# coding: utf-8

from django import template

from aide.models import PanneauPageAide
from aide.views.aide import check_affichage_elements
from core.models import UserHelper

register = template.Library()


@register.filter
def onglet(value, onglet):
    """ Tester si le panneau appartient à l'onglet en paramètre """
    if isinstance(value, PanneauPageAide):
        return onglet in value.tabs.all().values_list('title', flat=True)
    return False


@register.filter
def note_panneaux(value, user):
    """Retourne les note de bas de page à afficher pour un panneau"""

    notes = check_affichage_elements(user, value.notes)
    return notes