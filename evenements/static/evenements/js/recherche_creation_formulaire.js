$(document).ready(function () {

    $('#submit-id-submit').attr('disabled', true);
    var voiepublique;

    if ($('#id_departement').val() != "") {
        $('#id_departement').trigger("change");
        $('#id_departement').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen
    }

    // Ajouter une div pour l'affichage des icones
    $('#div_id_discipline').append('<div class="col-sm-2 text-right disc-icone"></div>');
    $('#div_id_emprise').append('<div class="col-sm-2 text-right voie-icone"></div>');
    $('#div_id_nb_participants').append('<div class="col-sm-2 text-right nbp-icone"></div>');
    $('#div_id_competition').parent().parent().append('<div class="col-sm-2 text-right comp-icone"></div>');
    $('#div_id_circuitnonperm').parent().parent().append('<div class="col-sm-2 text-right circ-icone"></div>');
    $('#div_id_homologue').parent().parent().append('<div class="col-sm-2 text-right homo-icone"></div>');
    var ivtm = '<span class="fa-stack" data-toggle="tooltip" title="Avec véhicule terrestre à moteur"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fa fa-car fa-stack-1x fa-inverse"></i></span>';
    var icycl = '<span class="fa-stack" data-toggle="tooltip" title="Cyclisme"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fas fa-bicycle fa-stack-1x fa-inverse"></i></span>';
    var i_vtm = '<span class="fa-stack" data-toggle="tooltip" title="Sans véhicule terrestre à moteur"><i class="fa fa-car fa-stack-1x"></i><i class="fa fa-ban fa-stack-2x text-danger"></i>';
    var ivoie = '<span class="fa-stack" data-toggle="tooltip" title="Sur voie publique"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fa fa-road fa-stack-1x fa-inverse"></i></span>';
    var i_voie = '<span class="fa-stack" data-toggle="tooltip" title="Hors voie publique"><i class="fa fa-road fa-stack-1x"></i><i class="fa fa-ban fa-stack-2x text-danger"></i></span>';
    var icirc = '<span class="fa-stack" data-toggle="tooltip" title="Cicuit non permanent"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fa fa-route fa-stack-1x fa-inverse"></i></span>';
    var ihomo = '<span class="fa-stack" data-toggle="tooltip" title="Cicuit homologue"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fa fa-draw-polygon fa-stack-1x fa-inverse"></i></span>';
    var i_homo = '<span class="fa-stack" data-toggle="tooltip" title="Cicuit non homologue"><i class="fa fa-draw-polygon fa-stack-1x"></i><i class="fa fa-ban fa-stack-2x text-danger"></i></span>';
    var icomp = '<span class="fa-stack" data-toggle="tooltip" title="Compétition"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fa fa-trophy fa-stack-1x fa-inverse"></i></span>';
    var i_comp = '<span class="fa-stack" data-toggle="tooltip" title="Sans compétition"><i class="fa fa-trophy fa-stack-1x"></i><i class="fa fa-ban fa-stack-2x text-danger"></i></span>';
    var igroup = '<span class="fa-stack" data-toggle="tooltip" title="Circulation groupée"><i class="fa fa-circle fa-stack-2x text-info"></i><i class="fa fa-users fa-stack-1x fa-inverse"></i></span>';

    function recherche_formulaire() {

        if (vtm == true && voiepublique == true && $('[name="competition"]').is(':checked')) return "Avtm";
        if (vtm == true && voiepublique == true && !$('[name="competition"]').is(':checked') && $('[name="nb_participants"]').val() >= 50) return "Dvtm";
        if (vtm == true && voiepublique == false && $('[name="circuitnonperm"]').is(':checked')) return "Avtm";
        if (vtm == true && voiepublique == false && !$('[name="circuitnonperm"]').is(':checked') && $('[name="homologue"]').is(':checked')) return "Avtmcir";
        if (vtm == true && voiepublique == false && !$('[name="circuitnonperm"]').is(':checked') && !$('[name="homologue"]').is(':checked')) return "Avtm";
        if (vtm == false && voiepublique == true && $('[name="competition"]').is(':checked') && $('#id_discipline option:selected').text() == 'Cyclisme') return "Dcnmc";
        if (vtm == false && voiepublique == true && $('[name="competition"]').is(':checked') && $('#id_discipline option:selected').text() != 'Cyclisme') return "Dcnm";
        if (vtm == false && voiepublique == true && !$('[name="competition"]').is(':checked') && $('[name="nb_participants"]').val() >= 100 && $('#id_discipline option:selected').text() == 'Cyclisme') return "Dnmc";
        if (vtm == false && voiepublique == true && !$('[name="competition"]').is(':checked') && $('[name="nb_participants"]').val() >= 100 && $('#id_discipline option:selected').text() != 'Cyclisme') return "Dnm";
        return "";
    }

    function check_vtm(discipline) {
        for (var i in listevtm){
            if (discipline == listevtm[i]) {
                return true;
            }
        }
        return false;
    }

    function affich_discipl(motor) {
        if (motor == true) {
            $('#div_id_circuitnonperm').parent().parent().show();
            $('.circ-icone').show();
            $('#div_id_homologue').parent().parent().show();
            $('.homo-icone').show();
            $('.disc-icone').html(ivtm);
            if ($('[name="nb_participants"]').val() > 50) $('.nbp-icone').html(igroup);
        } else {
            $('#div_id_circuitnonperm').parent().parent().hide();
            $('.circ-icone').hide();
            $('#div_id_homologue').parent().parent().hide();
            $('.homo-icone').hide();
            if ($('#id_discipline option:selected').text() == 'Cyclisme') {
                $('.disc-icone').html(icycl);
            } else {
                $('.disc-icone').html(i_vtm);
            }
            if ($('[name="nb_participants"]').val() > 100) {
                $('.nbp-icone').html(igroup);
            } else {
                $('.nbp-icone').empty();
            }
            $('[name="circuitnonperm"]').prop("checked", false);
            $('[name="homologue"]').prop("checked", false);
        }
    }

    function get_page_aide(discipline) {
        // on va chercher l'eventuelle page d'aide
        $.get('/sports/ajax/discipline/aide/?pk='+discipline, function (data) {
            $('#aide_discipline').remove();
            if (data){
                let html = `<a id="aide_discipline" href="${data}" target="_blank" class="float-right">Consulter la page d'aide dédiée à cette discipline</a>`;
                $('#id_discipline').parent().append(html);
            }
        });
    }

    // Lorsqu'une sélection est effectuée dans le champ extra_activite
    $('#id_extra_activite').bind('added', function () {
        // Récupérer la valeur du champ (le nom exact de l'activité)
        var act_name = he.decode($(this).val());

        // Récupérer l'ID de la discipline en AJAX
        $.ajax({
                   url: "/sports/ajax/discipline/by_activite_name/",
                   type: 'GET',
                   data: 'name=' + encodeURIComponent(act_name),
                   dataType: 'html',
                   success: function (data, status) {
                       // On a récupéré l'ID de la discipline sous forme de chaîne

                       // On sélectionne l'option du select de disciplines dont l'ID correspond
                       // Cela va, via le javascript de clever-selects, peupler les options de
                       // id_discipline. On va donc ensuite sélectionner la bonne option dans le
                       // champ de disciplines
                       $('#id_discipline').val(data);
                       $('#id_discipline').trigger("change");
                       $('#id_discipline').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen

                       // Refaire le même type de selection sur le champ en cascade (activité)
                       setTimeout(function () {
                           $.ajax({
                                      url: "/sports/ajax/activite/by_name/",
                                      type: 'GET',
                                      data: 'name=' + act_name,
                                      dataType: 'html',
                                      success: function (data, status) {
                                          // On a récupéré l'ID de la discipline sous forme de chaîne

                                          // On sélectionne l'option du select d'activités dont l'ID correspond
                                          $('#id_activite').val(data);
                                          $('#id_activite').trigger("change");
                                          $('#id_activite').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen
                                      }
                                  });
                       }, 50);
                   }
               });
        $(this).val('');

    });

    $('#searchform').on('click', function () {
        // Récupérer les données du formulaire
        var donnees = $('#evenement-form').serializeArray();
        /*
            jQuery.each(donnees, function (i, item) {
                console.log(item);
                console.log(item['name']);
                console.log(item['value']);
            });
        */
        // Controler les trois champs nécessaires
        var discipl = $('#id_discipline').val();
        var nb_part = $('[name="nb_participants"]').val();
        var emprise = $('#id_emprise').val();
        if (discipl == "" || nb_part == "" || emprise == _emprise.null) {
            $('#info-cerfa').html("Questionnaire incomplet pour la recherche du formulaire !");
            scrollTo("#info-cerfa");
        } else {
            if (emprise >= _emprise.partiel) {
                voiepublique = true;
            } else {
                voiepublique = false;
            }
            form = recherche_formulaire();
            if (form !== "") {
                $('#info-cerfa').html(form);
                $('#submit-id-submit').attr('disabled', false);
                $('#id_formulaire').val(form);
                get_url = '/aide/CERFA/' + form + '?embed=true';
                $.get( get_url, function (data) {
                    $('#info-cerfa').html(data);
                    scrollTo("#info-cerfa");
                });
            } else {
                $('#info-cerfa').html("D’après les informations fournies ci-dessus et selon le Code du sport," +
                    "vous n'êtes pas soumis à la déclaration ou à une demande d’autorisation pour votre manifestation." +
                    " Toutefois, des démarches peuvent être nécessaires selon votre réglementation locale ou d’autres codes en vigueur.");
                scrollTo("#info-cerfa");
            }
        }
    });

    // Désactiver le bouton d'accès au formulaire et vider la div 'info-cerfa' quand le questionnaire change
    $(".form-control").on('change', function () {
        $('#submit-id-submit').attr('disabled', true);
        $('#info-cerfa').empty();
    });
    $(".form-check-input").on('change', function () {
        $('#submit-id-submit').attr('disabled', true);
        $('#info-cerfa').empty();
    });

    // Gérer les actions dûes aux différents champs
    // Champ discipline
    if ($('#id_discipline').val() != "") {
        $('#id_discipline').trigger("change");
        $('#id_discipline').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen
        var discipline = $('#id_discipline').val();
        vtm = check_vtm(discipline);
        affich_discipl(vtm);
        get_page_aide(discipline);
    }

    $('#id_discipline').on('change', function () {
        var discipline = $('#id_discipline').val();
        vtm = check_vtm(discipline);
        affich_discipl(vtm);
        if (discipline != "") {
            get_page_aide(discipline);
        }
    });

    // Champ compétition
    if ($('[name="competition"]').is(':checked')) {
        $('.comp-icone').html(icomp);
    } else {
        $('.comp-icone').html(i_comp);
    }

    $('#id_competition').on('change', function() {
        if ($('[name="competition"]').is(':checked')) {
            $('.comp-icone').html(icomp);
        } else {
            $('.comp-icone').html(i_comp);
        }
    });

    // Champ emprise
    if ($('#id_emprise').val() > _emprise.hors) {
        $('.voie-icone').html(ivoie);
    } else if ($('#id_emprise').val() == _emprise.hors) {
        $('.voie-icone').html(i_voie);
    } else {
        $('.voie-icone').empty();
    }

    $('#id_emprise').on('change', function() {
        if ($('#id_emprise').val() > _emprise.hors) {
            $('.voie-icone').html(ivoie);
        } else if ($('#id_emprise').val() == _emprise.hors) {
            $('.voie-icone').html(i_voie);
        } else {
            $('.voie-icone').empty();
        }
    });

    // Champ nb_participants
    // Valeur initiale traitée par le champ discipline
    $('#id_nb_participants').on('change', function() {
        if (vtm == false && $('[name="nb_participants"]').val() > 100) {
            $('.nbp-icone').html(igroup);
        } else if (vtm == true && $('[name="nb_participants"]').val() > 50) {
            $('.nbp-icone').html(igroup);
        } else {
            $('.nbp-icone').empty();
        }
    });

    // Champ circuitnonperm
    if ($('[name="circuitnonperm"]').is(':checked')) {
        $('#div_id_homologue').parent().parent().hide();
        $('.circ-icone').html(icirc);
    } else {
        $('#div_id_homologue').parent().parent().show();
        $('.circ-icone').empty();
    }

    $("#id_circuitnonperm").on('change', function () {
        if ($('[name="circuitnonperm"]').is(':checked')) {
            $('#div_id_homologue').parent().parent().hide();
            $('.homo-icone').hide();
            $('.circ-icone').html(icirc);
        } else {
            $('#div_id_homologue').parent().parent().show();
            $('.homo-icone').show();
            $('.circ-icone').empty();
        }
    });

    // Champ homologue
    if ($('[name="homologue"]').is(':checked')) {
        $('.homo-icone').html(ihomo);
    } else {
        $('.homo-icone').html(i_homo);
    }

    $("#id_homologue").on('change', function () {
        if ($('[name="homologue"]').is(':checked')) {
            $('.homo-icone').html(ihomo);
        } else {
            $('.homo-icone').html(i_homo);
        }
    });

    $('#div_id_circuitnonperm').parent().parent().hide();
    $('.circ-icone').hide();
    $('#div_id_homologue').parent().parent().hide();
    $('.homo-icone').hide();


});

