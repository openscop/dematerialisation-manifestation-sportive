from django_cron import CronJobBase, Schedule
from django_cron.models import CronJobLog
from django.template.loader import render_to_string
from django.conf import settings
from django.utils import timezone
import datetime, logging

from evenements.models import Manif
from instructions.models.instruction import Instruction
from core.util.types import one_line

from messagerie.models.message import Message


mail_logger = logging.getLogger('smtp')


class UpdateDelai(CronJobBase):
    RUN_AT_TIME = ['6:25']  # à lancer à 6h25

    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'evenements.updatedelai'

    def do(self):

        # Purger les logs
        CronJobLog.objects.filter(code="evenements.updatedelai").filter(is_success=True).filter(
            end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        Manif.objects.last().ecrire_delai()


class CompletudeDossier(CronJobBase):
    """
    Job cron destiné à rappeler aux organisateurs les pièces à joindre en cours d'instruction.
    C'sst à dire aux deux dates clés : J-21 et J-6
    """
    RUN_AT_TIME = ['6:45']  # à lancer à 6h45

    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'evenements.conpletudedossier'    # a unique code

    def do(self):

        # Purger les logs sans message
        CronJobLog.objects.filter(code="evenements.conpletudedossier").filter(is_success=True).filter(
            message__isnull=True).filter(end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

        log_retour = ""

        # Dossier en instruction dont la date n'est pas dépassée
        liste = Instruction.objects.filter(manif__date_fin__gte=timezone.now()).exclude(etat="annulée")
        for dossier in liste:
            manifestation = dossier.manif
            # A ce stade, seul certaines pièces jointes peuvent manquer
            if not manifestation.get_cerfa().dossier_complet():
                # Détermination de la date limite de l'étape en cours
                if manifestation.etape_en_cours() == 'etape 1':
                    date_etape = manifestation.get_date_etape_1()
                elif dossier.manif.etape_en_cours() == 'etape 2':
                    date_etape = manifestation.get_date_etape_2()
                else:
                    date_etape = 0
                if date_etape:
                    if datetime.date.today() >= date_etape.date() - datetime.timedelta(days=5):
                        delai = manifestation.get_cerfa().delai_en_cours()
                        nb_jours = (date_etape.date() - datetime.date.today()).days
                        log_retour += '%s ; delai : %sj, échéance : %s ; jours restants : %s' % (manifestation.nom, delai, date_etape.date(), nb_jours)
                        envoyer_alerte_dossier(nb_jours, dossier.manif)
        return log_retour


def envoyer_alerte_dossier(jour, manifestation):
    """
    Ajouter une notification pour l'organisateur et envoyer un mail

    :param jour: nombre de jours restants avant de changer d'étape
    :param manifestation: manifestation ciblée par la notification
    """

    if manifestation.structure.organisateur.user.is_active:
        if jour == 0:
            titre = 'Alerte, pièces jointes manquantes'
        else:
            titre = 'Rappel, pièces jointes manquantes'
        data = {'subject': titre, 'jour': jour, 'manifestation': manifestation, 'url': settings.MAIN_URL}
        data['liste'] = manifestation.get_cerfa().liste_manquants()
        titre = one_line(render_to_string('notifications/mail/subject.txt', data))
        contenu = render_to_string('notifications/mail/message_organisateur_fichiers_manquants.txt', data)
        destinataires = manifestation.structure.organisateur

        Message.objects.creer_et_envoyer('action', None, [destinataires], titre, contenu, manifestation_liee=manifestation, objet_lie_nature="dossier")
