# coding: utf-8
""" Classes abstraites ajoutant des champs et fonctionnalités aux modèles """
from django.db import models

from core.util.admin import set_admin_info


class OnePerDepartementMixin(models.Model):
    """ Ajoute un champ OneToOne vers un département à un modèle """

    departement = models.OneToOneField('administrative_division.departement', verbose_name="département", on_delete=models.CASCADE)

    # Getter
    @set_admin_info(admin_order_field='departement', short_description="Département")
    def get_departement(self):
        """ Renvoie le département de l'objet """
        return self.departement

    def get_instance(self):
        return self.departement.get_instance()

    # Overrides
    def natural_key(self):
        return self.departement.natural_key()

    # Métadonnées
    class Meta:
        abstract = True


class ManyForDepartementMixin(models.Model):
    """ Ajoute un champ m2m vers un département à un modèle """

    departements = models.ManyToManyField('administrative_division.departement', verbose_name="départements")

    # Getter
    @set_admin_info(short_description="Départements")
    def get_departements(self):
        """ Renvoie les départements de l'objet """
        deps = self.departements.all()
        list_dep = ''
        for dep in deps:
            if list_dep == '':
                list_dep += dep.name
            else:
                list_dep += ' - ' + dep.name
        return list_dep

    def get_instance(self):
        from core.models import Instance
        return Instance.objects.filter(departement__in=self.departements.all())

    # Métadonnées
    class Meta:
        abstract = True


class DepartementableBase(models.Model):
    """ Ajoute un champ ForeignKey vers un département à un modèle """

    @set_admin_info(admin_order_field='departement', short_description="Département")
    def get_departement(self):
        """ Renvoie le département de l'objet """
        return self.departement

    @set_admin_info(short_description="Instance")
    def get_instance(self):
        """
        Renvoyer l'instance du département attaché

        :returns: l'instance du département attaché, ou instance master si aucun département
        """
        if self.get_departement():
            return self.get_departement().get_instance()
        else:
            from core.models import Instance
            return Instance.objects.get_master()

    def natural_key(self):
        return (self.pk,) + self.departement.natural_key()

    class Meta:
        abstract = True


class ManyPerDepartementMixin(DepartementableBase):
    """ Ajoute un champ ForeignKey vers un département à un modèle """

    departement = models.ForeignKey('administrative_division.departement', verbose_name="département", on_delete=models.CASCADE)

    class Meta:
        abstract = True


class DepartementableMixin(DepartementableBase):
    """ Ajoute un champ ForeignKey vers un département à un modèle """

    departement = models.ForeignKey('administrative_division.departement', null=True, blank=True, verbose_name="département", on_delete=models.SET_NULL)

    class Meta:
        abstract = True


class OnePerArrondissementMixin(models.Model):
    """ Ajoute un champ OneToOne vers un arrondissement à un modèle """

    arrondissement = models.OneToOneField('administrative_division.arrondissement', verbose_name="Arrondissement",
                                          on_delete=models.CASCADE)

    # Getter
    @set_admin_info(admin_order_field='arrondissement', short_description="Arrondissement")
    def get_arrondissement(self):
        """ Renvoie la commune de l'objet """
        return self.arrondissement

    @set_admin_info(admin_order_field='arrondissement__departement', short_description="Département")
    def get_departement(self):
        """
        Renvoie le département de l'arrondissement de l'objet

        :rtype: administrative_division.models.departement.Departement
        """
        return self.arrondissement.departement

    @set_admin_info(short_description="Instance")
    def get_instance(self):
        """
        Renvoie l'instance du département de l'arrondissement de l'objet

        :rtype: core.models.instance.Instance
        """
        return self.get_departement().get_instance()

    # Overrides
    def natural_key(self):
        return self.arrondissement.natural_key()

    class Meta:
        abstract = True


class ManyPerArrondissementMixin(models.Model):
    """ Ajoute un champ OneToOne vers un arrondissement à un modèle """

    arrondissement = models.ForeignKey('administrative_division.arrondissement', verbose_name="Arrondissement",
                                       on_delete=models.CASCADE)

    @set_admin_info(admin_order_field='arrondissement', short_description="Arrondissement")
    def get_arrondissement(self):
        """ Renvoie la commune de l'objet """
        return self.arrondissement

    @set_admin_info(admin_order_field='arrondissement__departement', short_description="Département")
    def get_departement(self):
        """ Renvoie le département de l'arrondissement de l'objet """
        return self.arrondissement.departement

    class Meta:
        abstract = True


class ManyPerCommuneMixin(models.Model):
    """ Ajoute un champ ForeignKey vers une commune à un modèle """

    commune = models.ForeignKey('administrative_division.commune', verbose_name='Commune', on_delete=models.CASCADE)

    @set_admin_info(admin_order_field='commune', short_description="Commune")
    def get_commune(self):
        """ Renvoie la commune de l'objet """
        return self.commune

    @set_admin_info(admin_order_field='commune__arrondissement', short_description="Arrondissement")
    def get_arrondissement(self):
        """ Renvoie la commune de l'objet """
        return self.commune.arrondissement

    @set_admin_info(admin_order_field='commune__arrondissement__departement', short_description="Département")
    def get_departement(self):
        """ Renvoie le département de l'arrondissement de l'objet """
        return self.commune.arrondissement.departement

    def get_instance(self):
        return self.get_departement().get_instance()

    def natural_key(self):
        return (self.pk,) + self.commune.natural_key()

    class Meta:
        abstract = True


class OnePerDepartementQuerySetMixin(models.QuerySet):
    """ Manager/Queryset """

    # Overrides
    def get_by_natural_key(self, name):
        return self.get(departement__name=name)


class OnePerArrondissementQuerySetMixin(models.QuerySet):
    """ Manager/Queryset """

    # Overrides
    def get_by_natural_key(self, code):
        return self.get(arrondissement__code=code)


class ManyPerCommuneQuerySetMixin(models.QuerySet):
    """ Queryset """

    # Overrides
    def get_by_natural_key(self, pk, code, name):
        return self.get(id=pk, commune__code=code, commune__name=name)


class ManyPerDepartementQuerySetMixin(models.QuerySet):
    """ Queryset """

    # Overrides
    def get_by_natural_key(self, pk, name):
        return self.get(id=pk, departement__name=name)
