var ORExtMap = {
    LAYER_MAP: "MAP",
    LAYER_PHOTO: "PHOTO"
};


ORExtMap.getIGNBaseMap = function (map, drm, layer, options, originators) {
    var tileSize, copyright, territory = "FXX",
        listeners = {},
        scale0Geop = 559082264,
        options = options || {},
        googProj = new ORExtMap.Projection.google(),
        options = {
            alt: options.alt || "IGN " + layer,
            getTileUrl: getTileUrl,
            isPng: options.isPng || false,
            maxZoom: options.maxZoom || 18,
            minZoom: options.minZoom || 2,
            name: options.name || "IGN " + layer,
            tileSize: new google.maps.Size(256, 256),
            style: options.style || "normal"
        },
        params = {
            ANF: {
                bounds: [11.7, -64, 18.18, -59]
            },
            ASP: {
                bounds: [-40, 76, -36, 79]
            },
            CRZ: {
                bounds: [-48, 47, -44, 55]
            },
            FXX: {
                bounds: [27.33, -31.17, 80.83, 69.03]
            },
            GUF: {
                bounds: [-4.3, -62.1, 11.5, -46]
            },
            MYT: {
                Kx: 108886.89283435,
                bounds: [-17.5, 40, 3, 56]
            },
            NCL: {
                Kx: 103213.63456212,
                bounds: [-24.3, 160, -17.1, 170]
            },
            PYF: {
                Kx: 107526.37112657,
                bounds: [-28.2, -160, 11, -108]
            },
            REU: {
                Kx: 103925.69769224,
                bounds: [-26.2, 37.5, -17.75, 60]
            },
            SPM: {
                Kx: 75919.710164,
                bounds: [43.5, -60, 52, -50]
            },
            WLF: {
                Kx: 108012.82616793,
                bounds: [-14.6, -178.5, -12.8, -175.8]
            }
        },
        mapType = new google.maps.ImageMapType(options),
        hasDRM = drm.hasDRM || false,
        locked = false,
        drmID = drm.drmID || "",
        drmKey = "",
        drmCache = {},
        drmTilesCount = 0,
        drmStartCount = false,
        originators = originators;
    for (var t in params) {
        if (params.hasOwnProperty(t)) {
            params[t].bounds = new google.maps.LatLngBounds(new google.maps.LatLng(params[t].bounds[0], params[t].bounds[1]), new google.maps.LatLng(params[t].bounds[2], params[t].bounds[3]))
        }
    }
    function getTileUrl(point, zoom) {
        if (drmStartCount) {
            var id = "" + layer + zoom + point.x + point.y;
            if (!drmCache[id]) {
                drmCache[id] = 1;
                drmTilesCount++
            } else {
                drmCache[id] += 1
            }
        }
        // console.log(point)
        // console.log(zoom)
        var zfactor = Math.pow(2, zoom);
        var lULP = new google.maps.Point(point.x * 256 / zfactor, (point.y + 1) * 256 / zfactor);
        // console.log(lULP)
        var lLRP = new google.maps.Point((point.x + 1) * 256 / zfactor, point.y * 256 / zfactor);
        // console.log(lLRP)
        var ll = ORMain.map.getProjection().fromPointToLatLng(lULP);
        var ur = ORMain.map.getProjection().fromPointToLatLng(lLRP);
        var dt = new Date();
        var nowtime = dt.getTime();
        // var tileurl = "http://ws.carmencarto.fr/WMS/119/fxx_inpn?REQUEST=GetMap&SERVICE=WMS&reaspect=false&VERSION=1.1.1&LAYERS={layer}&STYLES=default&FORMAT=image/png&BGCOLOR=0xFFFFFF&TRANSPARENT=TRUE&SRS=EPSG:4326";
        // var tileurl = "http://217.70.190.13/cgi-bin/w   orld/qgis_mapserv.fcgi?service=WMS&request=GetMap&layers=world&styles=&format=image/png&transparent=TRUE&version=1.3.0&srs=EPSG:4326";
        // coor="&BBOX=" + ll.lng() + "," + ll.lat() + "," + ur.lng() + "," + ur.lat();
        // // console.log(coor)
        // tileurl += "&BBOX=" + ll.lat() + "," + ll.lng() + "," + ur.lat() + "," + ur.lng();
        // tileurl += "&WIDTH=256&HEIGHT=256";
        // return tileurl.replace("{layer}", "world")

        return "https://wxs.ign.fr/{key}/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER={layer}&STYLE={style}&TILEMATRIXSET=PM&TILEMATRIX={tilematrix}&TILEROW={tilerow}&TILECOL={tilecol}&FORMAT=image/{imgFormat}".replace("{tilerow}", point.y).replace("{tilecol}", point.x).replace("{key}", drmKey).replace("{tilematrix}", zoom).replace("{layer}", layer).replace("{style}", options.style).replace("{imgFormat}", (options.isPng) ? "png" : "jpeg")
    }
    function initCopyright() {
        var or = ORMapCRUtils.createCRDOM(originators);
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(or);
        return or
    }
    function initProjection() {
        var scale, projFactory, oldTerritory = territory,
            zoom = map.getZoom();
        var bounds = map.getBounds();
        if (!params[territory] || (bounds && !params[territory].bounds.intersects(bounds))) {
            for (var t in params) {
                if (params.hasOwnProperty(t) && bounds && params[t].bounds.intersects(bounds)) {
                    territory = t;
                    break
                }
            }
        }
        scale = 0.00028 * scale0Geop;
        projFactory = function (territory) {
            return ORExtMap.Projection.geoportalV3()
        };
        if (!bounds || oldTerritory != territory) {
            var center = map.getCenter();
            googProj.proj = projFactory(territory);
            googProj.scale0 = scale;
            map.setCenter(center)
        }
    }
    initProjection();
    copyright = initCopyright();
    mapType.projection = googProj;
    function countIgnDrm() {
        if (drmStartCount && drmTilesCount > 0) {
            var tce = "tc:" + drmTilesCount + ",tt:" + drmID + ",tk:" + drmKey;
            new Ajax.Request("account/uptc.php", {
                method: "post",
                parameters: {
                    tce: $j.b64.encode(tce)
                },
                asynchronous: true,
                onSuccess: function (transport) {
                    var ret = $j.b64.decode(transport.responseText);
                    if (ret < 0) {
                        document.fire("or:drmerror", {
                            cr: ret,
                            tt: drmID
                        })
                    }
                }
            });
            drmTilesCount = 0
        }
    }
    return {
        mapType: mapType,
        bounds: (function () {
            var bounds = [];
            for (var t in params) {
                if (params.hasOwnProperty(t)) {
                    bounds.push(params[t].bounds)
                }
            }
            return bounds
        })(),
        enable: function () {
            initProjection();
            listeners.zoom = google.maps.event.addListener(map, "zoom_changed", initProjection);
            listeners.center = google.maps.event.addListener(map, "center_changed", initProjection);
            listeners.igndrm = google.maps.event.addListener(map, "idle", countIgnDrm);
            copyright.style.display = "block"
        },
        disable: function () {
            listeners.zoom && google.maps.event.removeListener(listeners.zoom);
            listeners.center && google.maps.event.removeListener(listeners.center);
            listeners.igndrm && google.maps.event.removeListener(listeners.igndrm);
            copyright.style.display = "none"
        },
        locked: locked,
        hasDRM: function () {
            return hasDRM
        },
        getDrmID: function () {
            return drmID
        },
        setDrmKey: function (key) {
            drmKey = key
        },
        setDrmStartCount: function (start) {
            drmStartCount = start
        }
    }
};

ORExtMap.getGeoJsonLayer = function(map, layer, options, originators, wmshost, type) {
    var copyright,
        options = options || {},
        options = {
            alt: options.alt || 'OSM ' + layer,
            getTileUrl: getTileUrl,
            isPng: options.isPng || false,
            maxZoom: options.maxZoom || 18,
            minZoom: options.minZoom || 5,
            name: options.name || 'OSM',
            tileSize: new google.maps.Size(5000, 5000)
        },
        mapType = new google.maps.ImageMapType(options),
        data = new google.maps.Data();


    // Originators : CR
    originators = originators;

    // Host, permet de dynamiser le getTileUrl pour OSM, MapQuest, OpenCycleMap, HikeBikeMap
    wmshost = wmshost;
    /**
     * Create a control to display the copyright
     */
    function initCopyright() {
        // Construction de la liste des CR liés à ce fond de crte ou sur-couche
        var or = ORMapCRUtils.createCRDOM(originators);
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(or);
        return or;
    }

    /**
     * Returns the URL of the request tile
     *
     * @param {google.maps.Point} point Tile coordinate
     * @param {number}            zoom  The zoom level
     *
     * @return {string} The tile URL
     */
    function getTileUrl(point, zoom) {
        // Calcul BBOX
        var gmapbbox = map.getBounds();
        var sw = gmapbbox.getSouthWest();
        var ne = gmapbbox.getNorthEast();
        var bbox = [sw.lng(), sw.lat(), ne.lng(), ne.lat()].join()+",EPSG:4326";
        var url = ("https://carto2.manifestationsportive.fr/geoserver/cite/ows")

        var parameters = {
            service: 'WFS',
            version: '2.0.0',
            request: 'GetFeature',
            typeName: 'cite:'+type,
            maxFeatures: 50,
            outputFormat: 'application/json',
            bbox: bbox,
            srsName:'EPSG:4326'

        };
        // Récupération des data
        var URL = url + getParamString(parameters);
        removeAllFeature();

        data.loadGeoJson(URL,null, function(features) {
            data.setStyle(function(feature) {
                return /** @type {google.maps.Data.StyleOptions} */({
                    fillColor: feature.getProperty('color'),
                    strokeWeight: 1
                });
            });
            data.addListener('mouseup', function(event) {
                var feature = event.feature;
                // contruction du tooltip,
                let fprop = "<div style='width:300px;'><ul>";

                if (type==="communespsql"){
                    fprop+= "<li>Nom : "+feature.i.nom+"</li>"
                    fprop+= "<li>Code Insee : "+feature.i.insee+"</li>"
                }
                else if (type==="natura_directive_habitat" || type==="natura_protection_speciale"){
                    fprop+="<li>Nom : "+feature.i.nom_site+"</li>"
                    fprop+="<li>Code : "+feature.i.sitecode+"</li>"
                    fprop+="<li>Fiche : <a href='https://inpn.mnhn.fr/site/natura2000/"+feature.i.sitecode+"' target='_blank'>https://inpn.mnhn.fr/site/natura2000/"+feature.i.sitecode+"</a></li>"
                }
                else if (type==="parc_national"){
                    fprop+="<li>Nom : "+feature.i.gest_site+"</li>"
                    fprop+="<li>Gestionnaire : "+feature.i.operateur+"</li>"
                    fprop+="<li>Fiche : <a href='"+feature.i.url_fiche+"' target='_blank'>"+feature.i.url_fiche+"</a></li>"
                }
                else if (type==="reserve_nationale"){
                    fprop+="<li>Nom : "+feature.i.nom_site+"</li>"
                    fprop+="<li>Gestionnaire : "+feature.i.operateur+"</li>"
                    fprop+="<li>Fiche : <a href='"+feature.i.url_fiche+"' target='_blank'>"+feature.i.url_fiche+"</a></li>"
                }
                else if (type==="parcelles_forestieres_publique0"){
                    fprop+="<li>Nom : "+feature.i.llib_frt+"</li>"
                    fprop+="<li>Code : "+feature.i.iidtn_frt+"</li>"
                }
                else if (type==="protection_biotope0"){
                    fprop+="<li>Nom : "+feature.i.nom_site+"</li>"
                    fprop+="<li>Gestionnaire : "+feature.i.operateur+"</li>"
                    fprop+="<li>Fiche : <a href='"+feature.i.url_fiche+"' target='_blank'>"+feature.i.url_fiche+"</a></li>"
                }
                else if (type==="gend_police0"){
                    fprop+="<li>Nom : "+feature.i.service+"</li>"
                    fprop+="<li>Téléphone : "+feature.i.telephone+"</li>"
                }
                else if (type==="liste_passage_niveau"){
                    fprop+="<li>Nom : "+feature.i.libelle_if+"</li>"
                    fprop+="<li>Code_ligne : "+feature.i.code_ligne+"</li>"
                }
                else if (type==="bornage_routier0"){
                    fprop+="<li>Route : "+feature.i.route+"</li>"
                    fprop+="<li>Gestionnaire : "+feature.i.gestionnai+"</li>"
                }
                else if (type==="epci0"){
                    fprop+="<li>Code: "+feature.i.code_epci+"</li>"
                    fprop+="<li>Nom : "+feature.i.nom_epci+"</li>"
                    fprop+="<li>Type : "+feature.i.type_epci+"</li>"
                }
                else if (type==="competence_police_gendarmerie0"){
                    fprop+="<li>Nom : "+feature.i.nom_com+"</li>"
                    fprop+="<li>Code : "+feature.i.insee_com+"</li>"
                    fprop+="<li>Compétence : "+feature.i.competence+"" +
                        "<br/>"+feature.i.competen_1+"" +
                        "<br/>"+feature.i.competen_2+"</li>"
                    if (feature.i.cob_libell){
                        fprop+="<li>Libellé COB : "+feature.i.cob_libell+"</li>"
                    }
                    if (feature.i.cob_type){
                        fprop+="<li>Type COB : "+feature.i.cob_type+"</li>"
                    }
                    if (feature.i.comp_libel){
                        fprop+="<li>Libellé compétence : "+feature.i.comp_libel+"</li>"
                    }
                    if (feature.i.comp_type){
                        fprop+="<li>Type compétence : "+feature.i.comp_type+"</li>"
                    }
                    if (feature.i.csp_libell){
                        fprop+="<li>Libellé CSP : "+feature.i.csp_libell+"</li>"
                    }
                    if (feature.i.csp_type){
                        fprop+="<li>Type CSP : "+feature.i.csp_type+"</li>"
                    }
                    if (feature.i.bta_libell){
                        fprop+="<li>Libellé BTA : "+feature.i.bta_libell+"</li>"
                    }
                    if (feature.i.bta_type){
                        fprop+="<li>Type BTA : "+feature.i.bta_type+"</li>"
                    }
                }
                else{
                    fprop+="<li>Nom : "+feature.i.nom_site+"</li>"
                    fprop+="<li>Gestionnaire : "+feature.i.gest_site+"</li>"
                    fprop+="<li>Fiche : <a href='"+feature.i.url_fiche+"' target='_blank'>"+feature.i.url_fiche+"</a></li>"
                }


                fprop+="</ul></div>"

                if (infowindow != null){
                    infowindow.close()
                    infowindow = null
                }
                infowindow = new google.maps.InfoWindow()
                infowindow.setContent(fprop);
                // position the infowindow on the marker
                infowindow.setPosition(event.latLng);
                // anchor the infowindow on the marker
                infowindow.setOptions({pixelOffset: new google.maps.Size(0,0)});
                infowindow.open(map);
            });
        });



        return null;
    }

    function removeAllFeature() {

        data.forEach(function(feature) {
            // If you want, check here for some constraints.
            data.remove(feature);
        });
    }

    function getParamString (obj, existingUrl, uppercase) {
        var params = [];
        for (var i in obj) {
            params.push(encodeURIComponent(uppercase ? i.toUpperCase() : i) + '=' + encodeURIComponent(obj[i]));
        }
        return ((!existingUrl || existingUrl.indexOf('?') === -1) ? '?' : '&') + params.join('&');
    }

    copyright = initCopyright();

    return {
        /** data */
        mapType: mapType,
        data: data,
        map : map,
        /**
         * The area covered by the tiles
         * @type {google.maps.LatLngBounds}
         */
        bounds: new google.maps.LatLngBounds(
            new google.maps.LatLng(-180, -90),
            new google.maps.LatLng(180, 90)
        ),
        /**
         * This function must be called before displaying the map
         */
        enable: function() {
            copyright.style.display = 'block';
            data.setMap(map);
            return true;
        },
        /**
         * This function must be called when the map is no more displayed
         */
        disable: function() {
            copyright.style.display = 'none';
            data.setMap(null);
            return true;
        },
        /**
         * This function must be called before enabling the map
         */
        isLocked : function() {
            return locked;
        }

    }
};




ORExtMap.getSWTBaseMap = function (map, drm, layer, options, originators) {
    var gZoom, copyright, center, listeners = {},
        server = 0,
        options = options || {},
        googProj = new ORExtMap.Projection.google(ORExtMap.Projection.swisstopo()),
        params = {
            5: {
                scale: 4000,
                zoom: 0
            },
            6: {
                scale: 2500,
                zoom: 6
            },
            7: {
                scale: 1250,
                zoom: 11
            },
            8: {
                scale: 650,
                zoom: 14
            },
            9: {
                scale: 250,
                zoom: 16
            },
            10: {
                scale: 200,
                zoom: 17
            },
            11: {
                scale: 100,
                zoom: 17
            },
            12: {
                scale: 50,
                zoom: 18
            },
            13: {
                scale: 20,
                zoom: 19
            },
            14: {
                scale: 10,
                zoom: 20
            },
            15: {
                scale: 5,
                zoom: 21
            },
            16: {
                scale: 2.5,
                zoom: 22
            },
            17: {
                scale: 1.5,
                zoom: 24
            },
            18: {
                scale: 0.5,
                zoom: 26
            }
        },
        options = {
            alt: options.alt || "SwissTopo " + layer,
            getTileUrl: getTileUrl,
            isPng: false,
            maxZoom: options.maxZoom || 21,
            minZoom: options.minZoom || 18,
            name: options.name || "SwissTopo " + layer,
            tileSize: new google.maps.Size(256, 256)
        },
        mapType = new google.maps.ImageMapType(options),
        hasDRM = drm.hasDRM || false,
        locked = false,
        drmID = drm.drmID || "",
        drmKey = "",
        drmCache = {},
        drmTilesCount = 0,
        drmStartCount = false,
        originators = originators;
    function initCopyright() {
        var or = ORMapCRUtils.createCRDOM(originators);
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(or);
        return or
    }
    function initProjection() {
        var zoom = map.getZoom();
        if (gZoom && zoom == 10) {
            var delta = zoom - gZoom;
            zoom = zoom + delta / Math.abs(delta);
            map.setZoom(zoom)
        }
        gZoom = zoom;
        googProj.scale0 = params[gZoom].scale * Math.pow(2, zoom);
        map.setCenter(center)
    }
    function getTileUrl(point, zoom) {
        if (drmStartCount) {
            var id = "" + layer + zoom + point.x + point.y;
            if (!drmCache[id]) {
                drmCache[id] = 1;
                drmTilesCount++
            } else {
                drmCache[id] += 1
            }
        }
        return "https://wmts.geo.admin.ch/1.0.0/{layer}/default/{date}/{proj}/{zoom}/{y}/{x}.jpeg".replace("{y}", point.y).replace("{x}", point.x).replace("{proj}", googProj.proj.name).replace("{zoom}", params[gZoom].zoom).replace("{date}", layer === ORExtMap.LAYER_MAP ? "20111206" : "20110914").replace("{layer}", layer === ORExtMap.LAYER_MAP ? "ch.swisstopo.pixelkarte-farbe" : "ch.swisstopo.swissimage")
    }
    function countIgnDrm() {
        if (drmStartCount && drmTilesCount > 0) {
            var tce = "tc:" + drmTilesCount + ",tt:" + drmID + ",tk:" + drmKey;
            new Ajax.Request("account/uptc.php", {
                method: "post",
                parameters: {
                    tce: $j.b64.encode(tce)
                },
                asynchronous: true,
                onSuccess: function (transport) {
                    var ret = $j.b64.decode(transport.responseText);
                    if (ret < 0) {
                        document.fire("or:drmerror", {
                            cr: ret,
                            tt: drmID
                        })
                    }
                }
            });
            drmTilesCount = 0
        }
    }
    center = map.getCenter();
    initProjection();
    copyright = initCopyright();
    mapType.projection = googProj;
    return {
        mapType: mapType,
        bounds: new google.maps.LatLngBounds(new google.maps.LatLng(45.398181, 5.140242), new google.maps.LatLng(48.230651, 11.47757)),
        enable: function () {
            center = map.getCenter();
            initProjection();
            listeners.idle = google.maps.event.addListener(map, "idle", function () {
                center = map.getCenter();
                countIgnDrm()
            });
            listeners.zoom = google.maps.event.addListener(map, "zoom_changed", initProjection);
            copyright.style.display = "block";
            return true
        },
        disable: function () {
            listeners.idle && google.maps.event.removeListener(listeners.idle);
            listeners.zoom && google.maps.event.removeListener(listeners.zoom);
            copyright.style.display = "none";
            return true
        },
        locked: locked,
        hasDRM: function () {
            return hasDRM
        },
        getDrmID: function () {
            return drmID
        },
        setDrmKey: function (key) {
            drmKey = key
        },
        setDrmStartCount: function (start) {
            drmStartCount = start
        }
    }
};
var IGN_ENTRY_KEY = undefined;
var IGN_GPPKEY = undefined;
var IGN_GPPKEY_UPDADTEID = undefined;
var FIRST_CALL = false;
function getFirstToken() {
    if (!FIRST_CALL) {
        getTokenIgn(IGN_ENTRY_KEY);
        FIRST_CALL = false
    }
}
function getTokenIgn(apikey) {
    if (!IGN_ENTRY_KEY && apikey) {
        IGN_ENTRY_KEY = apikey
    }
    var requestParams = new Object();
    requestParams.key = IGN_ENTRY_KEY;
    requestParams.output = "json";
    $j.ajax({
        type: "GET",
        url: "https://jeton-api.ign.fr/getToken",
        cache: false,
        data: requestParams,
        dataType: "jsonp",
        success: onGetTokenIgn,
        error: onGlobalError
    })
}
function onGetTokenIgn(result) {
    IGN_GPPKEY = result.gppkey;
    if (IGN_GPPKEY && !IGN_GPPKEY_UPDADTEID) {
        IGN_GPPKEY_UPDADTEID = window.setInterval(getTokenIgn, 60000 * 9)
    } else {
        if (!IGN_GPPKEY && IGN_GPPKEY_UPDADTEID) {
            window.clearInterval(IGN_GPPKEY_UPDADTEID);
            IGN_GPPKEY_UPDADTEID = null
        }
    }
}
function onGlobalError(result) {
    alert(result)
}
ORExtMap.getNATURABaseMap = function (map, drm, layer, options, originators) {
    var copyright, options = options || {},
        options = {
            alt: options.alt || "carmen " + layer,
            getTileUrl: getTileUrl,
            isPng: options.isPng || false,
            maxZoom: options.maxZoom || 18,
            minZoom: options.minZoom || 7,
            name: options.name || "carmen",
            tileSize: new google.maps.Size(256, 256)
        },
        mapType = new google.maps.ImageMapType(options),
        locked = drm.locked || false,
        drmID = drm.drmID || "";
    mapType.setOpacity(0.5);
    shown = false;
    originators = originators;
    function initCopyright() {
        var or = ORMapCRUtils.createCRDOM(originators);
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(or);
        return or
    }
    function getTileUrl(point, zoom) {
        var zfactor = Math.pow(2, zoom);
        var lULP = new google.maps.Point(point.x * 256 / zfactor, (point.y + 1) * 256 / zfactor);
        var lLRP = new google.maps.Point((point.x + 1) * 256 / zfactor, point.y * 256 / zfactor);
        var ll = ORMain.map.getProjection().fromPointToLatLng(lULP);
        var ur = ORMain.map.getProjection().fromPointToLatLng(lLRP);
        var dt = new Date();
        var nowtime = dt.getTime();
        var tileurl = "https://ws.carmencarto.fr/WMS/119/fxx_inpn?REQUEST=GetMap&SERVICE=WMS&reaspect=false&VERSION=1.1.1&LAYERS={layer}&STYLES=default&FORMAT=image/png&BGCOLOR=0xFFFFFF&TRANSPARENT=TRUE&SRS=EPSG:4326";
        // var tileurl = "http://217.70.190.13/cgi-bin/world/qgis_mapserv.fcgi?service=WMS&request=GetMap&layers=world&styles=&format=image/png&transparent=TRUE&version=1.1.1&srs=EPSG:385";
        //
        // tileurl += "&BBOX=" + ll.lng() + "," + ll.lat() + "," + ur.lng() + "," + ur.lat();
        // tileurl += "&WIDTH=256&HEIGHT=256";
        return tileurl.replace("{layer}", "world")
    }
    copyright = initCopyright();
    return {
        mapType: mapType,
        bounds: new google.maps.LatLngBounds(new google.maps.LatLng(40.9208, -5.58548), new google.maps.LatLng(51.4462, 10.755)),
        enable: function () {
            copyright.style.display = "block";
            shown = true;
            return true
        },
        disable: function () {
            copyright.style.display = "none";
            shown = false;
            return true
        },
        isLocked: function () {
            return locked
        },
        isShown: function () {
            return shown
        }
    }
};
ORExtMap.getSKARTBaseMap = function (map, drm, layer, options, originators) {
    var copyright, options = options || {},
        options = {
            alt: options.alt || "statkart " + layer,
            getTileUrl: getTileUrl,
            isPng: options.isPng || false,
            maxZoom: options.maxZoom || 18,
            minZoom: options.minZoom || 7,
            name: options.name || "statkart",
            tileSize: new google.maps.Size(256, 256)
        },
        mapType = new google.maps.ImageMapType(options),
        hasDRM = drm.hasDRM || false,
        locked = false,
        drmID = drm.drmID || "",
        drmToken = "";
    originators = originators;
    function initCopyright() {
        var or = ORMapCRUtils.createCRDOM(originators);
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(or);
        return or
    }
    function getTileUrl(point, zoom) {
        return "https://opencache.statkart.no/gatekeeper/gk/gk.open_gmaps?layers=topo2&zoom=" + zoom + "&x=" + point.x + "&y=" + point.y + ""
    }
    copyright = initCopyright();
    return {
        mapType: mapType,
        bounds: new google.maps.LatLngBounds(new google.maps.LatLng(58, 4.95), new google.maps.LatLng(71.15, 31.06)),
        enable: function () {
            copyright.style.display = "block";
            return true
        },
        disable: function () {
            copyright.style.display = "none";
            return true
        },
        locked: locked,
        hasDRM: function () {
            return hasDRM
        },
        getDrmID: function () {
            return drmID
        },
        setDrmToken: function (tok) {
            drmToken = tok
        }
    }
};
ORExtMap.getIGNSpainBaseMap = function (map, drm, layer, options, originators) {
    var zone, center, copyright, layerNames, server = 0,
        listeners = {},
        zoomOffset = Math.round(Math.log(2 * Math.PI * 6378137 / (2048 * 256)) / Math.LN2),
        options = options || {},
        googProj = new ORExtMap.Projection.google(ORExtMap.Projection.iberpix(30), 2048 * Math.pow(2, zoomOffset)),
        options = {
            alt: options.alt || "IGNE " + layer,
            getTileUrl: getTileUrl,
            isPng: false,
            maxZoom: options.maxZoom || 16,
            minZoom: options.minZoom || 6,
            name: options.name || "IGNE " + layer,
            tileSize: new google.maps.Size(256, 256)
        },
        mapType = new google.maps.ImageMapType(options),
        hasDRM = drm.hasDRM || false,
        locked = false,
        drmID = drm.drmID || "",
        drmToken = "";
    originators = originators;
    layerNames = layer === ORExtMap.LAYER_MAP ? ["mapa_millon", "mapa_mtn200", "mapa_mtn50", "mapa_mtn25"] : ["mapa_inicio", "spot5", "pnoa", "pnoa"];
    function getLayer(zoom) {
        if (zoom < 11) {
            return layerNames[0]
        } else {
            if (zoom < 13) {
                return layerNames[1]
            } else {
                if (zoom < 15) {
                    return layerNames[2]
                } else {
                    return layerNames[3]
                }
            }
        }
    }
    function getTileUrl(point, zoom) {
        return "https://www2.ign.es/iberpix/tileserver/n={layer};z={zone};r={scale};i={x};j={y}.jpg".replace("{layer}", getLayer(zoom)).replace("{zone}", zone).replace("{scale}", 2048 / Math.pow(2, zoom - 6) * 1000).replace("{x}", point.x).replace("{y}", -(point.y + 1))
    }
    function initCopyright() {
        var or = ORMapCRUtils.createCRDOM(originators);
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(or);
        return or
    }
    function initProjection() {
        var oldZone = zone,
            zoom = map.getZoom(),
            center = map.getCenter();
        if (zoom < 11) {
            zone = 30
        } else {
            var lngCenter = center.lng();
            if (lngCenter < -6) {
                zone = 29
            } else {
                if (lngCenter < 0) {
                    zone = 30
                } else {
                    zone = 31
                }
            }
        }
        if (oldZone != zone) {
            googProj.proj = ORExtMap.Projection.iberpix(zone);
            map.setCenter(center)
        }
    }
    initProjection();
    copyright = initCopyright();
    mapType.projection = googProj;
    return {
        mapType: mapType,
        bounds: bounds = new google.maps.LatLngBounds(new google.maps.LatLng(34, -13.5), new google.maps.LatLng(44, 4)),
        enable: function () {
            initProjection();
            listeners.zoom = google.maps.event.addListener(map, "zoom_changed", initProjection);
            listeners.center = google.maps.event.addListener(map, "center_changed", initProjection);
            copyright.style.display = "block";
            return true
        },
        disable: function () {
            listeners.zoom && google.maps.event.removeListener(listeners.zoom);
            listeners.center && google.maps.event.removeListener(listeners.center);
            copyright.style.display = "none";
            return true
        },
        locked: locked,
        hasDRM: function () {
            return hasDRM
        },
        getDrmID: function () {
            return drmID
        },
        setDrmToken: function (tok) {
            drmToken = tok
        }
    }
};
ORExtMap.getOSMBaseMap = function (map, drm, layer, options, originators, wmshost) {
    var copyright, options = options || {},
        options = {
            alt: options.alt || "OSM " + layer,
            getTileUrl: getTileUrl,
            isPng: options.isPng || false,
            maxZoom: options.maxZoom || 18,
            minZoom: options.minZoom || 5,
            name: options.name || "OSM",
            tileSize: new google.maps.Size(256, 256)
        },
        mapType = new google.maps.ImageMapType(options),
        locked = drm.locked || false,
        drmID = drm.drmID || "";
    originators = originators;
    wmshost = wmshost;
    function initCopyright() {
        var or = ORMapCRUtils.createCRDOM(originators);
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(or);
        return or
    }
    function getTileUrl(point, zoom) {
        return "https://" + wmshost + "/" + zoom + "/" + point.x + "/" + point.y + ".png"
    }
    copyright = initCopyright();
    return {
        mapType: mapType,
        bounds: new google.maps.LatLngBounds(new google.maps.LatLng(-180, -90), new google.maps.LatLng(180, 90)),
        enable: function () {
            copyright.style.display = "block";
            return true
        },
        disable: function () {
            copyright.style.display = "none";
            return true
        },
        isLocked: function () {
            return locked
        }
    }
};
ORExtMap.getUSGSBaseMap = function (map, drm, layer, options, originators) {
    var copyright, options = options || {},
        options = {
            alt: options.alt || "USGS " + layer,
            getTileUrl: getTileUrl,
            isPng: options.isPng || false,
            maxZoom: options.maxZoom || 18,
            minZoom: options.minZoom || 7,
            name: options.name || "USGS",
            tileSize: new google.maps.Size(256, 256)
        },
        mapType = new google.maps.ImageMapType(options),
        hasDRM = drm.hasDRM || false,
        locked = false,
        drmID = drm.drmID || "",
        drmToken = "";
    originators = originators;
    function initCopyright() {
        var or = ORMapCRUtils.createCRDOM(originators);
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(or);
        return or
    }
    function getTileUrl(point, zoom) {
        var zfactor = Math.pow(2, zoom);
        var lULP = new google.maps.Point(point.x * 256 / zfactor, (point.y + 1) * 256 / zfactor);
        var lLRP = new google.maps.Point((point.x + 1) * 256 / zfactor, point.y * 256 / zfactor);
        var ll = ORMain.map.getProjection().fromPointToLatLng(lULP);
        var ur = ORMain.map.getProjection().fromPointToLatLng(lLRP);
        return "https://msrmaps.com/ogcmap.ashx?LAYERS={layer}&EXCEPTIONS=text/xml&FORMAT=image/{imgFormat}&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&SRS=EPSG:4326&BBOX={bbox}&WIDTH=256&HEIGHT=256".replace("{bbox}", (ll.lng() + "," + ll.lat() + "," + ur.lng() + "," + ur.lat())).replace("{layer}", layer).replace("{imgFormat}", (options.isPng) ? "png" : "jpeg")
    }
    copyright = initCopyright();
    return {
        mapType: mapType,
        bounds: new google.maps.LatLngBounds(new google.maps.LatLng(15, -178.21), new google.maps.LatLng(75, -60.97)),
        enable: function () {
            copyright.style.display = "block";
            return true
        },
        disable: function () {
            copyright.style.display = "none";
            return true
        },
        locked: locked,
        hasDRM: function () {
            return hasDRM
        },
        getDrmID: function () {
            return drmID
        },
        setDrmToken: function (tok) {
            drmToken = tok
        }
    }
};
ORExtMap.getToporamaBaseMap = function (map, drm, layer, options, originators) {
    var copyright, options = options || {},
        options = {
            alt: options.alt || "TOPORAMA " + layer,
            getTileUrl: getTileUrl,
            isPng: options.isPng || false,
            maxZoom: options.maxZoom || 18,
            minZoom: options.minZoom || 7,
            name: options.name || "TOPORAMA",
            tileSize: new google.maps.Size(256, 256)
        },
        mapType = new google.maps.ImageMapType(options),
        hasDRM = drm.hasDRM || false,
        locked = false,
        drmID = drm.drmID || "",
        drmToken = "";
    originators = originators;
    function initCopyright() {
        var or = ORMapCRUtils.createCRDOM(originators);
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(or);
        return or
    }
    function getTileUrl(point, zoom) {
        var zfactor = Math.pow(2, zoom);
        var lULP = new google.maps.Point(point.x * 256 / zfactor, (point.y + 1) * 256 / zfactor);
        var lLRP = new google.maps.Point((point.x + 1) * 256 / zfactor, point.y * 256 / zfactor);
        var ll = ORMain.map.getProjection().fromPointToLatLng(lULP);
        var ur = ORMain.map.getProjection().fromPointToLatLng(lLRP);
        return "https://wms.sst-sw.rncan.gc.ca/wms/toporama_en?LAYERS={layer}&EXCEPTIONS=text/xml&FORMAT=image/{imgFormat}&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&SRS=EPSG:4326&BBOX={bbox}&WIDTH=256&HEIGHT=256".replace("{bbox}", (ll.lng() + "," + ll.lat() + "," + ur.lng() + "," + ur.lat())).replace("{layer}", layer).replace("{imgFormat}", (options.isPng) ? "png" : "jpeg")
    }
    copyright = initCopyright();
    return {
        mapType: mapType,
        bounds: new google.maps.LatLngBounds(new google.maps.LatLng(41.68, -141), new google.maps.LatLng(83.11, -52.62)),
        enable: function () {
            copyright.style.display = "block";
            return true
        },
        disable: function () {
            copyright.style.display = "none";
            return true
        },
        locked: locked,
        hasDRM: function () {
            return hasDRM
        },
        getDrmID: function () {
            return drmID
        },
        setDrmToken: function (tok) {
            drmToken = tok
        }
    }
};
ORExtMap.getMapSRTM3BaseMap = function (map, drm, layer, options, originators) {
    var copyright, options = options || {},
        options = {
            alt: options.alt || "HikeBike " + layer,
            getTileUrl: getTileUrl,
            isPng: options.isPng || false,
            maxZoom: options.maxZoom || 18,
            minZoom: options.minZoom || 5,
            name: options.name || "HikeBike",
            tileSize: new google.maps.Size(256, 256)
        },
        mapType = new google.maps.ImageMapType(options),
        locked = drm.locked || false,
        hasDRM = drm.hasDRM || false,
        drmID = drm.drmID || "";
    originators = originators;
    function initCopyright() {
        var or = ORMapCRUtils.createCRDOM(originators);
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(or);
        return or
    }
    function getTileUrl(point, zoom) {
        return "https://toolserver.org/~cmarqu/hill/" + zoom + "/" + point.x + "/" + point.y + ".png"
    }
    copyright = initCopyright();
    return {
        mapType: mapType,
        bounds: new google.maps.LatLngBounds(new google.maps.LatLng(-180, -90), new google.maps.LatLng(180, 90)),
        enable: function () {
            copyright.style.display = "block";
            return true
        },
        disable: function () {
            copyright.style.display = "none";
            return true
        },
        isLocked: function () {
            return locked
        },
        hasDRM: function () {
            return hasDRM
        }
    }
};
ORExtMap.Projection = (function () {
    function deg2rad(deg) {
        return deg * Math.PI / 180
    }
    function rad2deg(rad) {
        return rad / Math.PI * 180
    }
    function deg2secsex(angle) {
        var deg = parseInt(angle),
            min = parseInt((angle - deg) * 60),
            sec = (((angle - deg) * 60) - min) * 60;
        return sec + min * 60 + deg * 3600
    }
    var wgs84 = {
        A: 6378137,
        B: 6356752.314,
        IF: 298.257220143
    };
    function TransverseMercator(params) {
        params = params || {};
        this.a_ = params.semi_major / params.unit;
        var f_i = params.inverse_flattening;
        this.k0_ = params.scale_factor;
        var phi0 = deg2rad(params.latitude_of_origin);
        this.lamda0_ = deg2rad(params.central_meridian);
        this.FE_ = params.false_easting;
        this.FN_ = params.false_northing;
        var f = 1 / f_i;
        this.es_ = 2 * f - f * f;
        this.ep4_ = this.es_ * this.es_;
        this.ep6_ = this.ep4_ * this.es_;
        this.eas_ = this.es_ / (1 - this.es_);
        this.M0_ = this.calc_m_(phi0, this.a_, this.es_, this.ep4_, this.ep6_)
    }
    TransverseMercator.prototype.calc_m_ = function (phi, a, es, ep4, ep6) {
        return a * ((1 - es / 4 - 3 * ep4 / 64 - 5 * ep6 / 256) * phi - (3 * es / 8 + 3 * ep4 / 32 + 45 * ep6 / 1024) * Math.sin(2 * phi) + (15 * ep4 / 256 + 45 * ep6 / 1024) * Math.sin(4 * phi) - (35 * ep6 / 3072) * Math.sin(6 * phi))
    };
    TransverseMercator.prototype.forward = function (lnglat) {
        var phi = deg2rad(lnglat[1]);
        var lamda = deg2rad(lnglat[0]);
        var nu = this.a_ / Math.sqrt(1 - this.es_ * Math.pow(Math.sin(phi), 2));
        var T = Math.pow(Math.tan(phi), 2);
        var C = this.eas_ * Math.pow(Math.cos(phi), 2);
        var A = (lamda - this.lamda0_) * Math.cos(phi);
        var M = this.calc_m_(phi, this.a_, this.es_, this.ep4_, this.ep6_);
        var E = this.FE_ + this.k0_ * nu * (A + (1 - T + C) * Math.pow(A, 3) / 6 + (5 - 18 * T + T * T + 72 * C - 58 * this.eas_) * Math.pow(A, 5) / 120);
        var N = this.FN_ + this.k0_ * (M - this.M0_) + nu * Math.tan(phi) * (A * A / 2 + (5 - T + 9 * C + 4 * C * C) * Math.pow(A, 4) / 120 + (61 - 58 * T + T * T + 600 * C - 330 * this.eas_) * Math.pow(A, 6) / 720);
        return [E, N]
    };
    TransverseMercator.prototype.inverse = function (coords) {
        var E = coords[0];
        var N = coords[1];
        var e1 = (1 - Math.sqrt(1 - this.es_)) / (1 + Math.sqrt(1 - this.es_));
        var M1 = this.M0_ + (N - this.FN_) / this.k0_;
        var mu1 = M1 / (this.a_ * (1 - this.es_ / 4 - 3 * this.ep4_ / 64 - 5 * this.ep6_ / 256));
        var phi1 = mu1 + (3 * e1 / 2 - 27 * Math.pow(e1, 3) / 32) * Math.sin(2 * mu1) + (21 * e1 * e1 / 16 - 55 * Math.pow(e1, 4) / 32) * Math.sin(4 * mu1) + (151 * Math.pow(e1, 3) / 6) * Math.sin(6 * mu1) + (1097 * Math.pow(e1, 4) / 512) * Math.sin(8 * mu1);
        var C1 = this.eas_ * Math.pow(Math.cos(phi1), 2);
        var T1 = Math.pow(Math.tan(phi1), 2);
        var N1 = this.a_ / Math.sqrt(1 - this.es_ * Math.pow(Math.sin(phi1), 2));
        var R1 = this.a_ * (1 - this.es_) / Math.pow((1 - this.es_ * Math.pow(Math.sin(phi1), 2)), 3 / 2);
        var D = (E - this.FE_) / (N1 * this.k0_);
        var phi = phi1 - (N1 * Math.tan(phi1) / R1) * (D * D / 2 - (5 + 3 * T1 + 10 * C1 - 4 * C1 * C1 - 9 * this.eas_) * Math.pow(D, 4) / 24 + (61 + 90 * T1 + 28 * C1 + 45 * T1 * T1 - 252 * this.eas_ - 3 * C1 * C1) * Math.pow(D, 6) / 720);
        var lamda = this.lamda0_ + (D - (1 + 2 * T1 + C1) * Math.pow(D, 3) / 6 + (5 - 2 * C1 + 28 * T1 - 3 * C1 * C1 + 8 * this.eas_ + 24 * T1 * T1) * Math.pow(D, 5) / 120) / Math.cos(phi1);
        return [rad2deg(lamda), rad2deg(phi)]
    };
    function Projection(proj, scale0) {
        this.proj = proj;
        this.scale0 = scale0
    }
    Projection.prototype.fromLatLngToPoint = function (latLng) {
        var coord = this.proj.forward(latLng.lat(), latLng.lng());
        return new google.maps.Point((coord.x - this.proj.origin.x) / this.scale0, (this.proj.origin.y - coord.y) / this.scale0)
    };
    Projection.prototype.fromPointToLatLng = function (point, noWrap) {
        var coord = this.proj.inverse(this.proj.origin.x + point.x * this.scale0, this.proj.origin.y - point.y * this.scale0);
        return new google.maps.LatLng(coord.lat, coord.lng, noWrap)
    };
    return {
        swisstopo: function () {
            return {
                name: "21781",
                origin: {
                    x: 420000,
                    y: 350000
                },
                forward: function (lat, lng) {
                    lat = (deg2secsex(lat) - 169028.66) / 10000;
                    lng = (deg2secsex(lng) - 26782.5) / 10000;
                    var lat2 = Math.pow(lat, 2),
                        lat3 = Math.pow(lat, 3),
                        lng2 = Math.pow(lng, 2),
                        lng3 = Math.pow(lng, 3);
                    return {
                        x: 600072.37 + 211455.93 * lng - 10938.51 * lng * lat - 0.36 * lng * lat2 - 44.54 * lng3,
                        y: 200147.07 + 308807.95 * lat + 3745.25 * lng2 + 76.63 * lat2 - 194.56 * lng2 * lat + 119.79 * lat3
                    }
                },
                inverse: function (x, y) {
                    x = (x - 600000) / 1000000;
                    y = (y - 200000) / 1000000;
                    var y2 = Math.pow(y, 2),
                        y3 = Math.pow(y, 3),
                        x2 = Math.pow(x, 2),
                        x3 = Math.pow(x, 3);
                    return {
                        lat: 100 / 36 * (16.9023892 + 3.238272 * y - 0.270978 * x2 - 0.002528 * y2 - 0.0447 * x2 * y - 0.014 * y3),
                        lng: 100 / 36 * (2.6779094 + 4.728982 * x + 0.791484 * x * y + 0.1306 * x * y2 - 0.0436 * x3)
                    }
                }
            }
        },
        geoportal: function (factors) {
            return {
                name: "IGNF:GEOPORTAL{territory}",
                origin: {
                    x: 0,
                    y: 0
                },
                forward: function (lat, lng) {
                    return {
                        x: lng * factors.Kx,
                        y: lat * factors.Ky
                    }
                },
                inverse: function (x, y) {
                    return {
                        lat: y / factors.Ky,
                        lng: x / factors.Kx
                    }
                }
            }
        },
        geoportalV3: function () {
            return {
                name: "IGNF:GEOPORTAL{territory}",
                origin: {
                    x: -20037508,
                    y: 20037508
                },
                forward: function (lat, lng) {
                    return {
                        x: wgs84.A * deg2rad(lng),
                        y: wgs84.A * Math.log(Math.tan(deg2rad(lat / 2) + Math.PI / 4))
                    }
                },
                inverse: function (x, y) {
                    return {
                        lng: rad2deg(x) / wgs84.A,
                        lat: rad2deg(2 * (Math.atan(Math.exp(y / 6378137)) - Math.PI / 4))
                    }
                }
            }
        },
        miller: function () {
            return {
                name: "IGNF:MILLER",
                origin: {
                    x: 0,
                    y: 0
                },
                forward: function (lat, lng) {
                    return {
                        x: deg2rad(lng) * 6378137,
                        y: 6378137 * Math.log(Math.tan(Math.PI / 4 + deg2rad(lat) * 0.4)) * 1.25
                    }
                },
                inverse: function (x, y) {
                    return {
                        lng: rad2deg(x) / 6378137,
                        lat: rad2deg(Math.atan(Math.exp(y / 6378137 / 1.25)) - Math.PI / 4) / 0.4
                    }
                }
            }
        },
        iberpix: function (zone) {
            var proj = new TransverseMercator({
                semi_major: wgs84.A,
                inverse_flattening: wgs84.IF,
                unit: 1,
                scale_factor: 0.9996,
                false_easting: 500000,
                latitude_of_origin: 0,
                false_northing: 0,
                central_meridian: zone * 6 - 183
            });
            return {
                name: "UTM",
                origin: {
                    x: 0,
                    y: 0
                },
                forward: function (lat, lng) {
                    var point = proj.forward([lng, lat]);
                    return {
                        x: point[0],
                        y: point[1]
                    }
                },
                inverse: function (x, y) {
                    var ll = proj.inverse([x, y]);
                    return {
                        lat: ll[1],
                        lng: ll[0]
                    }
                }
            }
        },
        google: function (proj, scale0) {
            return new Projection(proj, scale0)
        }
    }
})();
var ORMapManager = function (map, maps, customControl) {
    var nbMaps = maps.length,
        refMapId = "",
        initMapId, registryMapID = {},
        registryOverlayMapID = {},
        extDynMap = [],
        fixMap = [],
        mapBounds = [],
        mapBoundsRef = [],
        refOverlayList = [],
        refAOverlayList = [],
        refMapList = "";
    function activateMaps(currentMapId) {
        if (!currentMapId || currentMapId == refMapId) {
            return
        }
        var omt = registryMapID[refMapId];
        if (omt && omt.type != ORRegisteredMapType.GMAPS) {
            omt.rmap.disable()
        }
        var mt = registryMapID[currentMapId];
        if (mt.rmap.hasDRM && mt.rmap.hasDRM()) {}
        setTimeout(function () {
            refMapId = currentMapId;
            if (mt.type == ORRegisteredMapType.GMAPS) {
                map.setMapTypeId(mt.rmap)
            } else {
                mt.rmap.enable();
                map.setMapTypeId(mt.name)
            }
            enableOverlays(currentMapId)
        }, 200)
    }
    function activateOverlays(currentOverlayId) {
        var olt = registryOverlayMapID[refMapId];
        if (olt) {
            for (var i = 0, j = olt.length; i < j; i++) {
                var ol = olt[i];
                if (ol.name == currentOverlayId) {
                    switch (ol.type) {
                    case ORRegisteredMapType.GLOL:
                        if (ol.activated) {
                            ol.activated = false;
                            var mt = registryMapID[refMapId];
                            map.setMapTypeId(mt.rmap)
                        } else {
                            ol.activated = true;
                            map.setMapTypeId(ol.rmap)
                        }
                        break;
                    case ORRegisteredMapType.GOL:
                        if (ol.activated) {
                            ol.activated = false;
                            ol.rmap.setMap(null)
                        } else {
                            ol.activated = true;
                            ol.rmap.setMap(map)
                        }
                        break;
                    case ORRegisteredMapType.EXTOL:
                        if (ol.activated) {
                            ol.activated = false;
                            map.overlayMapTypes.setAt(ol.idx, null);
                            ol.rmap.disable()
                        } else {
                            ol.activated = true;
                            map.overlayMapTypes.setAt(ol.idx, ol.rmap.mapType);
                            ol.rmap.enable()
                        }
                        break
                    }
                }
            }
        }
    }
    function showOverlay(ol) {
        switch (ol.type) {
        case ORRegisteredMapType.GLOL:
            map.setMapTypeId(ol.rmap);
            break;
        case ORRegisteredMapType.GOL:
            ol.rmap.setMap(map);
            break;
        case ORRegisteredMapType.EXTOL:
            map.overlayMapTypes.setAt(ol.idx, ol.rmap.mapType);
            ol.rmap.enable();
            break
        }
    }
    function hideOverlay(ol) {
        switch (ol.type) {
        case ORRegisteredMapType.GLOL:
            break;
        case ORRegisteredMapType.GOL:
            ol.rmap.setMap(null);
            break;
        case ORRegisteredMapType.EXTOL:
            map.overlayMapTypes.setAt(ol.idx, null);
            ol.rmap.disable();
            break
        }
    }
    function enableMapsAndOverlays() {
        enableMaps();
        enableOverlays(refMapId)
    }
    function enableOverlays(currentMapId) {
        var allowedOL = [],
            allowedAOL = [],
            nbBounds, center = map.getCenter();
        if (currentMapId) {
            var ovt = registryOverlayMapID[currentMapId];
            if (ovt) {
                for (var i = 0, j = ovt.length; i < j; i++) {
                    var checkOL = false;
                    if (ovt[i].type != ORRegisteredMapType.GOL && ovt[i].type != ORRegisteredMapType.GLOL) {
                        nbBounds = ovt[i].rmap.bounds.length;
                        if (nbBounds) {
                            for (var k = 0; k < nbBounds; k++) {
                                if (ovt[i].rmap.bounds[k].contains(center)) {
                                    checkOL = true
                                }
                            }
                        } else {
                            if (ovt[i].rmap.bounds.contains(center)) {
                                checkOL = true
                            }
                        }
                    } else {
                        checkOL = true
                    }
                    if (checkOL) {
                        if (ovt[i].rmap.hasDRM && ovt[i].rmap.hasDRM()) {
                            var kdrm = "";
                            if (ORMain.GLOBAL_DRM[ovt[i].rmap.getDrmID()] && ORMain.GLOBAL_DRM[ovt[i].rmap.getDrmID()] != "") {
                                kdrm = ORMain.GLOBAL_DRM[ovt[i].rmap.getDrmID()];
                                ovt[i].rmap.setDrmStartCount(false)
                            } else {
                                if (ORMain.USER_DRM && ORMain.USER_DRM[ovt[i].rmap.getDrmID()] && ORMain.USER_DRM[ovt[i].rmap.getDrmID()] != "") {
                                    kdrm = ORMain.USER_DRM[ovt[i].rmap.getDrmID()];
                                    ovt[i].rmap.setDrmStartCount(true)
                                }
                            }
                            ovt[i].rmap.setDrmKey(kdrm)
                        }
                        allowedOL.push(ovt[i]);
                        allowedAOL[ovt[i].name] = ovt[i]
                    }
                }
            }
            if (allowedOL.length == 0 || refAOverlayList.join() != allowedOL.join()) {
                for (var key in refAOverlayList) {
                    if (!allowedAOL[key]) {
                        if (refAOverlayList[key].activated) {
                            hideOverlay(refAOverlayList[key])
                        }
                    }
                }
                for (var key in allowedAOL) {
                    if (!refAOverlayList[key]) {
                        if (allowedAOL[key].activated) {
                            showOverlay(allowedAOL[key])
                        }
                    }
                }
                refOverlayList = allowedOL;
                refAOverlayList = allowedAOL
            }
            customControl.setListOverlay(refOverlayList)
        }
    }
    function enableMaps() {
        refMapList = "";
        var nbBounds, found = false,
            foundThisMap = true,
            center = map.getCenter(),
            nbExtDynMap = extDynMap.length;
        allowedType = [];
        if (center) {
            for (var i = 0; i < nbExtDynMap; i++) {
                nbBounds = mapBounds[i].length;
                found = false;
                m = extDynMap[i];
                for (var j = 0; j < nbBounds; j++) {
                    if (mapBounds[i][j].contains(center)) {
                        if (m.rmap.hasDRM && m.rmap.hasDRM()) {
                            var kdrm = "";
                            if (ORMain.GLOBAL_DRM[m.rmap.getDrmID()] && ORMain.GLOBAL_DRM[m.rmap.getDrmID()] != "") {
                                kdrm = ORMain.GLOBAL_DRM[m.rmap.getDrmID()];
                                m.rmap.setDrmStartCount(false)
                            } else {
                                if (ORMain.USER_DRM && ORMain.USER_DRM[m.rmap.getDrmID()] && ORMain.USER_DRM[m.rmap.getDrmID()] != "") {
                                    kdrm = ORMain.USER_DRM[m.rmap.getDrmID()];
                                    m.rmap.setDrmStartCount(true)
                                }
                            }
                            if (kdrm != "") {
                                m.rmap.setDrmKey(kdrm);
                                m.rmap.locked = false
                            } else {
                                m.rmap.locked = true
                            }
                        }
                        allowedType.push(m);
                        found = true;
                        break
                    }
                }
                if (!found && refMapId === extDynMap[i].name) {
                    foundThisMap = false
                }
            }
            var allType = fixMap.concat(allowedType);
            refMapList = allType.join();
            customControl.setListMapType(allType, initMapId);
            if (!foundThisMap) {
                activateMaps(initMapId)
            }
        }
    }
    function registerMaps() {
        var bounds;
        var countOL = 0;
        for (var i = 0; i < nbMaps; i++) {
            var mt = maps[i];
            switch (mt.type) {
            case ORRegisteredMapType.GMAPS:
                registryMapID[mt.name] = mt;
                fixMap.push(mt);
                if (mt.rmap == google.maps.MapTypeId.ROADMAP && !initMapId) {
                    initMapId = mt.name
                }
                break;
            case ORRegisteredMapType.EXTFIXMAPS:
            case ORRegisteredMapType.EXTDYNMAPS:
                registryMapID[mt.name] = mt;
                map.mapTypes.set(mt.name, mt.rmap.mapType);
                if (mt.type == ORRegisteredMapType.EXTDYNMAPS) {
                    bounds = mt.rmap.bounds;
                    mapBounds.push(bounds.length ? bounds : [bounds]);
                    mapBoundsRef.push(mt.name);
                    extDynMap.push(mt)
                } else {
                    fixMap.push(mt)
                }
                break;
            case ORRegisteredMapType.GOL:
            case ORRegisteredMapType.GLOL:
                for (var key in mt.mapNameList) {
                    if (!registryOverlayMapID[mt.mapNameList[key]]) {
                        registryOverlayMapID[mt.mapNameList[key]] = []
                    }
                    registryOverlayMapID[mt.mapNameList[key]].push(mt)
                }
                break;
            case ORRegisteredMapType.EXTOL:
                mt.idx = countOL++;
                for (var key in mt.mapNameList) {
                    if (!registryOverlayMapID[mt.mapNameList[key]]) {
                        registryOverlayMapID[mt.mapNameList[key]] = []
                    }
                    registryOverlayMapID[mt.mapNameList[key]].push(mt)
                }
                bounds = mt.rmap.bounds;
                break
            }
        }
    }
    registerMaps();
    enableMaps();
    activateMaps(initMapId);
    google.maps.event.addListener(map, "maptypeid_changed", activateMaps);
    google.maps.event.addListener(map, "bounds_changed", enableMapsAndOverlays);
    google.maps.event.addListener(map, "overlays_changed", activateOverlays);
    this.getRefMapList = function () {
        return refMapList
    };
    this.getRefOverlayList = function () {
        return refOverlayList
    };
    this.getRegistryOverlayMapID = function () {
        return registryOverlayMapID
    };
    this.forceEnableMaps = function () {
        customControl.resetSignature();
        enableMaps();
        if (registryMapID[refMapId] && registryMapID[refMapId].rmap.locked) {
            activateMaps(initMapId)
        }
    }
};
var ORRegisteredMap = function (name, type, rmap, mapNameList) {
    this.name = name;
    this.type = type;
    this.rmap = rmap;
    this.mapNameList = mapNameList || [];
    this.activated = false;
    this.idx = 0;
    this.toString = function () {
        return this.name
    }
};
var ORRegisteredMapType = {
    GMAPS: "GMAPS",
    EXTFIXMAPS: "EXTFIXMAPS",
    EXTDYNMAPS: "EXTDYNMAPS",
    GLOL: "GLOL",
    GOL: "GOL",
    EXTOL: "EXTOL"
};
var ORMapCRUtils = {
    createCRDOM: function (originators) {
        var or = document.createElement("div");
        for (var i = 0, j = originators.length; i < j; i++) {
            var oru = originators[i];
            var link = document.createElement("a");
            link.href = oru.url;
            link.title = oru.textUrl;
            link.target = "_blank";
            link.style.borderWidth = "0";
            link.style.fontFamily = "Arial,sans-serif";
            link.style.fontSize = "10px";
            link.style.color = "black";
            link.style.display = "inline";
            link.style.padding = "3px";
            link.style.margin = "3px";
            if (oru.pictureUrl) {
                if (oru.textPictureUrl) {
                    var tp = document.createElement("span");
                    tp.innerHTML = oru.textPictureUrl;
                    tp.style.backgroundColor = "white";
                    link.appendChild(tp)
                }
                var img = document.createElement("img");
                img.alt = oru.textUrl;
                img.src = oru.pictureUrl;
                img.style.padding = img.style.margin = img.style.borderWidth = "0";
                link.appendChild(img)
            } else {
                link.style.backgroundColor = "white";
                link.style.border = "1px gray solid";
                link.innerHTML = oru.textUrl
            }
            or.style.display = "none";
            or.appendChild(link)
        }
        or.style.margin = "3px";
        return or
    }
};
var ORCustomMapControl = function (domID, map) {
    var domID = domID,
        map = map,
        signature, signatureOL, blListener, domReady = false;
    function createDOM() {
        var cc = document.getElementById(domID);
        if (cc) {
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(cc)
        }
        domReady = true;
        $j("#or_control_layer_anchor").show();
        $j("#or_control_layer_list").hide();
        $j("#or_control_layer_anchor").click(function () {
            $j("#or_control_layer_list").show();
            $j("#or_control_layer_anchor").hide()
        });
        $j("#layer_control_close").click(function () {
            $j("#or_control_layer_list").hide();
            $j("#or_control_layer_anchor").show()
        })
    }
    function updateDOM(bllist, refMapId) {
        var outBL = "";
        for (var i = 0, j = bllist.length; i < j; i++) {
            if (bllist[i].type == ORRegisteredMapType.EXTDYNMAPS && bllist[i].rmap.locked) {
                if (SHOW_MAP_DRM == 1) {
                    outBL += '<label><input name="or_bl_entity" type="radio" ' + ((bllist[i].type == ORRegisteredMapType.EXTDYNMAPS && bllist[i].rmap.locked) ? 'disabled="disabled"' : "") + ((bllist[i].name == refMapId) ? 'checked="checked"' : "") + 'value="' + bllist[i].name + '">';
                    outBL += '<a href="#" onclick="javascript:ORMain.showAlertOR(\'Test\');return false;">' + bllist[i].name + "</a>";
                    outBL += "</label>"
                }
            } else {
                outBL += '<label><input name="or_bl_entity" type="radio" ' + ((bllist[i].type == ORRegisteredMapType.EXTDYNMAPS && bllist[i].rmap.locked) ? 'disabled="disabled"' : "") + ((bllist[i].name == refMapId) ? 'checked="checked"' : "") + 'value="' + bllist[i].name + '">';
                outBL += bllist[i].name;
                outBL += "</label>"
            }
        }
        var blZ = document.getElementById("blZone");
        if (blZ) {
            blZ.innerHTML = outBL;
            $j("input[name='or_bl_entity']").bind("click", function () {
                if (($j(this).val() === ORConstants.TOPO_FRANCE || $j(this).val() === ORConstants.TOPO_FRANCE_CLS || $j(this).val() === ORConstants.TOPO_FRANCE_STD) && !ORAccMain.isUserConnected()) {
                    Modalbox.show("account/userNotAuthenticated.php", {
                        slideDownDuration: '0',
                        title: ORConstants.MODAL_TITLE,
                        width: ORConstants.MODAL_MEDIUM_WIDTH,
                        height: ORConstants.MODAL_MEDIUM_HEIGHT,
                        overlayClose: false
                    })
                } else {
                    google.maps.event.trigger(map, "maptypeid_changed", $j(this).val())
                }
            })
        }
    }
    function updateOLDOM(ollist) {
        var outOL = "";
        for (var i = 0, j = ollist.length; i < j; i++) {
            outOL += '<label><input type="checkbox"' + ((ollist[i].activated) ? 'checked="checked"' : "") + 'value="' + ollist[i].name + '">' + ollist[i].name + "</label>"
        }
        var olZ = document.getElementById("olZone");
        if (olZ) {
            olZ.innerHTML = "";
            if (outOL != "") {
                olZ.innerHTML = '<div class="or-layer-control-header or-layer-control-header-ol">' + MAPS_LAYER + "</div>"
            }
            olZ.innerHTML += outOL;
            $j("input[type='checkbox']").bind("click", function () {
                if (($j(this).val() === ORConstants.OL_ZPS || $j(this).val() === ORConstants.OL_SIC || $j(this).val() === ORConstants.OL_PNR || $j(this).val() === ORConstants.OL_RNR || $j(this).val() === ORConstants.OL_SA || $j(this).val() === ORConstants.OL_CIS || $j(this).val() === ORConstants.OL_ADM || $j(this).val() === ORConstants.OL_CAD) && !ORAccMain.isUserConnected()) {
                    Modalbox.show("account/userNotAuthenticated.php", {
                        slideDownDuration: '0',
                        title: ORConstants.MODAL_TITLE,
                        width: ORConstants.MODAL_MEDIUM_WIDTH,
                        height: ORConstants.MODAL_MEDIUM_HEIGHT,
                        overlayClose: false
                    })
                } else {
                    google.maps.event.trigger(map, "overlays_changed", $j(this).val())
                }
            })
        }
    }
    return {
        setListMapType: function (lm, refMapId) {
            if (!domReady) {
                createDOM()
            }
            var newMapTypeId = lm.toString();
            if (signature != newMapTypeId) {
                updateDOM(lm, refMapId);
                signature = newMapTypeId
            }
        },
        setListOverlay: function (lo) {
            var newOverlayId = lo.toString();
            if (signatureOL != newOverlayId) {
                updateOLDOM(lo);
                signatureOL = newOverlayId
            }
        },
        setDefaultMap: function (mapid) {
            var found = false;
            $j.each($j("#blZone input"), function (key, value) {
                if (!found && value.value == mapid) {
                    value.checked = "checked";
                    found = true
                }
            })
        },
        resetSignature: function () {
            signature = signatureOL = ""
        }
    }
};
var ORMainUTM = {
    polyUTM: [],
    labelUTM: [],
    offsetNLabel: 30,
    offsetELabel: 30,
    displayUTMGrid: function (map) {
        ORMainUTM.hideUTMGrid();
        if (map.getZoom() > 12) {
            var bounds = map.getBounds();
            var NE_tab = ORUTMConvertor.calcCarMatrix(bounds.getNorthEast().lat(), bounds.getSouthWest().lng(), bounds.getSouthWest().lat(), bounds.getNorthEast().lng());
            var N_tab = NE_tab.N_TAB;
            var m = $j("#map");
            var dLng = Math.abs(bounds.getSouthWest().lng() - bounds.getNorthEast().lng());
            var lon_offset = ORMainUTM.offsetNLabel * dLng / (m.width());
            for (var i = 0, j = N_tab.length; i < j; i++) {
                var liN = N_tab[i];
                var path = [];
                path.push(new google.maps.LatLng(liN.LATS, liN.LONS));
                path.push(new google.maps.LatLng(liN.LATE, liN.LONE));
                poly = new google.maps.Polyline({
                    path: path,
                    strokeColor: "#00668d",
                    strokeWeight: 1,
                    clickable: false,
                    editable: false
                });
                poly.setMap(map);
                var myOptions = {
                    content: '<div class="gridlabeltext">' + liN.UTM + "</div>",
                    disableAutoPan: true,
                    pixelOffset: new google.maps.Size(-5, -10),
                    position: new google.maps.LatLng(liN.LATS, bounds.getSouthWest().lng() + lon_offset),
                    closeBoxURL: "",
                    isHidden: false,
                    enableEventPropagation: false
                };
                var lab = new InfoBox(myOptions);
                lab.open(map);
                ORMainUTM.polyUTM.push(poly);
                ORMainUTM.labelUTM.push(lab)
            }
            var E_tab = NE_tab.E_TAB;
            var dLat = Math.abs(bounds.getSouthWest().lat() - bounds.getNorthEast().lat());
            var lat_offset = ORMainUTM.offsetELabel * dLat / (m.height());
            for (var i = 0, j = E_tab.length; i < j; i++) {
                var liE = E_tab[i];
                var path = [];
                path.push(new google.maps.LatLng(liE.LATS, liE.LONS));
                path.push(new google.maps.LatLng(liE.LATE, liE.LONE));
                poly = new google.maps.Polyline({
                    path: path,
                    strokeColor: "#00668d",
                    strokeWeight: 1,
                    clickable: false,
                    editable: false
                });
                poly.setMap(map);
                var myOptions = {
                    content: '<div class="gridlabeltext">' + liE.UTM + "</div>",
                    disableAutoPan: true,
                    pixelOffset: new google.maps.Size(-25, -10),
                    position: new google.maps.LatLng(bounds.getNorthEast().lat() - lat_offset, liE.LONS),
                    closeBoxURL: "",
                    isHidden: false,
                    enableEventPropagation: false
                };
                var lab = new InfoBox(myOptions);
                lab.open(map);
                ORMainUTM.polyUTM.push(poly);
                ORMainUTM.labelUTM.push(lab)
            }
            return true
        } else {
            return false
        }
    },
    hideUTMGrid: function () {
        if (ORMainUTM.polyUTM.length > 0) {
            for (var i = 0, j = ORMainUTM.polyUTM.length; i < j; i++) {
                var p = ORMainUTM.polyUTM.pop();
                p.setMap(null);
                delete p
            }
        }
        if (ORMainUTM.labelUTM.length > 0) {
            for (var i = 0, j = ORMainUTM.labelUTM.length; i < j; i++) {
                var p = ORMainUTM.labelUTM.pop();
                p.setMap(null);
                delete p
            }
        }
    }
};
var ORUTMConvertor = (function () {
    var DEG2RAD = 0.01745329252;
    var _a = 6378137;
    var _f = 1 / 298.257222101;
    var _k0 = 0.9996;
    var _E0 = 500000;
    var _N0 = 0;
    var _e2 = 2 * _f - Math.pow(_f, 2);
    var _ep2 = _e2 / (1 - _e2);
    var _e4 = _e2 * _e2;
    var _e6 = _e4 * _e2;
    var _m1s = (1 - _e2 / 4 - 3 * _e4 / 64 - 5 * _e6 / 256);
    var _m2s = (3 * _e2 / 8 + 3 * _e4 / 32 + 45 * _e6 / 1024);
    var _m3s = (15 * _e4 / 256 + 45 * _e6 / 1024);
    var _m4s = (35 * _e6 / 3072);
    var _m = _mz = _m1 = _m2 = _m3 = _m4 = _mz1 = _mz2 = _mz3 = _mz4 = 0;
    var _phi = _phiz = _lambda = _lambdaz = 0;
    var _lon = 0;
    var _zone = 31;
    var _N = _E = 0;
    function _getZone(lon) {
        return Math.floor((lon + 180) / 6) + 1
    }
    function _setConstantsLatLon2UTM(lat, lon) {
        _N0 = 0;
        if (lat < 0) {
            _N0 = 10000000
        }
        _lon = lon;
        _zone = _getZone(_lon);
        _lambdaz = DEG2RAD * (6 * _zone - 183);
        _lambda = DEG2RAD * lon;
        _phi = lat * DEG2RAD;
        _m1 = _m1s * _phi;
        _m2 = _m2s * Math.sin(2 * _phi);
        _m3 = _m3s * Math.sin(4 * _phi);
        _m4 = _m4s * Math.sin(6 * _phi);
        _m1z = _m1s * _phiz;
        _m2z = _m2s * Math.sin(2 * _phiz);
        _m3z = _m3s * Math.sin(4 * _phiz);
        _m4z = _m4s * Math.sin(6 * _phiz);
        _m = _a * (_m1 - _m2 + _m3 - _m4);
        _mz = _a * (_m1z - _m2z + _m3z - _m4z)
    }
    function _calculateEN() {
        var t = Math.pow(Math.tan(_phi), 2);
        var c = (_e2 * Math.pow(Math.cos(_phi), 2)) / (1 - _e2);
        var a1 = (_lambda - _lambdaz) * Math.cos(_phi);
        var v = _a / Math.sqrt((1 - _e2 * Math.pow(Math.sin(_phi), 2)));
        var E = _E0 + _k0 * v * (a1 + (1 - t + c) * Math.pow(a1, 3) / 6 + (5 - 18 * t + Math.pow(t, 2) + 72 * c - 58 * _ep2) * Math.pow(a1, 5) / 120);
        var N = _N0 + _k0 * (_m - _mz + v * Math.tan(_phi) * (Math.pow(a1, 2) / 2 + (5 - t + 9 * c + 4 * Math.pow(c, 2)) * Math.pow(a1, 4) / 24 + (61 - 58 * t + Math.pow(t, 2) + 600 * c - 330 * _ep2) * Math.pow(a1, 6) / 720));
        return {
            E: E,
            N: N,
            Z: _zone,
            N0: _N0
        }
    }
    function _setConstantsUTM2LatLon(E, N, Z) {
        _E = E;
        _N = N;
        _zone = Z;
        _lambdaz = DEG2RAD * (6 * _zone - 183);
        _m1z = _m1s * _phiz;
        _m2z = _m2s * Math.sin(2 * _phiz);
        _m3z = _m3s * Math.sin(4 * _phiz);
        _m4z = _m4s * Math.sin(6 * _phiz);
        _mz = _a * (_m1z - _m2z + _m3z - _m4z)
    }
    function _calculateLatLon() {
        var up = (_mz + (_N - _N0) / _k0) / (_a * (1 - _e2 / 4 - 3 * _e4 / 64 - 5 * _e6 / 256));
        var ep = (1 - Math.sqrt((1 - _e2))) / (1 + Math.sqrt((1 - _e2)));
        var mp1 = (3 * ep / 2 - 27 * Math.pow(ep, 3) / 32) * Math.sin(2 * up);
        var mp2 = (21 * Math.pow(ep, 2) / 16 - 55 * Math.pow(ep, 4) / 32) * Math.sin(4 * up);
        var mp3 = (151 * Math.pow(ep, 3) / 96) * Math.sin(6 * up);
        var mp4 = (1097 * Math.pow(ep, 4) / 512) * Math.sin(8 * up);
        var phip = up + mp1 + mp2 + mp3 + mp4;
        var t = Math.pow(Math.tan(phip), 2);
        var c = _ep2 * Math.pow(Math.cos(phip), 2);
        var v = _a / Math.sqrt(1 - _e2 * Math.pow(Math.sin(phip), 2));
        var r = _a * (1 - _e2) / Math.pow((1 - _e2 * Math.pow(Math.sin(phip), 2)), 1.5);
        var d = (_E - _E0) / (v * _k0);
        var phi = phip - (v * Math.tan(phip) / r) * (Math.pow(d, 2) / 2 - (5 + 3 * t + 10 * c - 4 * Math.pow(c, 2) - 9 * _ep2) * Math.pow(d, 4) / 24 + (61 + 90 * t + 298 * c + 45 * Math.pow(t, 2) - 252 * _ep2 - 3 * Math.pow(c, 2)) * Math.pow(d, 6) / 720);
        var lambda = _lambdaz + (d - (1 + 2 * t + c) * Math.pow(d, 3) / 6 + (5 - 2 * c + 28 * t - 3 * Math.pow(c, 2) + 8 * _ep2 + 24 * Math.pow(t, 2)) * Math.pow(d, 5) / 120) / Math.cos(phip);
        return {
            LAT: phi / DEG2RAD,
            LON: lambda / DEG2RAD
        }
    }
    function _convertLatLon2UTM(lat, lon) {
        _setConstantsLatLon2UTM(lat, lon);
        return _calculateEN()
    }
    function _convertUTM2LatLon(E, N, Z) {
        _setConstantsUTM2LatLon(E, N, Z);
        return _calculateLatLon()
    }
    function _locateBreakZone(lat, zone) {
        var lon_break = 6 * zone - 180;
        var UTM_max_zone_1 = _convertLatLon2UTM(lat, (lon_break - 1e-7));
        var UTM_min_zone_2 = _convertLatLon2UTM(lat, lon_break);
        return {
            Emax: Math.round(UTM_max_zone_1 / 1000),
            ZEmax: _zone,
            Emin: Math.round(UTM_min_zone_2 / 1000),
            ZEmin: _zone
        }
    }
    function _getTransNPt(pt1, pt2, lon_break_max) {
        var a = (pt2.LAT - pt1.LAT) / (pt2.LON - pt1.LON);
        var b = pt2.LAT - a * pt2.LON;
        var lat = a * lon_break_max + b;
        return {
            LAT: lat,
            LON: lon_break_max
        }
    }
    function _getTransEPt(pt1, pt2, lon_break_max, lats, late) {
        var a = (pt2.LAT - pt1.LAT) / (pt2.LON - pt1.LON);
        var b = pt2.LAT - a * pt2.LON;
        var lat = a * lon_break_max + b;
        if (lat <= lats && lat >= late) {
            return {
                LAT: lat,
                LON: lon_break_max
            }
        }
        return pt2
    }
    return {
        getUTMFromLatLon: function (lat, lon) {
            var u = _convertLatLon2UTM(lat, lon);
            if (u.N0 > 0) {
                return {
                    E: Math.ceil(u.E),
                    N: Math.floor(u.N),
                    Z: u.Z + " S"
                }
            } else {
                return {
                    E: Math.ceil(u.E),
                    N: Math.floor(u.N),
                    Z: +u.Z + " N"
                }
            }
        },
        convertLatLon2UTM: function (lat, lon) {
            _convertLatLon2UTM(lat, lon)
        },
        convertUTM2LatLon: function (E, N, Z) {
            _convertUTM2LatLon(E, N, Z)
        },
        calcCarMatrix: function (lats, lons, late, lone) {
            var zs = _getZone(lons);
            var ze = _getZone(lone);
            var lon_break_min = 0;
            var lon_break_max = 0;
            var flg_break = false;
            if (zs != ze) {
                lon_break_min = 6 * Math.min(zs, ze) - 180.00000001;
                lon_break_max = 6 * Math.min(zs, ze) - 180;
                flg_break = true
            }
            var utm_top_left = _convertLatLon2UTM(lats, lons);
            var utm_top_right = _convertLatLon2UTM(lats, lone);
            var utm_bottom_left = _convertLatLon2UTM(late, lons);
            var utm_bottom_right = _convertLatLon2UTM(late, lone);
            var utm_limits = null;
            if (flg_break) {
                var utm_top_break_min = _convertLatLon2UTM(lats, lon_break_min);
                var utm_bottom_break_min = _convertLatLon2UTM(late, lon_break_min);
                var utm_top_break_max = _convertLatLon2UTM(lats, lon_break_max);
                var utm_bottom_break_max = _convertLatLon2UTM(late, lon_break_max);
                utm_limits = {
                    TL_E: Math.ceil(utm_top_left.E / 1000),
                    TL_N: Math.floor(utm_top_left.N / 1000),
                    TL_Z: utm_top_left.Z,
                    TR_E: Math.floor(utm_top_right.E / 1000),
                    TR_N: Math.floor(utm_top_right.N / 1000),
                    TR_Z: utm_top_right.Z,
                    BR_E: Math.floor(utm_bottom_right.E / 1000),
                    BR_N: Math.ceil(utm_bottom_right.N / 1000),
                    BR_Z: utm_bottom_right.Z,
                    BL_E: Math.ceil(utm_bottom_left.E / 1000),
                    BL_N: Math.ceil(utm_bottom_left.N / 1000),
                    BL_Z: utm_bottom_left.Z,
                    BRM_E: Math.floor(Math.max(utm_top_break_min.E, utm_bottom_break_min.E) / 1000),
                    BRM_N: Math.ceil(Math.max(utm_top_break_min.N, utm_bottom_break_min.N) / 1000),
                    BRM_Z: utm_top_break_min.Z,
                    BRX_E: Math.ceil(Math.max(utm_top_break_max.E, utm_bottom_break_max.E) / 1000),
                    BRX_N: Math.ceil(Math.max(utm_top_break_max.E, utm_bottom_break_max.E) / 1000),
                    BRX_Z: utm_top_break_max.Z
                }
            } else {
                utm_limits = {
                    TL_E: Math.ceil(utm_top_left.E / 1000),
                    TL_N: Math.floor(utm_top_left.N / 1000),
                    TL_Z: utm_top_left.Z,
                    TR_E: Math.floor(utm_top_right.E / 1000),
                    TR_N: Math.floor(utm_top_right.N / 1000),
                    TR_Z: utm_top_right.Z,
                    BR_E: Math.floor(utm_bottom_right.E / 1000),
                    BR_N: Math.ceil(utm_bottom_right.N / 1000),
                    BR_Z: utm_bottom_right.Z,
                    BL_E: Math.ceil(utm_bottom_left.E / 1000),
                    BL_N: Math.ceil(utm_bottom_left.N / 1000),
                    BL_Z: utm_bottom_left.Z
                }
            }
            var N_tab = [];
            var ton = Math.max(utm_limits.TL_N, utm_limits.TR_N);
            var bon = Math.min(utm_limits.BL_N, utm_limits.BR_N);
            var le = Math.min(utm_limits.TL_E, utm_limits.BL_E) - 1;
            var re = Math.max(utm_limits.TR_E, utm_limits.BR_E) + 1;
            for (var i = bon; i <= ton; i++) {
                var vl = false;
                var vr = false;
                var coos = _convertUTM2LatLon(le * 1000, i * 1000, utm_limits.TL_Z);
                var cooe = _convertUTM2LatLon(re * 1000, i * 1000, utm_limits.TR_Z);
                var coot = {
                    LAT: 0,
                    LON: 0
                };
                if (flg_break) {
                    var cootm = _convertUTM2LatLon(utm_limits.BRM_E * 1000, i * 1000, utm_limits.BRM_Z);
                    var cootx = _convertUTM2LatLon(utm_limits.BRX_E * 1000, i * 1000, utm_limits.BRX_Z);
                    coot = _getTransNPt(coos, cootm, lon_break_max)
                }
                if (i <= utm_limits.TL_N && i >= utm_limits.BL_N) {
                    vl = true
                }
                if (i <= utm_limits.TR_N && i >= utm_limits.BR_N) {
                    vr = true
                }
                N_tab.push({
                    UTM: i,
                    LATS: coos.LAT,
                    LONS: coos.LON,
                    LATE: cooe.LAT,
                    LONE: cooe.LON,
                    LATC: coot.LAT,
                    LONC: coot.LON,
                    VL: vl,
                    VR: vr,
                    BR: flg_break
                })
            }
            var E_tab = [];
            if (flg_break) {
                var le = Math.min(utm_limits.TL_E, utm_limits.BL_E);
                var re = Math.max(utm_limits.TR_E, utm_limits.BRM_E);
                var ton = Math.max(utm_limits.TL_N, utm_limits.TR_N) + 1;
                var bon = Math.min(utm_limits.BL_N, utm_limits.BR_N) - 1;
                for (var i = le; i <= re; i++) {
                    var vt = false;
                    var vb = false;
                    var coos = _convertUTM2LatLon(i * 1000, ton * 1000, utm_limits.BRM_Z);
                    var cooe = _convertUTM2LatLon(i * 1000, bon * 1000, utm_limits.BRM_Z);
                    var cool = _getTransEPt(coos, cooe, lon_break_max, lats, late);
                    if (i <= utm_limits.BRM_E && i >= utm_limits.TL_E) {
                        vt = true
                    }
                    if (i <= utm_limits.BR_E && i >= utm_limits.BL_E) {
                        vb = true
                    }
                    E_tab.push({
                        UTM: i,
                        LATS: coos.LAT,
                        LONS: coos.LON,
                        LATE: cool.LAT,
                        LONE: cool.LON,
                        VT: vt,
                        VB: vb
                    })
                }
                le = Math.min(utm_limits.TL_E, utm_limits.BRX_E);
                re = Math.max(utm_limits.TR_E, utm_limits.BR_E);
                for (var i = le; i <= re; i++) {
                    var vt = false;
                    var vb = false;
                    var coos = _convertUTM2LatLon(i * 1000, ton * 1000, utm_limits.BRX_Z);
                    var cooe = _convertUTM2LatLon(i * 1000, bon * 1000, utm_limits.BRX_Z);
                    var cool = _getTransEPt(coos, cooe, lon_break_max, lats, late);
                    if (i <= utm_limits.TR_E && i >= utm_limits.BRX_E) {
                        vt = true
                    }
                    if (i <= utm_limits.BR_E && i >= utm_limits.BL_E) {
                        vb = true
                    }
                    E_tab.push({
                        UTM: i,
                        LATS: coos.LAT,
                        LONS: coos.LON,
                        LATE: cool.LAT,
                        LONE: cool.LON,
                        VT: vt,
                        VB: vb
                    })
                }
            } else {
                var le = Math.min(utm_limits.TL_E, utm_limits.BL_E);
                var re = Math.max(utm_limits.TR_E, utm_limits.BR_E);
                var ton = Math.max(utm_limits.TL_N, utm_limits.TR_N) + 1;
                var bon = Math.min(utm_limits.BL_N, utm_limits.BR_N) - 1;
                for (var i = le; i <= re; i++) {
                    var vt = false;
                    var vb = false;
                    var coos = _convertUTM2LatLon(i * 1000, ton * 1000, utm_limits.TL_Z);
                    var cooe = _convertUTM2LatLon(i * 1000, bon * 1000, utm_limits.TL_Z);
                    if (i <= utm_limits.TR_E && i >= utm_limits.TL_E) {
                        vt = true
                    }
                    if (i <= utm_limits.BR_E && i >= utm_limits.BL_E) {
                        vb = true
                    }
                    E_tab.push({
                        UTM: i,
                        LATS: coos.LAT,
                        LONS: coos.LON,
                        LATE: cooe.LAT,
                        LONE: cooe.LON,
                        VT: vt,
                        VB: vb
                    })
                }
            }
            return {
                E_TAB: E_tab,
                N_TAB: N_tab
            }
        }
    }
})();
