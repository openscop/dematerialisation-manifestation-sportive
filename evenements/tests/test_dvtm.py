# coding: utf-8

from instructions.factories import InstructionFactory
from .base import EvenementsTestsBase
from ..factories.manif import DvtmFactory
from evaluation_incidence.factories import N2kSiteConfigFactory


class DvtmTests(EvenementsTestsBase):
    """ Tests Dvtm """

    def setUp(self):
        """
        La manifestation créée ici est une manifestation compétitive
        sportive motorisée soumise à déclaration.
        """
        self.instruction = InstructionFactory.create(manif=DvtmFactory.create())
        self.instruction.manif.description = "description manif"
        self.instruction.manif.descriptions_parcours = "description parcours"
        self.instruction.manif.prenom_contact = "john"
        self.instruction.manif.nom_contact = "Doe"
        self.instruction.manif.tel_contact = "0666666666"
        self.instruction.manif.nb_vehicules_accompagnement = 5
        self.instruction.manif.nb_personnes_pts_rassemblement = 500
        self.instruction.manif.reglement_manifestation = "/reglement"
        self.instruction.manif.engagement_organisateur = "/engagement"
        self.instruction.manif.disposition_securite = "/dispo_secu"
        self.instruction.manif.itineraire_horaire = "/horaire"
        self.instruction.manif.cartographie = "/carto"
        super().setUp()
        self.n2ksiteconfig = N2kSiteConfigFactory.create()
        self.n2ksite = self.n2ksiteconfig.n2ksite
        self.n2ksite.departements.add(self.departement)
        self.n2ksiteconfig.vtm_seuil_participants = 700
        self.n2ksiteconfig.vtm_seuil_vehicules = 40
        self.n2ksiteconfig.vtm_seuil_total = 6000
        self.n2ksiteconfig.save()

    def test_get_absolute_url(self):
        """ Tester les URL d'accès """
        print('Tests Dvtm')
        manifestation = DvtmFactory.build(pk=4)
        self.assertEqual(manifestation.get_absolute_url(), '/Dvtm/4/')

    def test_completude(self):
        self.assertTrue(self.manifestation.formulaire_complet())
        self.assertTrue(self.manifestation.carto_complet())
        self.assertEqual(len(self.manifestation.liste_manquants()), 0)
        self.assertTrue(self.manifestation.dossier_complet())

    def test_display_natura2000_eval_panel(self):
        """ Vérifier que le panneau d'évaluation N2K doit s'afficher dans les cas suivants """
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # Critères nationaux
        self.manifestation.lucratif = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.lucratif = False
        self.manifestation.gros_budget = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = False
        self.manifestation.delivrance_titre = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.delivrance_titre = False
        self.manifestation.vtm_hors_circulation = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.vtm_hors_circulation = False
        # Critères de site sans formulaire ciblé
        # La manif passe sur le site n2k, les critères sont respectés
        self.manifestation.sites_natura2000.add(self.n2ksite)
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # Critère nombre de participants
        self.n2ksiteconfig.vtm_seuil_participants = 200
        self.n2ksiteconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        # Critère nombre de véhicules
        self.n2ksiteconfig.vtm_seuil_participants = 0
        self.n2ksiteconfig.vtm_seuil_vehicules = 25
        self.n2ksiteconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        # Critère nombre total
        self.n2ksiteconfig.vtm_seuil_vehicules = 0
        self.n2ksiteconfig.vtm_seuil_total = 5000
        self.n2ksiteconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        # Critères de site avec formulaire ciblé, condition sur seuil total
        # formulaire non concerné
        self.n2ksiteconfig.vtm_formulaire = ['avtm']
        self.n2ksiteconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # formulaire concerné
        self.n2ksiteconfig.vtm_formulaire = ['dvtm', 'avtm']
        self.n2ksiteconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        # Critères de site avec charte de dispense, condition sur seuil total
        self.manifestation.signature_charte_dispense_site_n2k = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2ksiteconfig.charte_dispense_acceptee = True
        self.n2ksiteconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # Pas de formulaire ciblé, dispense toujours active
        self.n2ksiteconfig.vtm_formulaire = []
        self.n2ksiteconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # Plus de dispense, critère nombre total appliqué
        self.manifestation.signature_charte_dispense_site_n2k = False
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())

    def test_legal_delay_for_declared(self):
        self.assertEqual(self.manifestation.get_delai_legal(), 60)
