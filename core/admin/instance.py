# coding: utf-8
from django.contrib import admin

from core.models.instance import Instance


@admin.register(Instance)
class InstanceAdmin(admin.ModelAdmin):
    """ Administration des instances de configuration """

    # Configuration
    list_display = ['pk', 'name', 'departement', 'workflow_ggd', 'instruction_mode', 'acces_arrondissement']
    list_filter = ['workflow_ggd', 'workflow_ddsp', 'workflow_sdis', 'workflow_cg', 'instruction_mode']
    search_fields = ['name__unaccent']
    fields = ['name', 'departement', 'instruction_mode', 'acces_arrondissement', 'color',
              'sender_email', 'email_settings', 'or_map_enabled', 'or_map_required',
              ('active_ggd','workflow_ggd', 'avis_direct_ggd', 'preavis_non_bloquant_ggd'),
              ('active_ddsp','workflow_ddsp', 'avis_direct_ddsp', 'preavis_non_bloquant_ddsp'),
              ('active_sdis','workflow_sdis', 'avis_direct_sdis', 'preavis_non_bloquant_sdis'),
              ('active_cg','workflow_cg', 'avis_direct_cg', 'preavis_non_bloquant_cg'),
              'avis_delai_federation', 'avis_delai_service', 'avis_delai_relance', 'manifestation_delay',
              'manifestation_anm_1dept_delay', 'manifestation_anm_ndept_delay', 'manifestation_dvtm_delay',
              'manifestation_avtmc_delay', 'manifestation_avtm_vp_delay', 'manifestation_avtm_vn_delay',
              'manifestation_avtm_homolog_delay']
    list_per_page = 25
    list_per_page = 25
