# coding: utf-8
from django.core.management.base import BaseCommand
from django.db import transaction

from protected_areas.models import SiteN2K, RNR, OperateurSiteN2K, AdministrateurRNR
from evaluation_incidence.models import N2kSite, RnrZone, N2kOperateurSite, RnrAdministrateur


class Command(BaseCommand):
    """
    Remplir les modèles de l'application "evaluation_incidence" avec ceux de "protected_areas"
    """
    args = ''
    help = "Transférer les données de 'protected_areas' vers 'evaluation_incidence'"

    def add_arguments(self, parser):
        parser.add_argument('--no-input', action='store_false', dest='interactive', default=True,
                            help="Ne pas demander de validation de l'utilisateur")

    def handle(self, *args, **options):
        """ Exécuter la commande """
        interactive = options.get('interactive')
        reply = ''

        if interactive is True:
            reply = input("Cette commande va transférer toutes les instances. Continuer ? [oui/*]\n")

        if reply.lower() in ["oui", "o"] or not interactive:
            with transaction.atomic():
                for n2k in SiteN2K.objects.all():
                    site = N2kSite(nom=n2k.name, site_type=n2k.site_type, index=n2k.index)
                    site.save()
                    site.departements.add(n2k.departement)

                    if hasattr(n2k, 'operateursiten2k'):
                        oper = n2k.operateursiten2k
                        new = N2kOperateurSite(nom=oper.name,
                                               email=oper.email,
                                               site=site,
                                               departement=oper.departement)
                        if oper.address.count() != 0:
                            if oper.address.count() != 1:
                                print("plusieurs adresses trouvées pour l'operateurSiteN2k : " + str(oper.pk))
                            new.adresse = oper.address.first().address
                            new.commune = oper.address.first().commune
                        text = ""
                        for pers in oper.person_in_charge.all():
                            text += pers.first_name + " " + pers.last_name + " " + pers.phone + chr(10)
                        new.contacts = text
                        new.save()

                print(str(N2kSite.objects.all().count()) + " sites Natura2000 créés sur " + str(SiteN2K.objects.all().count()))
                print(str(N2kOperateurSite.objects.all().count()) + " opérateur de site Natura 2000 créés sur " + str(OperateurSiteN2K.objects.all().count()))

                for rnr in RNR.objects.all():
                    zone = RnrZone(nom=rnr.name, code=rnr.code)
                    zone.save()
                    zone.departements.add(rnr.departement)

                    if hasattr(rnr, 'administrateurrnr'):
                        admin = rnr.administrateurrnr
                        newadm = RnrAdministrateur(nom_court=admin.short_name,
                                                   nom=admin.name,
                                                   email=admin.email,
                                                   rnr=zone,
                                                   departement=oper.departement)
                        if admin.address.count() != 0:
                            if admin.address.count() != 1:
                                print("plusieurs adresses trouvées pour l'AdministrateurRNR : " + str(admin.pk))
                            newadm.adresse = admin.address.first().address
                            newadm.commune = admin.address.first().commune
                        text = ""
                        for pers in admin.conservator.all():
                            text += pers.first_name + " " + pers.last_name + " " + pers.phone + chr(10)
                        newadm.contacts = text
                        newadm.save()

                print(str(RnrZone.objects.all().count()) + " zones RNR créées sur " + str(RNR.objects.all().count()))
                print(str(RnrAdministrateur.objects.all().count()) + " administrateurs RNR créés sur " + str(AdministrateurRNR.objects.all().count()))
        else:
            print("Commande abandonnée.")
