# coding: utf-8
from django.urls import reverse
from django.conf import settings
from django.db import models


class InstructeurQuerySet(models.QuerySet):
    """ Surtout là pour aider aux modes d'instruction des instances """

    def by_prefecture(self, prefecture):
        """
        Renvoyer les instructeurs concernés dans une préfecture
        
        Note : quand le mode d'instruction d'une instance/département est
        défini à 'Départemental', tous les instructeurs de toutes les
        sous-préfectures/arrondissements du département sont toujours concernés
        tous ensemble par les événements du département.
        """
        from core.models import Instance
        instance = prefecture.get_instance()
        mode = instance.get_instruction_mode()
        if mode in [Instance.IM_ARRONDISSEMENT, Instance.IM_ARRONDISSEMENT_COMPLEXE_5]:
            return prefecture.instructeurs.all()
        elif mode == Instance.IM_DEPARTEMENT:
            return self.filter(prefecture__arrondissement__departement=prefecture.get_departement())


class Instructeur(models.Model):
    """ Instructeur à la préfecture : instruit le dossier """

    # Champs
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="instructeur", verbose_name='utilisateur', on_delete=models.CASCADE)
    prefecture = models.ForeignKey('administration.prefecture', verbose_name='préfecture', on_delete=models.CASCADE)
    objects = InstructeurQuerySet.as_manager()

    # Overrides
    def __str__(self):
        return self.user.get_username()

    def natural_key(self):
        return self.user.username,

    def save(self, *args, **kwargs):
        super(Instructeur, self).save(*args, **kwargs)
        self.user.remplir_tableau_role()

    def delete(self, *args, **kwargs):
        user = self.user
        super(Instructeur, self).delete(*args, **kwargs)
        user.refresh_from_db()
        user.remplir_tableau_role()

    # Getter
    def get_prefecture(self):
        return self.prefecture

    def get_url(self):
        """ Renvoyer l'URL de l'objet """
        return reverse('admin:administration_instructeur_change', args=(self.pk,))

    # Meta
    class Meta:
        verbose_name = "Préfecture - Instructeur"
        verbose_name_plural = "Préfecture - Instructeurs"
        app_label = 'administration'
        default_related_name = 'instructeurs'


class Observateur(models.Model):
    """ Observateur """

    # Champs
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="observateur", verbose_name='utilisateur', on_delete=models.CASCADE)

    def __str__(self):
        return self.user.get_username()

    def save(self, *args, **kwargs):
        super(Observateur, self).save(*args, **kwargs)
        self.user.remplir_tableau_role()

    def delete(self, *args, **kwargs):
        user = self.user
        super(Observateur, self).delete(*args, **kwargs)
        user.refresh_from_db()
        user.remplir_tableau_role()

    def get_url(self):
        """ Renvoyer l'URL de l'objet """
        # pas d'admin pour cette classe
        return ''

    class Meta:
        app_label = 'administration'
        verbose_name = "Observateur"
        verbose_name_plural = "Observateurs"
        default_related_name = 'observateurs'


class Secouriste(Observateur):
    """ Observateur : Secouriste """
    observateur_ptr = models.OneToOneField("administration.observateur", parent_link=True, related_name='secouriste', on_delete=models.CASCADE)
    association = models.ForeignKey('emergencies.association1erssecours', verbose_name="Association de premiers secours", on_delete=models.CASCADE)

    def get_url(self):
        """ Renvoyer l'URL de l'objet """
        return reverse('admin:administration_secouriste_change', args=(self.pk,))

    class Meta:
        verbose_name = "Secouriste"
        verbose_name_plural = "Secouristes"
        app_label = 'administration'
        default_related_name = 'secouristes'
