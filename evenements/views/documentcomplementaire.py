# coding: utf-8
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, View
from django.forms import modelformset_factory
from django.http import HttpResponse

from organisateurs.decorators import verifier_proprietaire
from instructions.decorators import verifier_secteur_instruction
from evenements.forms.documentcomplementaire import DocumentComplementaireRequestForm
from evenements.models import *


@method_decorator(verifier_secteur_instruction(arr_wrt=True), name='dispatch')
class DocumentComplementaireRequestView(SuccessMessageMixin, CreateView):
    """ Demande de documents complémentaires """

    # Configuration
    model = DocumentComplementaire
    form_class = DocumentComplementaireRequestForm
    success_message = "Demande de documents complémentaires envoyée avec succès"

    def form_valid(self, form):
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        form.instance.manif = manif
        response = super().form_valid(form)
        manif.instruction.referent = self.request.user
        manif.instruction.save()
        form.instance.demande_doc()
        return response

    def get_context_data(self, **kwargs):
        context = super(DocumentComplementaireRequestView, self).get_context_data(**kwargs)
        context['manif'] = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        return context

    def get_success_url(self):
        return reverse('instructions:instruction_detail', kwargs={'pk': self.object.manif.instruction.pk})


@method_decorator(verifier_proprietaire, name='dispatch')
class EnvoiDocumentComplementaire(View):
    """Affichage et envoi des documents complémentaires par l'organisateur"""

    # Affichage du formulaire
    def get(self, request, pk):
        doc_formset = modelformset_factory(DocumentComplementaire, fields=('information_requise', 'document_attache'), extra=0)
        docs = DocumentComplementaire.objects.filter(manif=pk).order_by('-pk')
        done = any([False for doc in docs if not doc.document_attache] or [True, ])
        forms = doc_formset(queryset=docs)
        context = {
            'pk': pk,
            'forms': forms,
            'done': done
        }
        return render(request, 'evenements/documentcomplementaire_form_provide.html', context)

    # envoi du formulaire
    def post(self, request, pk):
        manif = get_object_or_404(Manif, pk=pk)
        doc_formset = modelformset_factory(DocumentComplementaire, fields=('information_requise', 'document_attache'), extra=0)
        forms = doc_formset(request.POST, request.FILES)
        if forms.is_valid():
            for form in forms:
                form.save()
            return redirect(manif.cerfa.get_absolute_url())
        else:
            return HttpResponse(forms.errors)
