# coding: utf-8
from django.db import models
from datetime import date
from django.utils import timezone
from django.urls import reverse
from django_fsm import FSMField
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django_fsm import transition
from django.conf import settings

from administration.models.agentlocal import AgentLocal
from core.models import Instance
from administration.models import EDSR


class PreAvisQuerySet(models.QuerySet):
    """ Queryset de base des préavis """

    # Getter
    def en_cours(self):
        """ Renvoyer les préavis dont la date de manifestation n'est pas encore passée """
        return self.filter(avis__instruction__manif__date_fin__gte=timezone.now())

    def termine(self):
        """ Renvoyer les préavis dont la date de manifestation est  passée """
        return self.filter(avis__instruction__manif__date_fin__lt=timezone.now()).order_by('-avis__instruction__manif__date_debut')

    def par_instance(self, request=None, instances=None):
        """
        Renvoyer les avis pour l'utilisateur ou les instances

        :param request: requête HTTP
        :param instances: liste d'instances
        :type instances: list<core.instance>
        """
        if request and not request.user.is_anonymous:
            # Renvoyer les avis pour l'utilisateur s'il est renseigné
            return self.filter(avis__instruction__manif__instance_id=request.user.get_instance().pk)
        elif instances:
            # Renvoyer les avis pour les instances si elles sont renseignées
            return self.filter(avis__instruction__manif__instance__in=instances)
        else:
            return self


class PreAvis(models.Model):
    """ Modèle de base des préavis """

    LIST_SERVICES = [('cgd', 'cgd'), ('services', 'services'), ('compagnies', 'compagnies'),
                     ('commissariats', 'commissariats'), ('edsr', 'edsr')]
    LIST_SERVICES_SUP = {'cgd': 'edsr', 'services': 'cg', 'compagnies': 'sdis', 'commissariats': 'ddsp', 'edsr': 'ggd'}

    # Champs
    etat = FSMField(default='demandé')  # Voir liste des états dans : documentation/Version-4_description.md
    service_concerne = models.CharField(max_length=20, choices=LIST_SERVICES)
    date_demande = models.DateField("Date de demande", default=date.today)
    date_reponse = models.DateField("Date de retour", blank=True, null=True)
    favorable = models.BooleanField("Avis favorable ?", default=False, help_text="Si coché, l'avis rendu est considéré favorable")
    prescriptions = models.TextField("prescriptions", blank=True)

    avis = models.ForeignKey("instructions.avis", verbose_name="avis", on_delete=models.CASCADE)
    agentlocal = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="Dernier intervenant", on_delete=models.SET_NULL, null=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    destination_object = GenericForeignKey()

    objects = PreAvisQuerySet.as_manager()

    # Overrides
    def __str__(self):
        service = self.service_concerne.capitalize()
        if self.destination_object:
            return ' - '.join([service, str(self.destination_object)])
        else:
            return '{0} - *** PAS DE SERVICE RATTACHE ***'.format(service)

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'avis """
        return reverse('instructions:preavis_detail', kwargs={'pk': self.pk})

    def get_services_sup(self):
        """ Retrouver le service qui a demandé le préavis """
        config = self.avis.get_instance().get_workflow_ggd()
        # Si c'est un préavis CGD, dans le cas du WF_GGD_EDSR, c'est l'EDSR qui a demandé le préavis, alors que l'avis appartient au GGD
        if config == Instance.WF_GGD_EDSR and self.service_concerne == 'cgd':
            return self.avis.get_instance().get_departement().edsr
        return self.avis.get_service()

    def get_agents(self):
        """ Renvoyer les agents concernés par le préavis """
        # Si un service n'existe plus, le préavis n'est plus relié par "destination_object"
        if self.destination_object is None:
            return []
        if self.service_concerne == "cgd":
            #  ggdagentslocaux correspond au related_name des cgd
            return self.destination_object.ggdagentslocaux.all()
        elif self.service_concerne == "services":
            return self.destination_object.cgserviceagentslocaux.all()
        elif self.service_concerne == "compagnies":
            return self.destination_object.compagnieagentslocaux.all()
        elif self.service_concerne == "commissariats":
            return self.destination_object.commissariatagentslocaux.all()
        elif self.service_concerne == "edsr":
            return self.destination_object.edsragentslocaux.all()
        else:
            return []

    def get_agents_sup(self):
        """ Renvoyer les agents concernés par l'avis du préavis """
        if self.service_concerne == "commissariats":
            return self.get_services_sup().ddspagents.all()
        elif self.service_concerne == "compagnies":
            return self.get_services_sup().sdisagents.all()
        elif self.service_concerne == "cgd":
            if type(self.get_services_sup()) is EDSR:
                return self.get_services_sup().edsragents.all()
            else:
                return self.get_services_sup().ggdagents.all()
        elif self.service_concerne == "services":
            return self.get_services_sup().cgagents.all()
        elif self.service_concerne == "edsr":
            return self.get_services_sup().ggdagents.all()
        else:
            return []

    # Action
    @transition(field='etat', source=['demandé', 'notifié'], target='rendu')
    def rendrePreavis(self, origine):
        """ Rendre le préavis """
        service = self.get_services_sup()
        self.notifier_preavis_rendu(agents=self.get_agents_sup(), origine=origine, service_destinataire=service)

    @transition(field='etat', source=['demandé', 'notifié'], target='notifié')
    def notifier_brigades(self, brigades, origine):
        """ Notifier tous les agents des brigades concernées par le préavis """
        destinataires = [agent for brigade in brigades for agent in brigade.brigadeagents.all()] + [brigade for brigade in brigades]
        from messagerie.models.message import Message
        titre = 'Nouveau dossier'
        contenu = 'Prenez connaissance des informations au sujet de la manifestation '+ self.avis.get_manifestation().nom
        Message.objects.creer_et_envoyer('action', origine, destinataires, titre,
                                         contenu, manifestation_liee=self.avis.get_manifestation(),
                                         objet_lie_nature='preavis', objet_lie_pk=self.pk)

    def notifier_creation_preavis(self, agents, origine):
        """ Notifier de la création du préavis """
        service_destinataire = self.destination_object
        destinataires = AgentLocal.users_from_agents(agents) + [service_destinataire]
        from messagerie.models.message import Message
        titre = 'Demande de préavis à traiter'
        contenu = titre + ' au sujet de la manifestation ' + self.avis.get_manifestation().nom
        Message.objects.creer_et_envoyer('action', origine, destinataires, titre, contenu,
                                         manifestation_liee=self.avis.get_manifestation(),
                                         objet_lie_pk=self.pk, objet_lie_nature="preavis")

    def notifier_suppression_preavis(self, agents, origine):
        """
        Notifier de la suppresion du préavis
        :param agents:
        :param origine:
        :return:
        """
        from messagerie.models.message import Message
        service_destinataire = self.destination_object
        destinataires = AgentLocal.users_from_agents(agents) + [service_destinataire]
        titre = 'Suppression de la demande de préavis'
        contenu = titre + ' au sujet de la manifestation ' + self.avis.get_manifestation().nom
        Message.objects.creer_et_envoyer('info_suivi', origine, destinataires, titre,
                                         contenu, manifestation_liee=self.avis.get_manifestation(),
                                         objet_lie_nature="preavis")

    def notifier_preavis_rendu(self, agents, origine,  service_destinataire=None):
        """ Notifier du rendu du préavis """
        destinataires = AgentLocal.users_from_agents(agents) + [service_destinataire]
        from messagerie.models.message import Message
        a_rendre = self.avis.get_nb_preavis_a_rendre() - 1  # Le préavis passera à l'état rendu au retour de la méthode
        if a_rendre == 0:
            titre = 'Tous les préavis sont rendus'
            contenu = titre + ' au sujet de la manifestation ' + self.avis.get_manifestation().nom + '.'
        else:
            titre = 'Préavis rendu (' + str(a_rendre) + ' en attente)'
            contenu = titre + ' au sujet de la manifestation ' + self.avis.get_manifestation().nom + '. L\'avis peut être mis en forme.'
        type_msg = 'action' if a_rendre == 0 else "info_suivi"
        Message.objects.creer_et_envoyer(type_msg, origine, destinataires, titre,  contenu,
                                         manifestation_liee=self.avis.get_manifestation(), objet_lie_nature="preavis",
                                         objet_lie_pk=self.pk)

    def notifier_ajout_pj(self, origine):
        """ Notifier de l'ajout d'une pièce jointe """
        # Aux n+1
        services = [self.get_services_sup()]
        agents = list(self.get_agents_sup())
        # Aux n+2 suivant le circuit et l'état du dossier
        instance = self.avis.get_instance()
        if self.avis.etat in ["formaté", "rendu"]:
            if self.service_concerne == "cgd":
                # Ajouter le GGD qui n'est pas renvoyé par le get_service_sup()
                if instance.get_workflow_ggd() != Instance.WF_GGD_SUBEDSR:
                    services.append(instance.departement.ggd)
                    agents += list(instance.departement.ggd.get_ggdagents())
            if self.service_concerne == "services":
                # Ajouter le CG supérieur
                agents += self.avis.destination_object.get_cgsuperieurs()
        destinataires = AgentLocal.users_from_agents(agents) + services
        from messagerie.models.message import Message
        titre = 'Pièce jointe ajoutée à un préavis'
        contenu = titre + ' au sujet de la manifestation '+self.avis.get_manifestation().nom
        Message.objects.creer_et_envoyer('info_suivi', origine, destinataires, titre, contenu,
                                         manifestation_liee=self.avis.get_manifestation(), objet_lie_nature="preavis",
                                         objet_lie_pk=self.pk)

    # Meta
    class Meta:
        verbose_name = 'pré-avis'
        verbose_name_plural = 'pré-avis'
        default_related_name = "preavis"
        app_label = "instructions"
