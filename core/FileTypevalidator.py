from django.core.exceptions import ValidationError
import os

"""
 Fonction pour verifier les types de fichiers envoyer dans la plateforme.
 Le premier est un validateur via form à mettre dans le model du fichier
 Le second étant un validateur quand on utilise pas de formulaire
"""

VALID_EXTENSIONS = ['bmp', 'csv', 'doc', 'docx', 'dotx', 'eml', 'gbd', 'gif', 'jpeg', 'jpg', 'kml',
                        'kmz', 'msg', 'odg', 'odp', 'ods', 'odt','ott', 'pdf', 'png', 'pps', 'ppsm', 'ppsx', 'ppt',
                        'pptm', 'pptx', 'rtf', 'tif', 'tiff', 'trk', 'txt', 'webp', 'xls', 'xlsm', 'xlsx', 'xml',]


def file_type_validator(value):
    ext = value.name.split('.')[-1]

    if not ext.lower() in VALID_EXTENSIONS:
        # ici on verifie si le fichier existe dans le server, dans ce cas où c'est un fichier déposé avant la création du validateur
        if not os.path.exists(value.path):
            raise ValidationError(u'Ce type de fichier n\'est pas pris en charge')


def file_type_valide_non_form(value):
    ext = value.name.split('.')[-1]
    return True if ext.lower() in VALID_EXTENSIONS or os.path.exists(value.path) else False