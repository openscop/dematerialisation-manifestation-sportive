# coding: utf-8
from django.core.files.storage import default_storage
from django.db import models
from django.utils import timezone

from configuration.directory import UploadPath
from evenements.models.manifestation import Manif, ManifRelatedModel
from messagerie.models.message import Message
from core.tasks import creation_thumbail_doc_complementaire
from core.FileTypevalidator import file_type_validator


class DocumentComplementaire(ManifRelatedModel):
    """ Document supplémentaire pour une manifestation """

    # Champs
    manif = models.ForeignKey(Manif, on_delete=models.CASCADE)
    information_requise = models.TextField("information requise")
    date_demande = models.DateTimeField("date de demande", null=True, editable=False, blank=True)
    date_depot = models.DateTimeField("date de dépot", null=True, editable=False, blank=True)
    document_attache = models.FileField(upload_to=UploadPath('additional_data'), blank=True, null=True,
                                        verbose_name="Document", max_length=512, validators=[file_type_validator])

    # Override
    def __str__(self):
        """ Renvoyer la représentation texte de l'objet """
        return self.information_requise

    def save(self, *args, **kwargs):
        if self.pk:
            fichier_donne = False if DocumentComplementaire.objects.get(pk=self.pk).document_attache else True
            # on log si le fichier viens d'être mis( si plusieurs formsets, tous les objets sont enregistrés)
            if fichier_donne and self.document_attache:
                self.date_depot = timezone.now()
                self.notifier_doc_fourni()
        else:
            self.date_demande = timezone.now()
        super().save(*args, **kwargs)
        # creation de la vignette du fichier
        creation_thumbail_doc_complementaire.delay(self.pk)


    def notifier_doc_fourni(self):
        """ Notifier la provision de document """
        titre = "Documents complémentaires envoyés par l'organisateur"
        contenu = "<p>" + titre + ' pour la manifestation ' + self.manif.nom + " concernant la demande :</p>" \
                  "<p><em>" + self.information_requise + "</em></p>"
        destinataires = self.manif.instruction.get_instructeurs(arr_wrt=True)
        destinataires += self.manif.instruction.get_all_agents()
        Message.objects.creer_et_envoyer('info_suivi', self.manif.structure.organisateur.user, destinataires, titre,
                                        contenu, manifestation_liee=self.manif, objet_lie_nature="piece_jointe",
                                        objet_lie_pk=self.pk)

    def demande_doc(self):
        """ Notifier la demande de document """
        titre = "Documents complémentaires requis "
        contenu = "<p>" + titre + ' pour la manifestation ' + self.manif.nom + " concernant la demande :</p>" \
                  "<p><em>" + self.information_requise + "</em></p>" \
                  "<p>(Veuillez déposer votre document dans l'onglet 'Document complémentaire'.)</p>"
        destinataire = self.manif.structure.organisateur.user
        expediteur = self.manif.instruction.referent
        Message.objects.creer_et_envoyer('action', expediteur, [destinataire], titre, contenu, manifestation_liee=self.manif,
                                         objet_lie_nature="piece_jointe", objet_lie_pk=self.pk)

    def existe(self):
        """ Renvoyer si le fichier de la pièce jointe existe sur le système de fichiers """
        field_populated = self.id and self.document_attache and self.document_attache.path
        file_exists = default_storage.exists(self.document_attache.name)
        return field_populated and file_exists

    # Meta
    class Meta:
        verbose_name = "document complémentaire"
        verbose_name_plural = "documents complémentaires"
        default_related_name = "documentscomplementaires"
        app_label = "evenements"
