# coding: utf-8
from django.core.management.base import BaseCommand

from core.util.security import protect_code
from sports.models.sport import Discipline


class Command(BaseCommand):
    """
    Déplacer toutes les activités d'une discipline sportive vers une autre
    """
    args = ''
    help = "Déplacer toutes les activités d'une discipline sportive vers une autre"

    def add_arguments(self, parser):
        """ Arguments """
        parser.add_argument('disciplines', nargs='+', type=str, help="Noms des disciplines sportives source et destination")

    def handle(self, *args, **options):
        """ Exécuter la commande """
        protect_code()

        disciplines = options.get('disciplines', [])

        # Ne lancer la commande que si le nombre exact d'arguments est respecté
        if len(disciplines) == 2:
            source = disciplines[0]
            destination = disciplines[1]
            # Remplacer la discipline de toutes les activités sources
            try:
                discipline_source = Discipline.objects.get_by_name(source)
                discipline_destination = Discipline.objects.get_by_name(destination)
                activites = discipline_source.get_activites()
                total = activites.count()
                for activite in activites:
                    activite.discipline = discipline_destination
                    activite.save()
                print("{count} activités ont correctement été déplacées de {source} vers {dest}".format(count=total, source=source, dest=destination))
                return "Opération effectuée avec succès"
            except Discipline.DoesNotExist:
                print("Une discipline passée en argument n'existe pas dans la base de données")
                return "Échec de l'opération"
        else:
            print("Vous avez passé {num} arguments à la commande. Elle prend en charge exactement 2 (deux) arguments".format(num=len(disciplines)))
            return "Échec de l'opération"
