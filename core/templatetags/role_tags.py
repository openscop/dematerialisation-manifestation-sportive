from django import template
from core.models import User

register = template.Library()


@register.filter
def hasgroup(user, group):
    """ Renvoyer True ou False suivant l'appartenance de 'user' au groupe 'group' """
    if isinstance(user, User):
        if group in user.groups.values_list('name', flat=True):
            return True
    return False
