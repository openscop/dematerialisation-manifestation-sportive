# coding: utf-8
import factory

from administration.factories import CGDFactory
from agreements.factories import EDSRAvisFactory
from ..models import PreAvisCGD


class PreAvisCGDFactory(factory.django.DjangoModelFactory):
    """ Factory pour les préavis CGD """

    avis = factory.SubFactory(EDSRAvisFactory)
    cgd = factory.SubFactory(CGDFactory)

    class Meta:
        model = PreAvisCGD
