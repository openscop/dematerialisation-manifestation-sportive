# coding: utf-8
from sports.models.federation import Federation
from .manif import ManifDetail, ManifCreate, ManifUpdate, ManifFilesUpdate, ManifDelete
from ..forms.dcnm import DcnmForm, DcnmFilesForm
from ..models.dnm import Dcnm


class DcnmDetail(ManifDetail):
    """ Détail de manifestation """

    # Configuration
    model = Dcnm


class DcnmCreate(ManifCreate):
    """ Création de manifestation """

    # Configuration
    model = Dcnm
    form_class = DcnmForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dcnm'
        return context


class DcnmUpdate(ManifUpdate):
    """ Modifier une manifestation """

    # Configuration
    model = Dcnm
    form_class = DcnmForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dcnm'
        return context


class DcnmFilesUpdate(ManifFilesUpdate):
    """ Ajout de fichiers à une manif """

    # Configuration
    model = Dcnm
    form_class = DcnmFilesForm


class DcnmDelete(ManifDelete):
    """ Supprimer une manifestation """

    # Configuration
    model = Dcnm