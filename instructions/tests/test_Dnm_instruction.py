import re

from django.test import TestCase
from django.contrib.auth.hashers import make_password as make

from post_office.models import EmailTemplate

from core.models import Instance
from evenements.factories import DnmFactory
from evenements.models import Manif
from instructions.factories import InstructionFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from organisateurs.factories import StructureFactory
from core.factories import UserFactory
from administration.factories import InstructeurFactory, MairieAgentFactory
from sports.factories import ActiviteFactory


class InstructionDnm(TestCase):
    """
    Test du circuit d'instruction d'une DNM
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========= DNM 1 commune (Clt) ==========')
        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_EDSR,
                                        instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        cls.prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        structure = StructureFactory(commune=cls.commune)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        cls.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur, prefecture=cls.prefecture)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance)
        MairieAgentFactory.create(user=cls.agent_mairie, commune=cls.commune)

        EmailTemplate.objects.create(name='new_msg')

        # Création de l'événement
        cls.manifestation = DnmFactory.create(ville_depart=cls.commune, structure=structure,
                                              nom='Manifestation_Test', activite=activ)

    def test_Instruction_Dnm(self):
        print('**** test 1 creation manif ****')
        manif = Manif.objects.get()
        self.instruction = InstructionFactory.create(manif=manif)
        self.instruction.notifier_creation()

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.structure, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline)
        self.assertEqual(str(self.instruction.manif), str(self.manifestation))

        print('**** test 2 instruction préfecture ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur', password='123'))
        # Appel de la page
        url = '/instructions/tableaudebord/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content)
        # Aucune instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc1.group('nb')) + int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
        # Appel de la page
        url = '/instructions/tableaudebord/?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # Une instruction dans le TdB mairie
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))

        print('**** test 3 instruction mairie ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='agent_mairie', password='123'))
        url = '/instructions/tableaudebord/?filtre_specifique=instructionmairie'
        # Appel de la page
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # Une instruction dans le TdB mairie
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))


class InstructionDnm2com(TestCase):
    """
    Test du circuit d'instruction d'une DNM avec 1 autre commune traversée
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========= DNM 2 communes (Clt) ==========')
        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_EDSR,
                                        instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        cls.prefecture = arrondissement.prefecture
        cls.depart = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.commune = CommuneFactory(name='Aboen', arrondissement=arrondissement)
        structure = StructureFactory(commune=cls.commune)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        cls.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur, prefecture=cls.prefecture)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance)
        MairieAgentFactory.create(user=cls.agent_mairie, commune=cls.commune)

        EmailTemplate.objects.create(name='new_msg')

        # Création de l'événement
        cls.manifestation = DnmFactory.create(ville_depart=cls.commune, structure=structure, activite=activ,
                                              villes_traversees=[cls.commune,], nom='Manifestation_Test')

    def test_Instruction_Dnm(self):
        print('**** test 1 creation manif ****')
        manif = Manif.objects.get()
        self.instruction = InstructionFactory.create(manif=manif)
        self.instruction.notifier_creation()

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.structure, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline)
        self.assertEqual(str(self.instruction.manif), str(self.manifestation))

        print('**** test 2 instruction préfecture ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur', password='123'))
        # Appel de la page
        url = '/instructions/tableaudebord/?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content)
        # Aucune instruction dans le TdB mairie
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc1.group('nb')) + int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
        # Appel de la page
        url = '/instructions/tableaudebord/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))

        print('**** test 3 instruction mairie ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='agent_mairie', password='123'))
        url = '/instructions/tableaudebord/?filtre_specifique=instructionmairie'
        # Appel de la page
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # Aucune instruction dans le TdB mairie
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc1.group('nb')) + int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
