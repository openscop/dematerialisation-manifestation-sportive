# coding: utf-8
from ajax_select import register, LookupChannel
from django.db.models import Q
from unidecode import unidecode

from .models import PdesiLieu, N2kSite, RnrZone
from evenements.models import Manif


@register('lieupdesi')
class LieuPdesiLookup(LookupChannel):
    """ Lookup AJAX des lieux inscrit au PDESI """

    # Configuration
    model = PdesiLieu

    # Overrides
    def get_query(self, q, request):
        q = unidecode(q)
        try:
            object_id = int(request.META['HTTP_REFERER'].split('/')[-3])
            manif = Manif.objects.get(pk=object_id)
            filtre = Q(departement__in=manif.get_departements_traverses())
        except:
            filtre = Q()
        return self.model.objects.filter(filtre).filter(nom__unaccent__icontains=q).order_by('nom')[:20]

    def format_item_display(self, item):
        return u"<span class='tag'>%s</span>" % item.nom


@register('siten2k')
class N2kSiteLookup(LookupChannel):
    """ Lookup AJAX des sites Natura 2000 """

    # Configuration
    model = N2kSite

    # Overrides
    def get_query(self, q, request):
        q = unidecode(q)
        try:
            object_id = int(request.META['HTTP_REFERER'].split('/')[-3])
            manif = Manif.objects.get(pk=object_id)
            filtre = Q(departements__in=manif.get_departements_traverses())
        except:
            filtre = Q()
        return self.model.objects.filter(filtre).filter(nom__unaccent__icontains=q).order_by('nom')[:20]

    def format_item_display(self, item):
        return u"<span class='tag'>%s</span>" % item.nom


@register('zonernr')
class RnrZoneLookup(LookupChannel):
    """ Lookup AJAX des zones RNR """

    # Configuration
    model = RnrZone

    # Overrides
    def get_query(self, q, request):
        q = unidecode(q)
        try:
            object_id = int(request.META['HTTP_REFERER'].split('/')[-3])
            manif = Manif.objects.get(pk=object_id)
            filtre = Q(departements__in=manif.get_departements_traverses())
        except:
            filtre = Q()
        return self.model.objects.filter(filtre).filter(nom__unaccent__icontains=q).order_by('nom')[:20]

    def format_item_display(self, item):
        return u"<span class='tag'>%s</span>" % item.nom
