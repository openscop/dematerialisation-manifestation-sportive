import logging

from django.views.decorators.csrf import csrf_exempt
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone

from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from django_filters.rest_framework import DjangoFilterBackend, DateTimeFilter, FilterSet
from drf_yasg.utils import swagger_auto_schema
from bootstrap_datepicker_plus import DateTimePickerInput

from instructions.models import PreAvis
from api.serializer.instruction import PreAvisSerializer, PreAvisRendreSerializer, PreAvisCourtSerializer
from api.permissions import IsAgentLocal, IsInGroupApi

api_logger = logging.getLogger('api')


class PreavisFilter(FilterSet):

    date_debut = DateTimeFilter(field_name="avis__instruction__manif__date_debut",
                                label='date de début de la manifestation',
                                widget=DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'locale': 'fr'}),
                                lookup_expr='gte')

    date_demande = DateTimeFilter(field_name="date_demande",
                                  label='date de demande',
                                  widget=DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'locale': 'fr'}),
                                  lookup_expr='gte')

    class Meta:
        model = PreAvis
        fields = ('etat', 'date_demande', 'avis', 'date_debut', 'id')


class PreavisViewSet(viewsets.ModelViewSet):
    """
    Liste des préavis pour un agent local\n
    Cette route est réservée aux agents locaux.
    """
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter,)
    queryset = PreAvis.objects.all()
    permission_classes = (IsAuthenticated, IsAgentLocal, IsInGroupApi)
    serializer_class = PreAvisCourtSerializer
    detail_serializer_class = PreAvisSerializer
    rendre_serializer_class = PreAvisRendreSerializer
    filter_class = PreavisFilter
    search_fields = ('prescriptions',)
    ordering_fields = ('date_demande', 'date_reponse')

    """
    Utilisation de la fonction finalize_response pour bénéficier des variables request et response
    dans lesquelles se trouvent les infos à logger
    """
    def finalize_response(self, request, response, *args, **kwargs):
        if 'count' in response.data:
            count = response.data['count']
        else:
            count = 'none'
        api_logger.info("Utilisateur %s | %s de %s | %s | réponse %s, %s | items : %s" %
                        (request.user, request.method, self.request.get_host(), self.request.get_full_path(),
                         response.status_code, response.status_text, count
                         ))
        return super().finalize_response(request, response, *args, **kwargs)

    def get_serializer_class(self):
        if self.action == 'retrieve':
            if hasattr(self, 'detail_serializer_class'):
                return self.detail_serializer_class
        if self.action == 'rendre':
            if hasattr(self, 'rendre_serializer_class'):
                return self.rendre_serializer_class
        return super().get_serializer_class()

    def get_queryset(self):
        service = self.request.user.get_service()
        ct_service = ContentType.objects.get_for_model(service)
        return PreAvis.objects.filter(content_type=ct_service, object_id=service.id)

    @swagger_auto_schema(auto_schema=None)
    def partial_update(self, request, *args, **kwargs):
        return Response({"detail": "Opération interdite."}, status=403)

    @swagger_auto_schema(auto_schema=None)
    def update(self, request, *args, **kwargs):
        return Response({"detail": "Opération interdite."}, status=403)

    @swagger_auto_schema(auto_schema=None)
    def create(self, request, *args, **kwargs):
        return Response({"detail": "Opération interdite."}, status=403)

    @swagger_auto_schema(auto_schema=None)
    def destroy(self, request, *args, **kwargs):
        return Response({"detail": "Opération interdite."}, status=403)

    def retrieve(self, request, pk=None, *args, **kwargs):
        """
        Détail d'un préavis\n
        {id} est le numéro du préavis.
        Cette route est réservée aux agents locaux, le préavis demandé doit appartenir au service de l'agent local.
        """
        preavis = self.get_object()
        serializer = PreAvisSerializer(preavis)
        return Response(serializer.data)

    @csrf_exempt
    @swagger_auto_schema(responses={'200': 'Préavis rendu'})
    @action(methods=['post'], detail=True)
    def rendre(self, request, *args, **kwargs):
        """
        Rendre un préavis\n
        {id} est le numéro du préavis.
        Cette route est réservée aux agents locaux.
        """
        preavis = self.get_object()
        if preavis.etat == 'rendu':
            return Response({"detail": "Préavis déjà rendu"}, status=406)
        try:
            serializer = PreAvisRendreSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            preavis.date_reponse = timezone.now()
            preavis.agentlocal = self.request.user
            preavis.favorable = serializer.validated_data['favorable']
            preavis.prescriptions = serializer.validated_data['prescriptions']
            preavis.rendrePreavis(self.request.user)
            preavis.save()
            return Response("Préavis rendu.")
        except:
            return Response({"detail": "Problème de données."}, status=406)

    # @csrf_exempt
    # @swagger_auto_schema(responses={'201': 'Préavis créé'})
    # @action(methods=['post'], detail=True)
    # def creer(self, request, *args, **kwargs):
    #     instruction = Instruction.objects.get(pk=pk)
    #     avis = Avis.objects.filter(instruction=instruction).get(service_concerne="cg")
    #     if hasattr(request.user, 'agentlocal') and hasattr(request.user.agentlocal, 'cgserviceagentlocal'):
    #         serializer = PreAvisCreateSerializer(data=request.data)
    #         serializer.is_valid(raise_exception=True)
    #         service = request.user.get_service()
    #         liste_preavis = PreAvis.objects.filter(service_concerne='services').filter(avis=avis)
    #         if service not in [preavis.destination_object for preavis in liste_preavis]:
    #             PreAvis.objects.create(service_concerne='services', avis=avis, destination_object=service)
    #             return Response("Préavis créé.")
    #         return Response("Préavis déjà créé.")
    #     return Response("Vous devez appartenir à un service du conseil départemental")
