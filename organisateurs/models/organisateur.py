# coding: utf-8
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from django.conf import settings
from django.db import models

from carto.consumer.openrunner import openrunner_api
from carto.models.openrunnerusers import OpenRunnerUsers


class Organisateur(models.Model):
    """ Organisateur d'événements """

    # Champs
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="organisateur", verbose_name="utilisateur", on_delete=models.CASCADE)
    cgu = models.BooleanField("acceptation des CGU", default=False)

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.user.get_username()

    def save(self, *args, **kwargs):
        # Si l'organisateur est sauvegardé, définir l'instance de l'utilisateur
        # Uniquement si l'utilisateur n'avait pas déjà d'instance par défaut.
        # On utilise l'instance de la structure comme base.
        super().save(*args, **kwargs)
        self.user.remplir_tableau_role()
        if self.user.default_instance is None:
            try:
                if self.structure.commune.arrondissement.departement.get_instance() is not None:
                    self.user.default_instance = self.structure.commune.arrondissement.departement.get_instance()
                    self.user.save()
            except ObjectDoesNotExist:
                pass

    def delete(self, *args, **kwargs):
        user = self.user
        super(Organisateur, self).delete(*args, **kwargs)
        user.refresh_from_db()
        user.remplir_tableau_role()

    # Getter
    def get_departement(self):
        """ Renvoyer le nom du département de la structure """
        return self.structure.get_departement()

    def get_departement_name(self):
        """ Renvoyer le nom du département de la structure """
        return self.get_departement().name

    def get_url(self):
        """ Renvoyer l'URL de l'objet """
        return reverse('admin:organisateurs_organisateur_change', args=(self.pk,))

    # Action
    def create_openrunner_user(self):
        """ Créer un compte OpenRunner (si possible) """
        if not OpenRunnerUsers.objects.is_known(self.user):
            openrunner_api.create_user(self.user.username, self.get_departement().name, 'organizer', record=True)
            return True
        return False

    # Meta
    class Meta:
        verbose_name = "organisateur"
        default_related_name = "organisateurs"
        app_label = "organisateurs"
